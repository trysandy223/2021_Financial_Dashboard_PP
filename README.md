# 2021_PP_Financial_Dashboard

## 1. Introduction
PP Properti Financial Dashboard (SI MantaPP) application 2021 with Laravel 8 and Metronic 7.

## 2. Features
- Standard authentication (login, reset password, update profile information, and update password)
- Authorization (role and permission) with [Spatie Laravel-permission](https://spatie.be/docs/laravel-permission/v3/installation-laravel)
- Activity log with [Spatie Laravel-activitylog](https://spatie.be/docs/laravel-permission/v3/installation-laravel)
- User, Role, and Permission management
- Financial Dashboard
- PEN Dashboard

## 3. Installations
Metronic similarly uses additional plugins and frameworks, so ensure You have [Composer](https://getcomposer.org/) and [Node](https://nodejs.org/) installed on your machine. Assuming your machine meets all requirements - let's process to installation of Metronic Laravel integration.

### 3.1. Local
```bash
1. run composer install
2. copy .env.example file to .env
3. set APP_ENV in .env file to local
4. set APP_DEBUG in .env file to true
5. set APP_URL in .env file to your base url application (example: http://127.0.0.1:8000)
6. set DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, and DB_PASSWORD in .env file to your database configuration
7. run php artisan key:generate
8. run php artisan migrate:refresh --seed
9. run npm install
10. run npm run dev
11. run php artisan serve
```

### 3.2. Production
```bash
1. run composer install
2. copy .env.example file to .env
3. set APP_NAME in .env file to your application name. Use double quotes if title contain space (example: "Si Mantapp PT PP (Persero) Tbk")
4. set APP_ENV in .env file to production
5. set APP_DEBUG in .env file to false
6. set APP_URL in .env file to your base url application (example: https://yourdomain.com)
7. set DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, and DB_PASSWORD in .env file to your database configuration
8. run php artisan key:generate
9. run php artisan migrate:refresh --seed
10. run npm install
11. run npm run dev
12. run php artisan config:cache
13. run php artisan route:cache
14. run php artisan view:cache
```
And navigate to generated server link (example: http://127.0.0.1:8000).

## 4. Security Vulnerabilities
If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## 5. Copyright
The software licensed to [PT. PP (Persero) Tbk](https://www.ptpp.co.id/).