.. Financial Dashboard PP documentation master file, created by
   sphinx-quickstart on Tue Nov 30 14:04:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Financial Dashboard PP's documentation!
==================================================

PT. PP (Persero) Tbk. Financial Dashboard (SI MantaPP) application 2021 with Laravel 8 and Metronic 7.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   pages/features
   pages/installation



Links:
------
* Source code: https://gitlab.com/trysandy223/2021_Financial_Dashboard_PP
* Live app: http://pp.egov.co.id/
* Laravel documentation: https://laravel.com/