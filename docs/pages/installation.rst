============
Installation
============

Metronic similarly uses additional plugins and frameworks, so ensure You have `Composer <https://getcomposer.org/>`_ and `Node <https://nodejs.org/>`_ installed on your machine. Assuming your machine meets all requirements - let's process to installation of Metronic Laravel integration.

Local
-----
#. Run ``composer install``
#. Copy ``.env.example`` file to ``.env``
#. Set ``APP_ENV`` in .env file to ``local``
#. Set ``APP_DEBUG`` in .env file to ``true``
#. Set ``APP_URL`` in .env file to your base url application (example: http://127.0.0.1:8000)
#. Set ``DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, and DB_PASSWORD`` in .env file to your database configuration
#. Run ``php artisan key:generate``
#. Run ``php artisan migrate:refresh --seed``
#. Run ``npm install``
#. Run ``npm run dev``
#. Run ``php artisan serve``

Production
----------
#. Run ``composer install``
#. Copy ``.env.example`` file to ``.env``
#. Set ``APP_NAME`` in .env file to your application name. Use double quotes if title contain space (example: "Si Mantapp PT PP (Persero) Tbk")
#. Set ``APP_ENV`` in .env file to ``production``
#. Set ``APP_DEBUG`` in .env file to ``false``
#. Set ``APP_URL`` in .env file to your base url application (example: https://yourdomain.com)
#. Set ``DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, and DB_PASSWORD`` in .env file to your database configuration
#. Run ``php artisan key:generate``
#. Run ``php artisan migrate:refresh --seed``
#. Run ``npm install``
#. Run ``npm run dev``
#. Run ``php artisan config:cache``
#. Run ``php artisan route:cache``
#. Run ``php artisan view:cache``