===============
List of Feature
===============

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium pretium urna sed tincidunt. Ut a est quis quam auctor mollis.
Donec volutpat egestas nisl, vel consectetur purus sodales quis.

- Standard authentication (login, reset password, update profile information, and update password).
- Authorization (role and permission) with `Spatie Laravel-permission <https://spatie.be/docs/laravel-permission/v3/installation-laravel>`_.
- Activity log with `Spatie Laravel-activitylog <https://spatie.be/docs/laravel-permission/v3/installation-laravel>`_.
- User, Role, and Permission management.
- Financial Dashboard.
- PEN Dashboard.