<div>
	@if (count($plans) > 0 && count($actuals) > 0)
		<div class="row">
            <div class="col-md-6">
                <div class="card card-custom gutter-b">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="card-title">
                            <h3 class="card-label">
                                {{ __("Category") }}
                            </h3>
                        </div>
                        <div>
                            <button class="btn btn-light-warning">PLAN</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-6">
                                <h6>{{ __('Flag') }}</h6>
                            </div>
                            <div class="col-6">
                                <h6>{{ __('Value') }}</h6>
                            </div>
                        </div>
                        <div class="row mb-3">
                            @foreach ($plans as $key => $pl)
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <input type="text" class="form-control" value="{{ $pl['flag'] ?? '' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <div class="input-group">
                                            @if ($pl['flag'] != 'IRR')
                                                <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                                            @endif
                                            <input wire:model="plans.{{ $key }}.value" type="number" step="any" class="form-control {{ $errors->has('plans.'.$key.'.value') ? 'is-invalid' : '' }}" placeholder="...." />
                                            @if ($pl['flag'] == 'IRR')
                                                <div class="input-group-append"><span class="input-group-text">%</span></div>
                                            @endif
                                            @error('plans.'.$key.'.value')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-custom gutter-b">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="card-title">
                            <h3 class="card-label">
                                {{ __("Category") }}
                            </h3>
                        </div>
                        <div>
                            <button class="btn btn-light-success">ACTUAL</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-6">
                                <h6>{{ __('Flag') }}</h6>
                            </div>
                            <div class="col-6">
                                <h6>{{ __('Value') }}</h6>
                            </div>
                        </div>
                        <div class="row mb-3">
                            @foreach ($actuals as $key => $ac)
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <input type="text" class="form-control" value="{{ $ac['flag'] ?? '' }}" disabled>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <div class="input-group">
                                            @if ($ac['flag'] != 'IRR')
                                                <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                                            @endif
                                            <input wire:model="actuals.{{ $key }}.value" type="number" step="any" class="form-control {{ $errors->has('actuals.'.$key.'.value') ? 'is-invalid' : '' }}" placeholder="...." />
                                            @if ($ac['flag'] == 'IRR')
                                                <div class="input-group-append"><span class="input-group-text">%</span></div>
                                            @endif
                                            @error('actuals.'.$key.'.value')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-custom gutter-b">
            <div class="card-body text-right">
                <a href="{{ route('financials::investments.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                <button wire:click="submit" wire:loading.attr="disabled" type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
            </div>
        </div>
	@else
		<div class="alert alert-warning">
			{{ __('There\'s no data to edit') }}
		</div>
	@endif
</div>
