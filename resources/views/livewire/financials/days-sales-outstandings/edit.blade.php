<div>
	@if (count($datas) > 0)
		<div class="card card-custom gutter-b">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Edit Data
					</h3>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<label class="form-control-label">{{ __('Aging') }}</label>
						@foreach ($datas as $key => $data)
							<div class="form-group mb-3">
								<input type="text" class="form-control" value="{{ $data['aging_name'] }}" disabled>
							</div>
						@endforeach
					</div>
					<div class="col-md-6">
						<label class="form-control-label">{{ __('Value') }}</label>
						@foreach ($datas as $key => $data)
							<div class="form-group mb-3">
								<div class="input-group">
									<div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
									<input wire:model="datas.{{ $key }}.value" type="text" class="rupiah-format form-control {{ $errors->has('datas.'.$key.'.value') ? 'is-invalid' : '' }}" placeholder="...." />
									@error('datas.'.$key.'.value')
										<div class="invalid-feedback">{{ $message }}</div>
									@enderror
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>

		<div class="card card-custom gutter-b">
			<div class="card-body text-right">
				<a href="{{ route('financials::days_sales_outstandings.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
				<button wire:click="submit" wire:loading.attr="disabled" type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
			</div>
		</div>
	@else
		<div class="alert alert-warning">
			{{ __('There\'s no data to edit') }}
		</div>
	@endif
</div>
