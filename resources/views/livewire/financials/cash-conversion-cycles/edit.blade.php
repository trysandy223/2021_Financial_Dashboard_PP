<div>
	@if (count($datas) > 0)
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">
                        {{ __("Edit Data") }}
                    </h3>
                </div>
            </div>
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-6">
                        <h6>{{ __('Flag') }}</h6>
                    </div>
                    <div class="col-6">
                        <h6>{{ __('Value') }}</h6>
                    </div>
                </div>
                <div class="row mb-3">
                    @foreach ($datas as $flagId => $dt)
                        <div class="col-6">
                            <div class="form-group mb-4">
                                <input type="text" class="form-control" value="{{ $dt['flag'] ?? '' }}" disabled>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-4">
                                @if ($dt['type'] == 'rupiah')
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                                        <input wire:model="datas.{{ $flagId }}.value" id="rupiah{{ $flagId }}" type="text" class="rupiah-format form-control {{ $errors->has('datas.'.$flagId.'.value') ? 'is-invalid' : '' }}" placeholder="...." />
                                        @error('datas.'.$flagId.'.value')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @else
                                    <div class="input-group">
                                        <input wire:model="datas.{{ $flagId }}.value" id="number{{ $flagId }}" type="number" step="any" class="form-control {{ $errors->has('datas.'.$flagId.'.value') ? 'is-invalid' : '' }}" placeholder="...." />
                                        @error('datas.'.$flagId.'.value')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card card-custom gutter-b">
            <div class="card-body text-right">
                <a href="{{ route('financials::cash_conversion_cycles.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                <button wire:click="submit" wire:loading.attr="disabled" type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
            </div>
        </div>
	@else
		<div class="alert alert-warning">
			{{ __('There\'s no data to edit') }}
		</div>
	@endif
</div>