<div>
	@if (count($datas) > 0)
		<div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">
                        Edit Data
                    </h3>
                </div>
            </div>
            @foreach ($datas as $key => $data)
                @if (!$loop->first)
                    <hr class="my-0">
                @endif

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group mb-0">
                                <label class="form-control-label">{{ __('Debt Allocation Group') }}</label>
                                <div class="{{ $errors->has('datas.'.$key.'.debt_allocation_group') ? 'border border-danger rounded' : '' }}">
                                    <div wire:ignore>
                                        <select id="debtAllocationGroup{{ $key }}" class="form-control debt-allocation-group" dag-id="{{ $key }}">
                                            <option value=""></option>
                                            @foreach ($debt_allocation_groups as $dag)
                                                <option value="{{ $dag->id }}">{{ $dag->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('datas.'.$key.'.debt_allocation_group')
                                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-0">
                                <label class="form-control-label">{{ __('Result Allocation') }}</label>
                                <div class="{{ $errors->has('datas.'.$key.'.result_allocation') ? 'border border-danger rounded' : '' }}">
                                    <div wire:ignore>
                                        <select id="resultAllocation{{ $key }}" class="form-control result-allocation" ra-id="{{ $key }}" disabled></select>
                                    </div>
                                </div>
                                @error('datas.'.$key.'.result_allocation')
                                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="d-flex align-items-end">
                                <div class="flex-fill form-group mb-0">
                                    <label class="form-control-label">{{ __('Value') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                                        <input wire:model="datas.{{ $key }}.value" type="text" class="rupiah-format form-control {{ $errors->has('datas.'.$key.'.value') ? 'is-invalid' : '' }}" placeholder="...." />
                                        @error('datas.'.$key.'.value')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="card card-custom gutter-b">
            <div class="card-body text-right">
                <a href="{{ route('financials::revolving_allocations.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                <button wire:click="submit" wire:loading.attr="disabled" type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
            </div>
        </div>

        <script>
            document.addEventListener('livewire:load', function () {
                var datas = @json($datas);

                for (var key in datas) {
                    initDebtGroup(key, datas[key]);
                    
                    $('#debtAllocationGroup' + key).val(datas[key].debt_allocation_group).trigger('change');
                }
                
                // debt_allocation_group
                function initDebtGroup(key, data) {
                    $('#debtAllocationGroup' + key).select2({
                        placeholder: "-- Select --"
                    }).on('change', function() {
                        var dataId = $("#debtAllocationGroup"+ key +" option:selected").val();
                        var dataText = $("#debtAllocationGroup"+ key +" option:selected").text();
                        @this.set('datas.'+ key +'.debt_allocation_group', dataId);

                        $('#resultAllocation' + key).attr('disabled', false);
                        $('#resultAllocation' + key).val("").trigger("change");

                        if (dataText.toUpperCase() === 'CONSTRUCTION PROJECT') {
                            // Get data from master project
                            var resultAllocationURL = "{{ route('pages::projects.selectBox') }}";
                        } else if (dataText.toUpperCase() === 'OTHER DEBT PAYMENT TERM') {
                            // Get data from term loan
                            var resultAllocationURL = "{{ route('financials::term_loans.selectBox') }}";
                        } else if (dataText.toUpperCase() === 'OTHER DEBT PAYMENT REVOLVING') {
                            // Get data from revolving loan
                            var resultAllocationURL = "{{ route('financials::revolving_loans.selectBox') }}";
                        } else {
                            // Get data from trading partner
                            var resultAllocationURL = "{{ route('pages::trading_partners.selectBox') }}";
                        }

                        $('#resultAllocation' + key).select2({
                            placeholder: data.result_allocation,
                            "ajax" : {
                                "delay": 250,
                                "url" : resultAllocationURL,
                                "type" : "POST",
                                "dataType" : "json",
                                "data": function (params) {
                                    // set params
                                    var query = {
                                        search: params.term,
                                        company_id: '{{ $company }}',
                                        type: 'PP',
                                        status: 1,
                                        _token: '{{ csrf_token() }}',
                                        page: params.page || 1
                                    }
                                    return query;
                                },
                                "processResults": function(data, params) {
                                    params.page = params.page || 1;
                                    
                                    return {
                                        results: data.results,
                                        pagination: {
                                            more: (params.page * 10) < data.total_count
                                        }
                                    };
                                },
                            }
                        }).on('change', function() {
                            var dataRAText = $("#resultAllocation"+ key +" option:selected").text();
                            @this.set('datas.'+ key +'.result_allocation', dataRAText);
                        });
                    });
                }
            });
        </script>
	@else
		<div class="alert alert-warning">
			{{ __('There\'s no data to edit') }}
		</div>
	@endif
</div>
