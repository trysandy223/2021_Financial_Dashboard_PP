<div>
	@if (count($datas) > 0)
		<div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">
                        Edit Data
                    </h3>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group mb-0">
                    <label class="form-control-label">{{ __('Plafon Value') }}</label>
                    <input type="text" class="form-control" value="Rp{{ $plafon_value ? number_format($plafon_value) : 0 }}" disabled>
                </div>
            </div>
            <hr class="my-0">
            @foreach ($datas as $key => $data)
                @if (!$loop->first)
                    <hr class="my-0">
                @endif
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group mb-0 flex-fill">
                                <label class="form-control-label">{{ __('Credit Line Detail') }}</label>
                                <input wire:model="datas.{{ $key }}.credit_line_detail" type="text" class="form-control {{ $errors->has('datas.'.$key.'.credit_line_detail') ? 'is-invalid' : '' }}" placeholder="cth: Pencairan 1">
                                @error('datas.'.$key.'.credit_line_detail')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">{{ __('Credit Loan Opening Date') }}</label>
                                <input wire:model="datas.{{ $key }}.credit_line_opening_date" type="date" class="form-control {{ $errors->has('datas.'.$key.'.credit_line_opening_date') ? 'is-invalid' : '' }}">
                                @error('datas.'.$key.'.credit_line_opening_date')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">{{ __('Maturity Date') }}</label>
                                <input wire:model="datas.{{ $key }}.maturity_date" type="date" class="form-control {{ $errors->has('datas.'.$key.'.maturity_date') ? 'is-invalid' : '' }}">
                                @error('datas.'.$key.'.maturity_date')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">{{ __('Transaction Type') }}</label>
                                <select wire:model="datas.{{ $key }}.transaction_type" class="form-control {{ $errors->has('datas.'.$key.'.transaction_type') ? 'is-invalid' : '' }}">
                                    <option value="">-- {{ __("Select") }} --</option>
                                    @foreach (config('const.financial_revolving_drawdown_transaction_type') as $k => $t)
                                        @if ($k == 'PENCAIRAN' || 
                                                ($init_sum_value + $remaining_plafon_value) != $plafon_value)
                                            <option value="{{ $k }}">{{ $t }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('datas.'.$key.'.transaction_type')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">{{ __('Value') }}</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            @if ($datas[$key]['transaction_type'] == 'PEMBAYARAN')
                                                <span class="text-danger">Rp (-)</span>                                                
                                            @else
                                                Rp
                                            @endif
                                        </span>
                                    </div>
                                    <input wire:model="datas.{{ $key }}.value" type="text" class="rupiah-format form-control {{ $errors->has('datas.'.$key.'.value') ? 'is-invalid' : '' }}" placeholder="...." />
                                    @error('datas.'.$key.'.value')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="card card-custom gutter-b">
            <div class="card-body text-right">
                <a href="{{ route('financials::revolving_drawdowns.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                <button wire:click="submit" wire:loading.attr="disabled" type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
            </div>
        </div>
	@else
		<div class="alert alert-warning">
			{{ __('There\'s no data to edit') }}
		</div>
	@endif
</div>
