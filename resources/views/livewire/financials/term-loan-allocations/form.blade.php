<div>
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <h3 class="card-title">{{ __('Informasi Proyek') }}</h3>
            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <a href="{{ route('financials::term_loan_allocations.index') }}" class="btn btn-secondary font-weight-bolder pull-right">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Pixels.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                <rect id="Rectangle-187" fill="#000000" x="4" y="16" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy-3" fill="#000000" x="4" y="10" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy" fill="#000000" x="10" y="16" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy-4" fill="#000000" opacity="0.3" x="10" y="10" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy-5" fill="#000000" x="4" y="4" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy-2" fill="#000000" x="16" y="16" width="4" height="4" rx="1"></rect>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>{{ __('Back') }}</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            {{-- old method --}}
            {{-- @if ($is_parent_company)
                <div class="form-group">
                    <label class="form-control-label">{{ __('Company Name') }}</label>
                    <div class="{{ $errors->has('company') ? 'border border-danger rounded' : '' }}">
                        <div wire:ignore>
                            <select id="selectCompany" class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    @error('company')
                        <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                    @enderror
                </div>
            @else
                <div class="form-group">
                    <label class="form-control-label">{{ __('Company Name') }}</label>
                    <input type="text" class="form-control" value="{{ $company->code . ' - ' . $company->name }}" disabled>
                </div>
            @endif --}}

            {{-- new method (semua user bisa memilih company. Tapi company yang tampil hanya company-nya dan anak-anaknya saja) --}}
            <div class="form-group">
                <label class="form-control-label">{{ __('Company Name') }}</label>
                <div class="{{ $errors->has('selectCompany') ? 'border border-danger rounded' : '' }}">
                    <div wire:ignore>
                        <select id="selectCompany" class="form-control">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                @error('selectCompany')
                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                @enderror
            </div>
            
            <div class="form-group">
                <label class="form-control-label">{{ __('Project Name') }}</label>
                <div class="{{ $errors->has('project') ? 'border border-danger rounded' : '' }}">
                    <div wire:ignore>
                        <select id="selectProject" class="form-control">
							<option value=""></option>
						</select>
                    </div>
                </div>
                @error('project')
                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                @enderror
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Category') }}</label>
                        <input type="text" class="form-control" value="{{ $this->category->name }}" disabled>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Posting Date') }}</label>
                        <input type="date" class="form-control {{ $errors->has('posting_date') ? 'is-invalid' : '' }}" wire:model="posting_date">
                        @error('posting_date')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Trading Partner') }}</label>
                        <div class="{{ $errors->has('partner') ? 'border border-danger rounded' : '' }}">
                            <div wire:ignore>
                                <select id="partner" class="form-control">
                                    <option value=""></option>
                                    @foreach ($partners as $p)
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @error('partner')
                            <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                        @enderror
                    </div>
                </div>
				<div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Debt Name') }}</label>
                        <div class="{{ $errors->has('debt_name') ? 'border border-danger rounded' : '' }}">
                            <div wire:ignore>
                                <select id="selectDebtName" class="form-control">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        @error('debt_name')
                            <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Source of Fund') }}</label>
                        <input type="text" class="form-control" value="{{ isset($term_loan->bank->code) ? $term_loan->bank->code . ' - ' . $term_loan->bank->name: '' }}" disabled>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Source Group') }}</label>
                        <input type="text" class="form-control" value="{{ $term_loan->source_group_name ?? '' }}" disabled>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Start Duration') }}</label>
                        <input type="date" class="form-control" value="{{ $term_loan->start_duration_date ?? '' }}" disabled>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Maturity Date') }}</label>
                        <input type="date" class="form-control" value="{{ $term_loan->maturity_date ?? '' }}" disabled>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Interest Rate') }}</label>
                        <div class="input-group">
                            <input type="text" class="form-control" value="{{ $term_loan->interest_rate ?? '' }}" disabled>
                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Value') }}</label>
                        <input type="text" class="form-control" value="{{ isset($term_loan->value) ? 'Rp'.number_format($term_loan->value) : 0 }}" disabled>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Loan Payment Status') }}</label>
                        <input type="text" class="form-control" value="{{ $term_loan->debt_payment_name ?? '' }}" disabled>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Flag') }}</label>
                        <input type="text" class="form-control" value="{{ $term_loan->debt_type_name ?? '' }}" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Tambah Data
                </h3>
            </div>
        </div>
        @foreach ($datas as $key => $data)
            @if (!$loop->first)
                <hr class="my-0">
            @endif

            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group mb-0">
                            <label class="form-control-label">{{ __('Debt Allocation Group') }}</label>
							<div class="{{ $errors->has('datas.'.$key.'.debt_allocation_group') ? 'border border-danger rounded' : '' }}">
								<div wire:ignore>
									<select id="debtAllocationGroup{{ $key }}" class="form-control debt-allocation-group" dag-id="{{ $key }}">
										<option value=""></option>
										@foreach ($debt_allocation_groups as $dag)
											<option value="{{ $dag->id }}">{{ $dag->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
                            @error('datas.'.$key.'.debt_allocation_group')
			                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-0">
                            <label class="form-control-label">{{ __('Result Allocation') }}</label>
							<div class="{{ $errors->has('datas.'.$key.'.result_allocation') ? 'border border-danger rounded' : '' }}">
								<div wire:ignore>
									<select id="resultAllocation{{ $key }}" class="form-control result-allocation" ra-id="{{ $key }}" disabled></select>
								</div>
							</div>
                            @error('datas.'.$key.'.result_allocation')
			                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
						<div class="d-flex align-items-end">
							<div class="flex-fill form-group mb-0">
								<label class="form-control-label">{{ __('Value') }}</label>
								<div class="input-group">
									<div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
									<input wire:model="datas.{{ $key }}.value" type="text" class="rupiah-format form-control {{ $errors->has('datas.'.$key.'.value') ? 'is-invalid' : '' }}" placeholder="...." />
									@error('datas.'.$key.'.value')
										<div class="invalid-feedback">{{ $message }}</div>
									@enderror
								</div>
							</div>
							<div>
								@if ($loop->first)
									<button wire:click="addData()" class="btn btn-icon btn-light-primary add-data ml-3"><i class="la la-plus-circle"></i></button>
								@else
									<button wire:click="subData({{ $key }})" class="btn btn-icon btn-light-danger ml-3"><i class="la la-times-circle"></i></button>
								@endif
							</div>
						</div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="card card-custom gutter-b">
        <div class="card-body text-right">
            <a href="{{ route('financials::term_loan_allocations.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
            <button wire:click="submit" wire:loading.attr="disabled" type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
        </div>
    </div>

    <script>
        document.addEventListener('livewire:load', function () {
            var subParentCompany = @json(config("const.sub_parent_company"));

            $("#selectProject").attr("disabled", true);
            $("#selectDebtName").attr("disabled", true);

            $("#selectCompany").select2({
				placeholder: "-- Select --",
                "ajax" : {
                    "delay": 250,
                    "url" : "{{ route('pages::companies.selectBox') }}",
                    "type" : "POST",
                    "dataType" : "json",
                    "data": function (params) {
                        // set params
                        var query = {
                            search: params.term,
                            status: 1,
                            text_format: 'code-name',
                            _token: '{{ csrf_token() }}',
                            page: params.page || 1
                        }
                        return query;
                    },
                    "processResults": function(data, params) {
                        params.page = params.page || 1;
                        
                        return {
                            results: data.results,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                }
            }).on('change', function(e) {
                var companyId = e.target.value;
				@this.set('selectCompany', companyId);

                $("#selectProject").empty().trigger("change");
                $("#selectProject").attr("disabled", false);

                if (subParentCompany.includes(parseInt(companyId))) {
                    initSelectProject('unit', companyId);
                } else {
                    initSelectProject('company', companyId);
                }

                $("#selectDebtName").attr("disabled", false);
                initSelectDebtName(companyId);

                $(".result-allocation").empty().trigger("change");
                $(".debt-allocation-group").val("").trigger("change");
                $(".result-allocation").attr("disabled", true);
                $(".debt-allocation-group").attr("disabled", false);
                initDebtGroup(companyId);
			});

            @if(!$is_parent_company)
                $("#selectProject").attr("disabled", false);
                @if($unit_id != null)
                    initSelectProject('unit', '{{ $unit_id }}');
                @else
                    initSelectProject('company', '{{ $company->id }}');
                @endif

                $("#selectDebtName").attr("disabled", false);
                initSelectDebtName('{{ $company->id }}');
    			initDebtGroup('{{ $company->id }}');
            @else
                $(".debt-allocation-group").attr("disabled", true);
            @endif

            $('#partner').select2({
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#partner option:selected").val();
                @this.set('partner', data);
            });

            $('#type').select2({
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#type option:selected").val();
                @this.set('type', data);
            });

            window.addEventListener('initDebtGroup', event => {
                initDebtGroup(event.detail.companyId);
            });

			function initDebtGroup(companyId) {
				$('.debt-allocation-group').select2({
					placeholder: "-- Select --"
				}).on('change', function() {
					var key = $(this).attr('dag-id');
					var dataId = $("#debtAllocationGroup"+ key +" option:selected").val();
					var dataText = $("#debtAllocationGroup"+ key +" option:selected").text();
					@this.set('datas.'+ key +'.debt_allocation_group', dataId);

					$('#resultAllocation' + key).attr('disabled', false);

                    var isProject = false;
					if (dataText.toUpperCase() === 'CONSTRUCTION PROJECT') {
						// Get data from master project
                        isProject = true;
						var resultAllocationURL = "{{ route('pages::projects.selectBox') }}";
					} else if (dataText.toUpperCase() === 'OTHER DEBT PAYMENT TERM') {
						// Get data from term loan
						var resultAllocationURL = "{{ route('financials::term_loans.selectBox') }}";
					} else if (dataText.toUpperCase() === 'OTHER DEBT PAYMENT REVOLVING') {
						// Get data from revolving loan
						var resultAllocationURL = "{{ route('financials::revolving_loans.selectBox') }}";
					} else {
						// Get data from trading partner
						var resultAllocationURL = "{{ route('pages::trading_partners.selectBox') }}";
					}

					@this.set('datas.'+ key +'.result_allocation', null);
					$('#resultAllocation' + key).empty().trigger("change");
					$('#resultAllocation' + key).select2({
						placeholder: "-- Select --",
						"ajax" : {
                        "delay": 250,
                        "url" : resultAllocationURL,
                        "type" : "POST",
                        "dataType" : "json",
                        "data": function (params) {
                            // set params
                            var unitId = '{{ $unit_id ?? 0 }}'

                            if (isProject === true) {
                                if (subParentCompany.includes(parseInt(unitId))) {
                                    var query = {
                                        search: params.term,
                                        unit_id: unitId,
                                        _token: '{{ csrf_token() }}',
                                        page: params.page || 1
                                    }
                                } else {
                                    var query = {
                                        search: params.term,
                                        inherit_company_id: companyId,
                                        _token: '{{ csrf_token() }}',
                                        page: params.page || 1
                                    }
                                }
                            } else {
                                var query = {
                                    search: params.term,
                                    company_id: companyId,
                                    type: 'PP',
                                    status: 1,
                                    _token: '{{ csrf_token() }}',
                                    page: params.page || 1
                                }
                            }
                            return query;
                        },
                        "processResults": function(data, params) {
                            params.page = params.page || 1;
                            
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.total_count
                                }
                            };
                        },
                    }
					}).on('change', function() {
						var dataRAText = $("#resultAllocation"+ key +" option:selected").text();
						@this.set('datas.'+ key +'.result_allocation', dataRAText);
					});
				});
			}

            function initSelectProject(filter = 'company', companyId = null) {
                $("#selectProject").select2({
                    placeholder: "-- Select --",
                    "ajax" : {
                        "delay": 250,
                        "url" : "{{ route('pages::projects.selectBox') }}",
                        "type" : "POST",
                        "dataType" : "json",
                        "data": function (params) {
                            // set params
                            var query = {
                                search: params.term,
                                unit_id: filter === 'unit' ? companyId : '',
                                inherit_company_id: filter === 'company' ? companyId : '',
                                _token: '{{ csrf_token() }}',
                                page: params.page || 1
                            }
                            return query;
                        },
                        "processResults": function(data, params) {
                            params.page = params.page || 1;
                            
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.total_count
                                }
                            };
                        },
                        cache: true
                    }
                }).on('change', function(e) {
                    @this.set('project', e.target.value);
                });
            }

            function initSelectDebtName(companyId = null) {
                $("#selectDebtName").select2({
                    placeholder: "-- Select --",
                    "ajax" : {
                        "delay": 250,
                        "url" : "{{ route('financials::term_loans.selectBox') }}",
                        "type" : "POST",
                        "dataType" : "json",
                        "data": function (params) {
                            // set params
                            var query = {
                                search: params.term,
                                company_id: companyId,
                                _token: '{{ csrf_token() }}',
                                page: params.page || 1
                            }
                            return query;
                        },
                        "processResults": function(data, params) {
                            params.page = params.page || 1;
                            
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.total_count
                                }
                            };
                        },
                        cache: true
                    }
                }).on('change', function(e) {
                    @this.set('debt_name', e.target.value);
                });
            }
        });
    </script>
</div>
