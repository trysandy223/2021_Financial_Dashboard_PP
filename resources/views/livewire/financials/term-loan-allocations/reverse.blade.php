<div>
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <h3 class="card-title">{{ __('Reverse') }}</h3>
            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <a href="{{ route('financials::term_loan_allocations.index') }}" class="btn btn-secondary font-weight-bolder pull-right">
                        <span class="svg-icon svg-icon-md">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Pixels.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                    <rect id="Rectangle-187" fill="#000000" x="4" y="16" width="4" height="4" rx="1"></rect>
                                    <rect id="Rectangle-187-Copy-3" fill="#000000" x="4" y="10" width="4" height="4" rx="1">
                                    </rect>
                                    <rect id="Rectangle-187-Copy" fill="#000000" x="10" y="16" width="4" height="4" rx="1">
                                    </rect>
                                    <rect id="Rectangle-187-Copy-4" fill="#000000" opacity="0.3" x="10" y="10" width="4"
                                        height="4" rx="1"></rect>
                                    <rect id="Rectangle-187-Copy-5" fill="#000000" x="4" y="4" width="4" height="4" rx="1">
                                    </rect>
                                    <rect id="Rectangle-187-Copy-2" fill="#000000" x="16" y="16" width="4" height="4"
                                        rx="1"></rect>
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>{{ __('Back') }}</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            {{-- old method --}}
            {{-- @if ($is_parent_company)
                <div class="form-group">
                    <label class="form-control-label">{{ __('Company Name') }}</label>
                    <div class="{{ $errors->has('company') ? 'border border-danger rounded' : '' }}">
                        <div wire:ignore>
                            <select id="selectCompany" class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    @error('company')
                        <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                    @enderror
                </div>
            @else
                <div class="form-group">
                    <label class="form-control-label">{{ __('Company Name') }}</label>
                    <input type="text" class="form-control" value="{{ $company->code . ' - ' . $company->name }}" disabled>
                </div>
            @endif --}}

            {{-- new method (semua user bisa memilih company. Tapi company yang tampil hanya company-nya dan anak-anaknya saja) --}}
            <div class="form-group">
                <label class="form-control-label">{{ __('Company Name') }}</label>
                <div class="{{ $errors->has('selectCompany') ? 'border border-danger rounded' : '' }}">
                    <div wire:ignore>
                        <select id="selectCompany" class="form-control">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                @error('selectCompany')
                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                @enderror
            </div>
            
            <div class="form-group">
                <label class="form-control-label">{{ __('Debt Name') }}</label>
                <div class="{{ $errors->has('debt_name') ? 'border border-danger rounded' : '' }}">
                    <div wire:ignore>
                        <select id="selectLoanName" class="form-control">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                @error('debt_name')
                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                @enderror
            </div>

            @if ($debt_name)
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                          <label class="form-control-label">{{ __('Project') }}</label>
                          <input type="text" class="form-control" placeholder="...." value="{{ $data->project_name ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Category') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->category_name ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Posting Date') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->posting_date ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Flag') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->debt_type_name ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Trading Partner') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->trading_partner ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Source of Fund') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->bank_name ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Source Group') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->source_group_name ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Start Duration') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->start_duration_date ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Maturity Date') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->maturity_date ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Intereset Rate') }}</label>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="...." value="{{ $data->interest_rate ?? '0' }}" disabled>
                                <div class="input-group-append"><span class="input-group-text">%</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Value') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ isset($data->value) ? 'Rp'. number_format($data->value) : '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Type') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->type ?? '-' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Loan Payment Status') }}</label>
                            <input type="text" class="form-control" placeholder="...." value="{{ $data->debt_payment_name ?? '-' }}" disabled>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="card card-custom gutter-b">
        <div class="card-body text-right">
            <a href="{{ route('financials::term_loan_allocations.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
            <button id="submit" class="btn btn-primary mr-2">{{ __('Reverse') }}</button>
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function () {
            $("#selectLoanName").attr("disabled", true);
            $("#selectCompany").select2({
                placeholder: "-- Select --",
                "ajax" : {
                    "delay": 250,
                    "url" : "{{ route('pages::companies.selectBox') }}",
                    "type" : "POST",
                    "dataType" : "json",
                    "data": function (params) {
                        // set params
                        var query = {
                            search: params.term,
                            status: 1,
                            text_format: 'code-name',
                            _token: '{{ csrf_token() }}',
                            page: params.page || 1
                        }
                        return query;
                    },
                    "processResults": function(data, params) {
                        params.page = params.page || 1;
                        
                        return {
                            results: data.results,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                }
            }).on('change', function(e) {
                var companyId = e.target.value;
                // @this.set('company', companyId);
                @this.set('selectCompany', companyId);

                $("#selectLoanName").attr("disabled", false);
                initSelectLoanName(companyId);
            });

            @if(!$is_parent_company)
                $("#selectLoanName").attr("disabled", false);
                initSelectLoanName('{{ $company->id }}');
            @endif

            function initSelectLoanName(companyId = null) {
                $("#selectLoanName").select2({
                    placeholder: "-- Select --",
                    "ajax" : {
                        "delay": 250,
                        "url" : "{{ route('financials::term_loans.selectBox') }}",
                        "type" : "POST",
                        "dataType" : "json",
                        "data": function (params) {
                            // set params
                            var query = {
                                search: params.term,
                                company_id: companyId,
                                _token: '{{ csrf_token() }}',
                                page: params.page || 1
                            }
                            return query;
                        },
                        "processResults": function(data, params) {
                            params.page = params.page || 1;
                            
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.total_count
                                }
                            };
                        },
                        cache: true
                    }
                }).on('change', function(e) {
                    @this.set('debt_name', e.target.value);
                });
            }

            $(document).on('click', '#submit', function() {
                Swal.fire({
                    title: "Are you sure?",
                    text: "You wont be able to revert this!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, reverse it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emit('confirmReverse');
                    } else {
                        Swal.fire("Cancelled", "Okay, your data is safe", "error")
                    }
                });
            });
        });
    </script>
</div>
