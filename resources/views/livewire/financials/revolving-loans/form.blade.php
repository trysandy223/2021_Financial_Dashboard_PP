<div>
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <h3 class="card-title">{{ __('Informasi Proyek') }}</h3>
            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <a href="{{ route('financials::revolving_loans.index') }}" class="btn btn-secondary font-weight-bolder pull-right">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Pixels.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                <rect id="Rectangle-187" fill="#000000" x="4" y="16" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy-3" fill="#000000" x="4" y="10" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy" fill="#000000" x="10" y="16" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy-4" fill="#000000" opacity="0.3" x="10" y="10" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy-5" fill="#000000" x="4" y="4" width="4" height="4" rx="1"></rect>
                                <rect id="Rectangle-187-Copy-2" fill="#000000" x="16" y="16" width="4" height="4" rx="1"></rect>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>{{ __('Back') }}</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            {{-- old method --}}
            {{-- @if ($is_parent_company)
                <div class="form-group">
                    <label class="form-control-label">{{ __('Company Name') }}</label>
                    <div class="{{ $errors->has('company') ? 'border border-danger rounded' : '' }}">
                        <div wire:ignore>
                            <select id="selectCompany" class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    @error('company')
                        <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                    @enderror
                </div>
            @else
                <div class="form-group">
                    <label class="form-control-label">{{ __('Company Name') }}</label>
                    <input type="text" class="form-control" value="{{ $company->code . ' - ' . $company->name }}" disabled>
                </div>
            @endif --}}

            {{-- new method (semua user bisa memilih company. Tapi company yang tampil hanya company-nya dan anak-anaknya saja) --}}
            <div class="form-group">
                <label class="form-control-label">{{ __('Company Name') }}</label>
                <div class="{{ $errors->has('selectCompany') ? 'border border-danger rounded' : '' }}">
                    <div wire:ignore>
                        <select id="selectCompany" class="form-control">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                @error('selectCompany')
                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                @enderror
            </div>

            <div class="form-group">
                <label class="form-control-label">{{ __('Project Name') }}</label>
                <div class="{{ $errors->has('project') ? 'border border-danger rounded' : '' }}">
                    <div wire:ignore>
                        <select id="selectProject" class="form-control">
							<option value=""></option>
						</select>
                    </div>
                </div>
                @error('project')
                    <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                @enderror
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Category') }}</label>
                        <input type="text" class="form-control" value="{{ $this->category->name }}" disabled>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Posting Date') }}</label>
                        <input type="date" class="form-control {{ $errors->has('posting_date') ? 'is-invalid' : '' }}" wire:model="posting_date">
                        @error('posting_date')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Trading Partner') }}</label>
                        <div class="{{ $errors->has('partner') ? 'border border-danger rounded' : '' }}">
                            <div wire:ignore>
                                <select id="partner" class="form-control">
                                    <option value=""></option>
                                    @foreach ($partners as $p)
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @error('partner')
                            <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Flag') }}</label>
                        <input type="text" class="form-control" value="{{ $this->flag->name }}" disabled>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Source of Fund') }}</label>
                        <div class="{{ $errors->has('bank') ? 'border border-danger rounded' : '' }}">
                            <div wire:ignore>
                                <select id="bank" class="form-control">
                                    <option value=""></option>
                                    @foreach ($banks as $b)
                                        <option value="{{ $b->id }}">{{ $b->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @error('bank')
                            <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">{{ __('Source Group') }}</label>
                        <div class="{{ $errors->has('source_group') ? 'border border-danger rounded' : '' }}">
                            <div wire:ignore>
                                <select id="source" class="form-control">
                                    <option value=""></option>
                                    @foreach ($sourceGroups as $s)
                                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @error('source_group')
                            <div class="text-danger mt-1"><small>{{ $message }}</small></div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Tambah Data
                </h3>
            </div>
        </div>
        @foreach ($datas as $key => $data)
            @if (!$loop->first)
                <hr class="my-0">
            @endif
            <div class="card-body">
                <div class="mb-5">
                    <div class="d-flex align-items-end">
                        <div class="form-group mb-0 flex-fill">
                            <label class="form-control-label">{{ __('Loan Name') }}</label>
                            <input wire:model="datas.{{ $key }}.debt_name" type="text" class="form-control {{ $errors->has('datas.'.$key.'.debt_name') ? 'is-invalid' : '' }}" placeholder="cth: Pinjaman 1">
                        </div>
                        <div>
                            @if ($loop->first)
                                <button wire:click="addData()" class="btn btn-icon btn-light-primary ml-3"><i class="la la-plus-circle"></i></button>
                            @else
                                <button wire:click="subData({{ $key }})" class="btn btn-icon btn-light-danger ml-3"><i class="la la-times-circle"></i></button>
                            @endif
                        </div>
                    </div>
                    @error('datas.'.$key.'.debt_name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Credit Loan Opening Date') }}</label>
                            <input wire:model="datas.{{ $key }}.credit_loan_opening_date" type="date" class="form-control {{ $errors->has('datas.'.$key.'.credit_loan_opening_date') ? 'is-invalid' : '' }}">
                            @error('datas.'.$key.'.credit_loan_opening_date')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Maturity Date') }}</label>
                            <input wire:model="datas.{{ $key }}.maturity_date" type="date" class="form-control {{ $errors->has('datas.'.$key.'.maturity_date') ? 'is-invalid' : '' }}">
                            @error('datas.'.$key.'.maturity_date')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Interest Rate') }}</label>
                            <div class="input-group">
                                <input wire:model="datas.{{ $key }}.interest_rate" type="number" step="any" class="form-control {{ $errors->has('datas.'.$key.'.interest_rate') ? 'is-invalid' : '' }}" placeholder="...." />
                                <div class="input-group-append"><span class="input-group-text">%</span></div>
                                @error('datas.'.$key.'.interest_rate')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Plafon Value') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                                <input wire:model="datas.{{ $key }}.plafon_value" type="text" class="rupiah-format form-control {{ $errors->has('datas.'.$key.'.plafon_value') ? 'is-invalid' : '' }}" placeholder="...." />
                                @error('datas.'.$key.'.plafon_value')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Debt Equity') }}</label>
                            <input wire:model="datas.{{ $key }}.debt_equity" type="number" class="form-control {{ $errors->has('datas.'.$key.'.debt_equity') ? 'is-invalid' : '' }}" placeholder="...." />
                            @error('datas.'.$key.'.debt_equity')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Debt Ebitda') }}</label>
                            <input wire:model="datas.{{ $key }}.debt_ebitda" type="number" class="form-control {{ $errors->has('datas.'.$key.'.debt_ebitda') ? 'is-invalid' : '' }}" placeholder="...." />
                            @error('datas.'.$key.'.debt_ebitda')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('ISCR') }}</label>
                            <input wire:model="datas.{{ $key }}.iscr" type="number" class="form-control {{ $errors->has('datas.'.$key.'.iscr') ? 'is-invalid' : '' }}" placeholder="...." />
                            @error('datas.'.$key.'.iscr')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Loan Payment Status') }}</label>
                            <select wire:model="datas.{{ $key }}.status" class="form-control {{ $errors->has('datas.'.$key.'.status') ? 'is-invalid' : '' }}">
                                <option value="">-- {{ __("Select") }} --</option>
                                @foreach ($debtPayments as $dp)
                                    <option value="{{ $dp->id }}">{{ $dp->name }}</option>
                                @endforeach
                            </select>
                            @error('datas.'.$key.'.status')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="card card-custom gutter-b">
        <div class="card-body text-right">
            <a href="{{ route('financials::revolving_loans.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
            <button wire:click="submit" wire:loading.attr="disabled" type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
        </div>
    </div>

    <script>
        document.addEventListener('livewire:load', function () {
            $("#selectProject").attr("disabled", true);
            $("#selectCompany").select2({
				placeholder: "-- Select --",
                "ajax" : {
                    "delay": 250,
                    "url" : "{{ route('pages::companies.selectBox') }}",
                    "type" : "POST",
                    "dataType" : "json",
                    "data": function (params) {
                        // set params
                        var query = {
                            search: params.term,
                            status: 1,
                            text_format: 'code-name',
                            _token: '{{ csrf_token() }}',
                            page: params.page || 1
                        }
                        return query;
                    },
                    "processResults": function(data, params) {
                        params.page = params.page || 1;
                        
                        return {
                            results: data.results,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                }
            }).on('change', function(e) {
                var companyId = e.target.value;
				@this.set('selectCompany', companyId);

                $("#selectProject").empty().trigger("change");
                $("#selectProject").attr("disabled", false);

                var subParentCompany = @json(config("const.sub_parent_company"));
                if (subParentCompany.includes(parseInt(companyId))) {
                    initSelectProject('unit', companyId);
                } else {
                    initSelectProject('company', companyId);
                }
			});

            @if(!$is_parent_company)
                $("#selectProject").attr("disabled", false);
                @if($unit_id != null)
                    initSelectProject('unit', '{{ $unit_id }}');
                @else
                    initSelectProject('company', '{{ $company->id }}');
                @endif
            @endif

            $('#bank').select2({
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#bank option:selected").val();
                @this.set('bank', data);
            });

            $('#partner').select2({
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#partner option:selected").val();
                @this.set('partner', data);
            });

            $('#source').select2({
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#source option:selected").val();
                @this.set('source_group', data);
            });

            function initSelectProject(filter = 'company',companyId = null) {
                $("#selectProject").select2({
                    placeholder: "-- Select --",
                    "ajax" : {
                        "delay": 250,
                        "url" : "{{ route('pages::projects.selectBox') }}",
                        "type" : "POST",
                        "dataType" : "json",
                        "data": function (params) {
                            // set params
                            var query = {
                                search: params.term,
                                unit_id: filter === 'unit' ? companyId : '',
                                inherit_company_id: filter === 'company' ? companyId : '',
                                _token: '{{ csrf_token() }}',
                                page: params.page || 1
                            }
                            return query;
                        },
                        "processResults": function(data, params) {
                            params.page = params.page || 1;
                            
                            return {
                                results: data.results,
                                pagination: {
                                    more: (params.page * 10) < data.total_count
                                }
                            };
                        },
                        cache: true
                    }
                }).on('change', function(e) {
                    @this.set('project', e.target.value);
                });
            }
        });
    </script>
</div>
