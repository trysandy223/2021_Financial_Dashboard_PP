<div>
	@if (count($datas) > 0)
		<div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Edit Data
                </h3>
            </div>
        </div>
        @foreach ($datas as $key => $data)
            @if (!$loop->first)
                <hr class="my-0">
            @endif
            <div class="card-body">
                <div class="mb-5">
                    <div class="d-flex align-items-end">
                        <div class="form-group mb-0 flex-fill">
                            <label class="form-control-label">{{ __('Loan Name') }}</label>
                            <input wire:model="datas.{{ $key }}.debt_name" type="text" class="form-control {{ $errors->has('datas.'.$key.'.debt_name') ? 'is-invalid' : '' }}" placeholder="cth: Pinjaman 1">
                        </div>
                    </div>
                    @error('datas.'.$key.'.debt_name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Credit Loan Opening Date') }}</label>
                            <input wire:model="datas.{{ $key }}.credit_loan_opening_date" type="date" class="form-control {{ $errors->has('datas.'.$key.'.credit_loan_opening_date') ? 'is-invalid' : '' }}">
                            @error('datas.'.$key.'.credit_loan_opening_date')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Maturity Date') }}</label>
                            <input wire:model="datas.{{ $key }}.maturity_date" type="date" class="form-control {{ $errors->has('datas.'.$key.'.maturity_date') ? 'is-invalid' : '' }}">
                            @error('datas.'.$key.'.maturity_date')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Interest Rate') }}</label>
                            <div class="input-group">
                                <input wire:model="datas.{{ $key }}.interest_rate" type="number" step="any" class="form-control {{ $errors->has('datas.'.$key.'.interest_rate') ? 'is-invalid' : '' }}" placeholder="...." />
                                <div class="input-group-append"><span class="input-group-text">%</span></div>
                                @error('datas.'.$key.'.interest_rate')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Plafon Value') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                                <input wire:model="datas.{{ $key }}.plafon_value" type="text" class="rupiah-format form-control {{ $errors->has('datas.'.$key.'.plafon_value') ? 'is-invalid' : '' }}" placeholder="...." />
                                @error('datas.'.$key.'.plafon_value')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Debt Equity') }}</label>
                            <input wire:model="datas.{{ $key }}.debt_equity" type="number" class="form-control {{ $errors->has('datas.'.$key.'.debt_equity') ? 'is-invalid' : '' }}" placeholder="...." />
                            @error('datas.'.$key.'.debt_equity')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Debt Ebitda') }}</label>
                            <input wire:model="datas.{{ $key }}.debt_ebitda" type="number" class="form-control {{ $errors->has('datas.'.$key.'.debt_ebitda') ? 'is-invalid' : '' }}" placeholder="...." />
                            @error('datas.'.$key.'.debt_ebitda')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('ISCR') }}</label>
                            <input wire:model="datas.{{ $key }}.iscr" type="number" class="form-control {{ $errors->has('datas.'.$key.'.iscr') ? 'is-invalid' : '' }}" placeholder="...." />
                            @error('datas.'.$key.'.iscr')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">{{ __('Loan Payment Status') }}</label>
                            <select wire:model="datas.{{ $key }}.status" class="form-control {{ $errors->has('datas.'.$key.'.status') ? 'is-invalid' : '' }}">
                                <option value="">-- {{ __("Select") }} --</option>
                                @foreach ($debtPayments as $dp)
                                    <option value="{{ $dp->id }}">{{ $dp->name }}</option>
                                @endforeach
                            </select>
                            @error('datas.'.$key.'.status')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="card card-custom gutter-b">
        <div class="card-body text-right">
            <a href="{{ route('financials::revolving_loans.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
            <button wire:click="submit" wire:loading.attr="disabled" type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
        </div>
    </div>
	@else
		<div class="alert alert-warning">
			{{ __('There\'s no data to edit') }}
		</div>
	@endif
</div>
