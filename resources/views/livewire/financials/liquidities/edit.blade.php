<div>
	@if (count($datas) > 0)
		<div class="card card-custom gutter-b">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Edit Data
					</h3>
				</div>
			</div>
			<div class="card-body">
				<div class="row mb-2">
					<div class="col-6">
						<h6>{{ __('Plan Date') }}</h6>
					</div>
					<div class="col-6">
						<h6>{{ __('Value') }}</h6>
					</div>
				</div>
				@foreach ($datas as $key => $data)
					<div class="row mb-3">
						<div class="col-6">
							<div class="form-group mb-0">
								<input wire:model="datas.{{ $key }}.plan_date" type="text" class="form-control bg-light {{ $errors->has('datas.'.$key.'.plan_date') ? 'is-invalid' : '' }}" readonly>
								@error('datas.'.$key.'.plan_date')
									<div class="invalid-feedback">{{ $message }}</div>
								@enderror
							</div>
						</div>
						<div class="col-6">
							<div class="form-group mb-0">
								<div class="input-group">
									<div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
									<input wire:model="datas.{{ $key }}.value" type="number" class="form-control {{ $errors->has('datas.'.$key.'.value') ? 'is-invalid' : '' }}" placeholder="...." />
									@error('datas.'.$key.'.value')
										<div class="invalid-feedback">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>

		<div class="card card-custom gutter-b">
			<div class="card-body text-right">
				<a href="{{ route('financials::liquidities.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
				<button wire:click="submit" wire:loading.attr="disabled" type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
			</div>
		</div>
	@else
		<div class="alert alert-warning">
			{{ __('There\'s no data to edit') }}
		</div>
	@endif
</div>
