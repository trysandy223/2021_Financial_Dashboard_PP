{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::hunian_mbrs.store') }}" novalidate="" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pens.hunian_mbrs.form')
    </form>
    <!--end::Form-->
@endsection