{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::kontrak_jumlah_proyeks.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pens.kontrak_jumlah_proyeks.form')
    </form>
    <!--end::Form-->
@endsection