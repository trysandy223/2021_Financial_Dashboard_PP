{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::kontrak_jumlah_proyeks.update', ['id' => $data->id]) }}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pens.kontrak_jumlah_proyeks.form')
    </form>
    <!--end::Form-->
@endsection