{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::komponen_dalam_negeris.update', ['id' => $data->id]) }}" novalidate="" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pens.komponen_dalam_negeris.form')
    </form>
    <!--end::Form-->
@endsection