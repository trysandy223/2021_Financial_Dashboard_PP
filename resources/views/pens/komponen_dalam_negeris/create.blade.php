{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::komponen_dalam_negeris.store') }}" novalidate="" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pens.komponen_dalam_negeris.form')
    </form>
    <!--end::Form-->
@endsection