{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::penyerapan_tenaga_kerjas.update', ['id' => $data->id]) }}" novalidate="" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pens.penyerapan_tenaga_kerjas.form')
    </form>
    <!--end::Form-->
@endsection