{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::penyerapan_tenaga_kerjas.store') }}" novalidate="" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pens.penyerapan_tenaga_kerjas.form')
    </form>
    <!--end::Form-->
@endsection