@if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@elseif (session('failed'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('failed') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<div class="card card-custom mb-5">
    <div class="card-header">
        <h3 class="card-title">{{ __('Informasi Proyek') }}</h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <a href="{{ route('pens::realisasi_belanja_umkms.index') }}" class="btn btn-secondary font-weight-bolder pull-right">
                <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Pixels.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                            <rect id="Rectangle-187" fill="#000000" x="4" y="16" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy-3" fill="#000000" x="4" y="10" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy" fill="#000000" x="10" y="16" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy-4" fill="#000000" opacity="0.3" x="10" y="10" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy-5" fill="#000000" x="4" y="4" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy-2" fill="#000000" x="16" y="16" width="4" height="4" rx="1"></rect>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>{{ __('Back') }}</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if (in_array($company->id, config('const.parent_company')))
            <input type="hidden" class="form-control" name="company_id" value="{{ $company->id }}">
            <div class="form-group">
                <label class="form-control-label">{{ __('Company Name') }}</label>
                <div class="{{ $errors->has('company') ? 'border border-danger rounded' : '' }}">
                    <select required id="selectCompany" class="form-control">
                        @if ($data->company)
                            <option value="{{ $data->company_id }}" selected>{{ $data->company->code . ' - ' . $data->company->name }}</option>
                        @else
                            <option value=""></option>
                        @endif
                    </select>
                    <div class="invalid-feedback">This field is required</div>
                </div>
            </div>
        @else
            <div class="form-group">
                <label class="form-control-label">{{ __('Company Name') }}</label>
                @if (in_array($company->id, config('const.sub_parent_company')))
                    <input type="hidden" class="form-control" name="company_id" value="{{ $company->parent->id }}">
                    <input type="text" class="form-control" value="{{ $company->parent->code . ' - ' . $company->parent->name }}" disabled>
                @else
                    <input type="hidden" class="form-control" name="company_id" value="{{ $company->id }}">
                    <input type="text" class="form-control" value="{{ $company->code . ' - ' . $company->name }}" disabled>
                @endif
            </div>
        @endif

        <div class="form-group row">
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Date') }}</label><span class="text-danger"> *</span>
                <div class="input-group">
                    <input required type="text" class="form-control datepicker {{ $errors->has('date') ? 'is-invalid' : '' }}" name="date" placeholder="Enter date" value="{{ (!empty($data->date)) ? $data->date : old('date') }}" />
                    <div class="input-group-append"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
                    @if ($errors->has('date'))
                        <div class="invalid-feedback">{{ $errors->first('date') }}</div>
                    @else
                        <div class="invalid-feedback">This field is required</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form-group {{ $errors->has('project_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Project') }}</label><span class="text-danger"> *</span>
                    <select {{ $data->project ? '' : 'required' }} class="form-control select2  {{ $errors->has('project_id') ? 'is-invalid' : '' }}" name="project_id" id="project_id" style="width: 100%">
                        <option value="">Choose project</option>
                    </select>
                    @if ($errors->has('project_id'))
                        <div class="invalid-feedback">{{ $errors->first('project_id') }}</div>
                    @else
                        <div class="invalid-feedback">This field is required</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-6">
                <label class="form-control-label">{{ __('Sistem Padi') }}</label><span class="text-danger"> *</span>
                <input required type="number" class="form-control {{ $errors->has('sistem_padi') ? 'is-invalid' : '' }}" name="sistem_padi" placeholder="Enter sistem padi" value="{{ (!empty($data->sistem_padi)) ? $data->sistem_padi : old('sistem_padi') }}">
                @if ($errors->has('sistem_padi'))
                    <div class="invalid-feedback">{{ $errors->first('sistem_padi') }}</div>
                @else
                    <div class="invalid-feedback">This field is required</div>
                @endif
            </div>

            <div class="col-lg-6">
                <label class="form-control-label">{{ __('Sistem Milik Sendiri') }}</label><span class="text-danger"> *</span>
                <input required type="number" class="form-control {{ $errors->has('sistem_sendiri') ? 'is-invalid' : '' }}" name="sistem_sendiri" placeholder="Enter sistem milik sendiri" value="{{ (!empty($data->sistem_sendiri)) ? $data->sistem_sendiri : old('sistem_sendiri') }}">
                @if ($errors->has('sistem_sendiri'))
                    <div class="invalid-feedback">{{ $errors->first('sistem_sendiri') }}</div>
                @else
                    <div class="invalid-feedback">This field is required</div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="card card-custom text-right">
    <div class="card-footer">
        <a href="{{ route('pens::realisasi_belanja_umkms.index') }}" class="btn btn-secondary mr-2">{{ __('Cancel') }}</a>
        <button type="submit" class="btn btn-primary mr-2" name="submit" value="submit">{{ __('Submit & Close') }}</button>
        <button type="submit" class="btn btn-primary" name="save" value="save">{{ __('Submit & New') }}</button>
    </div>
</div>

{{-- Scripts Section --}}
@section('scripts')
<script>
        $("form").submit(function(e){
            var vForm = $(this);
            if (vForm[0].checkValidity() === false) {
                event.preventDefault()
                event.stopPropagation()
            } else {
                // submitted
            }

            vForm.addClass('was-validated');
        });

        $("#project_id").attr("disabled", true);

        @if(!in_array($company->id, config('const.parent_company')))
            $("#project_id").attr("disabled", false);
            @if(in_array($company->id, config('const.sub_parent_company')))
                initSelectProject('unit', '{{ $company->id }}');
            @else
                initSelectProject('company', '{{ $company->id }}');
            @endif
        @endif

        $("#selectCompany").select2({
            placeholder: "-- Select --",
            "ajax" : {
                "delay": 250,
                "url" : "{{ route('pages::companies.selectBox') }}",
                "type" : "POST",
                "dataType" : "json",
                "data": function (params) {
                    // set params
                    var query = {
                        search: params.term,
                        status: 1,
                        text_format: 'code-name',
                        _token: '{{ csrf_token() }}',
                        page: params.page || 1
                    }
                    return query;
                },
                "processResults": function(data, params) {
                    params.page = params.page || 1;
                    
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            }
        }).on('change', function(e) {
            var companyId = e.target.value;

            if (companyId != '') {
                $("#project_id").empty().trigger("change");
                $("#project_id").attr("disabled", false);

                var subParentCompany = @json(config("const.sub_parent_company"));
                if (subParentCompany.includes(parseInt(companyId))) {
                    initSelectProject('unit', companyId);
                } else {
                    initSelectProject('company', companyId);
                }
            }
        }).val("{{ $data->company_id ?? '' }}").trigger('change');

        function initSelectProject(filter = 'company', companyId = null) {
            $("#project_id").select2({
                placeholder: "{{ $data->project ? $data->project->code . ' - ' . $data->project->name : 'Choose project' }}",
                "ajax" : {
                    "delay": 250,
                    "url" : "{{ route('pages::projects.selectBox') }}",
                    "type" : "POST",
                    "dataType" : "json",
                    "data": function (params) {
                        // set params
                        var query = {
                            search: params.term,
                            unit_id: filter === 'unit' ? companyId : '',
                            inherit_company_id: filter === 'company' ? companyId : '',
                            _token: '{{ csrf_token() }}',
                            page: params.page || 1
                        }
                        return query;
                    },
                    "processResults": function(data, params) {
                        params.page = params.page || 1;
                        
                        return {
                            results: data.results,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                }
            });
        }
    </script>

    {{-- Init datepicker script --}}
    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        });
    </script>
@endsection