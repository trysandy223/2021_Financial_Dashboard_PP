{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::realisasi_belanja_umkms.update', ['id' => $data->id]) }}" novalidate="" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pens.realisasi_belanja_umkms.form')
    </form>
    <!--end::Form-->
@endsection