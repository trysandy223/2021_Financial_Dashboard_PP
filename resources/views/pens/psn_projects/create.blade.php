{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::psn_projects.store') }}" novalidate="" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pens.psn_projects.form')
    </form>
    <!--end::Form-->
@endsection