{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::psn_projects.update', ['id' => $data->id]) }}" novalidate="" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pens.psn_projects.form')
    </form>
    <!--end::Form-->
@endsection