{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::company_revenues.update', ['id' => $data->id]) }}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pens.company_revenues.form')
    </form>
    <!--end::Form-->
@endsection