{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::company_revenues.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pens.company_revenues.form')
    </form>
    <!--end::Form-->
@endsection