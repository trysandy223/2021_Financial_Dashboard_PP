@if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@elseif (session('failed'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('failed') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<div class="card card-custom mb-5">
    <div class="card-header">
        <h3 class="card-title">{{ __('Informasi Proyek') }}</h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <a href="{{ route('pens::company_revenues.index') }}" class="btn btn-secondary font-weight-bolder pull-right">
                <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Pixels.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                            <rect id="Rectangle-187" fill="#000000" x="4" y="16" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy-3" fill="#000000" x="4" y="10" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy" fill="#000000" x="10" y="16" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy-4" fill="#000000" opacity="0.3" x="10" y="10" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy-5" fill="#000000" x="4" y="4" width="4" height="4" rx="1"></rect>
                            <rect id="Rectangle-187-Copy-2" fill="#000000" x="16" y="16" width="4" height="4" rx="1"></rect>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>{{ __('Back') }}</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Date') }}</label><span class="text-danger"> *</span>
                <div class="input-group">
                    <input type="text" class="form-control datepicker {{ $errors->has('date') ? 'is-invalid' : '' }}" name="date" placeholder="Enter date" value="{{ (!empty($data->date)) ? $data->date : old('date') }}" />
                    <div class="input-group-append"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
                    @if ($errors->has('date'))
                        <div class="invalid-feedback">{{ $errors->first('date') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-8">
                <label class="form-control-label">{{ __('Company Name') }}</label>
                @if (in_array($company->id, config('const.sub_parent_company')))
                    <input type="text" class="form-control" value="{{ $company->parent->name }}" disabled>
                    <input type="text" class="form-control d-none" name="company_id" value="{{ $company->parent->id }}">
                @else
                    <input type="text" class="form-control" value="{{ $company->name }}" disabled>
                    <input type="text" class="form-control d-none" name="company_id" value="{{ $company->id }}">
                @endif
            </div>
        </div>

        <div class="form-group row pb-4">
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('business_segment_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Business Segment') }}</label><span class="text-danger"> *</span>
                    <select class="form-control select2  {{ $errors->has('business_segment_id') ? 'is-invalid' : '' }}" name="business_segment_id" id="business_segment_id" style="width: 100%">
                        <option value="">Choose business segment</option>
                        @foreach ($business_segments as $business_segment)
                            <option value="{{ $business_segment->id }}" {{ (!empty($data->business_segment_id)) ? ($data->business_segment_id == $business_segment->id) ? ' selected' : '' : '' }} {{ old("business_segment_id") == $business_segment->id ? ' selected' : '' }}> {{ $business_segment->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('business_segment_id'))
                        <div class="invalid-feedback">{{ $errors->first('business_segment_id') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-6">
                <label class="form-control-label">{{ __('Total Revenue') }}</label><span class="text-danger"> *</span>
                <input type="number" class="form-control {{ $errors->has('total_revenue') ? 'is-invalid' : '' }}" name="total_revenue" placeholder="Enter total revenue" value="{{ (!empty($data->total_revenue)) ? $data->total_revenue : old('total_revenue') }}">
                @if ($errors->has('total_revenue'))
                    <div class="invalid-feedback">{{ $errors->first('total_revenue') }}</div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="card card-custom text-right">
    <div class="card-footer">
        <a href="{{ route('pens::company_revenues.index') }}" class="btn btn-secondary mr-2">{{ __('Cancel') }}</a>
        <button type="submit" class="btn btn-primary mr-2" name="submit" value="submit">{{ __('Submit & Close') }}</button>
        <button type="submit" class="btn btn-primary" name="save" value="save">{{ __('Submit & New') }}</button>
    </div>
</div>

{{-- Scripts Section --}}
@section('scripts')
    {{-- Init datepicker script --}}
    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        });
    </script>
@endsection