{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::demand_material_constructions.store') }}" novalidate="" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pens.demand_material_constructions.form')
    </form>
    <!--end::Form-->
@endsection