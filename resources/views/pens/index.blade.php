{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    {{-- List of Projects --}}
    <div class="container-fluid">
        <!--begin::Row-->
        <div class="row">
            @can('pen-demand-material-construction-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/demand-material-construction.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/pens/demand_material_constructions" class="text-dark font-weight-bold text-hover-primary font-size-h4">Demand Material Construction</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/pens/demand_material_constructions" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
            @can('pen-penyediaan-hunian-mbr-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/penyediaan-hunian-mbr.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/pens/hunian_mbrs" class="text-dark font-weight-bold text-hover-primary font-size-h4">Penyediaan Hunian MBR</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/pens/hunian_mbrs" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
            @can('pen-pendapatan-usaha-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/pendapatan-usaha.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/pens/company_revenues" class="text-dark font-weight-bold text-hover-primary font-size-h4">Pendapatan Usaha</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/pens/company_revenues" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
            @can('pen-penyerapan-tenaga-kerja-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/penyerapan-tenaga-kerja.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/pens/penyerapan_tenaga_kerjas" class="text-dark font-weight-bold text-hover-primary font-size-h4">Penyerapan Tenaga Kerja</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/pens/penyerapan_tenaga_kerjas" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
            @can('pen-perolehan-kontrak-jumlah-proyek-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/perolehan-kontrak-jumlah-proyek.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/pens/kontrak_jumlah_proyeks" class="text-dark font-weight-bold text-hover-primary font-size-h4">Perolehan Kontrak Jumlah Proyek</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/pens/kontrak_jumlah_proyeks" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
            @can('pen-proyek-psn-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/proyek-psn.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/pens/psn_projects" class="text-dark font-weight-bold text-hover-primary font-size-h4">Proyek PSN</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/pens/psn_projects" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
            @can('pen-realisasi-belanja-umkm-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/realisasi-belanja-umkm.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/pens/realisasi_belanja_umkms" class="text-dark font-weight-bold text-hover-primary font-size-h4">Realisasi Belanja UMKM</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/pens/realisasi_belanja_umkms" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
            @can('pen-target-realisasi-capex-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/target-realisasi-capex.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/pens/target_realisasi_capexes" class="text-dark font-weight-bold text-hover-primary font-size-h4">Target Realisasi Capex</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/pens/target_realisasi_capexes" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
            @can('pen-tingkat-komponen-dalam-negeri-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/tingkat-komponen-dalam-negeri.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/pens/komponen_dalam_negeris" class="text-dark font-weight-bold text-hover-primary font-size-h4">Tingkat Komponen Dalam Negeri</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/pens/komponen_dalam_negeris" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
        </div>
        <!--end::Row-->
    </div>

@endsection