{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::target_realisasi_capexes.store') }}" novalidate="" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pens.target_realisasi_capexes.form')
    </form>
    <!--end::Form-->
@endsection