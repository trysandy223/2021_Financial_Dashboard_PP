{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pens::target_realisasi_capexes.update', ['id' => $data->id]) }}" novalidate="" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pens.target_realisasi_capexes.form')
    </form>
    <!--end::Form-->
@endsection