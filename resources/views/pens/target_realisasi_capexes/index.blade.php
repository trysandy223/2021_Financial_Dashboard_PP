{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

@if (session('failures'))
    <div class="alert alert-danger mb-3" role="alert">
        <strong>Failed to import data! Please check the following errors:</strong>
        
        <ul>
            @foreach (session('failures') as $failure)
                @foreach ($failure->errors() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            @endforeach
        </ul>
    </div>
@endif

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">{{ $page_title }}
            <span class="d-block text-muted pt-2 font-size-sm">{{ $page_description }}</span></h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="{{ route('pens::dashboard') }}" class="btn btn-secondary font-weight-bolder mr-3">
            <span class="svg-icon svg-icon-md">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Pixels.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                        <rect id="Rectangle-187" fill="#000000" x="4" y="16" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy-3" fill="#000000" x="4" y="10" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy" fill="#000000" x="10" y="16" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy-4" fill="#000000" opacity="0.3" x="10" y="10" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy-5" fill="#000000" x="4" y="4" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy-2" fill="#000000" x="16" y="16" width="4" height="4" rx="1"></rect>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>{{ __('Back') }}</a>
            <div class="dropdown dropdown-inline mr-2">
                <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24" />
                            <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
                            <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>Action</button>
                <!--begin::Dropdown Menu-->
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                    <!--begin::Navigation-->
                    <ul class="navi flex-column navi-hover py-2">
                        <li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">Choose an option:</li>
                        @can('pen-target-realisasi-capex-create')
                        <li class="navi-item">
                            <a href="{{ route('pens::target_realisasi_capexes.create') }}" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-plus-square"></i>
                                </span>
                                <span class="navi-text">New Record</span>
                            </a>
                        </li>
                        @endcan
                        <li class="navi-item">
                            <a href="javascript:void(0);" class="navi-link" data-toggle="modal" data-target="#importModal">
                                <span class="navi-icon">
                                    <i class="la la-file-import"></i>
                                </span>
                                <span class="navi-text">Import Excel</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="{{ route('pens::target_realisasi_capexes.export') }}" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-export"></i>
                                </span>
                                <span class="navi-text">Export Excel</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="{{ route('pens::target_realisasi_capexes.download-template') }}" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-export"></i>
                                </span>
                                <span class="navi-text">Download Template Import</span>
                            </a>
                        </li>
                    </ul>
                    <!--end::Navigation-->
                </div>
                <!--end::Dropdown Menu-->
            </div>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <h5>Filter</h5>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Date') }}</label>
                    <div class="input-group">
                        <input type="text" class="form-control datepicker" id="date" placeholder="Enter date">
                        <div class="input-group-append"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Company') }}</label>
                    <select id="company" class="form-control">
                        <option value=""></option>
                        @foreach ($companies as $company)
                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Project') }}</label>
                    <select id="project" class="form-control">
                        <option value=""></option>
                        @foreach ($projects as $project)
                            <option value="{{ $project->id }}">{{ $project->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <button class="btn btn-light-warning" onclick="resetFilter()">Reset</button>
    </div>
    <hr class="my-0">
    <div class="card-body">
        <!--begin: Datatable-->
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tableData">
                <thead>
                    <tr>
                        <th>{{ __('No') }}</th>
                        <th>{{ __('Date') }}</th>
                        <th>{{ __('Company') }}</th>
                        <th>{{ __('Project') }}</th>
                        <th>{{ __('Nilai Equity') }}</th>
                        <th>{{ __('Nilai Loan') }}</th>
                        <th>{{ __('Nilai Capex') }}</th>
                        <th>{{ __('Pekerjaan Fisik') }}</th>
                        <th>{{ __('Last Modified') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!--end: Datatable-->
    </div>
</div>

<!-- Import Modal-->
<div class="modal fade" id="importModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import {{ $page_title }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" enctype="multipart/form-data" action="{{ route('pens::target_realisasi_capexes.import') }}">
                    @csrf
                    <div class="form-group">
                        <label>Choose Excel File (.xlsx)</label>
                        <div></div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file_name" accept="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
                            <label class="custom-file-label">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
    <style>
        table.dataTable th:nth-child(1) {
            min-width: 30px;
        }

        table.dataTable th {
            min-width: 100px;
        }
    </style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        // Init datatables
        $(document).ready(function() {
            var table = $('#tableData').DataTable({
                scrollX: true,
                scrollCollapse: true,
                proccesing: true,
                serverSide: true,
                ajax: {
                    url: "/pens/target_realisasi_capexes"
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'date',
                        name: 'date'
                    },
                    {
                        data: 'company',
                        name: 'company'
                    },
                    {
                        data: 'project',
                        name: 'project'
                    },
                    {
                        data: 'nilai_equity',
                        name: 'nilai_equity'
                    },
                    {
                        data: 'nilai_loan',
                        name: 'nilai_loan'
                    },
                    {
                        data: 'nilai_capex',
                        name: 'nilai_capex'
                    },
                    {
                        data: 'pekerjaan_fisik',
                        name: 'pekerjaan_fisik'
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                language: {
                    searchPlaceholder: "search...",
                    sSearch: ""
                },
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
        
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
        
                    // Total over this page
                    pageNilaiEquity = api
                        .column( 4, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        rupiahFormat(pageNilaiEquity, 'Rp') +' ('+ (typeof data[0] != "undefined" ? rupiahFormat(data[0].sum_nilai_equity, 'Rp') : 0) +' total)'
                    );

                    // Total over this page
                    pageNilaiLoan = api
                        .column( 5, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column( 5 ).footer() ).html(
                        rupiahFormat(pageNilaiLoan, 'Rp') +' ('+ (typeof data[0] != "undefined" ? rupiahFormat(data[0].sum_nilai_loan, 'Rp') : 0) +' total)'
                    );

                    // Total over this page
                    pageNilaiCapex = api
                        .column( 6, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column( 6 ).footer() ).html(
                        rupiahFormat(pageNilaiCapex, 'Rp') +' ('+ (typeof data[0] != "undefined" ? rupiahFormat(data[0].sum_nilai_capex, 'Rp') : 0) +' total)'
                    );
                }
            });

            // filter by date
            $('#date').on('change', function() {
                var data = $("#date").val();
                table.columns(1).search(data).draw();
            });

            // filter by company
            $('#company').select2({
                allowClear: true,
                placeholder: "Select company"
            }).on('change', function() {
                var data = $("#company option:selected").text();
                table.columns(2).search(data).draw();
            });

            // filter by project
            $('#project').select2({
                allowClear: true,
                placeholder: "Select project"
            }).on('change', function() {
                var data = $("#project option:selected").text();
                table.columns(3).search(data).draw();
            });
        });
        
        // delete function
        function deleteData(id) {
            Swal.fire({
                title: "Are you sure?",
                text: "You wont be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "target_realisasi_capexes/"+id+"/delete",
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        error: function() {
                            $('#tableData').DataTable().ajax.reload();
                            Swal.fire("Failed!", "Failed to delete data! It may be that the data you want to delete is being used in another table.", "error")
                        },
                        success: function(data) {
                            $('#tableData').DataTable().ajax.reload();
                            Swal.fire("Deleted!", "Your data has been deleted.", "success")
                        }
                    });
                } else {
                    Swal.fire("Cancelled", "Okay, your data is safe", "error")
                }
            });
        }

        // go to edit page
        function editData(id) {
            window.location = 'target_realisasi_capexes/'+id+'/edit';
        }

        // reset filter
        function resetFilter() {
            $('#date').val('').trigger('change');
            $('#company').val('').trigger('change');
            $('#project').val('').trigger('change');
        }
    </script>

    {{-- Init datepicker script --}}
    <script>
        $('.datepicker').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            todayHighlight: true
        });
    </script>

    {{-- success message alert --}}
    @if (session('success'))
        <script>
            Swal.fire("Success!", '{{ session('success') }}', "success");
        </script>
    @endif

    {{-- failed message alert --}}
    @if (session('failed'))
        <script>
            Swal.fire("Failed!", '{{ session('failed') }}', "error");
        </script>
    @endif
@endsection