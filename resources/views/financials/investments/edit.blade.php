{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.investments.edit', ['reference' => $data])
    <!--end::Form-->

@endsection