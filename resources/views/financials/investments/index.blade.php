{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">{{ $page_title }}
            <span class="d-block text-muted pt-2 font-size-sm">{{ $page_description }}</span></h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="{{ route('financials::dashboard') }}" class="btn btn-secondary font-weight-bolder mr-3">
            <span class="svg-icon svg-icon-md">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Pixels.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                        <rect id="Rectangle-187" fill="#000000" x="4" y="16" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy-3" fill="#000000" x="4" y="10" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy" fill="#000000" x="10" y="16" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy-4" fill="#000000" opacity="0.3" x="10" y="10" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy-5" fill="#000000" x="4" y="4" width="4" height="4" rx="1"></rect>
                        <rect id="Rectangle-187-Copy-2" fill="#000000" x="16" y="16" width="4" height="4" rx="1"></rect>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>{{ __('Back') }}</a>
            @can('financial-investment-create')
            <a href="{{ route('financials::investments.create') }}" class="btn btn-primary font-weight-bolder">
            <span class="svg-icon svg-icon-md">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <circle fill="#000000" cx="9" cy="15" r="6" />
                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>{{ __('New Record') }}</a>
            <!--end::Button-->
            @endcan
        </div>
    </div>
	<div class="card-body">
        <h5>Filter</h5>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Company') }}</label>
                    <select id="company" class="form-control">
                        <option value=""></option>
                        @foreach ($companies as $company)
                            <option value="{{ $company->name }}">{{ $company->code . ' - ' . $company->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Project') }}</label>
                    <select id="project" class="form-control">
                        <option value=""></option>
                        @foreach ($projects as $project)
                            <option value="{{ $project->name }}">{{ $project->code . ' - ' . $project->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Source of Fund') }}</label>
                    <select id="bank" class="form-control">
                        <option value=""></option>
                        @foreach ($banks as $bank)
                            <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Category') }}</label>
                    <select id="category" class="form-control">
                        <option value=""></option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Flag') }}</label>
                    <select id="flag" class="form-control">
                        <option value=""></option>
                        @foreach ($flags as $flag)
                            <option value="{{ $flag->id }}">{{ $flag->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Status') }}</label>
                    <select id="status" class="form-control">
                        <option value=""></option>
                        @foreach (config('const.financial_investment_status') as $key => $status)
                            <option value="{{ $key }}">{{ $status }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <button class="btn btn-light-warning" onclick="resetFilter()">Reset</button>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <div class="table-responsive">
            <table class="table table-bordered table-striped responsive nowrap" id="tableData">
                <thead>
                    <tr>
                        <th data-priority="1">{{ __('No') }}</th>
                        <th data-priority="2">{{ __('Company') }}</th>
                        <th data-priority="3">{{ __('Project') }}</th>
                        <th>{{ __('Category') }}</th>
                        <th>{{ __('Flag') }}</th>
                        <th>{{ __('Source of Fund') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th>{{ __('Posting Date') }}</th>
                        <th>{{ __('Trade Partner') }}</th>
                        <th data-priority="5">{{ __('Value') }}</th>
                        <th data-priority="4">{{ __('Action') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!--end: Datatable-->
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        // Init datatables
        $(document).ready(function() {
            var table = $('#tableData').DataTable({
                responsive: true,
                proccesing: true,
                serverSide: true,
                ajax: {
                    url: "/financials/investments",
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'company',
                        name: 'company.name',
                    },
                    {
                        data: 'project_name',
                        name: 'project_name',
                    },
                    {
                        data: 'category_name',
                        name: 'category_name',
                    },
                    {
                        data: 'flag_investment_name',
                        name: 'flag_investment_name',
                    },
                    {
                        data: 'bank_name',
                        name: 'bank_name',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: 'posting_date',
                        name: 'posting_date',
                    },
                    {
                        data: 'trading_partner',
                        name: 'trading_partner',
                    },
                    {
                        data: 'value',
                        name: 'value',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                    },
                ],
                language: {
                    searchPlaceholder: "search...",
                    sSearch: ""
                }
            });

            $('#company').select2({
                allowClear: true,
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#company option:selected").val();
                table.columns(1).search( data ).draw();
            });

            $('#project').select2({
                allowClear: true,
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#project option:selected").val();
                table.columns(2).search( data ).draw();
            });

            $('#bank').select2({
                allowClear: true,
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#bank option:selected").text();
                table.columns(5).search( data ).draw();
            });

            $('#category').select2({
                allowClear: true,
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#category option:selected").text();
                table.columns(3).search( data ).draw();
            });

            $('#flag').select2({
                allowClear: true,
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#flag option:selected").text();
                table.columns(4).search( data ).draw();
            });

            $('#status').select2({
                allowClear: true,
                placeholder: "-- Select --"
            }).on('change', function() {
                var data = $("#status option:selected").text();
                table.columns(6).search( data ).draw();
            });
        });

        // delete function
        function deleteData(id) {
            Swal.fire({
                title: "Are you sure?",
                text: "You wont be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "investments/"+id+"/delete",
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $('#tableData').DataTable().ajax.reload();
                            Swal.fire("Deleted!", "Your data has been deleted.", "success")
                        }
                    });
                } else {
                    Swal.fire("Cancelled", "Okay, your data is safe", "error")
                }
            });
        }

        // go to edit page
        function editData(id) {
            window.location = 'investments/'+id+'/edit';

        }

        // reset filter
        function resetFilter() {
            $('#company').val('').trigger('change');
            $('#project').val('').trigger('change');
            $('#bank').val('').trigger('change');
            $('#category').val('').trigger('change');
            $('#flag').val('').trigger('change');
            $('#status').val('').trigger('change');
        }
    </script>

    {{-- success message alert --}}
    @if (session('success'))
        <script>
            Swal.fire("Success!", '{{ session('success') }}', "success");
        </script>
    @endif

    {{-- failed message alert --}}
    @if (session('failed'))
        <script>
            Swal.fire("Failed!", '{{ session('failed') }}', "error");
        </script>
    @endif
@endsection