{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.ncls.edit', ['reference' => $data])
    <!--end::Form-->

@endsection