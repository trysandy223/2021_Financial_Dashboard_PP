{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.revolving-loans.edit', ['reference' => $data])
    <!--end::Form-->

@endsection