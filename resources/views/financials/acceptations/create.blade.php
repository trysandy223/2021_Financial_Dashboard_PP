{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.acceptations.form', ['model' => $data])
    <!--end::Form-->

@endsection