{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.acceptations.edit', ['reference' => $data])
    <!--end::Form-->

@endsection