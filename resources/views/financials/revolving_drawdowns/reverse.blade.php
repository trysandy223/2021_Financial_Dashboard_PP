{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.revolving-drawdowns.reverse', ['model' => $data])
    <!--end::Form-->

@endsection