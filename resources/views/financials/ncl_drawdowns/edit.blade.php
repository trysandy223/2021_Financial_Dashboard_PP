{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.ncl-drawdowns.edit', ['reference' => $data])
    <!--end::Form-->

@endsection