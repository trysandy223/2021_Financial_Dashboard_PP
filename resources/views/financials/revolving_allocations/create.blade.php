{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.revolving-allocations.form', ['model' => $data])
    <!--end::Form-->

@endsection