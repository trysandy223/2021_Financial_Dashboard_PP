{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.term-loan-allocations.edit', ['reference' => $data])
    <!--end::Form-->

@endsection