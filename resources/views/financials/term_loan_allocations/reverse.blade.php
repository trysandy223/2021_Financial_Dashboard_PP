{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.term-loan-allocations.reverse', ['model' => $data])
    <!--end::Form-->

@endsection