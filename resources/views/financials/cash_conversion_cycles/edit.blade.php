{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.cash-conversion-cycles.edit', ['reference' => $data])
    <!--end::Form-->

@endsection