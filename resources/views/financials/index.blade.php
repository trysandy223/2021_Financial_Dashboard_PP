{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    {{-- List of Projects --}}
    <div class="container-fluid">
        <!--begin::Row-->
        <div class="row">
            @can('financial-liquidity-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/liquidity.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/liquidities" class="text-dark font-weight-bold text-hover-primary font-size-h4">Liquidity</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/liquidities" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-investment-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/investment.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/investments" class="text-dark font-weight-bold text-hover-primary font-size-h4">Investment</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/investments" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-dso-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/dso.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/days_sales_outstandings"
                                class="text-dark font-weight-bold text-hover-primary font-size-h4">Days Sales Outstanding</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/days_sales_outstandings"
                                class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-term-loan-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/term-loan.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/term_loans" class="text-dark font-weight-bold text-hover-primary font-size-h4">Term Loan</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/term_loans" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-term-loan-allocation-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/term-loan-allocation.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/term_loan_allocations" class="text-dark font-weight-bold text-hover-primary font-size-h4">Term Loan Allocation</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/term_loan_allocations" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-term-loan-allocation-create')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/reverse.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/term_loan_allocations/reverse" class="text-dark font-weight-bold text-hover-primary font-size-h4">Reverse Term Loan Allocation</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/term_loan_allocations/reverse" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-revolving-loan-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/revolving-loan.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/revolving_loans" class="text-dark font-weight-bold text-hover-primary font-size-h4">Revolving Loan</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/revolving_loans" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-revolving-drawdown-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/revolving-drawdown.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/revolving_drawdowns" class="text-dark font-weight-bold text-hover-primary font-size-h4">Revolving Drawdown</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/revolving_drawdowns" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-revolving-allocation-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/revolving-allocation.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/revolving_allocations" class="text-dark font-weight-bold text-hover-primary font-size-h4">Revolving Allocation</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/revolving_allocations" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-revolving-drawdown-create')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/reverse.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/revolving_drawdowns/reverse" class="text-dark font-weight-bold text-hover-primary font-size-h4">Reverse Revolving Drawdown</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/revolving_drawdowns/reverse" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-ncl-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/ncl.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/ncls" class="text-dark font-weight-bold text-hover-primary font-size-h4">NCL</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/ncls" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-ncl-drawdown-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/ncl-drawdown.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/ncl_drawdowns" class="text-dark font-weight-bold text-hover-primary font-size-h4">NCL Drawdown</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/ncl_drawdowns" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-acceptation-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/acceptation.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/acceptations" class="text-dark font-weight-bold text-hover-primary font-size-h4">Acceptation</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/acceptations" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-ncl-drawdown-create')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/reverse.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/ncl_drawdowns/reverse" class="text-dark font-weight-bold text-hover-primary font-size-h4">Reverse NCL Drawdown</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/ncl_drawdowns/reverse" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan

            @can('financial-ccc-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/ccc.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/cash_conversion_cycles" class="text-dark font-weight-bold text-hover-primary font-size-h4">Cash Conversion Cycle</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/cash_conversion_cycles" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
            
            @can('financial-project-profitability-list')
            <!--begin::Column-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body text-center pt-4">
                        <!--begin::User-->
                        <div class="mt-7">
                            <div class="symbol symbol-circle symbol-lg-90">
                                <img src="{{ asset('media/project-logos/project-profitability.png') }}" alt="image" />
                            </div>
                        </div>
                        <!--end::User-->
                        <!--begin::Name-->
                        <div class="my-4">
                            <a href="/financials/project_profitabilities" class="text-dark font-weight-bold text-hover-primary font-size-h4">Project Profitability</a>
                        </div>
                        <!--end::Name-->
                        <!--begin::Buttons-->
                        <div class="mt-9">
                            <a href="/financials/project_profitabilities" class="btn btn-light-primary font-weight-bolder btn-sm py-3 px-6 text-uppercase">View Detail</a>
                        </div>
                        <!--end::Buttons-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Column-->
            @endcan
        </div>
        <!--end::Row-->
    </div>

@endsection