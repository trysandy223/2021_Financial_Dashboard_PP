{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.term-loans.form', ['model' => $data])
    <!--end::Form-->

@endsection