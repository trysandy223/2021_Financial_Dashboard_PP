{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.liquidities.edit', ['reference' => $data])
    <!--end::Form-->

@endsection