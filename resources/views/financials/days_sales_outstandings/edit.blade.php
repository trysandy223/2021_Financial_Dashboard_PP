{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <!--begin::Form-->
    @livewire('financials.days-sales-outstandings.edit', ['reference' => $data])
    <!--end::Form-->

@endsection