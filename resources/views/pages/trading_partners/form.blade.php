<div class="card-body">
    <div class="form-group">
        <label class="form-control-label">{{ __('Cocode') }}</label><span class="text-danger"> *</span>
        <input type="text" class="form-control col-lg-2 {{ $errors->has('code') ? 'is-invalid' : '' }}" name="code" placeholder="Enter code" value="{{ (!empty($data->code)) ? $data->code : old('code') }}">
        @if ($errors->has('code'))
            <div class="invalid-feedback">{{ $errors->first('code') }}</div>
        @endif
    </div>

    <div class="form-group">
        <label class="form-control-label">{{ __('Name') }}</label><span class="text-danger"> *</span>
        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="Enter name" value="{{ (!empty($data->name)) ? $data->name : old('name') }}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>

    <div class="form-group">
        <label class="form-control-label">{{ __('Description') }}</label>
        <textarea rows="5" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" placeholder="Enter description">{{ (!empty($data->description)) ? $data->description : old('description') }}</textarea>
        @if ($errors->has('description'))
            <div class="invalid-feedback">{{ $errors->first('description') }}</div>
        @endif
    </div>

    <div class="form-group {{ $errors->has('type') ? 'is-invalid' : '' }}">
        <label class="form-control-label">{{ __('Type') }}</label><span class="text-danger"> *</span>
        <select class="form-control select2  {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" style="width: 100%">
            <option value="">Choose type</option>
            @foreach(config('const.trading_partner_type') as $name => $val)
                <option value="{{ $name }}" {{ (old('type') == $name) ? 'selected' : '' }} {{ (!empty($data->type)) ? ($data->type == $name) ? 'selected' : '' : '' }}> {{ $val }} </option>
            @endforeach
        </select>
        @if ($errors->has('type'))
            <div class="invalid-feedback">{{ $errors->first('type') }}</div>
        @endif
    </div>

    <div class="form-group">
        <label class="form-control-label">{{ __('Sorting') }}</label><span class="text-danger"> *</span>
        <input type="number" class="form-control col-lg-2 {{ $errors->has('sorting') ? 'is-invalid' : '' }}" name="sorting" placeholder="Enter sorting" value="{{ (!empty($data->sorting)) ? $data->sorting : old('sorting') }}">
        @if ($errors->has('sorting'))
            <div class="invalid-feedback">{{ $errors->first('sorting') }}</div>
        @endif
    </div>

    <div class="form-group">
        <label class="form-control-label">{{ __('Active') }}</label>
        <span class="switch switch-icon">
            <label>
                <input type="checkbox" name="status" {{ (!empty($data)) ? ($data->status) ? ' checked' : '' : ' checked' }}/>
                <span></span>
            </label>
        </span>
        @if ($errors->has('status'))
            <div class="invalid-feedback">{{ $errors->first('status') }}</div>
        @endif       
    </div>
</div>

<div class="card-footer">
    <button type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
    <a href="{{ route('pages::trading_partners.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
</div>