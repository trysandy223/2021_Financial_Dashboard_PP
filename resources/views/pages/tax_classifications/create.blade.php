{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            {{ $page_title }}
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('pages::tax_classifications.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pages.tax_classifications.form')
    </form>
    <!--end::Form-->
</div>

@endsection