{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pages::projects.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pages.projects.form')
    </form>
    <!--end::Form-->
@endsection