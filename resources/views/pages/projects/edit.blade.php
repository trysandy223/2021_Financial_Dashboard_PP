{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pages::projects.update', ['id' => $data->id]) }}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pages.projects.form')
    </form>
    <!--end::Form-->
@endsection