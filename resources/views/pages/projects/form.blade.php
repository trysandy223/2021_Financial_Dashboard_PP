<div class="card card-custom mb-5">
    <div class="card-header">
        <h3 class="card-title">
            {{ __('Create Project Data') }}
        </h3>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-lg-12">
                <div class="form-group {{ $errors->has('company_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Company Name') }}</label>
                    <select class="form-control {{ $errors->has('company_id') ? 'is-invalid' : '' }}" name="company_id">
                        {!! (!empty($companies->id)) ? '<option value="'.$companies->id.'" selected>'.$companies->name.'</option>' : '<option value="">-- Choose company --</option>' !!}
                    </select>
                    @if ($errors->has('company_id'))
                        <div class="invalid-feedback">{{ $errors->first('company_id') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group row pb-4">
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Code') }}</label><span class="text-danger"> *</span>
                <input type="text" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" name="code" placeholder="Enter code" value="{{ (!empty($data->code)) ? $data->code : old('code') }}">
                @if ($errors->has('code'))
                    <div class="invalid-feedback">{{ $errors->first('code') }}</div>
                @endif
            </div>
            <div class="col-lg-8">
                <label class="form-control-label">{{ __('Name') }}</label><span class="text-danger"> *</span>
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="Enter name" value="{{ (!empty($data->name)) ? $data->name : old('name') }}">  
                @if ($errors->has('name'))
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row pb-4">
            <div class="col-lg-6">
                <label class="form-control-label">{{ __('Province') }}</label>
                <input type="text" class="form-control {{ $errors->has('province') ? 'is-invalid' : '' }}" name="province" placeholder="Enter province" value="{{ (!empty($data->province)) ? $data->province : old('province') }}">
                @if ($errors->has('province'))
                    <div class="invalid-feedback">{{ $errors->first('province') }}</div>
                @endif
            </div>
            <div class="col-lg-6">
                <label class="form-control-label">{{ __('City or Regency') }}</label>
                <input type="text" class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}" name="city" placeholder="Enter city" value="{{ (!empty($data->city)) ? $data->city : old('city') }}">  
                @if ($errors->has('city'))
                    <div class="invalid-feedback">{{ $errors->first('city') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('unit_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Unit') }}</label>
                    <select class="form-control {{ $errors->has('unit_id') ? 'is-invalid' : '' }}" name="unit_id">
                        {!! (!empty($units->id)) ? '<option value="'.$units->id.'" selected>'.$units->name.'</option>' : '<option value="">-- Choose company --</option>' !!}
                    </select>
                    @if ($errors->has('unit_id'))
                        <div class="invalid-feedback">{{ $errors->first('unit_id') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('project_type_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Project Type') }}</label>
                    <select class="form-control select2  {{ $errors->has('project_type_id') ? 'is-invalid' : '' }}" name="project_type_id" style="width: 100%">
                        <option value="">Choose project type</option>
                        @foreach ($project_types as $project_type)
                            <option value="{{ $project_type->id }},{{ $project_type->name }}" {{ (!empty($data->project_type_id)) ? ($data->project_type_id == $project_type->id) ? ' selected' : '' : '' }} {{ old("project_type_id") == $project_type->id . ',' . $project_type->name ? ' selected' : '' }}> {{ $project_type->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('project_type_id'))
                        <div class="invalid-feedback">{{ $errors->first('project_type_id') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('project_strategic_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Project Strategic') }}</label>
                    <select class="form-control select2  {{ $errors->has('project_strategic_id') ? 'is-invalid' : '' }}" name="project_strategic_id" style="width: 100%">
                        <option value="">Choose project strategic</option>
                        @foreach ($project_strategics as $project_strategic)
                            <option value="{{ $project_strategic->id }},{{ $project_strategic->name }}" {{ (!empty($data->project_strategic_id)) ? ($data->project_strategic_id == $project_strategic->id) ? ' selected' : '' : '' }} {{ old("project_strategic_id") == $project_strategic->id . ',' . $project_strategic->name ? ' selected' : '' }}> {{ $project_strategic->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('project_strategic_id'))
                        <div class="invalid-feedback">{{ $errors->first('project_strategic_id') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('business_segment_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Business Segment') }}</label>
                    <select class="form-control select2  {{ $errors->has('business_segment_id') ? 'is-invalid' : '' }}" name="business_segment_id" style="width: 100%">
                        <option value="">Choose business segment</option>
                        @foreach ($business_segments as $business_segment)
                            <option value="{{ $business_segment->id }},{{ $business_segment->name }}" {{ (!empty($data->business_segment_id)) ? ($data->business_segment_id == $business_segment->id) ? ' selected' : '' : '' }} {{ old("business_segment_id") == $business_segment->id . ',' . $business_segment->name ? ' selected' : '' }}> {{ $business_segment->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('business_segment_id'))
                        <div class="invalid-feedback">{{ $errors->first('business_segment_id') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('customer_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Customer/Owner Name') }}</label>
                    <select class="form-control select2  {{ $errors->has('customer_id') ? 'is-invalid' : '' }}" name="customer_id" style="width: 100%">
                        <option value="">Choose customer</option>
                        @foreach ($customers as $customer)
                            <option value="{{ $customer->id }},{{ $customer->name }}" {{ (!empty($data->customer_id)) ? ($data->customer_id == $customer->id) ? ' selected' : '' : '' }} {{ old("customer_id") == $customer->id . ',' . $customer->name ? ' selected' : '' }}> {{ $customer->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('customer_id'))
                        <div class="invalid-feedback">{{ $errors->first('customer_id') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Project Manager') }}</label>
                <input type="text" class="form-control {{ $errors->has('manager') ? 'is-invalid' : '' }}" name="manager" placeholder="Enter project manager name" value="{{ (!empty($data->manager)) ? $data->manager : old('manager') }}">  
                @if ($errors->has('manager'))
                    <div class="invalid-feedback">{{ $errors->first('manager') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('payment_type_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Payment Type') }}</label>
                    <select class="form-control select2  {{ $errors->has('payment_type_id') ? 'is-invalid' : '' }}" name="payment_type_id" style="width: 100%">
                        <option value="">Choose payment type</option>
                        @foreach ($payment_types as $payment_type)
                            <option value="{{ $payment_type->id }},{{ $payment_type->name }}" {{ (!empty($data->payment_type_id)) ? ($data->payment_type_id == $payment_type->id) ? ' selected' : '' : '' }} {{ old("payment_type_id") == $payment_type->id . ',' . $payment_type->name ? ' selected' : '' }}> {{ $payment_type->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('payment_type_id'))
                        <div class="invalid-feedback">{{ $errors->first('payment_type_id') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Project Start Date') }}</label>
                <div class="input-group">
                    <input type="text" class="form-control datepicker {{ $errors->has('start_date') ? 'is-invalid' : '' }}" name="start_date" placeholder="Enter project start date" value="{{ (!empty($data->start_date)) ? $data->start_date : old('start_date') }}" readonly />
                    <div class="input-group-append"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
                    @if ($errors->has('start_date'))
                        <div class="invalid-feedback">{{ $errors->first('start_date') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Project Finish Date') }}</label>
                <div class="input-group">
                    <input type="text" class="form-control datepicker {{ $errors->has('finish_date') ? 'is-invalid' : '' }}" name="finish_date" placeholder="Enter project finish date" value="{{ (!empty($data->finish_date)) ? $data->finish_date : old('finish_date') }}" readonly />
                    <div class="input-group-append"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
                    @if ($errors->has('finish_date'))
                        <div class="invalid-feedback">{{ $errors->first('finish_date') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('RAB (NK-PPN)') }}</label>
                <div class="input-group">
                    <div class="input-group-append"><span class="input-group-text">Rp</span></div>
                    <input type="number" class="form-control {{ $errors->has('rab') ? 'is-invalid' : '' }}" name="rab" placeholder="Enter RAB" value="{{ (!empty($data->rab)) ? $data->rab : old('rab') }}">
                    @if ($errors->has('rab'))
                        <div class="invalid-feedback">{{ $errors->first('rab') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('RAPK') }}</label>
                <div class="input-group">
                    <div class="input-group-append"><span class="input-group-text">Rp</span></div>
                    <input type="number" class="form-control {{ $errors->has('rkn') ? 'is-invalid' : '' }}" name="rkn" placeholder="Enter RAPK" value="{{ (!empty($data->rkn)) ? $data->rkn : old('rkn') }}">
                    @if ($errors->has('rkn'))
                        <div class="invalid-feedback">{{ $errors->first('rkn') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card card-custom text-right">
    <div class="card-footer">
        <a href="{{ route('pages::projects.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
        <button type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
    </div>
</div>

{{-- Scripts Section --}}
@section('scripts')
    {{-- AJAX company selectbox (parent child) script --}}
    <script>
        $(document).ready(function() {
            $("select[name=unit_id]").select2({
                "ajax" : {
                    "delay": 250,
                    "url" : "{{ route('pages::companies.selectBox') }}",
                    "type" : "POST",
                    "dataType" : "json",
                    "data": function (params) {
                        // set params
                        var query = {
                            search: params.term,
                            type: 'public',
                            is_project: '1',
                            _token: '{{ csrf_token() }}',
                            page: params.page || 1
                        }
                        return query;
                    },
                    "processResults": function(data, params) {
                        params.page = params.page || 1;
                        
                        return {
                            results: data.results,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                }
            });

            $("select[name=company_id]").select2({
                "ajax" : {
                    "delay": 250,
                    "url" : "{{ route('pages::companies.selectBox') }}",
                    "type" : "POST",
                    "dataType" : "json",
                    "data": function (params) {
                        // set params
                        var query = {
                            search: params.term,
                            type: 'public',
                            is_project: '1',
                            _token: '{{ csrf_token() }}',
                            page: params.page || 1
                        }
                        return query;
                    },
                    "processResults": function(data, params) {
                        params.page = params.page || 1;
                        
                        return {
                            results: data.results,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                }
            });
        })
    </script>

    {{-- Init datepicker script --}}
    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        });
    </script>
@endsection