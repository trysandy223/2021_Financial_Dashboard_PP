{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    {{-- List of Projects --}}
    <div class="container-fluid">
        <!--begin::Row-->
        <div class="row">
            <!--begin::Col-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body pt-4">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-7">
                            <!--begin::Pic-->
                            <div class="flex-shrink-0 mr-4">
                                <div class="symbol symbol-circle symbol-lg-75">
                                    <img src="{{ asset('media/project-logos/financial-dashboard.png') }}" alt="image" />
                                </div>
                            </div>
                            <!--end::Pic-->
                            <!--begin::Title-->
                            <div class="d-flex flex-column">
                                <a href="{{ route('financials::dashboard') }}" class="text-dark font-weight-bold text-hover-primary font-size-h4 mb-0">Financial Dashboard</a>
                            </div>
                            <!--end::Title-->
                        </div>
                        <!--end::User-->
                        <!--begin::Desc-->
                        <p class="mb-7">Financial dashboard application data control in PT. PP (Persero) Tbk.</p>
                        <!--end::Desc-->
                        <a href="{{ route('financials::dashboard') }}" class="btn btn-block btn-sm btn-light-success font-weight-bolder text-uppercase py-4">View Project</a>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end:: Card-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <!--begin::Body-->
                    <div class="card-body pt-4">
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-7">
                            <!--begin::Pic-->
                            <div class="flex-shrink-0 mr-4">
                                <div class="symbol symbol-circle symbol-lg-75">
                                    <img src="{{ asset('media/project-logos/pen-dashboard.png') }}" alt="image" />
                                </div>
                            </div>
                            <!--end::Pic-->
                            <!--begin::Title-->
                            <div class="d-flex flex-column">
                                <a href="{{ route('pens::dashboard') }}" class="text-dark font-weight-bold text-hover-primary font-size-h4 mb-0">PEN Dashboard</a>
                            </div>
                            <!--end::Title-->
                        </div>
                        <!--end::User-->
                        <!--begin::Desc-->
                        <p class="mb-7">PEN dashboard application data control in PT. PP (Persero) Tbk.</p>
                        <!--end::Desc-->
                        <a href="{{ route('pens::dashboard') }}" class="btn btn-block btn-sm btn-light-success font-weight-bolder text-uppercase py-4">View Project</a>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end:: Card-->
            </div>
            <!--end::Col-->
        </div>
        <!--end::Row-->
    </div>

@endsection

@section('scripts')
    {{-- success message alert --}}
    @if (session('success'))
        <script>
            Swal.fire("Success!", '{{ session('success') }}', "success");
        </script>
    @endif

    {{-- failed message alert --}}
    @if (session('failed'))
        <script>
            Swal.fire("Failed!", '{{ session('failed') }}', "error");
        </script>
    @endif
@endsection