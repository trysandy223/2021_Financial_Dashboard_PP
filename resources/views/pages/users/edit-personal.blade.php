{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            {{ $page_title }} {{ Auth::user()->name }}
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('pages::users.update-personal') }}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        
        <div class="card-body">
            <div class="form-group">
                <label class="form-control-label">{{ __('Full Name') }}</label><span class="text-danger"> *</span>
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="Enter full name" value="{{ (!empty($data->name)) ? $data->name : old('name') }}">
                @if ($errors->has('name'))
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @endif
            </div>
        
            <div class="form-group">
                <label class="form-control-label">{{ __('Email') }}</label><span class="text-danger"> *</span>
                <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" placeholder="Enter valid email" value="{{ (!empty($data->email)) ? $data->email : old('email') }}">
                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>
        
            <div class="form-group">
                <label class="form-control-label">{{ __('Password') }}</label>
                <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" placeholder="8 characters minimum">
                @if ($errors->has('password'))
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                @endif
            </div>
        
            <div class="form-group">
                <label class="form-control-label">{{ __('Password Confirmation') }}</label>
                <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="confirm-password" placeholder="Enter password again">
                @if ($errors->has('password'))
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                @endif
            </div>
        </div>
        
        <div class="card-footer">
            <button type="submit" class="btn btn-primary mr-2">{{ __('Save') }}</button>
            <a href="{{ route('pages::dashboard') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
        </div>
    </form>
    <!--end::Form-->
</div>

@endsection