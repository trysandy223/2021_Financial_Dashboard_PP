<div class="card-body">
    <div class="form-group">
        <label class="form-control-label">{{ __('Full Name') }}</label><span class="text-danger"> *</span>
        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="Enter full name" value="{{ (!empty($data->name)) ? $data->name : old('name') }}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>

    <div class="form-group">
        <label class="form-control-label">{{ __('Email') }}</label><span class="text-danger"> *</span>
        <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" placeholder="Enter valid email" value="{{ (!empty($data->email)) ? $data->email : old('email') }}">
        @if ($errors->has('email'))
            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
        @endif
    </div>

    <div class="form-group {{ $errors->has('company') ? 'is-invalid' : '' }}">
        <label class="form-control-label">{{ __('Company') }}</label><span class="text-danger"> *</span>
        <select class="form-control" name="company">
            {!! (!empty($companies->id)) ? '<option value="'.$companies->id.'" selected>'.$companies->name.'</option>' : '<option value="">-- Choose company --</option>' !!}
        </select>
        @if ($errors->has('company'))
            <div class="invalid-feedback">{{ $errors->first('company') }}</div>
        @endif
    </div>

    <div class="form-group {{ $errors->has('role') ? 'is-invalid' : '' }}">
        <label class="form-control-label">{{ __('Role') }}</label><span class="text-danger"> *</span>
        <select class="form-control select2 {{ $errors->has('role') ? 'is-invalid' : '' }}" name="role">
            <option value="">Choose role</option>
            @foreach ($roles as $role)
                <option value="{{ $role->name }}" {{ (old('role') == $role->name) ? 'selected' : '' }} {{ ($data->hasRole($role->name)) ? ' selected' : '' }}>{{ $role->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('role'))
            <div class="invalid-feedback">{{ $errors->first('role') }}</div>
        @endif
    </div>

    <div class="form-group">
        <label class="form-control-label">{{ __('Password') }}</label><span class="text-danger"> *</span>
        <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" placeholder="8 characters minimum">
        @if ($errors->has('password'))
            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
        @endif
    </div>

    <div class="form-group">
        <label class="form-control-label">{{ __('Password Confirmation') }}</label><span class="text-danger"> *</span>
        <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="confirm-password" placeholder="Enter password again">
        @if ($errors->has('password'))
            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
        @endif
    </div>

    <div class="form-group">
        <label class="form-control-label">{{ __('Active') }}</label>
        <span class="switch switch-icon">
            <label>
                <input type="checkbox" name="status" {{ (!empty($data)) ? ($data->status) ? ' checked' : '' : ' checked' }}/>
                <span></span>
            </label>
        </span>
        @if ($errors->has('status'))
            <div class="invalid-feedback">{{ $errors->first('status') }}</div>
        @endif       
    </div>
</div>

<div class="card-footer">
    <button type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
    <a href="{{ route('pages::users.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
</div>

{{-- Scripts Section --}}
@section('scripts')
    {{-- AJAX company selectbox (parent child) script --}}
    <script>
        $(document).ready(function() {
            $("select[name=company]").select2({
                "ajax" : {
                    "delay": 250,
                    "url" : "{{ route('pages::companies.selectBox') }}",
                    "type" : "POST",
                    "dataType" : "json",
                    "data": function (params) {
                        // set params
                        var query = {
                            search: params.term,
                            type: 'public',
                            _token: '{{ csrf_token() }}',
                            page: params.page || 1
                        }
                        return query;
                    },
                    "processResults": function(data, params) {
                        params.page = params.page || 1;
                        
                        return {
                            results: data.results,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                }
            });
        })
    </script>
@endsection