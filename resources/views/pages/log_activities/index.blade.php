{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">{{ $page_title }}
            <span class="d-block text-muted pt-2 font-size-sm">{{ $page_description }}</span></h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="{{ route('pages::log_activities.export') }}" class="btn btn-primary font-weight-bolder">
            <span class="svg-icon svg-icon-md">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <circle fill="#000000" cx="9" cy="15" r="6" />
                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>{{ __('Export to Excel') }}</a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <h5>Filter</h5>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Activity Name') }}</label>
                    <input type="text" class="form-control" id="activity" placeholder="Enter activity name">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-control-label">{{ __('Performed By') }}</label>
                    <select id="user" class="form-control">
                        <option value=""></option>
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-control-label">{{ __('From Date') }}</label>
                    <div class="input-group">
                        <input type="text" class="form-control datepicker" id="fromDate" placeholder="Enter from date">
                        <div class="input-group-append"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-control-label">{{ __('To Date') }}</label>
                    <div class="input-group">
                        <input type="text" class="form-control datepicker" id="toDate" placeholder="Enter to date">
                        <div class="input-group-append"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-light-primary" id="btnFilter">Apply</button>
        <button class="btn btn-light-warning" id="btnReset">Reset</button>
    </div>
    <hr class="my-0">
    <div class="card-body">
        <!--begin: Datatable-->
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tableData">
                <thead>
                    <tr>
                        <th>{{ __('No') }}</th>
                        <th>{{ __('Activity Name') }}</th>
                        <th>{{ __('Performed By') }}</th>
                        <th>{{ __('Date and Time') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!--end: Datatable-->
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $(document).ready(function() {
            fill_datatable();
        });

        // init datatables
        function fill_datatable(activity = '', user = '', fromDate = '', toDate = '') {
            var table = $('#tableData').DataTable({
                proccesing: true,
                serverSide: true,
                searching: false,
                ajax: {
                    url: "/pages/log_activities",
                    data: {
                        activity: activity,
                        user: user,
                        fromDate: fromDate,
                        toDate: toDate
                    }
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        width: '5%'
                    },
                    {
                        data: 'description',
                        name: 'description',
                        width: '40%'
                    },
                    {
                        data: 'user_name',
                        name: 'user_name',
                        width: '20%'
                    },
                    {
                        data: 'date',
                        name: 'date',
                        width: '20%'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        width: '15%'
                    }
                ],
                language: {
                    searchPlaceholder: "search...",
                    sSearch: ""
                }
            });
        }

        // filter datatables
        $('#btnFilter').click(function() {
            var activity = $('#activity').val();
            var user = $('#user').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();

            $('#tableData').DataTable().destroy();
            fill_datatable(activity, user, fromDate, toDate);
        });

        // reset datatables
        $('#btnReset').click(function() {
            $('#activity').val('').trigger('change');
            $('#user').val('').trigger('change');
            $('#fromDate').val('').trigger('change');
            $('#toDate').val('').trigger('change');

            $('#tableData').DataTable().destroy();
            fill_datatable();
        });

        // go to detail page
        function detailData(id) {
            window.location = 'log_activities/detail/'+id;
        }
    </script>

    {{-- Init datepicker script --}}
    <script>
        $('.datepicker').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $('#user').select2({
            allowClear: true,
            placeholder: "Select user"
        });
    </script>
@endsection