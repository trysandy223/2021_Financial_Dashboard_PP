{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">{{ $page_title }}
            <span class="d-block text-muted pt-2 font-size-sm">{{ $page_description }}</span></h3>
        </div>
        <div class="card-toolbar">
            
        </div>
    </div>
    <div class="card-body">
        <p>Activity Name: {{ $data->description }}</p>
        <p>Date: {{ date('d F Y, H:i:s', strtotime($data->created_at)) }}</p>
        <p>Performed By: {{ $data->causer->name }}</p>
        <p>Detail Data:</p>
        <pre id="detailData" class="d-none">{{ $data->subject }}</pre>
        <pre id="detailDataConvert"></pre>
    </div>
    <div class="card-footer">
        <a href="{{ route('pages::log_activities.index') }}" class="btn btn-secondary float-right">{{ __('Back') }}</a>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var data = document.getElementById("detailData").innerHTML;
        var dataParse = JSON.parse(data);
        var detailDataConvert = document.getElementById("detailDataConvert");
        detailDataConvert.innerHTML = JSON.stringify(dataParse, null, 4);
    </script>
@endsection