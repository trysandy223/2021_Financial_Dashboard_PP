<div class="card-body">
    <div class="form-group">
        <label class="form-control-label">{{ __('Name') }}</label><span class="text-danger"> *</span>
        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="Enter role name" value="{{ (!empty($data->name)) ? $data->name : old('name') }}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>

    <div class="form-group validated">
        <label>{{ __('Permissions') }}</label><span class="text-danger font-weight-bold"> *</span>
        <div class="checkbox-list">
            <label class="checkbox">
                <input type="checkbox" value="" class="selectall"/>
                <span></span>{{ __('Select All') }}
            </label>
        </div>
        <div class="mt-2 container-checbox">
            <div class="checkbox-list">
                <div class="row">
                    @foreach($permissions as $permission)
                        <div class="col-sm-3 mb-4">
                            <label class="checkbox">
                                <input type="checkbox" name="permission[]" value="{{ $permission->id }}"
                                    {{ (!empty($rolePermission)) ? in_array($permission->id, $rolePermission) ? ' checked' : '' : ''}}
                                    class="individual" />
                                <span></span>{{ $permission->name }}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @if ($errors->has('permission'))
            <div class="invalid-feedback">{{ $errors->first('permission') }}</div>
        @endif
    </div>
</div>

<div class="card-footer">
    <button type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
    <a href="{{ route('pages::roles.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
</div>

@section('styles')
<style>
    .container-checbox {
        padding: 10px;
        border: 1px solid #ccc;
        height: 250px;
        overflow-y: scroll;
    }
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    // select all permission items
    $(".selectall").click(function () {
        $(".individual").prop("checked", $(this).prop("checked"));
    });
</script>
@endsection