{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pages::customers.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pages.customers.form')
    </form>
    <!--end::Form-->
@endsection