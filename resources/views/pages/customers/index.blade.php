{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">{{ $page_title }}
            <span class="d-block text-muted pt-2 font-size-sm">{{ $page_description }}</span></h3>
        </div>
        <div class="card-toolbar">
            @can('master-create')
            <!--begin::Button-->
            <a href="{{ route('pages::customers.create') }}" class="btn btn-primary font-weight-bolder">
            <span class="svg-icon svg-icon-md">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <circle fill="#000000" cx="9" cy="15" r="6" />
                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>{{ __('New Record') }}</a>
            <!--end::Button-->
            @endcan
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tableData">
                <thead>
                    <tr>
                        <th>{{ __('No') }}</th>
                        <th>{{ __('Code') }}</th>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('Title') }}</th>
                        <th>{{ __('Account Group') }}</th>
                        <th>{{ __('Street') }}</th>
                        <th>{{ __('District') }}</th>
                        <th>{{ __('City') }}</th>
                        <th>{{ __('Region') }}</th>
                        <th>{{ __('Country') }}</th>
                        <th>{{ __('Postal Code') }}</th>
                        <th>{{ __('Phone') }}</th>
                        <th>{{ __('Mobile Phone') }}</th>
                        <th>{{ __('Fax') }}</th>
                        <th>{{ __('Email') }}</th>
                        <th>{{ __('NPWP') }}</th>
                        <th>{{ __('Credit Limit') }}</th>
                        <th>{{ __('Reconsiliation Acc') }}</th>
                        <th>{{ __('Planning Group') }}</th>
                        <th>{{ __('TOP') }}</th>
                        <th>{{ __('House Bank') }}</th>
                        <th>{{ __('Currency') }}</th>
                        <th>{{ __('Tax Class') }}</th>
                        <th>{{ __('Last Modified') }}</th>
                        <th>{{ __('Search Term') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!--end: Datatable-->
    </div>
</div>
@endsection

@section('styles')
    <style>
        table.dataTable th:nth-child(1) {
            min-width: 30px;
        }

        table.dataTable th {
            min-width: 120px;
        }
    </style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        // Init datatables
        $(document).ready(function() {
            var table = $('#tableData').DataTable({
                scrollX: true,
                scrollCollapse: true,
                proccesing: true,
                serverSide: true,
                ajax: {
                    url: "/pages/customers"
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'code',
                        name: 'code'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'title',
                        name: 'title'
                    },
                    {
                        data: 'account_group_name',
                        name: 'account_group_name'
                    },
                    {
                        data: 'street',
                        name: 'street'
                    },
                    {
                        data: 'district',
                        name: 'district'
                    },
                    {
                        data: 'city',
                        name: 'city'
                    },
                    {
                        data: 'region',
                        name: 'region'
                    },
                    {
                        data: 'country',
                        name: 'country'
                    },
                    {
                        data: 'postal_code',
                        name: 'postal_code'
                    },
                    {
                        data: 'phone',
                        name: 'phone'
                    },
                    {
                        data: 'mobile_phone',
                        name: 'mobile_phone'
                    },
                    {
                        data: 'fax',
                        name: 'fax'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'npwp',
                        name: 'npwp'
                    },
                    {
                        data: 'credit_limit',
                        name: 'credit_limit'
                    },
                    {
                        data: 'reconsiliation_account_name',
                        name: 'reconsiliation_account_name'
                    },
                    {
                        data: 'planning_group_name',
                        name: 'planning_group_name'
                    },
                    {
                        data: 'terms_of_payment_name',
                        name: 'terms_of_payment_name'
                    },
                    {
                        data: 'house_bank_name',
                        name: 'house_bank_name'
                    },
                    {
                        data: 'currency_name',
                        name: 'currency_name'
                    },
                    {
                        data: 'tax_classification_name',
                        name: 'tax_classification_name'
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    },
                    {
                        data: 'search_term',
                        name: 'search_term'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                language: {
                    searchPlaceholder: "search...",
                    sSearch: ""
                }
            });
        });
        
        // delete function
        function deleteData(id) {
            Swal.fire({
                title: "Are you sure?",
                text: "You wont be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "customers/"+id+"/delete",
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        error: function() {
                            $('#tableData').DataTable().ajax.reload();
                            Swal.fire("Failed!", "Something wrong. Failed to delete data.", "error")
                        },
                        success: function(data) {
                            $('#tableData').DataTable().ajax.reload();
                            Swal.fire("Deleted!", "Your data has been deleted.", "success")
                        }
                    });
                } else {
                    Swal.fire("Cancelled", "Okay, your data is safe", "error")
                }
            });
        }

        // go to edit page
        function editData(id) {
            window.location = 'customers/'+id+'/edit';
        }
    </script>

    {{-- success message alert --}}
    @if (session('success'))
        <script>
            Swal.fire("Success!", '{{ session('success') }}', "success");
        </script>
    @endif

    {{-- failed message alert --}}
    @if (session('failed'))
        <script>
            Swal.fire("Failed!", '{{ session('failed') }}', "error");
        </script>
    @endif
@endsection