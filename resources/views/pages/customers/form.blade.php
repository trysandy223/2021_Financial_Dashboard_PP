<div class="card card-custom mb-5">
    <div class="card-header">
        <h3 class="card-title">
            General Data
        </h3>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('account_group_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Account Group') }}</label>
                    <select class="form-control select2  {{ $errors->has('account_group_id') ? 'is-invalid' : '' }}" name="account_group_id" style="width: 100%">
                        <option value="">Choose account group</option>
                        @foreach ($acc_groups as $acc_group)
                            <option value="{{ $acc_group->id }},{{ $acc_group->name }}" {{ (!empty($data->account_group_id)) ? ($data->account_group_id == $acc_group->id) ? ' selected' : '' : '' }} {{ old("account_group_id") == $acc_group->id . ',' . $acc_group->name ? ' selected' : '' }}> {{ $acc_group->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('account_group_id'))
                        <div class="invalid-feedback">{{ $errors->first('account_group_id') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-6">
                <label class="form-control-label">{{ __('Title') }}</label>
                <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" name="title" placeholder="Enter title" value="{{ (!empty($data->title)) ? $data->title : old('title') }}">
                @if ($errors->has('title'))
                    <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Code') }}</label><span class="text-danger"> *</span>
                <input type="text" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" name="code" placeholder="Enter code" value="{{ (!empty($data->code)) ? $data->code : old('code') }}">
                @if ($errors->has('code'))
                    <div class="invalid-feedback">{{ $errors->first('code') }}</div>
                @endif
            </div>
            <div class="col-lg-8">
                <label class="form-control-label">{{ __('Name') }}</label><span class="text-danger"> *</span>
                <input type="text" class="form-control typeahead {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="Enter name" value="{{ (!empty($data->name)) ? $data->name : old('name') }}" autocomplete="off">  
                @if ($errors->has('name'))
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-12">
                <label class="form-control-label">{{ __('Street') }}</label>
                <input type="text" class="form-control {{ $errors->has('street') ? 'is-invalid' : '' }}" name="street" placeholder="Enter street" value="{{ (!empty($data->street)) ? $data->street : old('street') }}">
                @if ($errors->has('street'))
                    <div class="invalid-feedback">{{ $errors->first('street') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('District') }}</label>
                <input type="text" class="form-control {{ $errors->has('district') ? 'is-invalid' : '' }}" name="district" placeholder="Enter district" value="{{ (!empty($data->district)) ? $data->district : old('district') }}">
                @if ($errors->has('district'))
                    <div class="invalid-feedback">{{ $errors->first('district') }}</div>
                @endif
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('City') }}</label>
                <input type="text" class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}" name="city" placeholder="Enter city" value="{{ (!empty($data->city)) ? $data->city : old('city') }}">
                @if ($errors->has('city'))
                    <div class="invalid-feedback">{{ $errors->first('city') }}</div>
                @endif
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Region') }}</label>
                <input type="text" class="form-control {{ $errors->has('region') ? 'is-invalid' : '' }}" name="region" placeholder="Enter region" value="{{ (!empty($data->region)) ? $data->region : old('region') }}">
                @if ($errors->has('region'))
                    <div class="invalid-feedback">{{ $errors->first('region') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Country') }}</label>
                <input type="text" class="form-control {{ $errors->has('country') ? 'is-invalid' : '' }}" name="country" placeholder="Enter country" value="{{ (!empty($data->country)) ? $data->country : old('country') }}">
                @if ($errors->has('country'))
                    <div class="invalid-feedback">{{ $errors->first('country') }}</div>
                @endif
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Postal Code') }}</label>
                <input type="text" class="form-control {{ $errors->has('postal_code') ? 'is-invalid' : '' }}" name="postal_code" placeholder="Enter postal code" value="{{ (!empty($data->postal_code)) ? $data->postal_code : old('postal_code') }}">
                @if ($errors->has('postal_code'))
                    <div class="invalid-feedback">{{ $errors->first('postal_code') }}</div>
                @endif
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Phone') }}</label>
                <input type="text" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" name="phone" placeholder="Enter phone" value="{{ (!empty($data->phone)) ? $data->phone : old('phone') }}">
                @if ($errors->has('phone'))
                    <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Mobile Phone') }}</label>
                <input type="text" class="form-control {{ $errors->has('mobile_phone') ? 'is-invalid' : '' }}" name="mobile_phone" placeholder="Enter mobile phone" value="{{ (!empty($data->mobile_phone)) ? $data->mobile_phone : old('mobile_phone') }}">
                @if ($errors->has('mobile_phone'))
                    <div class="invalid-feedback">{{ $errors->first('mobile_phone') }}</div>
                @endif
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Ext 1') }}</label>
                <input type="text" class="form-control {{ $errors->has('extension_1') ? 'is-invalid' : '' }}" name="extension_1" placeholder="Enter extension 1" value="{{ (!empty($data->extension_1)) ? $data->extension_1 : old('extension_1') }}">
                @if ($errors->has('extension_1'))
                    <div class="invalid-feedback">{{ $errors->first('extension_1') }}</div>
                @endif
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Ext 2') }}</label>
                <input type="text" class="form-control {{ $errors->has('extension_2') ? 'is-invalid' : '' }}" name="extension_2" placeholder="Enter extension 2" value="{{ (!empty($data->extension_2)) ? $data->extension_2 : old('extension_2') }}">
                @if ($errors->has('extension_2'))
                    <div class="invalid-feedback">{{ $errors->first('extension_2') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Fax') }}</label>
                <input type="text" class="form-control {{ $errors->has('fax') ? 'is-invalid' : '' }}" name="fax" placeholder="Enter fax" value="{{ (!empty($data->fax)) ? $data->fax : old('fax') }}">
                @if ($errors->has('fax'))
                    <div class="invalid-feedback">{{ $errors->first('fax') }}</div>
                @endif
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('Email') }}</label>
                <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" placeholder="Enter email" value="{{ (!empty($data->email)) ? $data->email : old('email') }}">
                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="col-lg-4">
                <label class="form-control-label">{{ __('NPWP Number') }}</label>
                <input type="text" class="form-control {{ $errors->has('npwp') ? 'is-invalid' : '' }}" name="npwp" placeholder="Enter npwp" value="{{ (!empty($data->npwp)) ? $data->npwp : old('npwp') }}">
                @if ($errors->has('npwp'))
                    <div class="invalid-feedback">{{ $errors->first('npwp') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-12">
                <label class="form-control-label">{{ __('Search Term') }}</label>
                <input type="text" class="form-control {{ $errors->has('search_term') ? 'is-invalid' : '' }}" name="search_term" placeholder="Separate with comma" value="{{ (!empty($data->search_term)) ? $data->search_term : old('search_term') }}">
                @if ($errors->has('search_term'))
                    <div class="invalid-feedback">{{ $errors->first('search_term') }}</div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="card card-custom mb-5">
    <div class="card-header">
        <h3 class="card-title">
            Company Code
        </h3>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('reconsiliation_account_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Reconciliation Account') }}</label>
                    <select class="form-control select2  {{ $errors->has('reconsiliation_account_id') ? 'is-invalid' : '' }}" name="reconsiliation_account_id" style="width: 100%">
                        <option value="">Choose rec account</option>
                        @foreach ($rec_accounts as $rec_account)
                            <option value="{{ $rec_account->id }},{{ $rec_account->name }}" {{ (!empty($data->reconsiliation_account_id)) ? ($data->reconsiliation_account_id == $rec_account->id) ? ' selected' : '' : '' }} {{ old("reconsiliation_account_id") == $rec_account->id . ',' . $rec_account->name ? ' selected' : '' }}> {{ $rec_account->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('reconsiliation_account_id'))
                        <div class="invalid-feedback">{{ $errors->first('reconsiliation_account_id') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('planning_group_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Planning Group') }}</label>
                    <select class="form-control select2  {{ $errors->has('planning_group_id') ? 'is-invalid' : '' }}" name="planning_group_id" style="width: 100%">
                        <option value="">Choose planning group</option>
                        @foreach ($plan_groups as $plan_group)
                            <option value="{{ $plan_group->id }},{{ $plan_group->name }}" {{ (!empty($data->planning_group_id)) ? ($data->planning_group_id == $plan_group->id) ? ' selected' : '' : '' }} {{ old("planning_group_id") == $plan_group->id . ',' . $plan_group->name ? ' selected' : '' }}> {{ $plan_group->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('planning_group_id'))
                        <div class="invalid-feedback">{{ $errors->first('planning_group_id') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('terms_of_payment_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Terms of Payment') }}</label>
                    <select class="form-control select2  {{ $errors->has('terms_of_payment_id') ? 'is-invalid' : '' }}" name="terms_of_payment_id" style="width: 100%">
                        <option value="">Choose terms of payment</option>
                        @foreach ($terms_payments as $terms_payment)
                            <option value="{{ $terms_payment->id }},{{ $terms_payment->name }}" {{ (!empty($data->terms_of_payment_id)) ? ($data->terms_of_payment_id == $terms_payment->id) ? ' selected' : '' : '' }} {{ old("terms_of_payment_id") == $terms_payment->id . ',' . $terms_payment->name ? ' selected' : '' }}> {{ $terms_payment->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('terms_of_payment_id'))
                        <div class="invalid-feedback">{{ $errors->first('terms_of_payment_id') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group {{ $errors->has('house_bank_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('House Bank') }}</label>
                    <select class="form-control select2  {{ $errors->has('house_bank_id') ? 'is-invalid' : '' }}" name="house_bank_id" style="width: 100%">
                        <option value="">Choose house bank</option>
                        @foreach ($house_banks as $house_bank)
                            <option value="{{ $house_bank->id }},{{ $house_bank->name }}" {{ (!empty($data->house_bank_id)) ? ($data->house_bank_id == $house_bank->id) ? ' selected' : '' : '' }} {{ old("house_bank_id") == $house_bank->id . ',' . $house_bank->name ? ' selected' : '' }}> {{ $house_bank->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('house_bank_id'))
                        <div class="invalid-feedback">{{ $errors->first('house_bank_id') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card card-custom mb-5">
    <div class="card-header">
        <h3 class="card-title">
            Sales Area
        </h3>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('currency_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Currency') }}</label>
                    <select class="form-control select2  {{ $errors->has('currency_id') ? 'is-invalid' : '' }}" name="currency_id" style="width: 100%">
                        <option value="">Choose currency</option>
                        @foreach ($currencies as $currency)
                            <option value="{{ $currency->id }},{{ $currency->name }}" {{ (!empty($data->currency_id)) ? ($data->currency_id == $currency->id) ? ' selected' : '' : '' }} {{ old("currency_id") == $currency->id . ',' . $currency->name ? ' selected' : '' }}> {{ $currency->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('currency_id'))
                        <div class="invalid-feedback">{{ $errors->first('currency_id') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('tax_classification_id') ? 'is-invalid' : '' }}">
                    <label class="form-control-label">{{ __('Tax Classification') }}</label>
                    <select class="form-control select2  {{ $errors->has('tax_classification_id') ? 'is-invalid' : '' }}" name="tax_classification_id" style="width: 100%">
                        <option value="">Choose tax classification</option>
                        @foreach ($tax_classes as $tax_class)
                            <option value="{{ $tax_class->id }},{{ $tax_class->name }}" {{ (!empty($data->tax_classification_id)) ? ($data->tax_classification_id == $tax_class->id) ? ' selected' : '' : '' }} {{ old("tax_classification_id") == $tax_class->id . ',' . $tax_class->name ? ' selected' : '' }}> {{ $tax_class->name }} </option>
                        @endforeach
                    </select>
                    @if ($errors->has('tax_classification_id'))
                        <div class="invalid-feedback">{{ $errors->first('tax_classification_id') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card card-custom mb-5">
    <div class="card-header">
        <h3 class="card-title">
            Segment Data
        </h3>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-lg-12">
                <label class="form-control-label">{{ __('Credit Limit') }}</label>
                <div class="input-group">
                    <div class="input-group-append"><span class="input-group-text">Rp</span></div>
                    <input type="number" class="form-control {{ $errors->has('credit_limit') ? 'is-invalid' : '' }}" name="credit_limit" placeholder="Enter credit limit" value="{{ (!empty($data->credit_limit)) ? $data->credit_limit : old('credit_limit') }}">
                </div>
                @if ($errors->has('credit_limit'))
                    <div class="invalid-feedback">{{ $errors->first('credit_limit') }}</div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="card card-custom text-right">
    <div class="card-footer">
        <a href="{{ route('pages::customers.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
        <button type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
    </div>
</div>

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js" ></script> 
    <script>
        var path = "{{ route('pages::customers.list') }}";

        $('input.typeahead').typeahead({
            source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
    </script>
@endsection