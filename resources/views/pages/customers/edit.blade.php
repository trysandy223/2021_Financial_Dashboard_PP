{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Form-->
    <form action="{{ route('pages::customers.update', ['id' => $data->id]) }}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pages.customers.form')
    </form>
    <!--end::Form-->
@endsection