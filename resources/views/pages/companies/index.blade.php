{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">{{ $page_title }}
            <span class="d-block text-muted pt-2 font-size-sm">{{ $page_description }}</span></h3>
        </div>
        <div class="card-toolbar">
            @can('master-create')
            <!--begin::Button-->
            <a href="{{ route('pages::companies.create') }}" class="btn btn-primary font-weight-bolder">
            <span class="svg-icon svg-icon-md">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <circle fill="#000000" cx="9" cy="15" r="6" />
                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>{{ __('New Record') }}</a>
            <!--end::Button-->
            @endcan
        </div>
    </div>
    <div class="card-body">
        <div id="tree_unit" class="tree-demo"></div>
    </div>

    <!-- begin:modal-detail -->
    <div class="modal fade" id="detailData" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Company Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mb-15">
                        <h3 class="font-weight-bolder text-dark mb-7" style="font-size: 25px;" id="d_name">-</h3>
                        <div class="line-height-xl" id="d_description">-</div>
                    </div>
                    <div class="row mb-6">
                        <div class="col-6 col-md-4">
                            <div class="mb-8 d-flex flex-column">
                                <span class="text-dark font-weight-bold mb-4">Parent Company</span>
                                <span class="text-muted font-weight-bolder font-size-lg" id="d_parent">-/span>
                            </div>
                        </div>
                        <div class="col-6 col-md-4">
                            <div class="mb-8 d-flex flex-column">
                                <span class="text-dark font-weight-bold mb-4">Cocode</span>
                                <span class="text-muted font-weight-bolder font-size-lg" id="d_cocode">-</span>
                            </div>
                        </div>
                        <div class="col-6 col-md-4">
                            <div class="mb-8 d-flex flex-column">
                                <span class="text-dark font-weight-bold mb-4">Type</span>
                                <span class="text-muted font-weight-bolder font-size-lg" id="d_type">-</span>
                            </div>
                        </div>
                        <div class="col-6 col-md-4">
                            <div class="mb-8 d-flex flex-column">
                                <span class="text-dark font-weight-bold mb-4">Portion</span>
                                <span class="text-muted font-weight-bolder font-size-lg" id="d_portion">-</span>
                            </div>
                        </div>
                        <div class="col-6 col-md-4">
                            <div class="mb-8 d-flex flex-column">
                                <span class="text-dark font-weight-bold mb-4">Project</span>
                                <span class="text-muted font-weight-bolder font-size-lg" id="d_project">-</span>
                            </div>
                        </div>
                        <div class="col-6 col-md-4">
                            <div class="mb-8 d-flex flex-column">
                                <span class="text-dark font-weight-bold mb-4">Status</span>
                                <span class="text-muted font-weight-bolder font-size-lg" id="d_status">-</span>
                            </div>
                        </div>
                        <div class="col-6 col-md-4">
                            <div class="mb-8 d-flex flex-column">
                                <span class="text-dark font-weight-bold mb-4">Update</span>
                                <span class="text-muted font-weight-bolder font-size-lg" id="d_update">-</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end:modal-detail -->
</div>
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/jstree/jstree.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('plugins/custom/jstree/jstree.bundle.js') }}"></script>

    <script>
        // Init datatables
        jQuery(function($) {
            $('#tree_unit').jstree({
                "plugins": ["themes", "html_data", "ui", "crrm", "types"],
                "check_callback": true,
                "core": {
                    "themes": {
                        "responsive": false
                    },
                    "data": {
                        type: "POST",
                        url: "{{ route('pages::companies.getTree') }}",
                        data: function(node) {
                            return {
                                'parent': node.id,
                                '_token': "{{ csrf_token() }}"
                            };
                        },
                        dataType: "json",
                        success: function (data) {
                            return data;
                        }
                    },
                },
                "types" : {
                    "default" : {
                        "icon" : "fa fa-folder text-warning"
                    },
                    "file" : {
                        "icon" : "fa fa-file  text-warning"
                    }
                },
            });
        });

        // delete function
        function deleteData(id) {
            Swal.fire({
                title: "Are you sure?",
                text: "You wont be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "companies/"+id+"/delete",
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        error: function() {
                            $('#tableData').DataTable().ajax.reload();
                            Swal.fire("Failed!", "Something wrong, Data cannot be deleted.", "error")
                        },
                        success: function(data) {
                            $('#tableData').DataTable().ajax.reload();
                            Swal.fire("Deleted!", "Your data has been deleted.", "success")
                        }
                    });
                } else {
                    Swal.fire("Cancelled", "Okay, your data is safe", "error")
                }
            });
        }

        // go to edit page
        function editData(id) {
            window.location = 'companies/'+id+'/edit';
        }

        // go to modal detail
        function detailData(id) {
            $.ajax({
				type: "GET",
				url: "companies/"+id+"/detail",
				dataType: "json",
				data: {
                    '_token': "{{ csrf_token() }}"
                },
				success: function(data){
                    $(`#d_name`).text(data.name);
                    $(`#d_description`).text(data.description);
                    $(`#d_parent`).text(data.name_parent);
                    $(`#d_cocode`).text(data.code);
                    $(`#d_type`).text(data.type);
                    $(`#d_portion`).text(data.portion+" %");
                    $(`#d_project`).html(data.is_project);
                    $(`#d_status`).html(data.status);
                    $(`#d_update`).text(data.updated_at);
					$('#detailData').modal('show');

				},
				error: function(xhr, ajaxOptions, thrownError){
					alert(xhr.status);
                    alert(thrownError);
				}
			});
        }
    </script>

    {{-- success message alert --}}
    @if (session('success'))
        <script>
            Swal.fire("Success!", '{{ session('success') }}', "success");
        </script>
    @endif

    {{-- failed message alert --}}
    @if (session('failed'))
        <script>
            Swal.fire("Failed!", '{{ session('failed') }}', "error");
        </script>
    @endif
@endsection
