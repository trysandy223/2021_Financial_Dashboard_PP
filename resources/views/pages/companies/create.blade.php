{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            {{ $page_title }}
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('pages::companies.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="form-control-label">{{ __('Parent Company') }}</label>
                    <select class="form-control" name="parent_id">
                        <option value="">-- Choose parent --</option>
                    </select>
                    @if ($errors->has('parent_id'))
                        <div class="invalid-feedback">{{ $errors->first('parent_id') }}</div>
                    @endif
                </div>
            </div>
        
            <div class="form-group row">
                <div class="col-lg-4">
                    <label class="form-control-label">{{ __('Cocode') }}</label><span class="text-danger"> *</span>
                    <input type="text" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" name="code" placeholder="Enter code" value="{{ (!empty($data->code)) ? $data->code : old('code') }}">
                    @if ($errors->has('code'))
                        <div class="invalid-feedback">{{ $errors->first('code') }}</div>
                    @endif
                </div>
                <div class="col-lg-8">
                    <label class="form-control-label">{{ __('Name') }}</label><span class="text-danger"> *</span>
                    <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="Enter name" value="{{ (!empty($data->name)) ? $data->name : old('name') }}">
                    @if ($errors->has('name'))
                        <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                    @endif
                </div>
            </div>
        
            <div class="form-group">
                <label class="form-control-label">{{ __('Description') }}</label>
                <textarea rows="5" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" placeholder="Enter description">{{ (!empty($data->description)) ? $data->description : old('description') }}</textarea>
                @if ($errors->has('description'))
                    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                @endif
            </div>
        
            <div class="form-group row">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('type') ? 'is-invalid' : '' }}">
                        <label class="form-control-label">{{ __('Type') }}</label><span class="text-danger"> *</span>
                        <select class="form-control select2  {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" style="width: 100%">
                            <option value="">Choose type</option>
                            @foreach(config('const.company_type') as $name => $val)
                                <option value="{{ $name }}" {{ (old('type') == $name) ? 'selected' : '' }} {{ (!empty($data->type)) ? ($data->type == $name) ? 'selected' : '' : '' }}> {{ $val }} </option>
                            @endforeach
                        </select>
                        @if ($errors->has('type'))
                            <div class="invalid-feedback">{{ $errors->first('type') }}</div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="form-control-label">{{ __('Portion') }}</label><span class="text-danger"> *</span>
                    <div class="input-group">
                        <input type="number" class="form-control {{ $errors->has('portion') ? 'is-invalid' : '' }}" name="portion" placeholder="Enter portion" value="{{ (!empty($data->portion)) ? $data->portion : old('portion') }}">
                        <div class="input-group-append"><span class="input-group-text">%</span></div>
                    </div>
                    @if ($errors->has('portion'))
                        <div class="invalid-feedback">{{ $errors->first('portion') }}</div>
                    @endif
                </div>
            </div>
        
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="form-control-label">{{ __('Is Project?') }}</label>
                    <span class="switch switch-icon">
                        <label>
                            <input type="checkbox" name="is_project" {{ (!empty($data)) ? ($data->is_project) ? ' checked' : '' : ' checked' }}/>
                            <span></span>
                        </label>
                    </span>
                    @if ($errors->has('is_project'))
                        <div class="invalid-feedback">{{ $errors->first('is_project') }}</div>
                    @endif
                </div>
            </div>
        
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="form-control-label">{{ __('Active') }}</label>
                    <span class="switch switch-icon">
                        <label>
                            <input type="checkbox" name="status" {{ (!empty($data)) ? ($data->status) ? ' checked' : '' : ' checked' }}/>
                            <span></span>
                        </label>
                    </span>
                    @if ($errors->has('status'))
                        <div class="invalid-feedback">{{ $errors->first('status') }}</div>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="card-footer">
            <button type="submit" class="btn btn-primary mr-2">{{ __('Submit') }}</button>
            <a href="{{ route('pages::companies.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
        </div>
    </form>
    <!--end::Form-->
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    {{-- AJAX company selectbox (parent child) script --}}
    <script>
        $(document).ready(function() {
            $("select[name=parent_id]").select2({
                "ajax" : {
                    "delay": 250,
                    "url" : "{{ route('pages::companies.selectBox') }}",
                    "type" : "POST",
                    "dataType" : "json",
                    "data": function (params) {
                        // set params
                        var query = {
                            search: params.term,
                            type: 'public',
                            _token: '{{ csrf_token() }}',
                            page: params.page || 1
                        }
                        return query;
                    },
                    "processResults": function(data, params) {
                        params.page = params.page || 1;
                        
                        return {
                            results: data.results,
                            pagination: {
                                more: (params.page * 10) < data.total_count
                            }
                        };
                    },
                    cache: true
                }
            });
        })
    </script>
@endsection