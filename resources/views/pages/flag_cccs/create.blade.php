{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            {{ $page_title }}
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('pages::flag_cccs.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @include('pages.flag_cccs.form')
    </form>
    <!--end::Form-->
</div>

@endsection