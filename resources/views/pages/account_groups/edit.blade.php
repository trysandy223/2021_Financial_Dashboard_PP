{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            {{ $page_title }}
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('pages::account_groups.update', ['id' => $data->id]) }}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        @include('pages.account_groups.form')
    </form>
    <!--end::Form-->
</div>

@endsection