<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use App\Models\FinancialRevolvingDrawdown;
use DB;

class RevolvingDrawdownSelectBox {

    public function data($search, $filter, $start, $length, $textFormat = null)
    {
        try {
            $data = FinancialRevolvingDrawdown::select('id', 'credit_line_detail');
    
            if(!empty($search)){
                $data->where('credit_line_detail', 'like', '%'.$search.'%');
            }

            // set filter
            if(!empty($filter)){
                foreach ($filter as $key => $val) {
                    $data->where($val['key'], $val['condition'], $val['value']);
                }
            }

            $data->skip($start)->take($length);
            $dataParents = $data->get();

            $result = array();
            if(!empty($dataParents)) {
                foreach ($dataParents as $key => $val) {
                    $result[$key]['text'] = $val->credit_line_detail;
                    $result[$key]['id'] = $val->id;
                }
            }

            return $result;
        } catch (\Exception $ex) {
            $result = array();
            $result['error'] = $ex;
            return $result;
        }
    }

    public function totalResult($search, $filter = array()) {
        $data = new FinancialRevolvingDrawdown;
        if(!empty($search)){
            $data->where('credit_line_detail', 'like', '%'.$search.'%');
        }
        // set filter
        if(!empty($filter)){
            foreach ($filter as $key => $val) {
                $data->where($val['key'], $val['condition'], $val['value']);
            }
        }
        $totalFiltered = $data->count();

        return $totalFiltered;
    }
}

?>