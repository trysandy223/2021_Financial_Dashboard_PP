<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use App\Models\Customer;
use DB;

class CustomerSelectBox {

    public function data($search, $filter, $start, $length, $textFormat = null)
    {
        try {
            $data = Customer::select('id', 'code', 'name');
    
            if(!empty($search)){
                $data->where('name', 'like', '%'.$search.'%');
            }

            // set filter
            if(!empty($filter)){
                foreach ($filter as $key => $val) {
                    $data->where($val['key'], $val['condition'], $val['value']);
                }
            }

            $data->skip($start)->take($length);
            $dataParents = $data->get();

            $result = array();
            if(!empty($dataParents)) {
                foreach ($dataParents as $key => $val) {
                    if ($textFormat == 'code-name') {
                        $result[$key]['text'] = $val->code." - ".$val->name;
                    } else {
                        $result[$key]['text'] = $val->name;
                    }
                    $result[$key]['id'] = $val->id;
                }
            }

            return $result;
        } catch (\Exception $ex) {
            $result = array();
            $result['error'] = $ex;
            return $result;
        }
    }

    public function totalResult($search, $filter = array()) {
        $data = new Customer;
        if(!empty($search)){
            $data->where('name', 'like', '%'.$search.'%');
        }
        // set filter
        if(!empty($filter)){
            foreach ($filter as $key => $val) {
                $data->where($val['key'], $val['condition'], $val['value']);
            }
        }
        $totalFiltered = $data->count();

        return $totalFiltered;
    }
}

?>