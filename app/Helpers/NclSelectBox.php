<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use App\Models\FinancialNcl;
use DB;

class NclSelectBox {

    public function data($search, $filter, $start, $length, $textFormat = null)
    {
        try {
            $data = FinancialNcl::select('id', 'debt_name', 'bank_name');
    
            if(!empty($search)){
                $data->where('debt_name', 'like', '%'.$search.'%');
            }

            // set filter
            if(!empty($filter)){
                foreach ($filter as $key => $val) {
                    $data->where($val['key'], $val['condition'], $val['value']);
                }
            }

            $data->skip($start)->take($length);
            $dataParents = $data->get();

            $result = array();
            if(!empty($dataParents)) {
                foreach ($dataParents as $key => $val) {
                    if ($textFormat == 'bank-name') {
                        $result[$key]['text'] = $val->bank_name . '/' . $val->debt_name;
                    } else {
                        $result[$key]['text'] = $val->debt_name;
                    }
                    $result[$key]['id'] = $val->id;
                }
            }

            return $result;
        } catch (\Exception $ex) {
            $result = array();
            $result['error'] = $ex;
            return $result;
        }
    }

    public function totalResult($search, $filter = array()) {
        $data = new FinancialNcl;
        if(!empty($search)){
            $data->where('debt_name', 'like', '%'.$search.'%');
        }
        // set filter
        if(!empty($filter)){
            foreach ($filter as $key => $val) {
                $data->where($val['key'], $val['condition'], $val['value']);
            }
        }
        $totalFiltered = $data->count();

        return $totalFiltered;
    }
}

?>