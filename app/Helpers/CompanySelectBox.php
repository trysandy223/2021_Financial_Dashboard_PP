<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use App\Models\Company;
use Auth, DB;

class CompanySelectBox {

    public function data($search, $filter, $start, $length, $textFormat = null)
    {
        try {
            // old method
            // $data = DB::table('companies')->select('id', 'code', 'name', 'level');

            // new method (semua user bisa memilih company. Tapi company yang tampil hanya company-nya dan anak-anaknya saja)
            $companyId = Auth::user()->company->first()->id;

            if (in_array($companyId, config('const.parent_company')) || in_array($companyId, config('const.sub_parent_company'))) {
                $data = DB::table('companies')->select('id', 'code', 'name', 'level');
            } else {
                $getParent = DB::table('companies')->where('id', Auth::user()->company->first()->id)->select('id', 'code', 'name', 'level');
                $getChild = DB::table('companies')->where('parent_id', Auth::user()->company->first()->id)->select('id', 'code', 'name', 'level')->union($getParent);
                $data = $getChild;
            }
    
            if(!empty($search)){
                $data->where('name', 'like', '%'.$search.'%');
            }

            // set filter
            if(!empty($filter)){
                foreach ($filter as $key => $val) {
                    $data->where($val['key'], $val['condition'], $val['value']);
                }
            }

            $data->orderBy('level', 'asc')->skip($start)->take($length);
            $dataParents = $data->get();

            $result = array();
            if(!empty($dataParents)) {
                foreach ($dataParents as $key => $val) {
                    if ($textFormat == 'code-name') {
                        $result[$key]['text'] = $val->code." - ".$val->name;
                    } else {
                        $setLevel = explode(".",$val->level);
                        $countLevel = count($setLevel);
                        $setName = "";
                        if($val->level != 0){
                            for($i=0; $i < $countLevel; $i++){
                                $setName = $setName."-";
                            }
                        }
                        $result[$key]['text'] = $setName." ".$val->name;
                    }

                    $result[$key]['id'] = $val->id;
                }
            }

            return $result;
        } catch (\Exception $ex) {
            $result = array();
            $result['error'] = $ex;
            return $result;
        }
    }

    public function totalResult($search) {
        $data = new Company;
        if(!empty($search)){
            $data->where('name', 'like', '%'.$search.'%');
        }
        if(!empty($filter)){
            foreach ($filter as $key => $val) {
                $data->where($val['key'], $val['condition'], $val['value']);
            }
        }
        $totalFiltered = $data->count();

        return $totalFiltered;
    }
}

?>