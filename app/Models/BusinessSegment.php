<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessSegment extends Model
{
    use HasFactory;

    protected $table = 'business_segments';

    public function companyRevenue() {
        return $this->hasMany('App\Models\PenCompanyRevenue', 'business_segment_id', 'id');
    }
}
