<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialLiquidity extends Model
{
    use HasFactory;

    protected $table = 'financial_liquidities';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function unit() {
        return $this->belongsTo('App\Models\Unit', 'unit_id');
    }

    public function flagCf() {
        return $this->belongsTo('App\Models\FlagCf', 'unit_id');
    }
}
