<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanningGroup extends Model
{
    use HasFactory;

    protected $table = 'planning_groups';
}
