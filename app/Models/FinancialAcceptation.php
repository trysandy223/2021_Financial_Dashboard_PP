<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialAcceptation extends Model
{
    use HasFactory;

    protected $table = 'financial_acceptations';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
}
