<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $table = 'projects';

    protected $casts = [
        'updated_at' => 'datetime:d F Y, H:i',
        'start_date' => 'datetime:d F Y',
        'finish_date' => 'datetime:d F Y',
    ];

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function demandMaterialConstruction() {
        return $this->hasMany('App\Models\PenDemandMaterialConstruction', 'project_id', 'id');
    }

    public function hunianMbr() {
        return $this->hasMany('App\Models\PenHunianMbr', 'project_id', 'id');
    }

    public function penyerapanTenagaKerja() {
        return $this->hasMany('App\Models\PenPenyerapanTenagaKerja', 'project_id', 'id');
    }

    public function psnProject() {
        return $this->hasMany('App\Models\PenPsnProject', 'project_id', 'id');
    }

    public function realisasiBelanjaUmkm() {
        return $this->hasMany('App\Models\PenRealisasiBelanjaUmkm', 'project_id', 'id');
    }

    public function targetRealisasiCapex() {
        return $this->hasMany('App\Models\PenTargetRealisasiCapex', 'project_id', 'id');
    }

    public function komponenDalamNegeri() {
        return $this->hasMany('App\Models\PenKomponenDalamNegeri', 'project_id', 'id');
    }
}
