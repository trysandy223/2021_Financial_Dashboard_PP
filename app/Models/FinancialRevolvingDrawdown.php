<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialRevolvingDrawdown extends Model
{
    use HasFactory;

    protected $table = 'financial_revolving_drawdowns';

    public function company() {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function revolvingLoan() {
        return $this->belongsTo(FinancialRevolvingLoan::class, 'revolving_loan_id');
    }
}
