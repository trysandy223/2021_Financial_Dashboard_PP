<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenKomponenDalamNegeri extends Model
{
    use HasFactory;

    protected $table = 'pen_komponen_dalam_negeris';

    protected $casts = [
        'updated_at' => 'datetime:d F Y, H:i',
        'date' => 'datetime:d F Y'
    ];

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id');
    }

    public function penProjectCategory() {
        return $this->belongsTo('App\Models\PenProjectCategory', 'pen_project_category_id');
    }
}
