<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TradingPartner extends Model
{
    use HasFactory;

    protected $table = 'trading_partners';

    protected $casts = [
        'updated_at' => 'datetime:d F Y, H:i'
    ];
}
