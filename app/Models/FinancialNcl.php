<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialNcl extends Model
{
    use HasFactory;

    protected $table = 'financial_ncls';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function remainingPlafonValue()
    {
        return $this->value - FinancialNclDrawdown::where('ncl_id', $this->id)->sum('value');
    }
}
