<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialNclDrawdown extends Model
{
    use HasFactory;

    protected $table = 'financial_ncl_drawdowns';

    public function company() {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function ncl() {
        return $this->belongsTo(FinancialNcl::class, 'ncl_id');
    }
}
