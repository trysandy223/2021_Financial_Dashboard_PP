<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Company extends Model
{
    use HasFactory;

    protected $table = 'companies';

    protected $casts = [
        'updated_at' => 'datetime:d F Y, H:i',
    ];

    // relation many to many with users table
    public function user() {
        return $this->belongsToMany('App\Models\User', 'user_companies', 'user_id', 'company_id');
    }

    // call children
    public function children() {
        return $this->hasMany('App\Models\Company', 'parent_id', 'id');
    }

    // call all children
    public function allChildren() {
        return $this->children()->with('allChildren');
    }

    // call parent
    public function parent() {
        return $this->belongsTo('App\Models\Company', 'parent_id', 'id');
    }

    // get parent level
    public static function getLevelParent($id) {
        $data = DB::table('companies')->select('id', 'name', 'level', 'sorting')
            ->where('id', $id)->orderBy('level', 'desc')
            ->take(1)->get();

        return $data;
    }

    // get level
    public static function getLevel($id) {
        $data = DB::table('companies')->select('level', 'sorting')
            ->where('parent_id', $id)->orderBy('sorting', 'desc')
            ->take(1)->get();

        return $data;
    }
    
    // get max level
    public static function getMaxLevel() {
        $data = DB::table('companies')->select('level', 'sorting')
            ->orderBy('level', 'desc')->take(1)->get();

        return $data;
    }

    public static function getDescendantCompany($companies) {
        $descendants = collect([]);

        foreach ($companies as $company) {
            // Main company of user
            $descendants->push($company->id);
            
            // Childs company of user
            $childs = Company::where('parent_id', $company->id)->get();
            foreach ($childs as $child) {
                $childLevel = Company::where('level', 'like', $child->level.'%')->pluck('id');
                $descendants->push($childLevel);
            }
        }

        $result = $descendants->flatten()->unique()->toArray();

        if (in_array($companies->first()->id ?? 0, config('const.sub_parent_company'))) {
            return array_merge($result, [$companies->first()->parent->id ?? 0]);
        }

        return $result;
    }
}
