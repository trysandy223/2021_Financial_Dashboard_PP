<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SourceGroup extends Model
{
    use HasFactory;

    protected $table = 'source_groups';
}
