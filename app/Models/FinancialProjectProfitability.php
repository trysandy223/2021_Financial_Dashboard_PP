<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialProjectProfitability extends Model
{
    use HasFactory;
    
    protected $table = 'financial_project_profitabilities';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
}
