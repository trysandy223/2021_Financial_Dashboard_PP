<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlagCcc extends Model
{
    use HasFactory;

    protected $table = 'flag_cccs';
}
