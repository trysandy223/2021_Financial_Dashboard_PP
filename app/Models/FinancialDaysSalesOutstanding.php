<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialDaysSalesOutstanding extends Model
{
    use HasFactory;

    protected $table = 'financial_days_sales_outstandings';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function customer() {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
}
