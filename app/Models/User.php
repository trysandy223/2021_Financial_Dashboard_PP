<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, HasRoles, HasApiTokens, Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // relation many to many with companies table
    public function company() {
        return $this->belongsToMany('App\Models\Company', 'user_companies', 'user_id', 'company_id');
    }

    // get all companies id of user (include all children company)
    public function allCompanyId() {
        $userId = auth()->id();
        $datas = \Cache::remember("user:{$userId}:companyId", 60*15, function () {
            return \App\Models\Company::getDescendantCompany($this->company);
        });

        return $datas;
    }
}
