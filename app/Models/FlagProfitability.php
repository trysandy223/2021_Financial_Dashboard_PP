<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlagProfitability extends Model
{
    use HasFactory;

    protected $table = 'flag_profitabilities';
}
