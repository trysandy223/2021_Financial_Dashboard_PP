<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialRevolvingLoan extends Model
{
    use HasFactory;

    protected $table = 'financial_revolving_loans';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function bank() {
        return $this->belongsTo('App\Models\Bank', 'bank_id');
    }
    
    public function remainingPlafonValue()
    {
        return $this->plafon_value - FinancialRevolvingDrawdown::where('revolving_loan_id', $this->id)->sum('value');
    }
}
