<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgingDso extends Model
{
    use HasFactory;

    protected $table = 'aging_dsos';
}
