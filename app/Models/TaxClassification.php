<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxClassification extends Model
{
    use HasFactory;

    protected $table = 'tax_classifications';
}
