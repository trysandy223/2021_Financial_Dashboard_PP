<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPeriod extends Model
{
    use HasFactory;

    protected $table = 'data_periods';
}
