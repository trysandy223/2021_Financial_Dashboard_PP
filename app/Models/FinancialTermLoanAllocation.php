<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialTermLoanAllocation extends Model
{
    use HasFactory;

    protected $table = 'financial_term_loan_allocations';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
}
