<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialTermLoan extends Model
{
    use HasFactory;

    protected $table = 'financial_term_loans';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function bank() {
        return $this->belongsTo('App\Models\Bank', 'bank_id');
    }
}
