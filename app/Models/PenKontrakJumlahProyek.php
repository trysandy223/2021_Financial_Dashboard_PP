<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenKontrakJumlahProyek extends Model
{
    use HasFactory;

    protected $table = 'pen_kontrak_jumlah_proyeks';

    protected $casts = [
        'updated_at' => 'datetime:d F Y, H:i',
        'date' => 'datetime:d F Y'
    ];

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
}
