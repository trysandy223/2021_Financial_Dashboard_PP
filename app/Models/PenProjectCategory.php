<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenProjectCategory extends Model
{
    use HasFactory;

    protected $table = 'pen_project_categories';
}
