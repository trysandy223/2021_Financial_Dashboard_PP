<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialInvestment extends Model
{
    use HasFactory;

    protected $table = 'financial_investments';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
}
