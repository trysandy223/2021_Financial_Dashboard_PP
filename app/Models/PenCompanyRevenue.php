<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenCompanyRevenue extends Model
{
    use HasFactory;

    protected $table = 'pen_company_revenues';

    protected $casts = [
        'updated_at' => 'datetime:d F Y, H:i',
        'date' => 'datetime:d F Y'
    ];

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function businessSegment() {
        return $this->belongsTo('App\Models\BusinessSegment', 'business_segment_id');
    }
}
