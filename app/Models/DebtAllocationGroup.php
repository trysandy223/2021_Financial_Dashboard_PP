<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DebtAllocationGroup extends Model
{
    use HasFactory;

    protected $table = 'debt_allocation_groups';
}
