<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenTargetRealisasiCapex extends Model
{
    use HasFactory;

    protected $table = 'pen_target_realisasi_capexes';

    protected $casts = [
        'updated_at' => 'datetime:d F Y, H:i',
        'date' => 'datetime:d F Y'
    ];

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id');
    }
}
