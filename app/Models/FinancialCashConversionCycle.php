<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialCashConversionCycle extends Model
{
    use HasFactory;

    protected $table = 'financial_cash_conversion_cycles';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
}
