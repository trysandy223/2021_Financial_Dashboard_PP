<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class KontrakJumlahProyekExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithMultipleSheets, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'pen_kontrak_jumlah_proyeks.date as date', 'companies.id as company_id', 'companies.code as company_code',
            'companies.name as company_name', 'pen_kontrak_jumlah_proyeks.psn_project_total as psn_project_total', 'pen_kontrak_jumlah_proyeks.psn_project_value as psn_project_value',
            'pen_kontrak_jumlah_proyeks.non_psn_project_total as non_psn_project_total', 'pen_kontrak_jumlah_proyeks.non_psn_project_value as non_psn_project_value')
        ->from('pen_kontrak_jumlah_proyeks')
        ->leftJoin('companies', 'pen_kontrak_jumlah_proyeks.company_id', '=', 'companies.id')
        ->whereIn('pen_kontrak_jumlah_proyeks.company_id', Auth::user()->allCompanyId())
        ->orderBy('pen_kontrak_jumlah_proyeks.updated_at', 'desc');

        return $data;
    }


    public function headings(): array
    {
        return [
            'Date',
            'Company ID',
            'Company Code',
            'Company Name',
            'PSN Project Total',
            'PSN Project Value',
            'Non PSN Project Total',
            'Non PSN Project Value',
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 4);

                $event->sheet->mergeCells('A1:C1');
                $event->sheet->mergeCells('A2:C2');

                $event->sheet->setCellValue('A1','Perolehan Kontrak Jumlah Proyek Data');
                $event->sheet->setCellValue('A2','PT PP (Persero) Tbk. - '.date('d M Y'));
                $event->sheet->getStyle('A1')->getFont()->setSize(14)->setBold(True);
                $event->sheet->getStyle('A2')->getFont()->setSize(14)->setBold(True);
                
                $event->sheet->styleCells(
                    'A5:H5',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Perolehan Kontrak Jumlah Proyek';
    }

    public function sheets(): array
    {
        return [
            'Perolehan Kontrak Jumlah Proyek' => new KontrakJumlahProyekExport()
        ];
    }
}
