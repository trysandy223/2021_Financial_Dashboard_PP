<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class CompanyProjectExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'companies.id as company_id', 'companies.code as company_code', 'companies.name as company_name',
            'projects.id as project_id', 'projects.code as project_code', DB::raw('CONCAT(projects.code, " - ", projects.name) as project_name'))
        ->from('projects')
        ->leftJoin('companies', 'projects.company_id', '=', 'companies.id')
        ->whereIn('projects.company_id', Auth::user()->allCompanyId())
        ->orderBy('projects.updated_at', 'desc');

        return $data;
    }


    public function headings(): array
    {
        return [
            'Company ID',
            'Company Code',
            'Company Name',
            'Project ID',
            'Project Code',
            'Project Name'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:F1',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Company Project';
    }
}
