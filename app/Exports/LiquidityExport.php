<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class LiquidityExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithMultipleSheets, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'financial_liquidities.posting_date as posting_date', 'companies.id as company_id', 'companies.code as company_code',
            'companies.name as company_name', 'projects.id as project_id', 'projects.code as project_code',
            'projects.name as project_name', 'categories.id as category_id', 'categories.name as category_name',
            'flag_cfs.id as flag_cf_id', 'flag_cfs.name as flag_cf_name', 'data_periods.id as data_period_id',
            'data_periods.name as data_period_name', 'banks.id as bank_id', 'banks.name as bank_name',
            'financial_liquidities.trading_partner as trading_partner', 'financial_liquidities.plan_date as plan_date', 'financial_liquidities.value as value')
        ->from('financial_liquidities')
        ->leftJoin('companies', 'financial_liquidities.company_id', '=', 'companies.id')
        ->leftJoin('projects', 'financial_liquidities.project_id', '=', 'projects.id')
        ->leftJoin('categories', 'financial_liquidities.category_id', '=', 'categories.id')
        ->leftJoin('flag_cfs', 'financial_liquidities.flag_cf_id', '=', 'flag_cfs.id')
        ->leftJoin('data_periods', 'financial_liquidities.data_period_id', '=', 'data_periods.id')
        ->leftJoin('banks', 'financial_liquidities.bank_id', '=', 'banks.id')
        ->whereIn('financial_liquidities.company_id', Auth::user()->allCompanyId())
        ->where('financial_liquidities.id', '<', 6)
        ->orderBy('financial_liquidities.updated_at', 'desc');

        return $data;
    }

    public function headings(): array
    {
        return [
            'Posting Date',
            'Company ID',
            'Company Code',
            'Company Name',
            'Project ID',
            'Project Code',
            'Project Name',
            'Category ID',
            'Category Name',
            'Flag CF ID',
            'Flag CF Name',
            'Data Period ID',
            'Data Period Name',
            'Bank ID',
            'Bank Name',
            'Trading Partner',
            'Plan Date',
            'Value'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 22);

                $event->sheet->mergeCells('A1:K1');
                $event->sheet->mergeCells('A2:K2');
                $event->sheet->mergeCells('A3:K3');
                $event->sheet->mergeCells('A4:K4');
                $event->sheet->mergeCells('A5:K5');
                $event->sheet->mergeCells('A6:K6');
                $event->sheet->mergeCells('A7:K7');
                $event->sheet->mergeCells('A8:K8');
                $event->sheet->mergeCells('A9:K9');
                $event->sheet->mergeCells('A10:K10');
                $event->sheet->mergeCells('A11:K11');
                $event->sheet->mergeCells('A12:K12');
                $event->sheet->mergeCells('A13:K13');
                $event->sheet->mergeCells('A14:K14');
                $event->sheet->mergeCells('A15:K15');
                $event->sheet->mergeCells('A16:K16');
                $event->sheet->mergeCells('A17:K17');
                $event->sheet->mergeCells('A18:K18');
                $event->sheet->mergeCells('A19:K19');
                $event->sheet->mergeCells('A20:K20');

                $event->sheet->setCellValue('A1','Template Import Liquidity');
                $event->sheet->setCellValue('A2','PT PP (Persero) Tbk. - '.date('d M Y'));
                $event->sheet->setCellValue('A4','Aturan untuk import:');

                $event->sheet->setCellValue('A5','1. Data yang diinput harus di sheet ini.');
                $event->sheet->setCellValue('A6','2. Nama sheet tidak boleh diubah.');
                $event->sheet->setCellValue('A7','3. Posisi header di sheet ini tidak boleh diubah, harus berada di baris ke 23.');
                $event->sheet->setCellValue('A8','4. Nama header di sheet ini tidak boleh diubah, termasuk huruf besar atau spasinya.');
                $event->sheet->setCellValue('A9','5. Data di kolom Posting Date diisi tanggal yang formatnya dd/mm/yy dan tipenya date.');
                $event->sheet->setCellValue('A10','6. Data di kolom Company ID hingga kolom Project Name bisa langsung di-copy paste dari sheet Company Project sesuai kebutuhan.');
                $event->sheet->setCellValue('A11','7. Data di kolom Category ID dan Category Name bisa langsung di-copy paste dari sheet Category sesuai kebutuhan.');
                $event->sheet->setCellValue('A12','8. Data di kolom Flag CF ID dan Flag CF Name bisa langsung di-copy paste dari sheet Flag CF sesuai kebutuhan.');
                $event->sheet->setCellValue('A13','9. Data di kolom Data Period ID dan Data Period Name bisa langsung di-copy paste dari sheet Data Period sesuai kebutuhan.');
                $event->sheet->setCellValue('A14','10. Data di kolom Bank ID dan Bank Name bisa langsung di-copy paste dari sheet Bank sesuai kebutuhan.');
                $event->sheet->setCellValue('A15','11. Data di kolom Trading Partner bisa langsung di-copy paste dari sheet Trading Partner sesuai kebutuhan.');
                $event->sheet->setCellValue('A16','12. Data di kolom Plan Date bisa langsung di-copy paste dari sheet Plan Date sesuai kebutuhan.');
                $event->sheet->setCellValue('A17','13. Kolom Value diisi angka biasa, tipenya general.');
                $event->sheet->setCellValue('A18','14. Jika ingin menghapus baris, pastikan menggunakan menu Clear > Clear All agar tidak ada format yang tersisa.');
                $event->sheet->setCellValue('A19','15. Jika pengisian data sudah yakin benar, silakan import file Excel ini melalui aplikasi.');
                $event->sheet->setCellValue('A20','16. Jika proses import gagal, silakan cek kembali datanya. Pastikan sudah sesuai aturan.');

                $event->sheet->getStyle('A1')->getFont()->setSize(16)->setBold(True);
                $event->sheet->getStyle('A2')->getFont()->setSize(16)->setBold(True);
                $event->sheet->getStyle('A4')->getFont()->setSize(16)->setBold(True)->getColor()->setRGB('fc0303');

                $event->sheet->styleCells(
                    'A5:A20',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  14
                        ],
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        ],
                    ]
                );
                
                $event->sheet->styleCells(
                    'A23:R23',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Liquidity';
    }

    public function sheets(): array
    {
        return [
            'Liquidity' => new LiquidityExport(),
            'Company Project' => new CompanyProjectExport(),
            'Category' => new CategoryExport(),
            'Flag CF' => new FlagCfExport(),
            'Data Period' => new DataPeriodExport(),
            'Bank' => new BankExport(),
            'Trading Partner' => new TradingPartnerExport(),
            'Plan Date' => new PlanDateExport()
        ];
    }
}
