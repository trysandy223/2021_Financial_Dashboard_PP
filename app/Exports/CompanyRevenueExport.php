<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class CompanyRevenueExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithMultipleSheets, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'pen_company_revenues.date as date', 'companies.id as company_id', 'companies.code as company_code', 'companies.name as company_name',
            'business_segments.id as business_segment_id', 'business_segments.name as business_segment_name', 'pen_company_revenues.total_revenue as total_revenue')
        ->from('pen_company_revenues')
        ->leftJoin('companies', 'pen_company_revenues.company_id', '=', 'companies.id')
        ->leftJoin('business_segments', 'pen_company_revenues.business_segment_id', '=', 'business_segments.id')
        ->whereIn('pen_company_revenues.company_id', Auth::user()->allCompanyId())
        ->orderBy('pen_company_revenues.updated_at', 'desc');

        return $data;
    }


    public function headings(): array
    {
        return [
            'Date',
            'Company ID',
            'Company Code',
            'Company Name',
            'Business Segment ID',
            'Business Segment Name',
            'Total Revenue'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 4);

                $event->sheet->mergeCells('A1:C1');
                $event->sheet->mergeCells('A2:C2');

                $event->sheet->setCellValue('A1','Pendapatan Usaha Data');
                $event->sheet->setCellValue('A2','PT PP (Persero) Tbk. - '.date('d M Y'));
                $event->sheet->getStyle('A1')->getFont()->setSize(14)->setBold(True);
                $event->sheet->getStyle('A2')->getFont()->setSize(14)->setBold(True);
                
                $event->sheet->styleCells(
                    'A5:G5',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Pendapatan Usaha';
    }

    public function sheets(): array
    {
        return [
            'Pendapatan Usaha' => new CompanyRevenueExport()
        ];
    }
}
