<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class RealisasiBelanjaUmkmTemplateExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithMultipleSheets, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'pen_realisasi_belanja_umkms.date as date', 'companies.id as company_id', 'companies.code as company_code',
            'companies.name as company_name', 'projects.id as project_id', 'projects.code as project_code',
            'projects.name as project_name', 'pen_realisasi_belanja_umkms.sistem_padi as sistem_padi', 'pen_realisasi_belanja_umkms.sistem_sendiri as sistem_sendiri')
        ->from('pen_realisasi_belanja_umkms')
        ->leftJoin('companies', 'pen_realisasi_belanja_umkms.company_id', '=', 'companies.id')
        ->leftJoin('projects', 'pen_realisasi_belanja_umkms.project_id', '=', 'projects.id')
        ->whereIn('pen_realisasi_belanja_umkms.company_id', Auth::user()->allCompanyId())
        ->where('pen_realisasi_belanja_umkms.id', '<', 3)
        ->orderBy('pen_realisasi_belanja_umkms.updated_at', 'desc');

        return $data;
    }


    public function headings(): array
    {
        return [
            'Date',
            'Company ID',
            'Company Code',
            'Company Name',
            'Project ID',
            'Project Code',
            'Project Name',
            'Sistem Padi',
            'Sistem Milik Sendiri'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 17);

                $event->sheet->mergeCells('A1:G1');
                $event->sheet->mergeCells('A2:G2');
                $event->sheet->mergeCells('A3:G3');
                $event->sheet->mergeCells('A4:G4');
                $event->sheet->mergeCells('A5:G5');
                $event->sheet->mergeCells('A6:G6');
                $event->sheet->mergeCells('A7:G7');
                $event->sheet->mergeCells('A8:G8');
                $event->sheet->mergeCells('A9:G9');
                $event->sheet->mergeCells('A10:G10');
                $event->sheet->mergeCells('A11:G11');
                $event->sheet->mergeCells('A12:G12');
                $event->sheet->mergeCells('A13:G13');
                $event->sheet->mergeCells('A14:G14');
                $event->sheet->mergeCells('A15:G15');

                $event->sheet->setCellValue('A1','Template Import Realisasi Belanja UMKM');
                $event->sheet->setCellValue('A2','PT PP (Persero) Tbk. - '.date('d M Y'));
                $event->sheet->setCellValue('A4','Aturan untuk import:');

                $event->sheet->setCellValue('A5','1. Data yang diinput harus di sheet ini.');
                $event->sheet->setCellValue('A6','2. Nama sheet tidak boleh diubah.');
                $event->sheet->setCellValue('A7','3. Posisi header di sheet ini tidak boleh diubah, harus berada di baris ke 18.');
                $event->sheet->setCellValue('A8','4. Nama header di sheet ini tidak boleh diubah, termasuk huruf besar atau spasinya.');
                $event->sheet->setCellValue('A9','5. Data di kolom Date diisi tanggal yang formatnya dd/mm/yy dan tipenya date.');
                $event->sheet->setCellValue('A10','6. Data di kolom Company ID hingga kolom Project Name bisa langsung di-copy paste dari sheet Company Project sesuai kebutuhan.');
                $event->sheet->setCellValue('A11','7. Data di kolom Sistem Padi diisi angka biasa, tipenya general.');
                $event->sheet->setCellValue('A12','8. Data di kolom Sistem Milik Sendiri diisi angka biasa, tipenya general.');
                $event->sheet->setCellValue('A13','9. Jika ingin menghapus baris, pastikan menggunakan menu Clear > Clear All agar tidak ada format yang tersisa.');
                $event->sheet->setCellValue('A14','10. Jika pengisian data sudah yakin benar, silakan import file Excel ini melalui aplikasi.');
                $event->sheet->setCellValue('A15','11. Jika proses import gagal, silakan cek kembali datanya. Pastikan sudah sesuai aturan.');

                $event->sheet->getStyle('A1')->getFont()->setSize(16)->setBold(True);
                $event->sheet->getStyle('A2')->getFont()->setSize(16)->setBold(True);
                $event->sheet->getStyle('A4')->getFont()->setSize(16)->setBold(True)->getColor()->setRGB('fc0303');

                $event->sheet->styleCells(
                    'A5:A15',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  14
                        ],
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        ],
                    ]
                );
                
                $event->sheet->styleCells(
                    'A18:I18',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Realisasi Belanja UMKM';
    }

    public function sheets(): array
    {
        return [
            'Realisasi Belanja UMKM' => new RealisasiBelanjaUmkmTemplateExport(),
            'Company Project' => new CompanyProjectExport()
        ];
    }
}