<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class DemandMaterialConstructionExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithMultipleSheets, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'pen_demand_material_constructions.date as date', 'companies.id as company_id', 'companies.code as company_code',
            'companies.name as company_name', 'projects.id as project_id', 'projects.code as project_code',
            'projects.name as project_name', 'materials.id as material_id', 'materials.name as material_name',
            'materials.satuan as material_satuan', 'pen_demand_material_constructions.demand_total as demand_total')
        ->from('pen_demand_material_constructions')
        ->leftJoin('companies', 'pen_demand_material_constructions.company_id', '=', 'companies.id')
        ->leftJoin('projects', 'pen_demand_material_constructions.project_id', '=', 'projects.id')
        ->leftJoin('materials', 'pen_demand_material_constructions.material_id', '=', 'materials.id')
        ->whereIn('pen_demand_material_constructions.company_id', Auth::user()->allCompanyId())
        ->orderBy('pen_demand_material_constructions.updated_at', 'desc');

        return $data;
    }

    public function headings(): array
    {
        return [
            'Date',
            'Company ID',
            'Company Code',
            'Company Name',
            'Project ID',
            'Project Code',
            'Project Name',
            'Material ID',
            'Material Name',
            'Material Satuan',
            'Demand Total'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 4);

                $event->sheet->mergeCells('A1:C1');
                $event->sheet->mergeCells('A2:C2');

                $event->sheet->setCellValue('A1','Demand Material Construction Data');
                $event->sheet->setCellValue('A2','PT PP (Persero) Tbk. - '.date('d M Y'));
                $event->sheet->getStyle('A1')->getFont()->setSize(14)->setBold(True);
                $event->sheet->getStyle('A2')->getFont()->setSize(14)->setBold(True);
                
                
                $event->sheet->styleCells(
                    'A5:K5',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Demand Material Construction';
    }

    public function sheets(): array
    {
        return [
            'Demand Material Construction' => new DemandMaterialConstructionExport()
        ];
    }
}