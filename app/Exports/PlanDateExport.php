<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Events\AfterSheet;

class PlanDateExport implements WithEvents, WithTitle
{
    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->setCellValue('A1','Plan Date');
                $event->sheet->setCellValue('A2','Cash In - Dasar Pengenaan Pajak');
                $event->sheet->setCellValue('A3','Cash In - Pajak PPN');
                $event->sheet->setCellValue('A4','Cash In - PPh');
                $event->sheet->setCellValue('A5','Cash Out - Reguler');
                $event->sheet->setCellValue('A6','Cash Out - SKBDN');
                $event->sheet->setCellValue('A7','Cash Out - PPN Wapu');
                $event->sheet->setCellValue('A8','Cash Out - PPh Vendor');
                $event->sheet->setCellValue('A9','Cash Out - Debet Nota');
                $event->sheet->setCellValue('A10','Investing activities');
                $event->sheet->setCellValue('A11','Divesting activities');
                $event->sheet->setCellValue('A12','Debt repayment');
                $event->sheet->setCellValue('A13','Inflow from new debt');
                $event->sheet->setCellValue('A14','Other financing CF');
                $event->sheet->setCellValue('A15','Starting Cash Balance');

                // Generate data FW1 - FW13
                for ($i=1; $i < 14; $i++) { 
                    $cellStartFrom = $i + 15;
                    $cell = 'A' . $cellStartFrom .'';
                    $planDateWeek = 'FW' . $i . '';

                    $event->sheet->setCellValue($cell, $planDateWeek);
                }

                // Generate data M1 - M12
                for ($i=1; $i < 13; $i++) { 
                    $cellStartFrom = $i + 28;
                    $cell = 'A' . $cellStartFrom .'';
                    $planDateMonth = 'M' . $i . '';

                    $event->sheet->setCellValue($cell, $planDateMonth);
                }

                // Generate data Y1 - Y5
                for ($i=1; $i < 6; $i++) { 
                    $cellStartFrom = $i + 40;
                    $cell = 'A' . $cellStartFrom .'';
                    $planDateYear = 'Y' . $i . '';

                    $event->sheet->setCellValue($cell, $planDateYear);
                }
                
                $event->sheet->styleCells(
                    'A1',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Plan Date';
    }
}
