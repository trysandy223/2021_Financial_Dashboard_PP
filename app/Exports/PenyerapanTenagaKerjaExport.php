<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class PenyerapanTenagaKerjaExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithMultipleSheets, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'pen_penyerapan_tenaga_kerjas.date as date', 'companies.id as company_id', 'companies.code as company_code',
            'companies.name as company_name', 'projects.id as project_id', 'projects.code as project_code',
            'projects.name as project_name', 'pen_penyerapan_tenaga_kerjas.pkwt as pkwt', 'pen_penyerapan_tenaga_kerjas.pkwtt as pkwtt',
            'pen_penyerapan_tenaga_kerjas.tenaga_konstruksi as tenaga_konstruksi')
        ->from('pen_penyerapan_tenaga_kerjas')
        ->leftJoin('companies', 'pen_penyerapan_tenaga_kerjas.company_id', '=', 'companies.id')
        ->leftJoin('projects', 'pen_penyerapan_tenaga_kerjas.project_id', '=', 'projects.id')
        ->whereIn('pen_penyerapan_tenaga_kerjas.company_id', Auth::user()->allCompanyId())
        ->orderBy('pen_penyerapan_tenaga_kerjas.updated_at', 'desc');

        return $data;
    }


    public function headings(): array
    {
        return [
            'Date',
            'Company ID',
            'Company Code',
            'Company Name',
            'Project ID',
            'Project Code',
            'Project Name',
            'PKWT',
            'PKWTT',
            'Tenaga Konstruksi'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 4);

                $event->sheet->mergeCells('A1:C1');
                $event->sheet->mergeCells('A2:C2');

                $event->sheet->setCellValue('A1','Penyerapan Tenaga Kerja Data');
                $event->sheet->setCellValue('A2','PT PP (Persero) Tbk. - '.date('d M Y'));
                $event->sheet->getStyle('A1')->getFont()->setSize(14)->setBold(True);
                $event->sheet->getStyle('A2')->getFont()->setSize(14)->setBold(True);
                
                $event->sheet->styleCells(
                    'A5:L5',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Penyerapan Tenaga Kerja';
    }

    public function sheets(): array
    {
        return [
            'Penyerapan Tenaga Kerja' => new PenyerapanTenagaKerjaExport()
        ];
    }
}
