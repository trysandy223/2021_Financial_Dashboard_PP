<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class TargetRealisasiCapexExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithMultipleSheets, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'pen_target_realisasi_capexes.date as date', 'companies.id as company_id', 'companies.code as company_code',
            'companies.name as company_name', 'projects.id as project_id', 'projects.code as project_code',
            'projects.name as project_name', 'pen_target_realisasi_capexes.nilai_equity as nilai_equity', 'pen_target_realisasi_capexes.nilai_loan as nilai_loan',
            'pen_target_realisasi_capexes.pekerjaan_fisik as pekerjaan_fisik')
        ->from('pen_target_realisasi_capexes')
        ->leftJoin('companies', 'pen_target_realisasi_capexes.company_id', '=', 'companies.id')
        ->leftJoin('projects', 'pen_target_realisasi_capexes.project_id', '=', 'projects.id')
        ->whereIn('pen_target_realisasi_capexes.company_id', Auth::user()->allCompanyId())
        ->orderBy('pen_target_realisasi_capexes.updated_at', 'desc');

        return $data;
    }


    public function headings(): array
    {
        return [
            'Date',
            'Company ID',
            'Company Code',
            'Company Name',
            'Project ID',
            'Project Code',
            'Project Name',
            'Nilai Equity',
            'Nilai Loan',
            'Pekerjaan Fisik'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 4);

                $event->sheet->mergeCells('A1:C1');
                $event->sheet->mergeCells('A2:C2');

                $event->sheet->setCellValue('A1','Target Realisasi Capex Data');
                $event->sheet->setCellValue('A2','PT PP (Persero) Tbk. - '.date('d M Y'));
                $event->sheet->getStyle('A1')->getFont()->setSize(14)->setBold(True);
                $event->sheet->getStyle('A2')->getFont()->setSize(14)->setBold(True);
                
                $event->sheet->styleCells(
                    'A5:J5',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Target Realisasi Capex';
    }

    public function sheets(): array
    {
        return [
            'Target Realisasi Capex' => new TargetRealisasiCapexExport()
        ];
    }
}
