<?php

namespace App\Exports;

use App\Models\Material;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;

class MaterialExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Material::select('id', 'name', 'satuan')->get();
    }

    public function headings(): array
    {
        return [
            'Material ID',
            'Material Name',
            'Material Satuan'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:C1',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Material';
    }
}
