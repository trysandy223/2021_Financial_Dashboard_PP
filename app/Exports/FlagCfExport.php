<?php

namespace App\Exports;

use App\Models\FlagCf;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;

class FlagCfExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return FlagCf::select('id', 'name')->get();
    }

    public function headings(): array
    {
        return [
            'Flag CF ID',
            'Flag CF Name'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:B1',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Flag CF';
    }
}
