<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class KomponenDalamNegeriExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithMultipleSheets, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'pen_komponen_dalam_negeris.date as date', 'companies.id as company_id', 'companies.code as company_code',
            'companies.name as company_name', 'projects.id as project_id', 'projects.code as project_code',
            'projects.name as project_name', 'pen_project_categories.id as pen_project_category_id', 'pen_project_categories.name as pen_project_category_name',
            'pen_komponen_dalam_negeris.nilai_proyek as nilai_proyek', 'pen_komponen_dalam_negeris.tingkat_komponen_dalam_negeri as tingkat_komponen_dalam_negeri')
        ->from('pen_komponen_dalam_negeris')
        ->leftJoin('companies', 'pen_komponen_dalam_negeris.company_id', '=', 'companies.id')
        ->leftJoin('projects', 'pen_komponen_dalam_negeris.project_id', '=', 'projects.id')
        ->leftJoin('pen_project_categories', 'pen_komponen_dalam_negeris.pen_project_category_id', '=', 'pen_project_categories.id')
        ->whereIn('pen_komponen_dalam_negeris.company_id', Auth::user()->allCompanyId())
        ->orderBy('pen_komponen_dalam_negeris.updated_at', 'desc');

        return $data;
    }


    public function headings(): array
    {
        return [
            'Date',
            'Company ID',
            'Company Code',
            'Company Name',
            'Project ID',
            'Project Code',
            'Project Name',
            'Project Category ID',
            'Project Category Name',
            'Project Value',
            'Tingkat Komponen Dalam Negeri'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 4);

                $event->sheet->mergeCells('A1:C1');
                $event->sheet->mergeCells('A2:C2');

                $event->sheet->setCellValue('A1','Tingkat Komponen Dalam Negeri Data');
                $event->sheet->setCellValue('A2','PT PP (Persero) Tbk. - '.date('d M Y'));
                $event->sheet->getStyle('A1')->getFont()->setSize(14)->setBold(True);
                $event->sheet->getStyle('A2')->getFont()->setSize(14)->setBold(True);
                
                $event->sheet->styleCells(
                    'A5:K5',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Tingkat Komponen Dalam Negeri';
    }

    public function sheets(): array
    {
        return [
            'Tingkat Komponen Dalam Negeri' => new KomponenDalamNegeriExport()
        ];
    }
}
