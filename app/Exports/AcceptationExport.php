<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use DB, Auth;

class AcceptationExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents, WithMultipleSheets, WithTitle
{
    public function query()
    {
        $data = DB::query()->select(
            'financial_acceptations.posting_date as posting_date', 'companies.id as company_id', 'companies.code as company_code',
            'companies.name as company_name', 'projects.id as project_id', 'projects.code as project_code',
            'projects.name as project_name', 'financial_acceptations.category_id as category_id', 'financial_acceptations.category_name as category_name',
            'financial_acceptations.trading_partner_id as trading_partner_id', 'financial_acceptations.trading_partner_name as trading_partner_name', 'financial_acceptations.bank_id as bank_id',
            'financial_acceptations.bank_name as bank_name', 'financial_ncl_drawdowns.id as ncl_drawdown_id', 'financial_ncl_drawdowns.credit_line_detail as ncl_drawdown_name',
            'financial_ncl_drawdowns.debt_type_id as debt_type_id', 'financial_ncl_drawdowns.debt_type_name as debt_type_name', 'financial_ncl_drawdowns.plafon_value as plafon_value',
            'financial_ncl_drawdowns.value as drawdown_value', 'financial_acceptations.debt_allocation_group_id as debt_allocation_group_id', 'financial_acceptations.debt_allocation_group_name as debt_allocation_group_name',
            'financial_acceptations.result_allocation_name as result_allocation_name', 'financial_acceptations.value as acceptation_value')
        ->from('financial_acceptations')
        ->leftJoin('companies', 'financial_acceptations.company_id', '=', 'companies.id')
        ->leftJoin('projects', 'financial_acceptations.project_id', '=', 'projects.id')
        ->leftJoin('financial_ncl_drawdowns', 'financial_acceptations.ncl_drawdown_id', '=', 'financial_ncl_drawdowns.id')
        ->whereIn('financial_acceptations.company_id', Auth::user()->allCompanyId())
        ->where('financial_acceptations.id', '<', 6)
        ->orderBy('financial_acceptations.updated_at', 'desc');

        return $data;
    }

    public function headings(): array
    {
        return [
            'Posting Date',
            'Company ID',
            'Company Code',
            'Company Name',
            'Project ID',
            'Project Code',
            'Project Name',
            'Category ID',
            'Category Name',
            'Trading Partner ID',
            'Trading Partner Name',
            'Bank ID',
            'Bank Name',
            'NCL Drawdown ID',
            'NCL Drawdown Name',
            'Debt Type ID',
            'Debt Type Name',
            'Plafon Value',
            'Drawdown Value',
            'Debt Allocation Group ID',
            'Debt Allocation Group Name',
            'Result Allocation Name',
            'Acceptation Value'
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1, 26);

                $event->sheet->mergeCells('A1:K1');
                $event->sheet->mergeCells('A2:K2');
                $event->sheet->mergeCells('A3:K3');
                $event->sheet->mergeCells('A4:K4');
                $event->sheet->mergeCells('A5:K5');
                $event->sheet->mergeCells('A6:K6');
                $event->sheet->mergeCells('A7:K7');
                $event->sheet->mergeCells('A8:K8');
                $event->sheet->mergeCells('A9:K9');
                $event->sheet->mergeCells('A10:K10');
                $event->sheet->mergeCells('A11:K11');
                $event->sheet->mergeCells('A12:K12');
                $event->sheet->mergeCells('A13:K13');
                $event->sheet->mergeCells('A14:K14');
                $event->sheet->mergeCells('A15:K15');
                $event->sheet->mergeCells('A16:K16');
                $event->sheet->mergeCells('A17:K17');
                $event->sheet->mergeCells('A18:K18');
                $event->sheet->mergeCells('A19:K19');
                $event->sheet->mergeCells('A20:K20');
                $event->sheet->mergeCells('A21:K21');
                $event->sheet->mergeCells('A22:K22');
                $event->sheet->mergeCells('A23:K23');
                $event->sheet->mergeCells('A24:K24');

                $event->sheet->setCellValue('A1','Template Import Acceptation');
                $event->sheet->setCellValue('A2','PT PP (Persero) Tbk. - '.date('d M Y'));
                $event->sheet->setCellValue('A4','Aturan untuk import:');

                $event->sheet->setCellValue('A5','1. Data yang diinput harus di sheet ini.');
                $event->sheet->setCellValue('A6','2. Nama sheet tidak boleh diubah.');
                $event->sheet->setCellValue('A7','3. Posisi header di sheet ini tidak boleh diubah, harus berada di baris ke 27.');
                $event->sheet->setCellValue('A8','4. Nama header di sheet ini tidak boleh diubah, termasuk huruf besar atau spasinya.');
                $event->sheet->setCellValue('A9','5. Data di kolom Posting Date diisi tanggal yang formatnya dd/mm/yy dan tipenya date.');
                $event->sheet->setCellValue('A10','6. Data di kolom Company ID hingga kolom Project Name bisa langsung di-copy paste dari sheet Company Project sesuai kebutuhan.');
                $event->sheet->setCellValue('A11','7. Data di kolom Category ID dan Category Name bisa langsung di-copy paste dari sheet Category sesuai kebutuhan.');
                $event->sheet->setCellValue('A12','8. Data di kolom Trading Partner ID dan Trading Partner Name bisa langsung di-copy paste dari sheet Trading Partner sesuai kebutuhan.');
                $event->sheet->setCellValue('A13','9. Data di kolom Bank ID dan Bank Name bisa langsung di-copy paste dari sheet Bank sesuai kebutuhan.');
                $event->sheet->setCellValue('A14','10. Data di kolom NCL Drawdown ID hingga kolom Drawdown Value bisa langsung di-copy paste dari sheet NCL Drawdown sesuai kebutuhan.');
                $event->sheet->setCellValue('A15','11. Data di kolom Debt Allocation Group ID dan Debt Allocation Group Name bisa langsung di-copy paste dari sheet Debt Allocation Group sesuai kebutuhan.');
                $event->sheet->setCellValue('A16','12. Untuk aturan mengisi data di kolom Result Allocation Name adalah sebagai berikut:');
                $event->sheet->setCellValue('A17','     - Data di kolom Result Allocation Name diisi Project Name jika Debt Allocation Group Name-nya CONSTRUCTION PROJECT.');
                $event->sheet->setCellValue('A18','     - Data di kolom Result Allocation Name diisi Trading Partner Name jika Debt Allocation Group Name-nya SHAREHOLDER LOANS, SUBSIDIARY INVESTMENT, atau CORP. INVEST & OTHERS.');
                $event->sheet->setCellValue('A19','     - Data di kolom Result Allocation Name diisi Debt Name dari Term Loan jika Debt Allocation Group Name-nya OTHER DEBT PAYMENT TERM.');
                $event->sheet->setCellValue('A20','     - Data di kolom Result Allocation Name diisi Debt Name dari Revolving Loan jika Debt Allocation Group Name-nya OTHER DEBT PAYMENT REVOLVING.');
                $event->sheet->setCellValue('A21','13. Kolom Acceptation Value diisi angka biasa, tipenya general.');
                $event->sheet->setCellValue('A22','14. Jika ingin menghapus baris, pastikan menggunakan menu Clear > Clear All agar tidak ada format yang tersisa.');
                $event->sheet->setCellValue('A23','15. Jika pengisian data sudah yakin benar, silakan import file Excel ini melalui aplikasi.');
                $event->sheet->setCellValue('A24','16. Jika proses import gagal, silakan cek kembali datanya. Pastikan sudah sesuai aturan.');

                $event->sheet->getStyle('A1')->getFont()->setSize(16)->setBold(True);
                $event->sheet->getStyle('A2')->getFont()->setSize(16)->setBold(True);
                $event->sheet->getStyle('A4')->getFont()->setSize(16)->setBold(True)->getColor()->setRGB('fc0303');

                $event->sheet->styleCells(
                    'A5:A24',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  14
                        ],
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        ],
                    ]
                );
                
                $event->sheet->styleCells(
                    'A27:W27',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ],
                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Acceptation';
    }

    public function sheets(): array
    {
        return [
            'Acceptation' => new AcceptationExport(),
            'Company Project' => new CompanyProjectExport(),
            'Category' => new CategoryExport(),
            'Trading Partner' => new TradingPartnerExport(),
            'Bank' => new BankExport(),
            'NCL Drawdown' => new NCLDrawdownExport(),
            'Debt Allocation Group' => new DebtAllocationGroupExport(),
            'Term Loan' => new TermLoanExport(),
            'Revolving Loan' => new RevolvingLoanExport()
        ];
    }
}
