<?php

namespace App\Imports;

use App\Models\PenPsnProject;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class PsnProjectImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new PenPsnProject;

        $data->company_id = $row['Company ID'];
        $data->project_id = $row['Project ID'];
        $data->date = Date::excelToDateTimeObject($row['Date'])->format('Y-m-d');
        $data->target_date = Date::excelToDateTimeObject($row['Target Date'])->format('Y-m-d');
        $data->project_value = $row['Project Value'];
        $data->progress = $row['Progress'];
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 18;
    }

    public function sheets(): array
    {
        return [
            'PSN Project' => new PsnProjectImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Project ID' => 'required|integer',
            '*.Date' => 'required|integer',
            '*.Target Date' => 'nullable|integer',
            '*.Project Value' => 'nullable|integer',
            '*.Progress' => 'nullable|numeric'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Date.integer' => ':attribute format is wrong.',
            'Target Date.integer' => ':attribute format is wrong.',
        ];
    }
}
