<?php

namespace App\Imports;

use App\Models\PenKomponenDalamNegeri;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class KomponenDalamNegeriImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new PenKomponenDalamNegeri;

        $data->company_id = $row['Company ID'];
        $data->project_id = $row['Project ID'];
        $data->pen_project_category_id = $row['Project Category ID'];
        $data->date = Date::excelToDateTimeObject($row['Date'])->format('Y-m-d');
        $data->nilai_proyek = $row['Project Value'];
        $data->tingkat_komponen_dalam_negeri = $row['Tingkat Komponen Dalam Negeri'];
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 18;
    }

    public function sheets(): array
    {
        return [
            'Tingkat Komponen Dalam Negeri' => new KomponenDalamNegeriImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Project ID' => 'required|integer',
            '*.Project Category ID' => 'required|integer',
            '*.Date' => 'required|integer',
            '*.Project Value' => 'nullable|integer',
            '*.Tingkat Komponen Dalam Negeri' => 'nullable|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Date.integer' => ':attribute format is wrong.'
        ];
    }
}
