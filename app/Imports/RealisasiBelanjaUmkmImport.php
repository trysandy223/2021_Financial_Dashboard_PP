<?php

namespace App\Imports;

use App\Models\PenRealisasiBelanjaUmkm;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class RealisasiBelanjaUmkmImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new PenRealisasiBelanjaUmkm;

        $data->company_id = $row['Company ID'];
        $data->project_id = $row['Project ID'];
        $data->date = Date::excelToDateTimeObject($row['Date'])->format('Y-m-d');
        $data->sistem_padi = $row['Sistem Padi'];
        $data->sistem_sendiri = $row['Sistem Milik Sendiri'];
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 18;
    }

    public function sheets(): array
    {
        return [
            'Realisasi Belanja UMKM' => new RealisasiBelanjaUmkmImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Project ID' => 'required|integer',
            '*.Date' => 'required|integer',
            '*.Sistem Padi' => 'nullable|integer',
            '*.Sistem Milik Sendiri' => 'nullable|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Date.integer' => ':attribute format is wrong.'
        ];
    }
}
