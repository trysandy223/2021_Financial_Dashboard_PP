<?php

namespace App\Imports;

use App\Models\PenKontrakJumlahProyek;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class KontrakJumlahProyekImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new PenKontrakJumlahProyek;

        $data->company_id = $row['Company ID'];
        $data->date = Date::excelToDateTimeObject($row['Date'])->format('Y-m-d');
        $data->psn_project_total = $row['PSN Project Total'];
        $data->psn_project_value = $row['PSN Project Value'];
        $data->non_psn_project_total = $row['Non PSN Project Total'];
        $data->non_psn_project_value = $row['Non PSN Project Value'];
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 17;
    }

    public function sheets(): array
    {
        return [
            'Perolehan Kontrak Jumlah Proyek' => new KontrakJumlahProyekImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Date' => 'required|integer',
            '*.PSN Project Total' => 'nullable|integer',
            '*.PSN Project Value' => 'nullable|integer',
            '*.Non PSN Project Total' => 'nullable|integer',
            '*.Non PSN Project Value' => 'nullable|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Date.integer' => ':attribute format is wrong.',
        ];
    }
}
