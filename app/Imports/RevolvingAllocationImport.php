<?php

namespace App\Imports;

use App\Models\FinancialRevolvingAllocation;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Str;

class RevolvingAllocationImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new FinancialRevolvingAllocation;

        $data->posting_date = Date::excelToDateTimeObject($row['Posting Date'])->format('Y-m-d');
        $data->company_id = $row['Company ID'];
        $data->project_id = $row['Project ID'];
        $data->project_name = $row['Project Name'];
        $data->category_id = $row['Category ID'];
        $data->category_name = $row['Category Name'];
        $data->trading_partner_id = $row['Trading Partner ID'];
        $data->trading_partner_name = $row['Trading Partner Name'];
        $data->revolving_drawdown_id = $row['Revolving Drawdown ID'];
        $data->revolving_drawdown_name = $row['Revolving Drawdown Name'];
        $data->debt_allocation_group_id = $row['Debt Allocation Group ID'];
        $data->debt_allocation_group_name = $row['Debt Allocation Group Name'];
        $data->result_allocation_name = $row['Result Allocation Name'];
        $data->value = $row['Allocation Value'];
        $data->reference_id = Str::random(10);
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 26;
    }

    public function sheets(): array
    {
        return [
            'Revolving Allocation' => new RevolvingAllocationImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Project ID' => 'required|integer',
            '*.Project Name' => 'required|string',
            '*.Category ID' => 'required|integer',
            '*.Category Name' => 'required|string',
            '*.Trading Partner ID' => 'nullable|integer',
            '*.Trading Partner Name' => 'nullable|string',
            '*.Posting Date' => 'required|integer',
            '*.Revolving Drawdown ID' => 'required|integer',
            '*.Revolving Drawdown Name' => 'required|string',
            '*.Debt Allocation Group ID' => 'nullable|integer',
            '*.Debt Allocation Group Name' => 'nullable|string',
            '*.Result Allocation Name' => 'nullable|string',
            '*.Allocation Value' => 'nullable|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Posting Date.integer' => ':attribute format is wrong.',
        ];
    }
}
