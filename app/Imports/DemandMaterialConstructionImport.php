<?php

namespace App\Imports;

use App\Models\PenDemandMaterialConstruction;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class DemandMaterialConstructionImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{    
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new PenDemandMaterialConstruction;

        $data->company_id = $row['Company ID'];
        $data->project_id = $row['Project ID'];
        $data->material_id = $row['Material ID'];
        $data->date = Date::excelToDateTimeObject($row['Date'])->format('Y-m-d');
        $data->demand_total = $row['Demand Total'];
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 18;
    }

    public function sheets(): array
    {
        return [
            'Demand Material Construction' => new DemandMaterialConstructionImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Project ID' => 'required|integer',
            '*.Material ID' => 'required|integer',
            '*.Date' => 'required|integer',
            '*.Demand Total' => 'nullable|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Date.integer' => ':attribute format is wrong.',
        ];
    }
}