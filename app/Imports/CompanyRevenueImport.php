<?php

namespace App\Imports;

use App\Models\PenCompanyRevenue;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class CompanyRevenueImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new PenCompanyRevenue;

        $data->company_id = $row['Company ID'];
        $data->business_segment_id = $row['Business Segment ID'];
        $data->date = Date::excelToDateTimeObject($row['Date'])->format('Y-m-d');
        $data->total_revenue = $row['Total Revenue'];
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 18;
    }

    public function sheets(): array
    {
        return [
            'Pendapatan Usaha' => new CompanyRevenueImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Business Segment ID' => 'required|integer',
            '*.Date' => 'required|integer',
            '*.Total Revenue' => 'nullable|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Date.integer' => ':attribute format is wrong.',
        ];
    }
}
