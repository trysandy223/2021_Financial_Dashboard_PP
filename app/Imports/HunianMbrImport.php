<?php

namespace App\Imports;

use App\Models\PenHunianMbr;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class HunianMbrImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new PenHunianMbr;

        $data->company_id = $row['Company ID'];
        $data->project_id = $row['Project ID'];
        $data->date = Date::excelToDateTimeObject($row['Date'])->format('Y-m-d');
        $data->unit_total = $row['Unit Total'];
        $data->unit_value = $row['Unit Value'];
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 18;
    }

    public function sheets(): array
    {
        return [
            'Penyediaan Hunian MBR' => new HunianMbrImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Project ID' => 'required|integer',
            '*.Date' => 'required|integer',
            '*.Unit Total' => 'nullable|integer',
            '*.Unit Value' => 'nullable|integer',
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Date.integer' => ':attribute format is wrong.',
        ];
    }
}
