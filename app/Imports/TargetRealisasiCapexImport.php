<?php

namespace App\Imports;

use App\Models\PenTargetRealisasiCapex;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class TargetRealisasiCapexImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new PenTargetRealisasiCapex;

        $data->company_id = $row['Company ID'];
        $data->project_id = $row['Project ID'];
        $data->date = Date::excelToDateTimeObject($row['Date'])->format('Y-m-d');
        $data->nilai_equity = $row['Nilai Equity'];
        $data->nilai_loan = $row['Nilai Loan'];
        $data->pekerjaan_fisik = $row['Pekerjaan Fisik'];
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 17;
    }

    public function sheets(): array
    {
        return [
            'Target Realisasi Capex' => new TargetRealisasiCapexImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Project ID' => 'required|integer',
            '*.Date' => 'required|integer',
            '*.Nilai Equity' => 'nullable|integer',
            '*.Nilai Loan' => 'nullable|integer',
            '*.Pekerjaan Fisik' => 'nullable|numeric'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Date.integer' => ':attribute format is wrong.'
        ];
    }
}
