<?php

namespace App\Imports;

use App\Models\FinancialLiquidity;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Str;

class LiquidityImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new FinancialLiquidity;

        $data->company_id = $row['Company ID'];
        $data->project_id = $row['Project ID'];
        $data->project_name = $row['Project Name'];
        $data->category_id = $row['Category ID'];
        $data->category_name = $row['Category Name'];
        $data->flag_cf_id = $row['Flag CF ID'];
        $data->flag_cf_name = $row['Flag CF Name'];
        $data->data_period_id = $row['Data Period ID'];
        $data->data_period_name = $row['Data Period Name'];
        $data->bank_id = $row['Bank ID'];
        $data->bank_name = $row['Bank Name'];
        $data->posting_date = Date::excelToDateTimeObject($row['Posting Date'])->format('Y-m-d');
        $data->trading_partner = $row['Trading Partner'];
        $data->plan_date = $row['Plan Date'];
        $data->value = $row['Value'];
        $data->reference_id = Str::random(10);
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 23;
    }

    public function sheets(): array
    {
        return [
            'Liquidity' => new LiquidityImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Project ID' => 'required|integer',
            '*.Project Name' => 'required|string',
            '*.Category ID' => 'required|integer',
            '*.Category Name' => 'required|string',
            '*.Flag CF ID' => 'nullable|integer',
            '*.Flag CF Name' => 'nullable|string',
            '*.Data Period ID' => 'required|integer',
            '*.Data Period Name' => 'required|string',
            '*.Bank ID' => 'nullable|integer',
            '*.Bank Name' => 'nullable|string',
            '*.Posting Date' => 'required|integer',
            '*.Trading Partner' => 'nullable|string',
            '*.Plan Date' => 'nullable|string',
            '*.Value' => 'required|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Posting Date.integer' => ':attribute format is wrong.',
        ];
    }
}
