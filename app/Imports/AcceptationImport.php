<?php

namespace App\Imports;

use App\Models\FinancialAcceptation;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Str;

class AcceptationImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new FinancialAcceptation;

        $data->posting_date = Date::excelToDateTimeObject($row['Posting Date'])->format('Y-m-d');
        $data->company_id = $row['Company ID'];
        $data->ncl_drawdown_id = $row['NCL Drawdown ID'];
        $data->ncl_drawdown_name = $row['NCL Drawdown Name'];
        $data->project_id = $row['Project ID'];
        $data->project_name = $row['Project Name'];
        $data->category_id = $row['Category ID'];
        $data->category_name = $row['Category Name'];
        $data->trading_partner_id = $row['Trading Partner ID'];
        $data->trading_partner_name = $row['Trading Partner Name'];
        $data->bank_id = $row['Bank ID'];
        $data->bank_name = $row['Bank Name'];
        $data->debt_type_id = $row['Debt Type ID'];
        $data->debt_type_name = $row['Debt Type Name'];
        $data->debt_allocation_group_id = $row['Debt Allocation Group ID'];
        $data->debt_allocation_group_name = $row['Debt Allocation Group Name'];
        $data->result_allocation_name = $row['Result Allocation Name'];
        $data->value = $row['Acceptation Value'];
        $data->reference_id = Str::random(10);
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 27;
    }

    public function sheets(): array
    {
        return [
            'Acceptation' => new AcceptationImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.NCL Drawdown ID' => 'required|integer',
            '*.NCL Drawdown Name' => 'required|string',
            '*.Project ID' => 'required|integer',
            '*.Project Name' => 'required|string',
            '*.Category ID' => 'required|integer',
            '*.Category Name' => 'required|string',
            '*.Posting Date' => 'required|integer',
            '*.Maturity Date' => 'nullable|integer',
            '*.Trading Partner ID' => 'nullable|integer',
            '*.Trading Partner Name' => 'nullable|string',
            '*.Bank ID' => 'required|integer',
            '*.Bank Name' => 'required|string',
            '*.Debt Type ID' => 'required|integer',
            '*.Debt Type Name' => 'required|string',
            '*.Debt Allocation Group ID' => 'nullable|integer',
            '*.Debt Allocation Group Name' => 'nullable|string',
            '*.Result Allocation Name' => 'nullable|string',
            '*.Acceptation Value' => 'nullable|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Posting Date.integer' => ':attribute format is wrong.',
            'Maturity Date.integer' => ':attribute format is wrong.',
        ];
    }
}
