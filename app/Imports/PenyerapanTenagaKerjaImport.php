<?php

namespace App\Imports;

use App\Models\PenPenyerapanTenagaKerja;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class PenyerapanTenagaKerjaImport implements ToModel, WithHeadingRow, WithMultipleSheets, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        HeadingRowFormatter::default('none');

        $data = new PenPenyerapanTenagaKerja;

        $data->company_id = $row['Company ID'];
        $data->project_id = $row['Project ID'];
        $data->date = Date::excelToDateTimeObject($row['Date'])->format('Y-m-d');
        $data->pkwt = $row['PKWT'];
        $data->pkwtt = $row['PKWTT'];
        $data->tenaga_konstruksi = $row['Tenaga Konstruksi'];
        $data->created_by = 1;

        $data->save();
    }

    public function headingRow(): int
    {
        return 17;
    }

    public function sheets(): array
    {
        return [
            'Penyerapan Tenaga Kerja' => new PenyerapanTenagaKerjaImport()
        ];
    }

    public function rules(): array
    {
        return [
            '*.Company ID' => 'required|integer',
            '*.Project ID' => 'required|integer',
            '*.Date' => 'required|integer',
            '*.PKWT' => 'nullable|integer',
            '*.PKWTT' => 'nullable|integer',
            '*.Tenaga Konstruksi' => 'nullable|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'Date.integer' => ':attribute format is wrong.',
        ];
    }
}
