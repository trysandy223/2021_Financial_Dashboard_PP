<?php

namespace App\Http\Controllers\Pens;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Builder;
use App\Exports\TargetRealisasiCapexExport;
use App\Exports\TargetRealisasiCapexTemplateExport;
use App\Imports\TargetRealisasiCapexImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\{PenTargetRealisasiCapex, Project, Company};
use Auth;

class TargetRealisasiCapexController extends Controller
{
    function __construct()
    {
        $this->model = PenTargetRealisasiCapex::class;
        $this->routePath = 'pens::target_realisasi_capexes';
        $this->prefix = 'pens.target_realisasi_capexes';
        $this->pageName = 'Target Realisasi Capex Data';
        $this->pageDescription = 'Target Realisasi Capex data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;
        $data['companies'] = Company::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['projects'] = Project::select('id', 'name')->get();

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view($this->prefix.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create '.$this->pageName;
        $data['data'] = new $this->model;
        $data['company'] = Auth::user()->company()->first();
        // $data['projects'] = Project::select('id', 'name')->get();

        return view($this->prefix.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);

        try {
            // start insert data
            $data = new $this->model;
            $data->company_id = $request->company_id;
            $data->project_id = $request->project_id;
            $data->date = $request->date;
            $data->nilai_equity = $request->nilai_equity;
            $data->nilai_loan = $request->nilai_loan;
            $data->pekerjaan_fisik = $request->pekerjaan_fisik;
            $data->created_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been added.');

            if ($request->get('submit')) {
                return redirect()->route($this->routePath. ".index")->with('success', __("Success to create data!"));
            } else if ($request->get('save')) {
                return back()->with('success', __("Success to create data!"));
            }
            
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $this->model::findOrFail($id);
        $data['company'] = Auth::user()->company()->first();
        $data['projects'] = Project::select('id', 'name')->get();

        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date' => 'required',
            'nilai_equity' => 'required',
            'nilai_loan' => 'required',
            'pekerjaan_fisik' => 'required'
        ]);

        try {
            // start update data
            $data = $this->model::findOrFail($id);
            $data->company_id = $request->company_id;
            if ($request->project_id != '') {
                $data->project_id = $request->project_id;
            }
            $data->date = $request->date;
            $data->nilai_equity = $request->nilai_equity;
            $data->nilai_loan = $request->nilai_loan;
            $data->pekerjaan_fisik = $request->pekerjaan_fisik;
            $data->updated_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been updated.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to update data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to edit data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->model::findOrFail($id);
            $data->delete();

            activity()->performedOn($data)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data! It may be that the data you want to delete is being used in another table.");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = $this->model::with(['company', 'project'])
            ->whereIn('company_id', Auth::user()->allCompanyId())
            ->orderBy('updated_at', 'desc')->get();

        $sum_nilai_equity = $data->sum('nilai_equity');
        $sum_nilai_loan = $data->sum('nilai_loan');

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('company', function($data) {
                $company = $data->company->name;

                return $company;
            })
            ->addColumn('project', function($data) {
                $project = $data->project->name;

                return $project;
            })
            ->addColumn('nilai_capex', function($data) {
                $nilai_capex = $data->nilai_equity + $data->nilai_loan;

                return $nilai_capex;
            })
            ->editColumn('pekerjaan_fisik', function($data) {
                $pekerjaan_fisik = $data->pekerjaan_fisik . ' %';

                return $pekerjaan_fisik;
            })
            ->addColumn('sum_nilai_equity', function() use ($sum_nilai_equity) {
                return $sum_nilai_equity;
            })
            ->addColumn('sum_nilai_loan', function() use ($sum_nilai_loan) {
                return $sum_nilai_loan;
            })
            ->addColumn('sum_nilai_capex', function() use ($sum_nilai_equity, $sum_nilai_loan) {
                return $sum_nilai_equity + $sum_nilai_loan;
            })
            ->addColumn('updated_at', function($data) {
                $updated_at = date('d F Y H:i', strtotime($data->updated_at));

                return $updated_at;
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('pen-target-realisasi-capex-edit')) {
                    $button .= '<button type="button" onClick="editData('.$data->id.')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('pen-target-realisasi-capex-delete')) {
                    $button .= '<button type="button" onClick="deleteData('.$data->id.')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->rawColumns(['updated_at', 'company', 'project', 'nilai_capex', 'pekerjaan_fisik', 'action'])
            ->make(true);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validation(Request $request)
    {
        $id = request('id') ?: 'NULL';

        $this->validate($request, [
            'project_id' => 'required',
            'date' => 'required',
            'nilai_equity' => 'required',
            'nilai_loan' => 'required',
            'pekerjaan_fisik' => 'required'
        ]);
    }

    /**
     * Export resource to excel.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        try {
            $date = date('d-m-Y');
            $fileName = $date.'-target-realisasi-capex-data.xlsx';

            return Excel::download(new TargetRealisasiCapexExport, $fileName);
        } catch (\Exception $ex) {
            $message = __("Failed to export data.");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        } 
    }

    /**
     * Import a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        try {
            $this->validate($request, [
                'file_name' => 'required|mimes:xls,xlsx'
            ]);

            Excel::import(new TargetRealisasiCapexImport, $request->file_name);

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to import data!"));
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            
            return redirect()->route($this->routePath. ".index")->with('failures', $failures);
        }
    }

    /**
     * Export template resource to excel.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadTemplate()
    {
        try {
            $date = date('d-m-Y');
            $fileName = $date.'-template-import-target-realisasi-capex.xlsx';

            return Excel::download(new TargetRealisasiCapexTemplateExport, $fileName);
        } catch (\Exception $ex) {
            $message = __("Failed to export data.");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        } 
    }
}
