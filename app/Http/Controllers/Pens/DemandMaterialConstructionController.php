<?php

namespace App\Http\Controllers\Pens;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Builder;
use App\Exports\DemandMaterialConstructionExport;
use App\Exports\DemandMaterialConstructionTemplateExport;
use App\Imports\DemandMaterialConstructionImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\{PenDemandMaterialConstruction, Project, Company, Material};
use Auth;

class DemandMaterialConstructionController extends Controller
{
    function __construct()
    {
        $this->model = PenDemandMaterialConstruction::class;
        $this->routePath = 'pens::demand_material_constructions';
        $this->prefix = 'pens.demand_material_constructions';
        $this->pageName = 'Demand Material Construction Data';
        $this->pageDescription = 'Demand Material Construction data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;
        $data['companies'] = Company::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['projects'] = Project::select('id', 'name')->get();
        $data['materials'] = Material::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view($this->prefix.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create '.$this->pageName;
        $data['data'] = new $this->model;
        $data['company'] = Auth::user()->company()->first();
        // $data['projects'] = Project::select('id', 'name')->get();
        $data['materials'] = Material::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();

        return view($this->prefix.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);

        try {
            // check if data exist, based on month and company
            if ($this->model::whereMonth('date', date('m', strtotime($request->date)))->exists() && $this->model::where('company_id', $request->company_id)->exists()) {
                $data = $this->model::whereMonth('date', date('m', strtotime($request->date)))->where('company_id', $request->company_id)->firstOrFail();

                return redirect()->route($this->routePath. ".index")->with(['id' => $data->id, 'exist' => 'Already Exist']);
            } else {
                // start insert data
                $data = new $this->model;
                $data->company_id = $request->company_id;
                $data->project_id = $request->project_id;
                $data->material_id = $request->material_id;
                $data->date = $request->date;
                $data->demand_total = $request->demand_total;
                $data->created_by = Auth::id();
                $data->save();

                activity()->performedOn($data)->log($this->pageName. ' has been added.');

                if ($request->get('submit')) {
                    return redirect()->route($this->routePath. ".index")->with('success', __("Success to create data!"));
                } else if ($request->get('save')) {
                    return back()->with('success', __("Success to create data!"));
                }
            }
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $this->model::findOrFail($id);
        $data['company'] = Auth::user()->company()->first();
        $data['projects'] = Project::select('id', 'name')->get();
        $data['materials'] = Material::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();

        // get data satuan from data material
        $data['satuan'] = Material::whereHas('demandMaterialConstruction', function(Builder $query) use($id) {
            $query->where('id', $id);
        })->select('satuan')->first();

        // get data province and city from data project
        $data['location'] = Project::whereHas('demandMaterialConstruction', function(Builder $query) use($id) {
            $query->where('id', $id);
        })->select('province', 'city')->first();

        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'material_id' => 'required',
            'date' => 'required',
            'demand_total' => 'required'
        ]);

        try {
            // start update data
            $data = $this->model::findOrFail($id);
            $data->company_id = $request->company_id;
            if ($request->project_id != '') {
                $data->project_id = $request->project_id;
            }
            $data->material_id = $request->material_id;
            $data->date = $request->date;
            $data->demand_total = $request->demand_total;
            $data->updated_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been updated.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to update data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to edit data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->model::findOrFail($id);
            $data->delete();

            activity()->performedOn($data)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data! It may be that the data you want to delete is being used in another table.");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = $this->model::with(['company', 'project', 'material'])
            ->whereIn('company_id', Auth::user()->allCompanyId())
            ->orderBy('updated_at', 'desc')->get();

        $sum_demand_total = $data->sum('demand_total');

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('company', function($data) {
                $company = $data->company->name;

                return $company;
            })
            ->addColumn('project', function($data) {
                $project = $data->project->name;

                return $project;
            })
            ->addColumn('province', function($data) {
                $province = $data->project->province;

                return $province;
            })
            ->addColumn('city', function($data) {
                $city = $data->project->city;

                return $city;
            })
            ->addColumn('material', function($data) {
                $material = $data->material->name;

                return $material;
            })
            ->addColumn('satuan', function($data) {
                $satuan = $data->material->satuan;

                return $satuan;
            })
            ->addColumn('sum_demand_total', function() use ($sum_demand_total) {
                return $sum_demand_total;
            })
            ->addColumn('updated_at', function($data) {
                $updated_at = date('d F Y H:i', strtotime($data->updated_at));

                return $updated_at;
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('pen-demand-material-construction-edit')) {
                    $button .= '<button type="button" onClick="editData('.$data->id.')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('pen-demand-material-construction-delete')) {
                    $button .= '<button type="button" onClick="deleteData('.$data->id.')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->rawColumns(['updated_at', 'company', 'project', 'province', 'city', 'material', 'satuan', 'action'])
            ->make(true);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validation(Request $request)
    {
        $id = request('id') ?: 'NULL';

        $this->validate($request, [
            'project_id' => 'required',
            'material_id' => 'required',
            'date' => 'required',
            'demand_total' => 'required'
        ]);
    }

    /**
     * Display a listing of the project data.
     *
     * @return \Illuminate\Http\Response
     */
    protected function getProject(Request $request)
    {
        $data = Project::select('province', 'city')->where('id', $request->project_id)->first();

        return response()->json($data);
    }

    /**
     * Display a listing of the material data.
     *
     * @return \Illuminate\Http\Response
     */
    protected function getMaterial(Request $request)
    {
        $data = Material::select('satuan')->where('id', $request->material_id)->first();

        return response()->json($data);
    }

    /**
     * Export resource to excel.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        try {
            $date = date('d-m-Y');
            $fileName = $date.'-demand-material-construction-data.xlsx';

            return Excel::download(new DemandMaterialConstructionExport, $fileName);
        } catch (\Exception $ex) {
            $message = __("Failed to export data.");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        } 
    }

    /**
     * Import a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        try {
            $this->validate($request, [
                'file_name' => 'required|mimes:xls,xlsx'
            ]);

            Excel::import(new DemandMaterialConstructionImport, $request->file_name);

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to import data!"));
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            
            return redirect()->route($this->routePath. ".index")->with('failures', $failures);
        }
    }

    /**
     * Export template resource to excel.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadTemplate()
    {
        try {
            $date = date('d-m-Y');
            $fileName = $date.'-template-import-demand-material-construction.xlsx';

            return Excel::download(new DemandMaterialConstructionTemplateExport, $fileName);
        } catch (\Exception $ex) {
            $message = __("Failed to export data.");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        } 
    }
}
