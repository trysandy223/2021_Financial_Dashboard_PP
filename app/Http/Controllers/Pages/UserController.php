<?php

namespace App\Http\Controllers\Pages;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use App\Models\{User, Company};
use DB;

class UserController extends Controller
{
    function __construct()
    {
        $this->model = User::class;
        $this->routePath = 'pages::users';
        $this->prefix = 'pages.users';
        $this->pageName = 'User Data';
        $this->pageDescription = 'User data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view($this->prefix.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create '.$this->pageName;
        $data['data'] = new $this->model;
        $data['roles'] = Role::select('id', 'name')->get();

        return view($this->prefix.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);

        try {
            $data = new $this->model;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->password = bcrypt($request->password);
            if(!empty($request->status)) {
                $data->status = 1;
            } else {
                $data->status = 0;
            }
            $data->save();

            // save to pivot table user_companies
            $data->company()->attach($request->company);

            // save to pivot table model_has_roles
            $data->assignRole($request->role);

            activity()->performedOn($data)->log($this->pageName. ' has been added.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to create data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $this->model::findOrFail($id);
        $data['roles'] = Role::select('id', 'name')->get();

        // get company id by user session
        $companyId = $data['data']->company->count();
        if ($companyId == 0) {
            $data['companies'] = null;
        } else {
            $data['companies'] = Company::select('id', 'name')->where('status', 1)->where('id', $data['data']->company[0]->id)->orderBy('level', 'asc')->first();
        }
        
        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validation($request);

        try {
            $data = $this->model::findOrFail($id);
            $data->name = $request->name;
            $data->email = $request->email;
            if(!empty($request->password)) {
                $data->password = bcrypt($request->password);
            }
            if(!empty($request->status)) {
                $data->status = 1;
            } else {
                $data->status = 0;
            }
            $data->save();

            // update pivot table user_companies
            DB::table('user_companies')->where('user_id', $id)->delete();
            $data->company()->attach($request->company);

            // update pivot table model_has_roles
            DB::table('model_has_roles')->where('model_id', $id)->delete();
            $data->assignRole($request->role);

            activity()->performedOn($data)->log($this->pageName. ' has been updated.');
            
            return redirect()->route($this->routePath. ".index")->with('success', __("Success to update data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->model::findOrFail($id);
            $data->delete();

            activity()->performedOn($data)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data!");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = DB::table('users')
            ->leftJoin('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
            ->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->leftJoin('user_companies', 'users.id', '=', 'user_companies.user_id')
            ->leftJoin('companies', 'user_companies.company_id', '=', 'companies.id')
            ->select('users.id as id', 'users.name as name', 'users.email as email', 'users.status as status', 'roles.name as roles', 'companies.name as companies')
            ->orderBy('users.created_at', 'desc')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('status', function($data) {
                if ($data->status == 1) {
                    $status = '<span class="label label-inline label-light-success font-weight-bold">Active</span>';
                } else {
                    $status = '<span class="label label-inline label-light-danger font-weight-bold">Inactive</span>';
                }

                return $status;
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('master-edit')) {
                    $button .= '<button type="button" onClick="editData('.$data->id.')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('master-delete')) {
                    $button .= '<button type="button" onClick="deleteData('.$data->id.')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validation(Request $request)
    {
        $id = request('id') ?: 'NULL';

        if ($request->isMethod('POST')) {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$id,
                'company' => 'required',
                'role' => 'required',
                'password' => 'required|min:8|same:confirm-password'
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$id,
                'company' => 'required',
                'role' => 'required'
            ]);
        }
    }

    /**
     * Show the form for editing the personal resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPersonal()
    {
        $data['page_title'] = 'Edit User';
        $data['data'] = $this->model::findOrFail(Auth::id());
        
        return view($this->prefix.'.edit-personal', $data);
    }

    /**
     * Update the specified personal in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePersonal(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.Auth::id()
        ]);

        if(!empty($request->password)) {
            $this->validate($request, [
                'password' => 'required|min:8|same:confirm-password'
            ]);
        }

        try {
            $data = $this->model::findOrFail(Auth::id());
            $data->name = $request->name;
            $data->email = $request->email;
            if(!empty($request->password)) {
                $data->password = bcrypt($request->password);
            }
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been updated.');
            
            return redirect()->route("pages::dashboard")->with('success', __("Success to update your personal information!"));
        } catch (\Exception $ex) {
            $message = __("Failed to update your personal information!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route("pages::dashboard")->with('failed', $message);
        }
    }
}
