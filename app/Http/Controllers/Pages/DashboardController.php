<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $page_title = 'Dashboard Menu';
        $page_description = 'Some description for the page';

        return view('pages.dashboards.index', compact('page_title', 'page_description'));
    }
}