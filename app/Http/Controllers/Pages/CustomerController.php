<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Helpers\CustomerSelectBox;
use App\Models\{Customer, AccountGroup, ReconsiliationAccount, PlanningGroup, TermsOfPayment, HouseBank, Currency, TaxClassification};
use Auth;

class CustomerController extends Controller
{
    function __construct()
    {
        $this->model = Customer::class;
        $this->routePath = 'pages::customers';
        $this->prefix = 'pages.customers';
        $this->pageName = 'Customer Data';
        $this->pageDescription = 'Customer data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view($this->prefix.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create '.$this->pageName;
        $data['data'] = new $this->model;
        $data['acc_groups'] = AccountGroup::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['rec_accounts'] = ReconsiliationAccount::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['plan_groups'] = PlanningGroup::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['terms_payments'] = TermsOfPayment::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['house_banks'] = HouseBank::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['currencies'] = Currency::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['tax_classes'] = TaxClassification::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();

        return view($this->prefix.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);

        try {
            // separate id and name value from selectbox
            $accGroup = explode(',', $request->account_group_id, 2);
            $recAccount = explode(',', $request->reconsiliation_account_id, 2);
            $planGroup = explode(',', $request->planning_group_id, 2);
            $termPayment = explode(',', $request->terms_of_payment_id, 2);
            $houseBank = explode(',', $request->house_bank_id, 2);
            $currency = explode(',', $request->currency_id, 2);
            $taxClass = explode(',', $request->tax_classification_id, 2);

            // if id is empty, fill with 0 value
            (empty($accGroup[0])) ? $accGroup[0] = 0 : '';
            (empty($recAccount[0])) ? $recAccount[0] = 0 : '';
            (empty($planGroup[0])) ? $planGroup[0] = 0 : '';
            (empty($termPayment[0])) ? $termPayment[0] = 0 : '';
            (empty($houseBank[0])) ? $houseBank[0] = 0 : '';
            (empty($currency[0])) ? $currency[0] = 0 : '';
            (empty($taxClass[0])) ? $taxClass[0] = 0 : '';

            // if name is empty, fill with - value
            (empty($accGroup[1])) ? $accGroup[1] = '-' : '';
            (empty($recAccount[1])) ? $recAccount[1] = '-' : '';
            (empty($planGroup[1])) ? $planGroup[1] = '-' : '';
            (empty($termPayment[1])) ? $termPayment[1] = '-' : '';
            (empty($houseBank[1])) ? $houseBank[1] = '-' : '';
            (empty($currency[1])) ? $currency[1] = '-' : '';
            (empty($taxClass[1])) ? $taxClass[1] = '-' : '';

            // start insert data
            $data = new $this->model;
            $data->company_id = Auth::user()->company()->first()->id;
            $data->title = $request->title;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->search_term = $request->search_term;
            $data->street = $request->street;
            $data->district = $request->district;
            $data->city = $request->city;
            $data->region = $request->region;
            $data->country = $request->country;
            $data->postal_code = $request->postal_code;
            $data->phone = $request->phone;
            $data->mobile_phone = $request->mobile_phone;
            $data->extension_1 = $request->extension_1;
            $data->extension_2 = $request->extension_2;
            $data->fax = $request->fax;
            $data->email = $request->email;
            $data->npwp = $request->npwp;
            $data->credit_limit = $request->credit_limit;
            $data->account_group_id = $accGroup[0];
            $data->account_group_name = $accGroup[1];
            $data->reconsiliation_account_id = $recAccount[0];
            $data->reconsiliation_account_name = $recAccount[1];
            $data->planning_group_id = $planGroup[0];
            $data->planning_group_name = $planGroup[1];
            $data->terms_of_payment_id = $termPayment[0];
            $data->terms_of_payment_name = $termPayment[1];
            $data->house_bank_id = $houseBank[0];
            $data->house_bank_name = $houseBank[1];
            $data->currency_id = $currency[0];
            $data->currency_name = $currency[1];
            $data->tax_classification_id = $taxClass[0];
            $data->tax_classification_name = $taxClass[1];
            $data->created_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been added.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to create data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $this->model::findOrFail($id);
        $data['acc_groups'] = AccountGroup::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['rec_accounts'] = ReconsiliationAccount::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['plan_groups'] = PlanningGroup::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['terms_payments'] = TermsOfPayment::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['house_banks'] = HouseBank::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['currencies'] = Currency::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['tax_classes'] = TaxClassification::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();

        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validation($request);

        try {
            // separate id and name value from selectbox
            $accGroup = explode(',', $request->account_group_id, 2);
            $recAccount = explode(',', $request->reconsiliation_account_id, 2);
            $planGroup = explode(',', $request->planning_group_id, 2);
            $termPayment = explode(',', $request->terms_of_payment_id, 2);
            $houseBank = explode(',', $request->house_bank_id, 2);
            $currency = explode(',', $request->currency_id, 2);
            $taxClass = explode(',', $request->tax_classification_id, 2);

            // if id is empty, fill with 0 value
            (empty($accGroup[0])) ? $accGroup[0] = 0 : '';
            (empty($recAccount[0])) ? $recAccount[0] = 0 : '';
            (empty($planGroup[0])) ? $planGroup[0] = 0 : '';
            (empty($termPayment[0])) ? $termPayment[0] = 0 : '';
            (empty($houseBank[0])) ? $houseBank[0] = 0 : '';
            (empty($currency[0])) ? $currency[0] = 0 : '';
            (empty($taxClass[0])) ? $taxClass[0] = 0 : '';

            // if name is empty, fill with - value
            (empty($accGroup[1])) ? $accGroup[1] = '-' : '';
            (empty($recAccount[1])) ? $recAccount[1] = '-' : '';
            (empty($planGroup[1])) ? $planGroup[1] = '-' : '';
            (empty($termPayment[1])) ? $termPayment[1] = '-' : '';
            (empty($houseBank[1])) ? $houseBank[1] = '-' : '';
            (empty($currency[1])) ? $currency[1] = '-' : '';
            (empty($taxClass[1])) ? $taxClass[1] = '-' : '';

            // start insert data
            $data = $this->model::findOrFail($id);
            $data->title = $request->title;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->search_term = $request->search_term;
            $data->street = $request->street;
            $data->district = $request->district;
            $data->city = $request->city;
            $data->region = $request->region;
            $data->country = $request->country;
            $data->postal_code = $request->postal_code;
            $data->phone = $request->phone;
            $data->mobile_phone = $request->mobile_phone;
            $data->extension_1 = $request->extension_1;
            $data->extension_2 = $request->extension_2;
            $data->fax = $request->fax;
            $data->email = $request->email;
            $data->npwp = $request->npwp;
            $data->credit_limit = $request->credit_limit;
            $data->account_group_id = $accGroup[0];
            $data->account_group_name = $accGroup[1];
            $data->reconsiliation_account_id = $recAccount[0];
            $data->reconsiliation_account_name = $recAccount[1];
            $data->planning_group_id = $planGroup[0];
            $data->planning_group_name = $planGroup[1];
            $data->terms_of_payment_id = $termPayment[0];
            $data->terms_of_payment_name = $termPayment[1];
            $data->house_bank_id = $houseBank[0];
            $data->house_bank_name = $houseBank[1];
            $data->currency_id = $currency[0];
            $data->currency_name = $currency[1];
            $data->tax_classification_id = $taxClass[0];
            $data->tax_classification_name = $taxClass[1];
            $data->updated_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been updated.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to update data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to edit data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->model::findOrFail($id);
            $data->delete();

            activity()->performedOn($data)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data!");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = $this->model::orderBy('created_at', 'desc');

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('updated_at', function($data) {
                $updated_at = date('d F Y H:i', strtotime($data->updated_at));

                return $updated_at;
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('master-edit')) {
                    $button .= '<button type="button" onClick="editData('.$data->id.')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('master-delete')) {
                    $button .= '<button type="button" onClick="deleteData('.$data->id.')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->rawColumns(['updated_at', 'action'])
            ->make(true);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validation(Request $request)
    {
        $id = request('id') ?: 'NULL';

        $this->validate($request, [
            'code' => 'required|unique:customers,code,'.$id,
            'name' => 'required|unique:customers,name,'.$id
        ]);
    }

    /**
     * Display customer list for autocomplete name.
     *
     * @return \Illuminate\Http\Response
     */
    function list(Request $request)
    {
        $input = $request->all();

        $data = $this->model::select("name")->where("name", "like", "%{$input['query']}%")->orderBy("name", "asc")->get();
   
        return response()->json($data);
    }

    public function selectBox(Request $request) {
        $search = !empty($request->search) ? $request->search : "";

        $page = $request->page;
        $start = 0;
        $length = 10;
        if($page > 1) {
            $start = ($page * $length) - 10;
        }

        // set filter
        $filter = array();
        // set filter by company
        if(!empty($request->company_id)) {
            $filter[0]['key'] = "company_id";
            $filter[0]['condition'] = "=";
            $filter[0]['value'] = $request->company_id;
        }

        $dataSelect = new CustomerSelectBox;
        $data = $dataSelect->data($search, $filter, $start, $length, $request->text_format);
        $total = $dataSelect->totalResult($search, $filter);
        
        $result = array();
        $result['results'] = $data;
        $result['total_count'] = $total;

        echo json_encode($result);
    }
}
