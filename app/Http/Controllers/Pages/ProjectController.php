<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\{Project, Company, Customer, ProjectType, ProjectStrategic, PaymentType, BusinessSegment};
use App\Helpers\ProjectSelectBox;
use Auth;

class ProjectController extends Controller
{
    function __construct()
    {
        $this->model = Project::class;
        $this->routePath = 'pages::projects';
        $this->prefix = 'pages.projects';
        $this->pageName = 'Project Data';
        $this->pageDescription = 'Project data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view($this->prefix.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create '.$this->pageName;
        $data['data'] = new $this->model;
        $data['project_types'] = ProjectType::select('id', 'name')->get();
        $data['project_strategics'] = ProjectStrategic::select('id', 'name')->get();
        $data['payment_types'] = PaymentType::select('id', 'name')->get();
        $data['business_segments'] = BusinessSegment::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['customers'] = Customer::select('id', 'name')->get();

        return view($this->prefix.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);

        try {
            // separate id and name value from selectbox
            $projectType = explode(',', $request->project_type_id, 2);
            $projectStrategic = explode(',', $request->project_strategic_id, 2);
            $paymentType = explode(',', $request->payment_type_id, 2);
            $businessSegment = explode(',', $request->business_segment_id, 2);
            $customer = explode(',', $request->customer_id, 2);

            // if id is empty, fill with 0 value
            (empty($projectType[0])) ? $projectType[0] = 0 : '';
            (empty($projectStrategic[0])) ? $projectStrategic[0] = 0 : '';
            (empty($paymentType[0])) ? $paymentType[0] = 0 : '';
            (empty($businessSegment[0])) ? $businessSegment[0] = 0 : '';
            (empty($customer[0])) ? $customer[0] = 0 : '';

            // if name is empty, fill with - value
            (empty($projectType[1])) ? $projectType[1] = '-' : '';
            (empty($projectStrategic[1])) ? $projectStrategic[1] = '-' : '';
            (empty($paymentType[1])) ? $paymentType[1] = '-' : '';
            (empty($businessSegment[1])) ? $businessSegment[1] = '-' : '';
            (empty($customer[1])) ? $customer[1] = '-' : '';

            // start insert data
            $data = new $this->model;
            $data->company_id = $request->company_id;
            $data->unit_id = $request->unit_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->manager = $request->manager;
            $data->start_date = $request->start_date;
            $data->finish_date = $request->finish_date;
            $data->rab = $request->rab;
            $data->rkn = $request->rkn;
            $data->province = $request->province;
            $data->city = $request->city;
            $data->project_type_id = $projectType[0];
            $data->project_type_name = $projectType[1];
            $data->project_strategic_id = $projectStrategic[0];
            $data->project_strategic_name = $projectStrategic[1];
            $data->payment_type_id = $paymentType[0];
            $data->payment_type_name = $paymentType[1];
            $data->business_segment_id = $businessSegment[0];
            $data->business_segment_name = $businessSegment[1];
            $data->customer_id = $customer[0];
            $data->customer_name = $customer[1];
            $data->created_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been added.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to create data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $this->model::findOrFail($id);
        $data['companies'] = Company::select('id', 'name')->where('status', 1)->where('id', $data['data']->company_id)->orderBy('level', 'asc')->first();
        $data['project_types'] = ProjectType::select('id', 'name')->get();
        $data['project_strategics'] = ProjectStrategic::select('id', 'name')->get();
        $data['payment_types'] = PaymentType::select('id', 'name')->get();
        $data['business_segments'] = BusinessSegment::select('id', 'name')->where('status', 1)->orderBy('sorting', 'asc')->get();
        $data['customers'] = Customer::select('id', 'name')->get();
        $data['units'] = Company::select('id', 'name')->where('status', 1)->where('id', $data['data']->unit_id)->orderBy('level', 'asc')->first();

        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validation($request);

        try {
            // separate id and name value from selectbox
            $projectType = explode(',', $request->project_type_id, 2);
            $projectStrategic = explode(',', $request->project_strategic_id, 2);
            $paymentType = explode(',', $request->payment_type_id, 2);
            $businessSegment = explode(',', $request->business_segment_id, 2);
            $customer = explode(',', $request->customer_id, 2);

            // if id is empty, fill with 0 value
            (empty($projectType[0])) ? $projectType[0] = 0 : '';
            (empty($projectStrategic[0])) ? $projectStrategic[0] = 0 : '';
            (empty($paymentType[0])) ? $paymentType[0] = 0 : '';
            (empty($businessSegment[0])) ? $businessSegment[0] = 0 : '';
            (empty($customer[0])) ? $customer[0] = 0 : '';

            // if name is empty, fill with - value
            (empty($projectType[1])) ? $projectType[1] = '-' : '';
            (empty($projectStrategic[1])) ? $projectStrategic[1] = '-' : '';
            (empty($paymentType[1])) ? $paymentType[1] = '-' : '';
            (empty($businessSegment[1])) ? $businessSegment[1] = '-' : '';
            (empty($customer[1])) ? $customer[1] = '-' : '';

            // start insert data
            $data = $this->model::findOrFail($id);
            $data->company_id = $request->company_id;
            $data->unit_id = $request->unit_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->manager = $request->manager;
            $data->start_date = $request->start_date;
            $data->finish_date = $request->finish_date;
            $data->rab = $request->rab;
            $data->rkn = $request->rkn;
            $data->province = $request->province;
            $data->city = $request->city;
            $data->project_type_id = $projectType[0];
            $data->project_type_name = $projectType[1];
            $data->project_strategic_id = $projectStrategic[0];
            $data->project_strategic_name = $projectStrategic[1];
            $data->payment_type_id = $paymentType[0];
            $data->payment_type_name = $paymentType[1];
            $data->business_segment_id = $businessSegment[0];
            $data->business_segment_name = $businessSegment[1];
            $data->customer_id = $customer[0];
            $data->customer_name = $customer[1];
            $data->updated_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been updated.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to update data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to edit data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->model::findOrFail($id);
            $data->delete();

            activity()->performedOn($data)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data! It may be that the data you want to delete is being used in another table.");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = $this->model::with('company')->whereIn('company_id', Auth::user()->allCompanyId())->orderBy('updated_at', 'desc');

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('company', function($data) {
                $company = $data->company->name;

                return $company;
            })
            ->addColumn('updated_at', function($data) {
                $updated_at = date('d F Y H:i', strtotime($data->updated_at));

                return $updated_at;
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('master-edit')) {
                    $button .= '<button type="button" onClick="editData('.$data->id.')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('master-delete')) {
                    $button .= '<button type="button" onClick="deleteData('.$data->id.')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->rawColumns(['updated_at', 'company', 'action'])
            ->make(true);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validation(Request $request)
    {
        $id = request('id') ?: 'NULL';

        $this->validate($request, [
            'code' => 'required',
            'name' => 'required'
        ]);
    }

    public function selectBox(Request $request) {
        $search = !empty($request->search) ? $request->search : "";

        $page = $request->page;
        $start = 0;
        $length = 10;
        if($page > 1) {
            $start = ($page * $length) - 10;
        }

        // set filter
        $filter = array();
        // set filter by company
        if(!empty($request->company_id)) {
            $filter[0]['key'] = "company_id";
            $filter[0]['condition'] = "=";
            $filter[0]['value'] = $request->company_id;
        }
        // set filter by unit
        if(!empty($request->unit_id)) {
            $filter[1]['key'] = "unit_id";
            $filter[1]['condition'] = "=";
            $filter[1]['value'] = $request->unit_id;
        }
        // set filter by company
        if(!empty($request->inherit_company_id)) {
            $filter[3]['key'] = "inherit_company_id";
            $filter[3]['condition'] = "in";
            $filter[3]['value'] = $request->inherit_company_id;
        }

        $dataSelect = new ProjectSelectBox;
        $data = $dataSelect->data($search, $filter, $start, $length, $request->text_format);
        $total = $dataSelect->totalResult($search, $filter);
        
        $result = array();
        $result['results'] = $data;
        $result['total_count'] = $total;

        echo json_encode($result);
    }
}
