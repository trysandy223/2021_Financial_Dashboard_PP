<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Company;
use App\Helpers\CompanySelectBox;
use Auth;
use DB;

class CompanyController extends Controller
{
    function __construct()
    {
        $this->model = Company::class;
        $this->routePath = 'pages::companies';
        $this->prefix = 'pages.companies';
        $this->pageName = 'Company Data';
        $this->pageDescription = 'Company data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view($this->prefix.'.index', $data);
    }

    public function getTree(Request $request){
        $order_dir = !empty($request->parent) ? $request->parent : "";

        $data = DB::table('companies')
            ->select("companies.*")
            ->where('status', 1);

        if($order_dir != "#"){
            $data->where('parent_id', '=', $order_dir);
        }else{
            $data->where('parent_id', '=', null);
        }
        $data = $data->get();

        $result = array();
        if(!empty($data)){
            foreach($data as $key=>$val){
                $btnDelete = "";

                $result[$key]['id'] = $val->id;
                $result[$key]['icon'] = "fa fa-folder text-success";
                if($order_dir != "#"){
                    $result[$key]['type'] = "root";
                }

                //check child
                $child = DB::table('companies');
                $child->where('parent_id', '=', $val->id);
                $child = $child->count();

                if($child == 0){
                    $result[$key]['children'] = false;
                    $btnDelete = '<a href="#" onClick="deleteData('.$val->id.')"><i class="far fa-trash-alt text-danger ml-2"></i></a>';
                }else{
                    $result[$key]['children'] = true;
                }

                $result[$key]['text'] = $val->name.' <a href="#" onClick="detailData('.$val->id.')"><i class="fab fa-sistrix text-success ml-2 mr-2"></i></a> <a href="#" onClick="editData('.$val->id.')"><i class="fas fa-pencil-alt text-success ml-2 mr-2"></i></a> '.$btnDelete;
            }
        }

        echo json_encode($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create '.$this->pageName;
        $data['data'] = new $this->model;
        $data['companies'] = $this->model::select('id', 'name')->where('status', 1)->orderBy('level', 'asc')->get();

        return view($this->prefix.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);

        try {
            $data = new $this->model;

            // Check if user not selecting parent combobox, then set to 0
            if (empty($request->parent_id)) {
                $request->parent_id = 0;
            }

            // Get level, parent level, and max level data depends on parent id
            $parentLevel = $this->model::getLevelParent($request->parent_id);
            $level = $this->model::getLevel($request->parent_id);
            $maxLevel = $this->model::getMaxLevel();

            // Set value for level and sorting fields
            if (!empty($level[0])) {
                $exp = explode('.', $level[0]->level);
                $arrSize = sizeof($exp);
                $arrLast = (int)$arrSize - 1;
                $right = $exp[$arrLast];
                $right_new = $right + 1;
            } else {
                $exp = explode('.', $parentLevel[0]->level);
                $arrSize = sizeof($exp);
                $arrLast = (int)$arrSize - 1;
                $right = $exp[$arrLast];
                $right_new = $right;
            }

            $expMax = explode('.', $maxLevel[0]->level);
            $left = $expMax[0];
            $left_new = (int)$left + 1;

            if(!empty($parentLevel) && $parentLevel[0]->level != 0) {
                if($arrLast != 0) {
                    for($i = 0; $i < $arrLast; $i++) {
                        $text1[] = $exp[$i];
                    }
                } else {
                    $text1[] = $right_new;
                }

                $text1 = implode('.', $text1);

                if(!empty($level[0])) {
                    $value['level'] = $text1.'.'.$right_new;
                    $value['sortBy'] = (int)$level[0]->sorting + 1;
                } else {
                    $value['level'] = $parentLevel[0]->level.'.1';
                    $value['sortBy'] = 1;
                }

            } else {
                $value['level'] = $left_new;
                $value['sortBy'] = $left_new;
            }

            // Set value for parent level field
            $exp = explode('.', $value['level']);
            $arrSize = sizeof($exp);
            $value['parent_level'] = $arrSize;

            // Start to insert data to table
            $data->parent_id = $request->parent_id;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->description = $request->description;
            $data->type = $request->type;
            $data->portion = $request->portion;
            if(!empty($request->is_project)) {
                $data->is_project = 1;
            } else {
                $data->is_project = 0;
            }
            if(!empty($request->status)) {
                $data->status = 1;
            } else {
                $data->status = 0;
            }
            $data->level = $value['level'];
            $data->parent_level = $value['parent_level'];
            $data->sorting = $value['sortBy'];
            $data->created_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been added.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to create data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model::findOrFail($id);
        if($data->is_project == 1) {
            $data->is_project = '<span class="label label-inline label-light-success font-weight-bold">Yes</span>';
        }else{
            $data->is_project = '<span class="label label-inline label-light-danger font-weight-bold">No</span>';
        }

        if($data->status == 1) {
            $data->status = '<span class="label label-inline label-light-success font-weight-bold">Active</span>';
        }else{
            $data->status = '<span class="label label-inline label-light-danger font-weight-bold">Inactive</span>';
        }

        $data->name_parent = "-";
        if((!empty($data->parent_id)) || ($data->parent_id != "")){
            $parent = $this->model::where('id', $data->parent_id)->first();
            $data->name_parent = $parent->name;
        }

        echo json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $this->model::findOrFail($id);

        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validation($request);

        try {
            $data = $this->model::findOrFail($id);
            $data->code = $request->code;
            $data->name = $request->name;
            $data->description = $request->description;
            $data->type = $request->type;
            $data->portion = $request->portion;
            if(!empty($request->is_project)) {
                $data->is_project = 1;
            } else {
                $data->is_project = 0;
            }
            if(!empty($request->status)) {
                $data->status = 1;
            } else {
                $data->status = 0;
            }
            $data->updated_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been updated.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to update data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to edit data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->model::findOrFail($id);
            $data->delete();

            activity()->performedOn($data)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data!");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = $this->model::with('parent')->orderBy('created_at', 'desc')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('parent', function($data) {
                $parent = $data->parent;

                if (!empty($parent)) {
                    return $parent->name;
                } else {
                    return '-';
                }
            })
            ->addColumn('status', function($data) {
                if ($data->status == 1) {
                    $status = '<span class="label label-inline label-light-success font-weight-bold">Active</span>';
                } else {
                    $status = '<span class="label label-inline label-light-danger font-weight-bold">Inactive</span>';
                }

                return $status;
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('master-edit')) {
                    $button .= '<button type="button" onClick="editData('.$data->id.')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('master-delete')) {
                    $button .= '<button type="button" onClick="deleteData('.$data->id.')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->rawColumns(['parent', 'status', 'action'])
            ->make(true);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validation(Request $request)
    {
        $id = request('id') ?: 'NULL';

        $this->validate($request, [
            'code' => 'required|unique:companies,code,'.$id,
            'name' => 'required|unique:companies,name,'.$id,
            'type' => 'required',
            'portion' => 'required'
        ]);
    }


    public function selectBox(Request $request) {
        $search = !empty($request->search) ? $request->search : "";

        $page = $request->page;
        $start = 0;
        $length = 10;
        if($page > 1) {
            $start = ($page * $length) - 10;
        }

        // set filter
        $filter = array();
        if(!empty($request->is_project)) {
            $filter[0]['key'] = "is_project";
            $filter[0]['condition'] = "=";
            $filter[0]['value'] = $request->is_project;
        }
        if(!empty($request->status)) {
            $filter[1]['key'] = "status";
            $filter[1]['condition'] = "=";
            $filter[1]['value'] = $request->status;
        }

        $dataSelect = new CompanySelectBox;
        $data = $dataSelect->data($search, $filter, $start, $length, $request->text_format);
        $total = $dataSelect->totalResult($search);
        
        $result = array();
        $result['results'] = $data;
        $result['total_count'] = $total;

        echo json_encode($result);
    }
}
