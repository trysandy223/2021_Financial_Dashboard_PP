<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\LogActivityExport;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Activitylog\Models\Activity;
use App\Models\User;
use DB, Auth;

class LogActivityController extends Controller
{
    function __construct()
    {
        $this->model = Activity::class;
        $this->routePath = 'pages::log_activities';
        $this->prefix = 'pages.log_activities';
        $this->pageName = 'Log Activity Data';
        $this->pageDescription = 'Log Activity data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;
        $data['users'] = User::select('id', 'name')->where('status', 1)->orderBy('created_at', 'desc')->get();

        if ($request->ajax()) {
            return $this->datatables($request);
        }

        return view($this->prefix.'.index', $data);
    }

    public function show($id)
    {
        $data['page_title'] = 'Detail Log Activity';
        $data['page_description'] = 'Detail ' . $this->pageDescription;
        $data['data'] = Activity::findOrFail($id);

        return view($this->prefix.'.show', $data);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables($request)
    {
        $data = Activity::query();

        if (!empty($request->activity)) {
            $data->where('description', 'like', '%'.$request->activity.'%');
        }
        
        if (!empty($request->user)) {
            $data->where('causer_id', $request->user);
        }

        if(!empty($request->fromDate && $request->toDate)) {
            $data->whereBetween(DB::raw('DATE(created_at)'), array(date('Y-m-d', strtotime($request->fromDate)), date('Y-m-d', strtotime($request->toDate))));
        }

        $data->orderBy('created_at', 'desc');

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('user_name', function($data) {
                $user_name = $data->causer->name;

                return $user_name;
            })
            ->addColumn('date', function($data) {
                $date = date('d M Y H:i:s', strtotime($data->created_at));

                return $date;
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('master-list')) {
                    $button .= '<button type="button" onClick="detailData('.$data->id.')" class="btn btn-primary btn-sm" title="Detail Data">Detail</button>';
                }

                return $button;
            })
            ->rawColumns(['date', 'user_name', 'action'])
            ->make(true);
    }

    /**
     * Export resource to excel.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        try {
            $date = date('d-m-Y');
            $fileName = $date.'-log-activity-data.xlsx';

            return Excel::download(new LogActivityExport, $fileName);
        } catch (\Exception $ex) {
            $message = __("Failed to export data.");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        } 
    }
}
