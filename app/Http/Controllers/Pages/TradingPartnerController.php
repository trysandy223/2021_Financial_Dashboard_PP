<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Helpers\TradingPartnerSelectBox;
use App\Models\TradingPartner;
use Auth;

class TradingPartnerController extends Controller
{
    function __construct()
    {
        $this->model = TradingPartner::class;
        $this->routePath = 'pages::trading_partners';
        $this->prefix = 'pages.trading_partners';
        $this->pageName = 'Trading Partner Data';
        $this->pageDescription = 'Trading Partner data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view($this->prefix.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create '.$this->pageName;
        $data['data'] = new $this->model;

        return view($this->prefix.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);

        try {
            $data = new $this->model;
            $data->code = $request->code;
            $data->name = $request->name;
            $data->description = $request->description;
            $data->type = $request->type;
            $data->sorting = $request->sorting;
            if(!empty($request->status)) {
                $data->status = 1;
            } else {
                $data->status = 0;
            }
            $data->created_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been added.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to create data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $this->model::findOrFail($id);

        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validation($request);

        try {
            $data = $this->model::findOrFail($id);
            $data->code = $request->code;
            $data->name = $request->name;
            $data->description = $request->description;
            $data->type = $request->type;
            $data->sorting = $request->sorting;
            if(!empty($request->status)) {
                $data->status = 1;
            } else {
                $data->status = 0;
            }
            $data->updated_by = Auth::id();
            $data->save();

            activity()->performedOn($data)->log($this->pageName. ' has been updated.');

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to update data!"));
        } catch (\Exception $ex) {
            $message = __("Failed to edit data!");

            Log::error($message, [
                'request' => $request->all(),
                'trace' => $ex->getTraceAsString()
            ]);

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->model::findOrFail($id);
            $data->delete();

            activity()->performedOn($data)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data!");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = $this->model::orderBy('created_at', 'desc');

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('status', function($data) {
                if ($data->status == 1) {
                    $status = '<span class="label label-inline label-light-success font-weight-bold">Active</span>';
                } else {
                    $status = '<span class="label label-inline label-light-danger font-weight-bold">Inactive</span>';
                }

                return $status;
            })
            ->addColumn('updated_at', function($data) {
                $updated_at = date('d F Y H:i', strtotime($data->updated_at));

                return $updated_at;
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('master-edit')) {
                    $button .= '<button type="button" onClick="editData('.$data->id.')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('master-delete')) {
                    $button .= '<button type="button" onClick="deleteData('.$data->id.')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->rawColumns(['updated_at', 'status', 'action'])
            ->make(true);
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validation(Request $request)
    {
        $id = request('id') ?: 'NULL';

        $this->validate($request, [
            'code' => 'required|unique:trading_partners,code,'.$id,
            'name' => 'required|unique:trading_partners,name,'.$id,
            'sorting' => 'required',
            'type' => 'required'
        ]);
    }

    public function selectBox(Request $request) {
        $search = !empty($request->search) ? $request->search : "";

        $page = $request->page;
        $start = 0;
        $length = 10;
        if($page > 1) {
            $start = ($page * $length) - 10;
        }

        // set filter
        $filter = array();
        // set filter by company
        if(!empty($request->type)) {
            $filter[0]['key'] = "type";
            $filter[0]['condition'] = "=";
            $filter[0]['value'] = $request->type;
        }
        if(!empty($request->status)) {
            $filter[1]['key'] = "status";
            $filter[1]['condition'] = "=";
            $filter[1]['value'] = $request->status;
        }

        $dataSelect = new TradingPartnerSelectBox;
        $data = $dataSelect->data($search, $filter, $start, $length, $request->text_format);
        $total = $dataSelect->totalResult($search, $filter);
        
        $result = array();
        $result['results'] = $data;
        $result['total_count'] = $total;

        echo json_encode($result);
    }
}
