<?php

namespace App\Http\Controllers\Financials;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialCashConversionCycle;
use App\Models\Company;
use App\Models\Project;
use App\Models\Category;
use App\Models\FlagCcc;
use Auth;

class CashConversionCycleController extends Controller
{
    function __construct()
    {
        $this->model = FinancialCashConversionCycle::class;
        $this->routePath = 'financials::cash_conversion_cycles';
        $this->prefix = 'financials.cash_conversion_cycles';
        $this->pageName = 'Cash Conversion Cycle Data';
        $this->pageDescription = 'Cash Conversion Cycle Data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;

        if ($request->ajax()) {
            return $this->datatables();
        }

        $data['flags'] = FlagCcc::where('status', 1)->orderBy('sorting')->get();
        $data['companies'] = Company::whereIn('id', Auth::user()->allCompanyId())->where('status', 1)->orderBy('sorting')->get();
        $data['categories'] = Category::where('name', 'ACTUAL')->orWhere('name', 'PLAN')->orderBy('sorting')->get();
        if (in_array(Auth::user()->company->first()->id ?? 0, config('const.sub_parent_company'))) {
            $data['projects'] = Project::where('unit_id', Auth::user()->company->first()->id)->get();
        } else {
            $data['projects'] = Project::whereIn('company_id', Auth::user()->allCompanyId())->get();
        }

        return view($this->prefix.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Input '.$this->pageName;
        $data['data'] = new $this->model;

        return view($this->prefix.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $id;

        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->model::where('reference_id', $id)->delete();

            $model = new FinancialCashConversionCycle();
            activity()->performedOn($model)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data!");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = $this->model::with('company')->whereIn('company_id', Auth::user()->allCompanyId())->select('financial_cash_conversion_cycles.*');

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('company', function ($data) {
                return $data->company->name;
            })
            ->editColumn('value', function($data) {
                return in_array($data->flag_ccc_name, ['DSO', 'DPO', 'ITO']) ? $data->value : "Rp".number_format($data->value);
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('financial-ccc-edit')) {
                    $button .= '<button type="button" onClick="editData(\''.$data->reference_id.'\')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('financial-ccc-delete')) {
                    $button .= '<button type="button" onClick="deleteData(\''.$data->reference_id.'\')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->order(function ($query) {
                $query->orderBy('updated_at', 'desc');
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
