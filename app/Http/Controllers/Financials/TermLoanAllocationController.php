<?php

namespace App\Http\Controllers\Financials;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Exports\TermLoanAllocationExport;
use App\Imports\TermLoanAllocationImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\{FinancialTermLoanAllocation, DebtAllocationGroup, Bank, Company, Project, DebtPayment, SourceGroup};
use Auth;

class TermLoanAllocationController extends Controller
{
    function __construct()
    {
        $this->model = FinancialTermLoanAllocation::class;
        $this->routePath = 'financials::term_loan_allocations';
        $this->prefix = 'financials.term_loan_allocations';
        $this->pageName = 'Term Loan Allocation Data';
        $this->pageDescription = 'Term Loan Allocation Data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;

        if ($request->ajax()) {
            return $this->datatables();
        }

        $data['sources'] = SourceGroup::get();
        $data['status'] = DebtPayment::get();
        $data['banks'] = Bank::where('status', 1)->orderBy('sorting')->get();
        $data['groups'] = DebtAllocationGroup::get();
        $data['companies'] = Company::whereIn('id', Auth::user()->allCompanyId())->where('status', 1)->orderBy('sorting')->get();
        if (in_array(Auth::user()->company->first()->id ?? 0, config('const.sub_parent_company'))) {
            $data['projects'] = Project::where('unit_id', Auth::user()->company->first()->id)->get();
        } else {
            $data['projects'] = Project::whereIn('company_id', Auth::user()->allCompanyId())->get();
        }

        return view($this->prefix.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Input '.$this->pageName;
        $data['data'] = new $this->model;

        return view($this->prefix.'.create', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reverse()
    {
        $data['page_title'] = 'Reverse '.$this->pageName;
        $data['data'] = new $this->model;

        return view($this->prefix.'.reverse', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $id;

        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->model::where('reference_id', $id)->delete();

            $model = new FinancialTermLoanAllocation();
            activity()->performedOn($model)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data!");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = $this->model::with('company')->whereIn('company_id', Auth::user()->allCompanyId())->select('financial_term_loan_allocations.*');

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('company', function ($data) {
                return $data->company->name;
            })
            ->editColumn('value', function($data) {
                return "Rp".number_format($data->value);
            })
            ->editColumn('total', function($data) {
                return "Rp".number_format($data->total);
            })
            ->editColumn('is_reverse', function($data) {
                return $data->is_reverse ? 'Reverse' : '';
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('financial-term-loan-allocation-edit')) {
                    $button .= '<button type="button" onClick="editData(\''.$data->reference_id.'\')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('financial-term-loan-allocation-delete')) {
                    $button .= '<button type="button" onClick="deleteData(\''.$data->reference_id.'\')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->order(function ($query) {
                $query->orderBy('updated_at', 'desc');
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Export resource to excel.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        try {
            $date = date('d-m-Y');
            $fileName = $date.'-template-import-term-loan-allocation.xlsx';

            return Excel::download(new TermLoanAllocationExport, $fileName);
        } catch (\Exception $ex) {
            $message = __("Failed to export data.");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        } 
    }

    /**
     * Import a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        try {
            $this->validate($request, [
                'file_name' => 'required|mimes:xls,xlsx'
            ]);

            Excel::import(new TermLoanAllocationImport, $request->file_name);

            return redirect()->route($this->routePath. ".index")->with('success', __("Success to import data!"));
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            return redirect()->route($this->routePath. ".index")->with('failures', $failures);
        }
    }
}
