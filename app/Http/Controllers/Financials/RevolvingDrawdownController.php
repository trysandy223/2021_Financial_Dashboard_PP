<?php

namespace App\Http\Controllers\Financials;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Helpers\RevolvingDrawdownSelectBox;
use App\Models\FinancialRevolvingDrawdown;
use App\Models\FinancialRevolvingAllocation;
use App\Models\{Company, Bank, Project, SourceGroup};
use Auth;

class RevolvingDrawdownController extends Controller
{
    function __construct()
    {
        $this->model = FinancialRevolvingDrawdown::class;
        $this->routePath = 'financials::revolving_drawdowns';
        $this->prefix = 'financials.revolving_drawdowns';
        $this->pageName = 'Revolving Drawdown Data';
        $this->pageDescription = 'Revolving Drawdown Data '.config('app.name');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page_title'] = $this->pageName;
        $data['page_description'] = $this->pageDescription;

        if ($request->ajax()) {
            return $this->datatables();
        }

        $data['sources'] = SourceGroup::get();
        $data['banks'] = Bank::where('status', 1)->orderBy('sorting')->get();
        $data['companies'] = Company::whereIn('id', Auth::user()->allCompanyId())->where('status', 1)->orderBy('sorting')->get();
        if (in_array(Auth::user()->company->first()->id ?? 0, config('const.sub_parent_company'))) {
            $data['projects'] = Project::where('unit_id', Auth::user()->company->first()->id)->get();
        } else {
            $data['projects'] = Project::whereIn('company_id', Auth::user()->allCompanyId())->get();
        }

        return view($this->prefix.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Input '.$this->pageName;
        $data['data'] = new $this->model;

        return view($this->prefix.'.create', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reverse()
    {
        $data['page_title'] = 'Reverse '.$this->pageName;
        $data['data'] = new $this->model;

        return view($this->prefix.'.reverse', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit '.$this->pageName;
        $data['data'] = $id;

        return view($this->prefix.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $datas = $this->model::where('reference_id', $id)->get();
            foreach ($datas as $data) {
                FinancialRevolvingAllocation::where('revolving_drawdown_id', $data->id)->delete();
                $data->delete();
            }

            $model = new FinancialRevolvingDrawdown();
            activity()->performedOn($model)->log($this->pageName. ' has been deleted.');
        } catch (\Exception $ex) {
            $message = __("Failed to delete data!");

            return redirect()->route($this->routePath. ".index")->with('failed', $message);
        }
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function datatables()
    {
        $data = $this->model::with('company')->whereIn('company_id', Auth::user()->allCompanyId())->select('financial_revolving_drawdowns.*');

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('company', function ($data) {
                return $data->company->name;
            })
            ->editColumn('value', function($data) {
                return "Rp".number_format($data->value);
            })
            ->editColumn('plafon_value', function($data) {
                return "Rp".number_format($data->plafon_value);
            })
            ->editColumn('is_reverse', function($data) {
                return $data->is_reverse ? 'Reverse' : '';
            })
            ->addColumn('action', function($data) {
                $button = '';

                if (Auth::user()->can('financial-revolving-drawdown-edit')) {
                    $button .= '<button type="button" onClick="editData(\''.$data->reference_id.'\')" class="btn btn-light-warning btn-icon btn-sm mr-2" title="Edit Data"><i class="flaticon-edit"></i></button>';
                }
                if (Auth::user()->can('financial-revolving-drawdown-delete')) {
                    $button .= '<button type="button" onClick="deleteData(\''.$data->reference_id.'\')" class="btn btn-light-danger btn-icon btn-sm" title="Delete Data"><i class="flaticon-delete"></i></button>';
                }

                return $button;
            })
            ->order(function ($query) {
                $query->orderBy('updated_at', 'desc');
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function selectBox(Request $request) {
        $search = !empty($request->search) ? $request->search : "";

        $page = $request->page;
        $start = 0;
        $length = 10;
        if($page > 1) {
            $start = ($page * $length) - 10;
        }

        // set filter
        $filter = array();
        // set filter by company
        if(!empty($request->company_id)) {
            $filter[0]['key'] = "company_id";
            $filter[0]['condition'] = "=";
            $filter[0]['value'] = $request->company_id;
        }
        // set filter by transaction type
        if(!empty($request->transaction_type)) {
            $filter[1]['key'] = "transaction_type";
            $filter[1]['condition'] = "=";
            $filter[1]['value'] = $request->transaction_type;
        }
        // set filter by is_reverse
        if(!empty($request->is_reverse) || $request->is_reverse == '0') {
            $filter[2]['key'] = "is_reverse";
            $filter[2]['condition'] = "=";
            $filter[2]['value'] = $request->is_reverse;
        }

        $dataSelect = new RevolvingDrawdownSelectBox;
        $data = $dataSelect->data($search, $filter, $start, $length);
        $total = $dataSelect->totalResult($search, $filter);
        
        $result = array();
        $result['results'] = $data;
        $result['total_count'] = $total;

        echo json_encode($result);
    }
}
