<?php

namespace App\Http\Livewire\Financials\CashConversionCycles;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialCashConversionCycle;

class Edit extends Component
{
    public $datas = [];

    public function mount($reference)
    {
        $datas = FinancialCashConversionCycle::where('reference_id', $reference)->get();
        foreach ($datas as $dt) {
            $this->datas[$dt->id] = [
                'value' => $dt->value,
                'flag' => $dt->flag_ccc_name,
                'type' => in_array($dt->flag_ccc_name, ['DSO', 'DPO', 'ITO']) ? 'numeric' : 'rupiah'
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.value' => __('Value'),
        ]);

        try {
            foreach ($this->datas as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);

                $data = FinancialCashConversionCycle::find($key);
                $data->value = $valueMask;
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Cash Conversion Cycle Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::cash_conversion_cycles.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::cash_conversion_cycles.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.cash-conversion-cycles.edit');
    }
}
