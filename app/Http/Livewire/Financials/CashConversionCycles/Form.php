<?php

namespace App\Http\Livewire\Financials\CashConversionCycles;

use DB;
use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\Project;
use App\Models\Company;
use App\Models\Category;
use App\Models\FlagCcc;
use App\Models\TradingPartner;

class Form extends Component
{
    public $model;
    public $company;
    public $project;
    public $partner;
    public $category;
    public $posting_date;
    public $is_parent_company;
    public $unit_id;

    // Variables for store select data
    public $selectCompany;
    public $flags;
    public $partners;
    public $categories;
    public $datas = [];

    public function mount($model)
    {
        $this->model = $model;

        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::cash_conversion_cycles.index");
        }
        
        $this->company = $company;
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->unit_id = $company->id;
            $this->company = $company->parent;
        }

        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();
        $this->categories = Category::where('name', 'ACTUAL')->orWhere('name', 'PLAN')->get();
    }

    public function updatedCategory($value)
    {
        $flags = [];
        $category = $this->categories->where('id', $value)->first()->name ?? '';

        if ($category == 'ACTUAL') {
            $type = 'rupiah';
            $flags = ['PIUTANG USAHA', 'HUTANG USAHA', 'PERSEDIAAN'];
        } elseif ($category == 'PLAN') {
            $type = 'numeric';
            $flags = ['DSO', 'DPO', 'ITO'];
        }

        $this->datas = [];
        $this->flags = FlagCcc::whereIn('name', $flags)->where('status', 1)->orderBy('sorting')->get();
        foreach ($this->flags as $flag) {
            $this->datas[$flag->id] = ['value' => 0, 'type' => $type];
        }

        if ($type == 'rupiah') {
            $this->dispatchBrowserEvent('changeRupiahFormat');
        }
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'project' => 'required',
            'category' => 'required',
            'posting_date' => 'required',
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.value' => __('Value'),
        ]);

        try {
            DB::transaction(function () {
                $reference = \Str::random(10);
                foreach ($this->datas as $flagId => $dt) {
                    // remove point from value
                    $valueMask = str_replace('.', '', $dt['value']);

                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = Project::find($this->project)->id ?? null;
                    $data->project_name = Project::find($this->project)->name ?? null;
                    $data->category_id = $this->category;
                    $data->category_name = $this->categories->where('id', $this->category)->first()->name ?? null;
                    $data->flag_ccc_id = $flagId;
                    $data->flag_ccc_name = $this->flags->where('id', $flagId)->first()->name ?? null;
                    $data->posting_date = $this->posting_date;
                    $data->trading_partner = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->value = $valueMask ?? 0;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }
            
                activity()->performedOn($data)->log('Cash Conversion Cycle Data has been added.');
            });

            session()->flash('success', __("Success to create data!"));
            return redirect()->route("financials::cash_conversion_cycles.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::cash_conversion_cycles.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.cash-conversion-cycles.form');
    }
}
