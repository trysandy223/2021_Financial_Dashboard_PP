<?php

namespace App\Http\Livewire\Financials\ProjectProfitabilities;

use DB;
use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\Project;
use App\Models\Company;
use App\Models\Category;
use App\Models\FlagProfitability;
use App\Models\TradingPartner;

class Form extends Component
{
    public $model;
    public $company;
    public $project;
    public $partner;
    public $posting_date;
    public $is_parent_company;
    public $unit_id;

    // Variables for store select data
    public $selectCompany;
    public $flags;
    public $partners;
    public $plans = [];
    public $actuals = [];

    public function mount($model)
    {
        $this->model = $model;

        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::liquidities.index");
        }
        
        $this->company = $company;
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->unit_id = $company->id;
            $this->company = $company->parent;
        }

        $this->flags = FlagProfitability::where('status', 1)->orderBy('sorting')->get();
        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();

        foreach ($this->flags as $flag) {
            $this->plans[$flag->id] = ['value' => 0, 'flag' => $flag->name];
            $this->actuals[$flag->id] = ['value' => 0, 'flag' => $flag->name];
        }
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'project' => 'required',
            'posting_date' => 'required',
            'plans.*.value' => 'required',
            'actuals.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'plans.*.value' => __('Value'),
            'actuals.*.value' => __('Value'),
        ]);

        try {
            DB::transaction(function () {
                $reference = \Str::random(10);

                $planCategory = Category::where('name', 'PLAN')->firstOrFail();
                foreach ($this->plans as $id => $plan) {
                    // remove point from value
                    $valueMask = str_replace('.', '', $plan['value']);

                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = Project::find($this->project)->id ?? null;
                    $data->project_name = Project::find($this->project)->name ?? null;
                    $data->category_id = $planCategory->id;
                    $data->category_name = $planCategory->name;
                    $data->flag_profitability_id = $id;
                    $data->flag_profitability_name = $this->flags->where('id', $id)->first()->name ?? null;
                    $data->posting_date = $this->posting_date;
                    $data->trading_partner = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->value = $plan['flag'] == 'PROGRES PEKERJAAN' ? $plan['value'] : $valueMask;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }

                $actualCategory = Category::where('name', 'ACTUAL')->firstOrFail();
                foreach ($this->actuals as $id => $actual) {
                    // remove point from value
                    $valueMask = str_replace('.', '', $actual['value']);

                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = Project::find($this->project)->id ?? null;
                    $data->project_name = Project::find($this->project)->name ?? null;
                    $data->category_id = $actualCategory->id;
                    $data->category_name = $actualCategory->name;
                    $data->flag_profitability_id = $id;
                    $data->flag_profitability_name = $this->flags->where('id', $id)->first()->name ?? null;
                    $data->posting_date = $this->posting_date;
                    $data->trading_partner = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->value = $actual['flag'] == 'PROGRES PEKERJAAN' ? $actual['value'] : $valueMask;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }

                activity()->performedOn($data)->log('Project Profitability Data has been added.');
            });

            session()->flash('success', __("Success to create data!"));
            return redirect()->route("financials::project_profitabilities.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::project_profitabilities.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.project-profitabilities.form');
    }
}
