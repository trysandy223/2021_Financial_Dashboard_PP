<?php

namespace App\Http\Livewire\Financials\ProjectProfitabilities;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialProjectProfitability;

class Edit extends Component
{
    public $plans = [];
    public $actuals = [];

    public function mount($reference)
    {
        $plans = FinancialProjectProfitability::where('category_name', 'PLAN')->where('reference_id', $reference)->get();
        foreach ($plans as $plan) {
            $this->plans[$plan->id] = [
                'value' => $plan->value,
                'flag' => $plan->flag_profitability_name,
            ];
        }

        $actuals = FinancialProjectProfitability::where('category_name', 'ACTUAL')->where('reference_id', $reference)->get();
        foreach ($actuals as $actual) {
            $this->actuals[$actual->id] = [
                'value' => $actual->value,
                'flag' => $actual->flag_profitability_name,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'plans.*.value' => 'required',
            'actuals.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'plans.*.value' => __('Value'),
            'actuals.*.value' => __('Value'),
        ]);

        try {
            foreach ($this->plans as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);

                $data = FinancialProjectProfitability::find($key);
                $data->value = $dt['flag'] == 'PROGRES PEKERJAAN' ? $dt['value'] : $valueMask;
                $data->updated_by = Auth::id();
                $data->save();
            }

            foreach ($this->actuals as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);

                $data = FinancialProjectProfitability::find($key);
                $data->value = $dt['flag'] == 'PROGRES PEKERJAAN' ? $dt['value'] : $valueMask;
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Project Profitability Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::project_profitabilities.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::project_profitabilities.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.project-profitabilities.edit');
    }
}
