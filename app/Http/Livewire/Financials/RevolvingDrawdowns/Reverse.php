<?php

namespace App\Http\Livewire\Financials\RevolvingDrawdowns;

use Auth;
use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialRevolvingAllocation;

class Reverse extends Component
{
    public $model;
    public $company;
    public $selectCompany;
    public $is_parent_company;
    public $credit_line_detail;
    public $data;

    protected $listeners = ['confirmReverse' => 'submit'];

    public function mount($model)
    {
        $this->model = $model;
        
        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::revolving_loans.index");
        }
        
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->company = $company->parent;
        } else {
            $this->company = $company;
        }
    }

    public function updatedCreditLineDetail($value)
    {
        $this->data = $this->model->find($value);
    }

    public function submit()
    {
        $this->validate([
            // 'company' => 'required',
            'selectCompany' => 'required',
            'credit_line_detail' => 'required',
        ]);

        try {
            $data = $this->model;
            // $data->company_id = $this->company->id ?? $this->company;
            $data->company_id = $this->selectCompany;
            $data->project_id = $this->data->project_id;
            $data->project_name = $this->data->project_name;
            $data->category_id = $this->data->category_id;
            $data->category_name = $this->data->category_name;
            $data->posting_date = Carbon::parse($this->data->posting_date)->addMonth();
            $data->trading_partner_id = $this->data->trading_partner_id;
            $data->trading_partner_name = $this->data->trading_partner_name;
            $data->revolving_loan_id = $this->data->revolving_loan_id;
            $data->revolving_loan_name = $this->data->revolving_loan_name;
            $data->debt_type_id = $this->data->debt_type_id;
            $data->debt_type_name = $this->data->debt_type_name;
            $data->bank_id = $this->data->bank_id;
            $data->bank_name = $this->data->bank_name;
            $data->source_group_id = $this->data->source_group_id;
            $data->source_group_name = $this->data->source_group_name;
            $data->plafon_value = $this->data->plafon_value;
            $data->credit_line_detail = $this->data->credit_line_detail;
            $data->credit_line_opening_date = $this->data->credit_line_opening_date;
            $data->maturity_date = $this->data->maturity_date;
            $data->transaction_type = 'PEMBAYARAN';
            $data->value = 0-$this->data->value;
            $data->reference_id = $this->data->reference_id;
            $data->is_reverse = 1;
            $data->created_by = Auth::id();
            $data->save();


            $allocations = FinancialRevolvingAllocation::where('revolving_drawdown_id', $this->data->id)->where('is_reverse', 0)->get();
            foreach ($allocations as $alloc) {
                $dt = new FinancialRevolvingAllocation();
                $dt->company_id = $this->company->id ?? $this->company;
                $dt->project_id = $alloc->project_id;
                $dt->project_name = $alloc->project_name;
                $dt->category_id = $alloc->category_id;
                $dt->category_name = $alloc->category_name;
                $dt->trading_partner_id = $alloc->trading_partner_id;
                $dt->trading_partner_name = $alloc->trading_partner_name;
                $dt->revolving_drawdown_id = $alloc->revolving_drawdown_id;
                $dt->revolving_drawdown_name = $alloc->revolving_drawdown_name;
                $dt->posting_date = Carbon::parse($alloc->posting_date)->addMonth();
                $dt->debt_allocation_group_id = $alloc->debt_allocation_group_id;
                $dt->debt_allocation_group_name = $alloc->debt_allocation_group_name;
                $dt->result_allocation_name = $alloc->result_allocation_name;
                $dt->value = 0-$alloc->value;
                $dt->reference_id = $alloc->reference_id;
                $dt->is_reverse = 1;
                $dt->created_by = Auth::id();
                $dt->save();
            }

            activity()->performedOn($data)->log('Revolving Drawdown Data has been reversed.');

            session()->flash('success', __("Success to reverse data!"));

            return redirect()->route("financials::revolving_drawdowns.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);

            return redirect()->route("financials::revolving_drawdowns.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.revolving-drawdowns.reverse');
    }
}
