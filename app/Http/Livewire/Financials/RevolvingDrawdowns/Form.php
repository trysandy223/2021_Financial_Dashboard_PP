<?php

namespace App\Http\Livewire\Financials\RevolvingDrawdowns;

use DB;
use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\Bank;
use App\Models\Project;
use App\Models\Company;
use App\Models\Category;
use App\Models\DebtType;
use App\Models\DebtPayment;
use App\Models\SourceGroup;
use App\Models\TradingPartner;
use App\Models\FinancialRevolvingLoan;

class Form extends Component
{
    public $model;
    public $bank;
    public $flag;
    public $company;
    public $project;
    public $partner;
    public $category;
    public $posting_date;
    public $source_group;
    public $loan_name;
    public $plafon_value;
    public $remaining_plafon_value;
    public $is_parent_company;

    // Variables for store select data
    public $selectCompany;
    public $banks;
    public $partners;
    public $sourceGroups;
    public $debtPayments;
    public $datas = [];

    public function mount($model)
    {
        $this->model = $model;
        
        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::revolving_loans.index");
        }
        
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->company = $company->parent;
        } else {
            $this->company = $company;
        }
        
        $this->category = Category::where('name', 'ACTUAL')->first();

        if (!$this->category) {
            session()->flash('failed', __("ACTUAL category is not found"));
            return redirect()->route("financials::revolving_drawdowns.index");
        }

        $this->flag = DebtType::where('name', 'REVOLVING LOAN')->first();

        if (!$this->flag) {
            session()->flash('failed', __("REVOLVING LOAN debt type is not found"));
            return redirect()->route("financials::revolving_drawdowns.index");
        }

        $this->banks = Bank::where('status', 1)->orderBy('sorting')->get();
        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();
        $this->sourceGroups = SourceGroup::get();
        $this->debtPayments = DebtPayment::get();

        $this->addData();
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'bank' => 'required',
            'project' => 'required',
            'posting_date' => 'required',
            'source_group' => 'required',
            'loan_name' => 'required',
            'datas.*.credit_line_detail' => 'required|unique:financial_revolving_drawdowns,credit_line_detail',
            'datas.*.transaction_type' => 'required',
            'datas.*.value' => 'required'
        ], [
            // Custom error message
        ], [
            'datas.*.credit_line_detail' => __("Credit Line Detail"),
            'datas.*.transaction_type' => __("Value"),
            'datas.*.value' => __("Value"),
        ]);

        // Validate Unique Credit Line Detail
        $duplicateNames = collect($this->datas)->pluck('credit_line_detail')->duplicates()->toArray();
        if (count($duplicateNames) > 0) {
            foreach ($duplicateNames as $key => $dn) {
                $this->addError('datas.'.$key.'.credit_line_detail', __('The Credit Line Detail has already been taken.'));
            }
            return;
        }

        // Validate remaining plafon value
        $sumCair = 0;
        $sumBayar = 0;
        $maxPayment = $this->plafon_value - $this->remaining_plafon_value;
        foreach ($this->datas as $key => $dt) {
            $valueMask = str_replace('.', '', $dt['value'] ?? 0);
            if ($dt['transaction_type'] == 'PENCAIRAN') {
                $sumCair += $valueMask;
                $this->addError('datas.'.$key.'.value', __('Remaining plafon value is ' . number_format($this->remaining_plafon_value)));
            } else {
                $sumBayar += $valueMask;
                $this->addError('datas.'.$key.'.value', __('Total amount that can be paid is ' . number_format($maxPayment)));
            }
        }

        if ($sumCair > $this->remaining_plafon_value || $sumBayar > $maxPayment) {
            if (count($this->getErrorBag()) > 0) {
                return;
            }
        }

        try {
            DB::transaction(function () {
                $reference = \Str::random(10);

                foreach ($this->datas as $dt) {
                    // remove point from value
                    $valueMask = str_replace('.', '', $dt['value'] ?? 0);

                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? $this->company;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = $this->project->id ?? null;
                    $data->project_name = $this->project->name ?? null;
                    $data->category_id = $this->category->id;
                    $data->category_name = $this->category->name;
                    $data->posting_date = $this->posting_date;
                    $data->trading_partner_id = $this->partner != '' ? $this->partner : null;
                    $data->trading_partner_name = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->revolving_loan_id = FinancialRevolvingLoan::find($this->loan_name)->id ?? null;
                    $data->revolving_loan_name = FinancialRevolvingLoan::find($this->loan_name)->debt_name ?? null;
                    $data->debt_type_id = $this->flag->id;
                    $data->debt_type_name = $this->flag->name;
                    $data->bank_id = $this->bank;
                    $data->bank_name = $this->banks->where('id', $this->bank)->first()->name ?? null;
                    $data->source_group_id = $this->source_group;
                    $data->source_group_name = $this->sourceGroups->where('id', $this->source_group)->first()->name ?? null;
                    $data->plafon_value = $this->plafon_value;
                    $data->credit_line_detail = $dt['credit_line_detail'] ?? null;
                    $data->credit_line_opening_date = $dt['credit_line_opening_date'] != '' ? $dt['credit_line_opening_date'] : null;
                    $data->maturity_date = $dt['maturity_date'] != '' ? $dt['maturity_date'] : null;
                    $data->transaction_type = $dt['transaction_type'] != '' ? $dt['transaction_type'] : null;
                    $data->value = $dt['transaction_type'] == 'PEMBAYARAN' ? (0 - abs($valueMask)) : $valueMask;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }
            
                activity()->performedOn($data)->log('Revolving Drawdown Data has been added.');
            });

            session()->flash('success', __("Success to create data!"));

            return redirect()->route("financials::revolving_drawdowns.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);

            return redirect()->route("financials::revolving_drawdowns.index");
        }
    }

    public function addData()
    {
        $this->datas[] = [
            'credit_line_detail' => "",
            'credit_line_opening_date' => "",
            'maturity_date' => "",
            'transaction_type' => "",
            'value' => 0,
        ];

        $this->dispatchBrowserEvent('changeRupiahFormat');
    }

    public function subData($i)
    {
        unset($this->datas[$i]);
    }

    public function updatedLoanName($value)
    {
        $data = FinancialRevolvingLoan::find($value);
        $this->project = Project::find($data->project_id);
        $this->plafon_value = $data->plafon_value;
        $this->remaining_plafon_value = $data->remainingPlafonValue();
    }

    public function render()
    {
        return view('livewire.financials.revolving-drawdowns.form');
    }
}