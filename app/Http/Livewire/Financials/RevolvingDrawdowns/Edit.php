<?php

namespace App\Http\Livewire\Financials\RevolvingDrawdowns;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\DebtPayment;
use App\Models\FinancialRevolvingDrawdown;

class Edit extends Component
{
    public $datas = [];
    public $plafon_value;
    public $remaining_plafon_value;
    public $init_sum_value = 0;

    public function mount($reference)
    {
        $datas = FinancialRevolvingDrawdown::where('reference_id', $reference)->get();
        foreach ($datas as $data) {
            $this->init_sum_value += $data->value;
            $this->plafon_value = $data->plafon_value;
            $this->remaining_plafon_value = $data->revolvingLoan->remainingPlafonValue();
            $this->datas[$data->id] = [
                'credit_line_detail' => $data->credit_line_detail,
                'credit_line_opening_date' => $data->credit_line_opening_date,
                'maturity_date' => $data->maturity_date,
                'transaction_type' => $data->transaction_type,
                'value' => abs($data->value),
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'datas.*.credit_line_detail' => 'required',
            'datas.*.transaction_type' => 'required',
            'datas.*.value' => 'required'
        ], [
            // Custom error message
        ], [
            'datas.*.credit_line_detail' => __("Credit Line Detail"),
            'datas.*.transaction_type' => __("Value"),
            'datas.*.value' => __("Value"),
        ]);

        foreach ($this->datas as $key => $dt) {
            // Validation to check is debt name unique
            $data = FinancialRevolvingDrawdown::where('credit_line_detail', $dt['credit_line_detail'])->where('id', '<>', $key)->first();
            if ($data) {
                $this->addError('datas.'.$key.'.credit_line_detail', __('The Credit Line Detail already been taken.'));
            }
        }

        if (count($this->getErrorBag()) > 0) {
            return;
        }

        $sumValue = 0;
        foreach ($this->datas as $key => $dt) {
            $valueMask = str_replace('.', '', $dt['value'] ?? 0);
            if ($dt['transaction_type'] == 'PENCAIRAN') {
                $sumValue += $valueMask;
            } else {
                $sumValue -= $valueMask;
            }
        }

        $delta = $sumValue - $this->init_sum_value;
        if ($this->init_sum_value != $sumValue && $delta > $this->remaining_plafon_value) {
            foreach ($this->datas as $key => $dt) {
                $this->addError('datas.'.$key.'.value', __('Remaining plafon value is ' . number_format($this->remaining_plafon_value)));
            }

            if (count($this->getErrorBag()) > 0) {
                return;
            }
        }

        try {
            foreach ($this->datas as $key => $dt) {
                // remove point from plafon value
                $valueMask = str_replace('.', '', $dt['value']);
                
                $data = FinancialRevolvingDrawdown::find($key);
                $data->credit_line_detail = $dt['credit_line_detail'] ?? null;
                $data->credit_line_opening_date = $dt['credit_line_opening_date'] != '' ? $dt['credit_line_opening_date'] : null;
                $data->maturity_date = $dt['maturity_date'] != '' ? $dt['maturity_date'] : null;
                $data->transaction_type = $dt['transaction_type'] != '' ? $dt['transaction_type'] : null;
                $data->value = $dt['transaction_type'] == 'PEMBAYARAN' ? (0 - abs($valueMask)) : $valueMask;
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Revolving Drawdown Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::revolving_drawdowns.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::revolving_drawdowns.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.revolving-drawdowns.edit');
    }
}