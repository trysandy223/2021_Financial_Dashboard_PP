<?php

namespace App\Http\Livewire\Financials\Liquidities;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\Bank;
use App\Models\FlagCf;
use App\Models\Project;
use App\Models\Company;
use App\Models\Category;
use App\Models\DataPeriod;
use App\Models\TradingPartner;

class Form extends Component
{
    public $model;
    public $bank;
    public $flag;
    public $company;
    public $periode;
    public $project;
    public $partner;
    public $category;
    public $is_parent_company;
    public $unit_id;

    // Variables for store select data
    public $selectCompany;
    public $banks;
    public $flags;
    public $periodes;
    public $partners;
    public $categories;

    public $posting_date;
    public $datas = [];

    public function mount($model)
    {
        $this->model = $model;

        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::liquidities.index");
        }
        
        $this->company = $company;
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->unit_id = $company->id;
            $this->company = $company->parent;
        }

        $this->banks = Bank::where('status', 1)->orderBy('sorting')->get();
        $this->flags = FlagCf::where('status', 1)->orderBy('sorting')->get();
        $this->periodes = DataPeriod::get();
        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();
        $this->categories = Category::where(function ($query) {
            $query->where('name', 'ACTUAL')->orWhere('name', 'PLAN');
        })->where('status', 1)->orderBy('sorting')->get();
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'project' => 'required',
            'periode' => 'required',
            'category' => 'required',
            'posting_date' => 'required',
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.plan_date' => __('Plan Date'),
            'datas.*.value' => __('Value'),
        ]);

        try {
            $reference = \Str::random(10);
            
            for ($i=0; $i < count($this->datas); $i++) {
                // remove point from value
                $valueMask = str_replace('.', '', $this->datas[$i]['value']);

                $data = new $this->model;
                // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                $data->company_id = $this->selectCompany;
                $data->project_id = Project::find($this->project)->id ?? null;
                $data->project_name = Project::find($this->project)->name ?? null;
                $data->category_id = $this->category;
                $data->category_name = $this->categories->where('id', $this->category)->first()->name ?? null;
                $data->flag_cf_id = $this->flag != '' ? $this->flag : null;
                $data->flag_cf_name = $this->flags->where('id', $this->flag)->first()->name ?? null;
                $data->data_period_id = $this->periode;
                $data->data_period_name = $this->periodes->where('id', $this->periode)->first()->name ?? null;
                $data->bank_id = $this->bank;
                $data->bank_name = $this->banks->where('id', $this->bank)->first()->name ?? null;
                $data->posting_date = $this->posting_date;
                $data->trading_partner = $this->partners->where('id', $this->partner)->first()->name ?? null;
                $data->plan_date = $this->datas[$i]['plan_date'] ?? null;
                $data->value = $valueMask ?? 0;
                $data->reference_id = $reference;
                $data->created_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Liquidity Data has been added.');

            session()->flash('success', __("Success to create data!"));
            return redirect()->route("financials::liquidities.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::liquidities.index");
        }
    }

    public function updatedFlag($value)
    {
        $this->updatedCategoryPeriode();
    }

    public function updatedPeriode($value)
    {
        $this->updatedCategoryPeriode();
    }

    public function updatedCategory($value)
    {
        $this->updatedCategoryPeriode();
    }

    public function updatedCategoryPeriode()
    {
        $this->datas = [];
        $category = Category::find($this->category);

        if ($category) {
            if (strtoupper($category->name) == 'ACTUAL') {
                foreach ($this->flags as $key => $flag) {
                    $this->datas[] = ['plan_date' => $flag->name, 'value' => '0'];
                }
            } elseif (strtoupper($category->name) == 'PLAN') {
                $selectedFlag = $this->flags->where('id', $this->flag)->first();
                $selectedPeriod = $this->periodes->where('id', $this->periode)->first();
                if ($selectedPeriod) {
                    if (strtoupper($selectedPeriod->name) == 'WEEK') {
                        $plan_date = 'FW';
                        $total_row = 13;
                    } elseif (strtoupper($selectedPeriod->name) == 'MONTH') {
                        $plan_date = 'M';
                        $total_row = 12;
                    } elseif (strtoupper($selectedPeriod->name) == 'YEAR') {
                        $plan_date = 'Y';
                        $total_row = 5;
                    }

                    if ($selectedFlag && (strtoupper($selectedFlag->name) == 'STARTING CASH BALANCE')) {
                        $total_row = 1;
                    }

                    for ($i=0; $i < $total_row; $i++) { 
                        $this->datas[] = ['plan_date' => $plan_date . ($i+1), 'value' => '0'];
                    }
                }
            } elseif (strpos(strtoupper($category->name), 'PROGNOS') !== false) {
                $this->datas[] = ['plan_date' => '', 'value' => '0'];
            }
        }

        $this->dispatchBrowserEvent('changeRupiahFormat');
    }

    public function render()
    {
        return view('livewire.financials.liquidities.form');
    }
}
