<?php

namespace App\Http\Livewire\Financials\Liquidities;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialLiquidity;

class Edit extends Component
{
    public $datas = [];

    public function mount($reference)
    {
        $datas = FinancialLiquidity::where('reference_id', $reference)->get();
        foreach ($datas as $data) {
            $this->datas[$data->id] = [
                'value' => $data->value,
                'plan_date' => $data->plan_date,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.plan_date' => __('Plan Date'),
            'datas.*.value' => __('Value'),
        ]);

        try {
            foreach ($this->datas as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);

                $data = FinancialLiquidity::find($key);
                $data->value = $valueMask;
                $data->plan_date = $dt['plan_date'];
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Liquidity Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::liquidities.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::liquidities.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.liquidities.edit');
    }
}
