<?php

namespace App\Http\Livewire\Financials\NclDrawdowns;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\DebtAllocationGroup;
use App\Models\FinancialNclDrawdown;

class Edit extends Component
{
    public $datas = [];
    public $company;
    public $plafon_value;
    public $remaining_plafon_value;
    public $init_sum_value = 0;
    
    // Variables for store select data
    public $debt_allocation_groups;
    
    public function mount($reference)
    {
        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::ncl_drawdowns.index");
        }

        $this->debt_allocation_groups = DebtAllocationGroup::get();

        $datas = FinancialNclDrawdown::where('reference_id', $reference)->get();
        $this->company = $datas->first()->company_id ?? null;
        foreach ($datas as $data) {
            $this->init_sum_value += $data->value;
            $this->plafon_value = $data->plafon_value;
            $this->remaining_plafon_value = $data->ncl->remainingPlafonValue();
            $this->datas[$data->id] = [
                'credit_line_detail' => $data->credit_line_detail,
                'credit_line_opening_date' => $data->credit_line_opening_date,
                'maturity_date' => $data->maturity_date,
                'transaction_type' => $data->transaction_type,
                'debt_allocation_group' => $data->debt_allocation_group_id,
                'result_allocation' => $data->result_allocation_name,
                'value' => $data->value,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'datas.*.credit_line_detail' => 'required',
            'datas.*.transaction_type' => 'required',
            'datas.*.value' => 'required'
        ], [
            // Custom error message
        ], [
            'datas.*.credit_line_detail' => __("Credit Line Detail"),
            'datas.*.transaction_type' => __("Transaction Type"),
            'datas.*.value' => __("Value"),
        ]);

        foreach ($this->datas as $key => $dt) {
            // Validation to check is debt name unique
            $data = FinancialNclDrawdown::where('credit_line_detail', $dt['credit_line_detail'])->where('company_id', $this->company)->where('id', '<>', $key)->first();
            if ($data) {
                $this->addError('datas.'.$key.'.credit_line_detail', __('The Credit Line Detail already been taken.'));
            }
        }

        if (count($this->getErrorBag()) > 0) {
            return;
        }

        $sumValue = 0;
        foreach ($this->datas as $key => $dt) {
            $valueMask = str_replace('.', '', $dt['value'] ?? 0);
            if ($dt['transaction_type'] == 'PENCAIRAN') {
                $sumValue += $valueMask;
            } else {
                $sumValue -= $valueMask;
            }
        }

        $delta = $sumValue - $this->init_sum_value;
        if ($this->init_sum_value != $sumValue && $delta > $this->remaining_plafon_value) {
            foreach ($this->datas as $key => $dt) {
                $this->addError('datas.'.$key.'.value', __('Remaining plafon value is ' . number_format($this->remaining_plafon_value)));
            }

            if (count($this->getErrorBag()) > 0) {
                return;
            }
        }

        try {
            foreach ($this->datas as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);

                $data = FinancialNclDrawdown::find($key);

                $data->credit_line_detail = $dt['credit_line_detail'] != '' ? $dt['credit_line_detail'] : null;
                $data->credit_line_opening_date = $dt['credit_line_opening_date'] != '' ? $dt['credit_line_opening_date'] : null;
                $data->maturity_date = $dt['maturity_date'] != '' ? $dt['maturity_date'] : null;
                $data->transaction_type = $dt['transaction_type'] != '' ? $dt['transaction_type'] : null;
                
                $data->debt_allocation_group_id = $dt['debt_allocation_group'] != '' ? $dt['debt_allocation_group'] : null;
                $data->debt_allocation_group_name = $this->debt_allocation_groups->where('id', $dt['debt_allocation_group'] ?? null)->first()->name ?? null;
                if ($dt['result_allocation'] != '') {
                    $data->result_allocation_name = $dt['result_allocation'];
                }
                $data->value = $dt['transaction_type'] == 'PEMBAYARAN' ? (0 - abs($valueMask)) : $valueMask;
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('NCL Drawdown Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::ncl_drawdowns.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::ncl_drawdowns.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.ncl-drawdowns.edit');
    }
}
