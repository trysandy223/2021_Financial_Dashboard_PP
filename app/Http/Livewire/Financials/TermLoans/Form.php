<?php

namespace App\Http\Livewire\Financials\TermLoans;

use DB;
use Auth;
use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\Bank;
use App\Models\Project;
use App\Models\Company;
use App\Models\Category;
use App\Models\DebtType;
use App\Models\DebtPayment;
use App\Models\SourceGroup;
use App\Models\TradingPartner;

class Form extends Component
{
    public $model;
    public $bank;
    public $flag;
    public $type;
    public $total;
    public $status;
    public $company;
    public $project;
    public $partner;
    public $category;
    public $posting_date;
    public $source_group;
    public $is_parent_company;
    public $unit_id;

    // Variables for generate data
    public $generate_debt_name;
    public $generate_start_duration;
    public $generate_loan_period;

    // Variables for store select data
    public $selectCompany;
    public $banks;
    public $partners;
    public $sourceGroups;
    public $debtPayments;

    public $datas = [];

    public function mount($model)
    {
        $this->model = $model;

        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::term_loans.index");
        }
        
        $this->company = $company;
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->unit_id = $company->id;
            $this->company = $company->parent;
        }
        
        $this->category = Category::where('name', 'ACTUAL')->first();
        if (!$this->category) {
            session()->flash('failed', __("ACTUAL category is not found"));
            return redirect()->route("financials::term_loans.index");
        }

        $this->flag = DebtType::where('name', 'TERM LOAN')->first();
        if (!$this->flag) {
            session()->flash('failed', __("TERM LOAN debt type is not found"));
            return redirect()->route("financials::term_loans.index");
        }

        $this->banks = Bank::where('status', 1)->orderBy('sorting')->get();
        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();
        $this->sourceGroups = SourceGroup::get();
        $this->debtPayments = DebtPayment::get();

        $this->addData();
    }

    public function generate()
    {
        $this->validate([
            'generate_debt_name' => 'required',
            'generate_start_duration' => 'required',
            'generate_loan_period' => 'required',
        ]);

        $this->datas = [];
        for ($i=1; $i <= $this->generate_loan_period; $i++) { 
            $this->datas[] = [
                "debt_name" => $this->generate_debt_name . " " . $i,
                "start_duration" => Carbon::parse($this->generate_start_duration)->addMonths($i-1)->format("Y-m-d"),
                "maturity_date" => Carbon::parse($this->generate_start_duration)->addMonths($i)->format("Y-m-d"),
                "interest_rate" => "",
                "value" => 0,
            ];
        }

        $this->dispatchBrowserEvent('changeRupiahFormat');
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'bank' => 'required',
            'project' => 'required',
            'posting_date' => 'required',
            'source_group' => 'required',
            'type' => 'required',
            'status' => 'required',
            'total' => $this->type == 'BULANAN' ? 'required' : '',
            'datas.*.debt_name' => 'required|unique:financial_term_loans,debt_name',
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.debt_name' => __("Debt Name"),
            'datas.*.value' => __("Value"),
        ]);

        // Validate Unique Debt Name
        $duplicateNames = collect($this->datas)->pluck('debt_name')->duplicates()->toArray();
        if (count($duplicateNames) > 0) {
            foreach ($duplicateNames as $key => $dn) {
                $this->addError('datas.'.$key.'.debt_name', __('The Debt Name has already been taken.'));
            }
            return;
        }

        // remove point from total
        $totalMask = str_replace('.', '', $this->total);
        $sumValue = 0;

        // Validation is value greather than total
        if ($this->type == 'BULANAN') {
            foreach ($this->datas as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);
                $sumValue += $valueMask;
                $this->addError('datas.'.$key.'.value', __('Max value is ' . number_format($totalMask)));
            }

            if ($sumValue > $totalMask) {
                if (count($this->getErrorBag()) > 0) {
                    return;
                }
            }
        }

        try {
            DB::transaction(function () use ($totalMask) {
                $reference = \Str::random(10);

                foreach ($this->datas as $key => $dt) {
                    // remove point from value
                    $valueMask = str_replace('.', '', $dt['value']);

                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = Project::find($this->project)->id ?? null;
                    $data->project_name = Project::find($this->project)->name ?? null;
                    $data->category_id = $this->category->id;
                    $data->category_name = $this->category->name;
                    $data->debt_type_id = $this->flag->id;
                    $data->debt_type_name = $this->flag->name;
                    $data->bank_id = $this->bank;
                    $data->bank_name = $this->banks->where('id', $this->bank)->first()->name ?? null;
                    $data->source_group_id = $this->source_group;
                    $data->source_group_name = $this->sourceGroups->where('id', $this->source_group)->first()->name ?? null;
                    $data->posting_date = $this->posting_date;
                    $data->trading_partner = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->debt_name = $dt['debt_name'] ?? null;
                    $data->start_duration_date = $dt['start_duration'] != '' ? $dt['start_duration'] : null;
                    $data->maturity_date = $dt['maturity_date'] != '' ? $dt['maturity_date'] : null;
                    $data->interest_rate = $dt['interest_rate'] != '' ? $dt['interest_rate'] : null;
                    $data->value = $valueMask ?? null;
                    $data->debt_payment_id = $this->status;
                    $data->debt_payment_name = $this->debtPayments->where('id', $this->status)->first()->name ?? null;
                    $data->type = $this->type;
                    $data->total = $totalMask != '' ? $totalMask : null;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }
                
                activity()->performedOn($data)->log('Term Loan Data has been added.');
            });

            session()->flash('success', __("Success to create data!"));
            return redirect()->route("financials::term_loans.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::term_loans.index");
        }
    }

    public function addData()
    {
        $this->datas[] = [
            'debt_name' => "",
            'start_duration' => "",
            'maturity_date' => "",
            'interest_rate' => "",
            'value' => 0,
        ];

        $this->dispatchBrowserEvent('changeRupiahFormat');
    }

    public function subData($i)
    {
        unset($this->datas[$i]);
    }

    public function updatedType()
    {
        $this->dispatchBrowserEvent('changeRupiahFormat');
    }

    public function render()
    {
        return view('livewire.financials.term-loans.form');
    }
}
