<?php

namespace App\Http\Livewire\Financials\TermLoans;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialTermLoan;

class Edit extends Component
{
    public $datas = [];

    public function mount($reference)
    {
        $datas = FinancialTermLoan::where('reference_id', $reference)->get();
        foreach ($datas as $data) {
            $this->datas[$data->id] = [
                'debt_name' => $data->debt_name,
                'start_duration' => $data->start_duration_date,
                'maturity_date' => $data->maturity_date,
                'interest_rate' => $data->interest_rate,
                'value' =>$data->value,
                'total' =>$data->total,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'datas.*.debt_name' => 'required',
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.debt_name' => __("Debt Name"),
            'datas.*.value' => __("Value"),
        ]);

        foreach ($this->datas as $key => $dt) {
            // Validation to check is debt name unique
            $data = FinancialTermLoan::where('debt_name', $dt['debt_name'])->where('id', '<>', $key)->first();
            if ($data) {
                $this->addError('datas.'.$key.'.debt_name', __('The Debt Name has already been taken.'));
            }

            // Validation to check is value greather than total
            if ($dt['total'] != NULL) {
                $valueMask = str_replace('.', '', $dt['value']);
                if ($valueMask > $dt['total']) {
                    $this->addError('datas.'.$key.'.value', __('Max value is ' . number_format($dt['total'])));
                }
            }
        }

        if (count($this->getErrorBag()) > 0) {
            return;
        }

        try {
            foreach ($this->datas as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);

                $data = FinancialTermLoan::find($key);
                $data->debt_name = $dt['debt_name'];
                $data->start_duration_date = $dt['start_duration'];
                $data->maturity_date = $dt['maturity_date'];
                $data->interest_rate = $dt['interest_rate'];
                $data->value = $valueMask;
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Term Loan Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::term_loans.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::term_loans.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.term-loans.edit');
    }
}
