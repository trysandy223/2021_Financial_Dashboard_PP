<?php

namespace App\Http\Livewire\Financials\RevolvingLoans;

use DB;
use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\Bank;
use App\Models\Project;
use App\Models\Company;
use App\Models\Category;
use App\Models\DebtType;
use App\Models\DebtPayment;
use App\Models\SourceGroup;
use App\Models\TradingPartner;

class Form extends Component
{
    public $model;
    public $bank;
    public $flag;
    public $company;
    public $project;
    public $partner;
    public $category;
    public $posting_date;
    public $source_group;
    public $is_parent_company;
    public $unit_id;

    // Variables for store select data
    public $selectCompany;
    public $banks;
    public $partners;
    public $sourceGroups;
    public $debtPayments;
    public $datas = [];

    public function mount($model)
    {
        $this->model = $model;

        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::revolving_loans.index");
        }
        
        $this->company = $company;
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->unit_id = $company->id;
            $this->company = $company->parent;
        }
        
        $this->category = Category::where('name', 'ACTUAL')->first();

        if (!$this->category) {
            session()->flash('failed', __("ACTUAL category is not found"));
            return redirect()->route("financials::revolving_loans.index");
        }

        $this->flag = DebtType::where('name', 'REVOLVING LOAN')->first();

        if (!$this->flag) {
            session()->flash('failed', __("REVOLVING LOAN debt type is not found"));
            return redirect()->route("financials::revolving_loans.index");
        }

        $this->banks = Bank::where('status', 1)->orderBy('sorting')->get();
        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();
        $this->sourceGroups = SourceGroup::get();
        $this->debtPayments = DebtPayment::get();

        $this->addData();
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'bank' => 'required',
            'project' => 'required',
            'posting_date' => 'required',
            'source_group' => 'required',
            'datas.*.debt_name' => 'required|unique:financial_revolving_loans,debt_name',
            'datas.*.plafon_value' => 'required'
        ], [
            // Custom error message
        ], [
            'datas.*.debt_name' => __("Debt Name"),
            'datas.*.plafon_value' => __("Value"),
        ]);

        // Validate Unique Debt Name
        $duplicateNames = collect($this->datas)->pluck('debt_name')->duplicates()->toArray();
        if (count($duplicateNames) > 0) {
            foreach ($duplicateNames as $key => $dn) {
                $this->addError('datas.'.$key.'.debt_name', __('The Debt Name has already been taken.'));
            }
            return;
        }

        try {
            DB::transaction(function () {
                $reference = \Str::random(10);

                foreach ($this->datas as $dt) {
                    // remove point from plafon value
                    $plafonValueMask = str_replace('.', '', $dt['plafon_value']);

                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = Project::find($this->project)->id ?? null;
                    $data->project_name = Project::find($this->project)->name ?? null;
                    $data->category_id = $this->category->id;
                    $data->category_name = $this->category->name;
                    $data->debt_type_id = $this->flag->id;
                    $data->debt_type_name = $this->flag->name;
                    $data->bank_id = $this->bank;
                    $data->bank_name = $this->banks->where('id', $this->bank)->first()->name ?? null;
                    $data->source_group_id = $this->source_group;
                    $data->source_group_name = $this->sourceGroups->where('id', $this->source_group)->first()->name ?? null;
                    $data->posting_date = $this->posting_date;
                    $data->trading_partner_id = $this->partner != '' ? $this->partner : null;
                    $data->trading_partner_name = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->debt_name = $dt['debt_name'] ?? null;
                    $data->credit_loan_opening_date = $dt['credit_loan_opening_date'] != '' ? $dt['credit_loan_opening_date'] : null;
                    $data->maturity_date = $dt['maturity_date'] != '' ? $dt['maturity_date'] : null;
                    $data->interest_rate = $dt['interest_rate'] != '' ? $dt['interest_rate'] : null;
                    $data->plafon_value = $plafonValueMask ?? null;
                    $data->debt_equity = $dt['debt_equity'] ?? null;
                    $data->debt_ebitda = $dt['debt_ebitda'] ?? null;
                    $data->iscr = $dt['iscr'] ?? null;
                    $data->debt_payment_id = $dt['status'] != '' ? $dt['status'] : null;
                    $data->debt_payment_name = $this->debtPayments->where('id', $dt['status'] ?? null)->first()->name ?? null;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }
                
                activity()->performedOn($data)->log('Revolving Loan Data has been added.');
            });

            session()->flash('success', __("Success to create data!"));

            return redirect()->route("financials::revolving_loans.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);

            return redirect()->route("financials::revolving_loans.index");
        }
    }

    public function addData()
    {
        $this->datas[] = [
            'debt_name' => "",
            'credit_loan_opening_date' => "",
            'maturity_date' => "",
            'interest_rate' => "",
            'status' => "",
            'plafon_value' => 0,
            'debt_equity' => 0,
            'debt_ebitda' => 0,
            'iscr' => 0,
        ];

        $this->dispatchBrowserEvent('changeRupiahFormat');
    }

    public function subData($i)
    {
        unset($this->datas[$i]);
    }

    public function render()
    {
        return view('livewire.financials.revolving-loans.form');
    }
}