<?php

namespace App\Http\Livewire\Financials\RevolvingLoans;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\DebtPayment;
use App\Models\FinancialRevolvingLoan;

class Edit extends Component
{
    public $datas = [];
    public $debtPayments;

    public function mount($reference)
    {
        $this->debtPayments = DebtPayment::get();
        $datas = FinancialRevolvingLoan::where('reference_id', $reference)->get();
        foreach ($datas as $data) {
            $this->datas[$data->id] = [
                'debt_name' => $data->debt_name,
                'credit_loan_opening_date' => $data->credit_loan_opening_date,
                'maturity_date' => $data->maturity_date,
                'interest_rate' => $data->interest_rate,
                'status' => $data->debt_payment_id,
                'plafon_value' => $data->plafon_value,
                'debt_equity' => $data->debt_equity,
                'debt_ebitda' => $data->debt_ebitda,
                'iscr' => $data->iscr,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'datas.*.debt_name' => 'required',
            'datas.*.plafon_value' => 'required'
        ], [
            // Custom error message
        ], [
            'datas.*.debt_name' => __("Debt Name"),
            'datas.*.plafon_value' => __("Value"),
        ]);

        foreach ($this->datas as $key => $dt) {
            // Validation to check is debt name unique
            $data = FinancialRevolvingLoan::where('debt_name', $dt['debt_name'])->where('id', '<>', $key)->first();
            if ($data) {
                $this->addError('datas.'.$key.'.debt_name', __('The Debt Name has already been taken.'));
            }
        }

        if (count($this->getErrorBag()) > 0) {
            return;
        }

        try {
            foreach ($this->datas as $key => $dt) {
                // remove point from plafon value
                $plafonValueMask = str_replace('.', '', $dt['plafon_value']);
                
                $data = FinancialRevolvingLoan::find($key);
                $data->debt_name = $dt['debt_name'];
                $data->credit_loan_opening_date = $dt['credit_loan_opening_date'];
                $data->maturity_date = $dt['maturity_date'];
                $data->interest_rate = $dt['interest_rate'];
                $data->plafon_value = $plafonValueMask;
                $data->debt_equity = $dt['debt_equity'];
                $data->debt_ebitda = $dt['debt_ebitda'];
                $data->iscr = $dt['iscr'];
                $data->debt_payment_id = $dt['status'];
                $data->debt_payment_name = $this->debtPayments->where('id', $dt['status'] ?? 0)->first()->name ?? null;
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Revolving Loan Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::revolving_loans.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::revolving_loans.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.revolving-loans.edit');
    }
}