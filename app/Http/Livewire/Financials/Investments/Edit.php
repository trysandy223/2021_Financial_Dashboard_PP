<?php

namespace App\Http\Livewire\Financials\Investments;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialInvestment;

class Edit extends Component
{
    public $plans = [];
    public $actuals = [];

    public function mount($reference)
    {
        $plans = FinancialInvestment::where('category_name', 'PLAN')->where('reference_id', $reference)->get();
        foreach ($plans as $plan) {
            $this->plans[$plan->id] = [
                'value' => $plan->value,
                'flag' => $plan->flag_investment_name,
            ];
        }

        $actuals = FinancialInvestment::where('category_name', 'ACTUAL')->where('reference_id', $reference)->get();
        foreach ($actuals as $actual) {
            $this->actuals[$actual->id] = [
                'value' => $actual->value,
                'flag' => $actual->flag_investment_name,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'plans.*.value' => 'required|numeric',
            'actuals.*.value' => 'required|numeric',
        ], [
            // Custom error message
        ], [
            'plans.*.value' => __('Value'),
            'actuals.*.value' => __('Value'),
        ]);

        try {
            foreach ($this->plans as $key => $dt) {
                $data = FinancialInvestment::find($key);
                $data->value = $dt['value'];
                $data->updated_by = Auth::id();
                $data->save();
            }

            foreach ($this->actuals as $key => $dt) {
                $data = FinancialInvestment::find($key);
                $data->value = $dt['value'];
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Investment Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::investments.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::investments.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.investments.edit');
    }
}
