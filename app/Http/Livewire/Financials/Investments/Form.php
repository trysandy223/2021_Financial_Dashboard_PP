<?php

namespace App\Http\Livewire\Financials\Investments;

use DB;
use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\Bank;
use App\Models\Project;
use App\Models\Company;
use App\Models\Category;
use App\Models\FlagInvestment;
use App\Models\TradingPartner;

class Form extends Component
{
    public $model;
    public $bank;
    public $status;
    public $company;
    public $project;
    public $partner;
    public $posting_date;
    public $is_parent_company;
    public $unit_id;

    // Variables for store select data
    public $selectCompany;
    public $banks;
    public $flags;
    public $partners;

    public $plans = [];
    public $actuals = [];

    public function mount($model)
    {
        $this->model = $model;

        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::investments.index");
        }
        
        $this->company = $company;
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->unit_id = $company->id;
            $this->company = $company->parent;
        }

        $this->banks = Bank::where('status', 1)->orderBy('sorting')->get();
        $this->flags = FlagInvestment::where('status', 1)->orderBy('sorting')->get();
        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();

        foreach ($this->flags as $flag) {
            $this->plans[$flag->id] = ['value' => 0];
            $this->actuals[$flag->id] = ['value' => 0];
        }
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'status' => 'required',
            'project' => 'required',
            'posting_date' => 'required',
            'plans.*.value' => 'required|numeric',
            'actuals.*.value' => 'required|numeric',
        ], [
            // Custom error message
        ], [
            'plans.*.value' => __('Value'),
            'actuals.*.value' => __('Value'),
        ]);

        try {
            DB::transaction(function () {
                $reference = \Str::random(10);
                $planCategory = Category::where('name', 'PLAN')->firstOrFail();
                foreach ($this->plans as $id => $plan) {
                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = Project::find($this->project)->id ?? null;
                    $data->project_name = Project::find($this->project)->name ?? null;
                    $data->category_id = $planCategory->id;
                    $data->category_name = $planCategory->name;
                    $data->flag_investment_id = $id;
                    $data->flag_investment_name = $this->flags->where('id', $id)->first()->name ?? null;
                    $data->bank_id = $this->bank;
                    $data->bank_name = $this->banks->where('id', $this->bank)->first()->name ?? null;
                    $data->posting_date = $this->posting_date;
                    $data->trading_partner = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->status = $this->status;
                    $data->value = $plan['value'] ?? 0;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }

                $actualCategory = Category::where('name', 'ACTUAL')->firstOrFail();
                foreach ($this->actuals as $id => $actual) {
                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = Project::find($this->project)->id ?? null;
                    $data->project_name = Project::find($this->project)->name ?? null;
                    $data->category_id = $actualCategory->id;
                    $data->category_name = $actualCategory->name;
                    $data->flag_investment_id = $id;
                    $data->flag_investment_name = $this->flags->where('id', $id)->first()->name ?? null;
                    $data->bank_id = $this->bank;
                    $data->bank_name = $this->banks->where('id', $this->bank)->first()->name ?? null;
                    $data->posting_date = $this->posting_date;
                    $data->trading_partner = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->status = $this->status;
                    $data->value = $actual['value'] ?? 0;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }

                activity()->performedOn($data)->log('Investment Data has been added.');
            });

            session()->flash('success', __("Success to create data!"));
            return redirect()->route("financials::investments.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::investments.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.investments.form');
    }
}
