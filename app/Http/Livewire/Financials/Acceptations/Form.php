<?php

namespace App\Http\Livewire\Financials\Acceptations;

use DB;
use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\Project;
use App\Models\Category;
use App\Models\DebtType;
use App\Models\TradingPartner;
use App\Models\DebtAllocationGroup;
use App\Models\FinancialNcl;
use App\Models\FinancialNclDrawdown;

class Form extends Component
{
    public $model;
    public $ncl;
    public $bank;
    public $flag;
    public $company;
    public $project;
    public $partner;
    public $category;
    public $posting_date;
    public $maturity_date;
    public $credit_line_detail;
    public $is_parent_company;
    public $unit_id;

    // Variables for store select data
    public $selectCompany;
    public $partners;
    public $categories;
    public $debt_allocation_groups;
    public $datas = [];

    public function mount($model)
    {
        $this->model = $model;

        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::term_loan_allocations.index");
        }
        
        $this->company = $company;
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->unit_id = $company->id;
            $this->company = $company->parent;
        }
        
        $this->categories = Category::where('name', 'ACTUAL')->orWhere('name', 'PLAN')->get();

        $this->flag = DebtType::where('name', 'NCL')->first();

        if (!$this->flag) {
            session()->flash('failed', __("NCL debt type is not found"));
            return redirect()->route("financials::acceptations.index");
        }

        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();
        $this->debt_allocation_groups = DebtAllocationGroup::get();

        $this->addData();
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'bank' => 'required',
            'project' => 'required',
            'category' => 'required',
            'posting_date' => 'required',
            'credit_line_detail' => 'required',
            'datas.*.value' => 'required'
        ], [
            // Custom error message
        ], [
            'datas.*.value' => __("Value"),
        ]);

        try {
            DB::transaction(function () {
                $reference = \Str::random(10);

                foreach ($this->datas as $dt) {
                    // remove point from value
                    $valueMask = str_replace('.', '', $dt['value'] ?? 0);

                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = Project::find($this->project)->id ?? null;
                    $data->project_name = Project::find($this->project)->name ?? null;
                    $data->ncl_drawdown_id = FinancialNclDrawdown::find($this->credit_line_detail)->id ?? null;
                    $data->ncl_drawdown_name = FinancialNclDrawdown::find($this->credit_line_detail)->credit_line_detail ?? null;
                    $data->category_id = $this->category;
                    $data->category_name = $this->categories->where('id', $this->category)->first()->name ?? null;
                    $data->trading_partner_id = $this->partner != '' ? $this->partner : null;
                    $data->trading_partner_name = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->posting_date = $this->posting_date;
                    $data->maturity_date = $this->maturity_date != '' ? $this->maturity_date : null;
                    $data->bank_id = $this->ncl->bank_id ?? null;
                    $data->bank_name = $this->ncl->bank_name ?? null;
                    $data->debt_type_id = $this->ncl->debt_type_id ?? null;
                    $data->debt_type_name = $this->ncl->debt_type_name ?? null;

                    $data->debt_allocation_group_id = $dt['debt_allocation_group'] ?? null;
                    $data->debt_allocation_group_name = $this->debt_allocation_groups->where('id', $dt['debt_allocation_group'] ?? null)->first()->name ?? null;
                    $data->result_allocation_name = $dt['result_allocation'] ?? null;
                    $data->value = $valueMask ?? null;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }
            
                activity()->performedOn($data)->log('Acceptation Data has been added.');
            });

            session()->flash('success', __("Success to create data!"));

            return redirect()->route("financials::acceptations.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);

            return redirect()->route("financials::acceptations.index");
        }
    }

    public function addData()
    {
        $this->datas[] = [
            'debt_allocation_group' => '',
            'result_allocation' => '',
            'value' => 0,
        ];

        $this->dispatchBrowserEvent('changeRupiahFormat');
        $this->dispatchBrowserEvent('initDebtGroup', ['companyId' => $this->company->id ?? $this->company]);
    }

    public function subData($i)
    {
        unset($this->datas[$i]);
    }

    public function updatedBank($value)
    {
        $this->ncl = FinancialNcl::find($value);
    }

    public function updatedCategory($value)
    {
        $this->maturity_date = null;
    }

    public function render()
    {
        return view('livewire.financials.acceptations.form');
    }
}