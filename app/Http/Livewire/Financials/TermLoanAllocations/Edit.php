<?php

namespace App\Http\Livewire\Financials\TermLoanAllocations;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialTermLoanAllocation;
use App\Models\DebtAllocationGroup;

class Edit extends Component
{
    public $company;
    public $datas = [];
    
    // Variables for store select data
    public $debt_allocation_groups;
    
    public function mount($reference)
    {
        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::term_loan_allocations.index");
        }

        $this->debt_allocation_groups = DebtAllocationGroup::get();

        $datas = FinancialTermLoanAllocation::where('reference_id', $reference)->get();
        $this->company = $datas->first()->company_id ?? null;
        foreach ($datas as $data) {
            $this->datas[$data->id] = [
                'debt_allocation_group' => $data->debt_allocation_group_id,
                'result_allocation' => $data->result_allocation_name,
                'value' => $data->value,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.debt_allocation_group' => __("Debt Allocation Group"),
            'datas.*.result_allocation' => __("Result Allocation"),
            'datas.*.value' => __("Value"),
        ]);

        try {
            foreach ($this->datas as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);

                $data = FinancialTermLoanAllocation::find($key);
                $data->value = $valueMask;
                $data->debt_allocation_group_id = $dt['debt_allocation_group'] != '' ? $dt['debt_allocation_group'] : null;
                $data->debt_allocation_group_name = $this->debt_allocation_groups->where('id', $dt['debt_allocation_group'] ?? null)->first()->name ?? null;
                if ($dt['result_allocation'] != '') {
                    $data->result_allocation_name = $dt['result_allocation'];
                }
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Term Loan Allocation Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::term_loan_allocations.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::term_loan_allocations.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.term-loan-allocations.edit');
    }
}
