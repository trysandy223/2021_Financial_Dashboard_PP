<?php

namespace App\Http\Livewire\Financials\TermLoanAllocations;

use Auth;
use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialTermLoan;
use App\Models\FinancialTermLoanAllocation;

class Reverse extends Component
{
    public $model;
    public $company;
    public $selectCompany;
    public $is_parent_company;
    public $debt_name;
    public $data;

    protected $listeners = ['confirmReverse' => 'submit'];

    public function mount($model)
    {
        $this->model = $model;
        
        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::term_loan_allocations.index");
        }
        
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->company = $company->parent;
        } else {
            $this->company = $company;
        }
    }

    public function updatedDebtName($value)
    {
        $this->data = FinancialTermLoan::find($value);
    }

    public function submit()
    {
        $this->validate([
            // 'company' => 'required',
            'selectCompany' => 'required',
            'debt_name' => 'required',
        ]);


        try {
            $datas = FinancialTermLoanAllocation::where('financial_term_loan_id', $this->data->id)->get();
            foreach ($datas as $data) {
                $dt = new FinancialTermLoanAllocation();
                // $dt->company_id = $this->company->id ?? $this->company;
                $dt->company_id = $this->selectCompany;
                $dt->project_id = $data->project_id;
                $dt->project_name = $data->project_name;
                $dt->category_id = $data->category_id;
                $dt->category_name = $data->category_name;
                $dt->trading_partner_id = $data->trading_partner_id;
                $dt->trading_partner_name = $data->trading_partner_name;
                $dt->posting_date = Carbon::parse($data->posting_date)->addMonth();
                $dt->financial_term_loan_id = $data->financial_term_loan_id;
                $dt->financial_term_loan_name = $data->financial_term_loan_name;
                $dt->bank_name = $data->bank_name;
                $dt->source_group_name = $data->source_group_name;
                $dt->total = $data->total;
                $dt->debt_payment_name = $data->debt_payment_name;
                $dt->debt_type_name = $data->debt_type_name;
                $dt->debt_allocation_group_id = $data->debt_allocation_group_id;
                $dt->debt_allocation_group_name = $data->debt_allocation_group_name;
                $dt->result_allocation_name = $data->result_allocation_name;
                $dt->value = 0-$data->value;
                $dt->reference_id = $data->reference_id;
                $dt->is_reverse = 1;
                $dt->created_by = Auth::id();
                $dt->save();
            }

            activity()->performedOn($dt)->log('Term Loan Allocation Data has been reversed.');

            session()->flash('success', __("Success to reverse data!"));

            return redirect()->route("financials::term_loan_allocations.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);

            return redirect()->route("financials::term_loan_allocations.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.term-loan-allocations.reverse');
    }
}
