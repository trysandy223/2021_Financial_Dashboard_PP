<?php

namespace App\Http\Livewire\Financials\RevolvingAllocations;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\DebtAllocationGroup;
use App\Models\FinancialRevolvingAllocation;
use App\Models\FinancialRevolvingDrawdown;

class Edit extends Component
{
    public $company;
    public $datas = [];
    
    // Variables for store select data
    public $debt_allocation_groups;
    
    public function mount($reference)
    {
        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::revolving_allocations.index");
        }

        $this->debt_allocation_groups = DebtAllocationGroup::get();

        $datas = FinancialRevolvingAllocation::where('reference_id', $reference)->get();
        $this->company = $datas->first()->company_id ?? null;
        foreach ($datas as $data) {
            $this->datas[$data->id] = [
                'revolving_drawdown_id' => $data->revolving_drawdown_id,
                'debt_allocation_group' => $data->debt_allocation_group_id,
                'result_allocation' => $data->result_allocation_name,
                'value' => $data->value,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.debt_allocation_group' => __("Debt Allocation Group"),
            'datas.*.result_allocation' => __("Result Allocation"),
            'datas.*.value' => __("Value"),
        ]);

        // Validate value
        foreach ($this->datas as $key => $dt) {
            $valueMask = str_replace('.', '', $dt['value'] ?? 0);
            $revolvingDrawdown = FinancialRevolvingDrawdown::find($dt['revolving_drawdown_id']);
            if ($valueMask > ($revolvingDrawdown->value ?? 0)) {
                $this->addError('datas.'.$key.'.value', __('Max value is ' . number_format($revolvingDrawdown->value ?? 0)));
            }
        }

        if (count($this->getErrorBag()) > 0) {
            return;
        }

        try {
            foreach ($this->datas as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);

                $data = FinancialRevolvingAllocation::find($key);
                $data->value = $valueMask;
                if ($dt['debt_allocation_group'] != '') {
                    $data->debt_allocation_group_id = $dt['debt_allocation_group'];
                }
                if ($dt['result_allocation'] != '') {
                    $data->result_allocation_name = $dt['result_allocation'];
                }
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Revolving Allocation Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::revolving_allocations.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::revolving_allocations.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.revolving-allocations.edit');
    }
}
