<?php

namespace App\Http\Livewire\Financials\RevolvingAllocations;

use DB;
use Auth;
use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\Project;
use App\Models\Category;
use App\Models\TradingPartner;
use App\Models\DebtAllocationGroup;
use App\Models\FinancialRevolvingDrawdown;

class Form extends Component
{
    public $model;
    public $company;
    public $project;
    public $partner;
    public $category;
    public $posting_date;
    public $credit_line_detail;
    public $revolving_drawdown;
    public $is_parent_company;
    public $unit_id;

    // Variables for store select data
    public $selectCompany;
    public $partners;
    public $debt_allocation_groups;
    public $datas = [];

    public function mount($model)
    {
        $this->model = $model;

        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::term_loan_allocations.index");
        }
        
        $this->company = $company;
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->unit_id = $company->id;
            $this->company = $company->parent;
        }
        
        $this->category = Category::where('name', 'ACTUAL')->first();
        if (!$this->category) {
            session()->flash('failed', __("ACTUAL category is not found"));
            return redirect()->route("financials::revolving_allocations.index");
        }

        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();
        $this->debt_allocation_groups = DebtAllocationGroup::get();
        $this->addData();
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'project' => 'required',
            'posting_date' => 'required',
            'credit_line_detail' => 'required',
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.debt_allocation_group' => __("Debt Allocation Group"),
            'datas.*.result_allocation' => __("Result Allocation"),
            'datas.*.value' => __("Value"),
        ]);

        // Validate value
        foreach ($this->datas as $key => $dt) {
            $valueMask = str_replace('.', '', $dt['value'] ?? 0);
            if ($valueMask > $this->revolving_drawdown->value) {
                $this->addError('datas.'.$key.'.value', __('Max value is ' . number_format($this->revolving_drawdown->value)));
            }
        }

        if (count($this->getErrorBag()) > 0) {
            return;
        }

        try {
            DB::transaction(function () {
                $reference = \Str::random(10);

                foreach ($this->datas as $dt) {
                    // remove point from value
                    $valueMask = str_replace('.', '', $dt['value']);

                    $data = new $this->model;
                    // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                    $data->company_id = $this->selectCompany;
                    $data->project_id = Project::find($this->project)->id ?? null;
                    $data->project_name = Project::find($this->project)->name ?? null;
                    $data->category_id = $this->category->id;
                    $data->category_name = $this->category->name;
                    $data->trading_partner_id = $this->partner != '' ? $this->partner : null;
                    $data->trading_partner_name = $this->partners->where('id', $this->partner)->first()->name ?? null;
                    $data->posting_date = $this->posting_date;
                    $data->revolving_drawdown_id = $this->revolving_drawdown->id;
                    $data->revolving_drawdown_name = $this->revolving_drawdown->credit_line_detail;

                    $data->debt_allocation_group_id = $dt['debt_allocation_group'] ?? null;
                    $data->debt_allocation_group_name = $this->debt_allocation_groups->where('id', $dt['debt_allocation_group'] ?? null)->first()->name ?? null;
                    $data->result_allocation_name = $dt['result_allocation'] ?? null;
                    $data->value = $valueMask ?? null;
                    $data->reference_id = $reference;
                    $data->created_by = Auth::id();
                    $data->save();
                }
            
                activity()->performedOn($data)->log('Revolving Allocation Data has been added.');
            });

            session()->flash('success', __("Success to create data!"));
            return redirect()->route("financials::revolving_allocations.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::revolving_allocations.index");
        }
    }

    public function addData()
    {
        $this->datas[] = [
            'debt_allocation_group' => '',
            'result_allocation' => '',
            'value' => 0,
        ];

        $this->dispatchBrowserEvent('changeRupiahFormat');
        $this->dispatchBrowserEvent('initDebtGroup', ['companyId' => $this->company->id ?? $this->company]);
    }

    public function subData($i)
    {
        unset($this->datas[$i]);
    }

    public function updatedCreditLineDetail($value)
    {
        $this->revolving_drawdown = FinancialRevolvingDrawdown::with('revolvingLoan')->find($value);
    }

    public function render()
    {
        return view('livewire.financials.revolving-allocations.form');
    }
}
