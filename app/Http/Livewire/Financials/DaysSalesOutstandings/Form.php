<?php

namespace App\Http\Livewire\Financials\DaysSalesOutstandings;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\AgingDso;
use App\Models\Project;
use App\Models\Company;
use App\Models\Category;
use App\Models\Customer;
use App\Models\DebtType;
use App\Models\TradingPartner;

class Form extends Component
{
    public $model;
    public $flag;
    public $company;
    public $project;
    public $partner;
    public $category;
    public $posting_date;
    public $is_parent_company;
    public $unit_id;

    // Variables for store select data
    public $selectCompany;
    public $partners;
    public $customer;
    public $datas = [];

    public function mount($model)
    {
        $this->model = $model;

        $company = Auth::user()->company->first();
        if (!$company) {
            session()->flash('failed', __("You are not related to any company"));
            return redirect()->route("financials::days_sales_outstandings.index");
        }
        
        $this->company = $company;
        if (in_array($company->id, config('const.parent_company'))) {
            $this->is_parent_company = true;
        } elseif (in_array($company->id, config('const.sub_parent_company'))) {
            $this->unit_id = $company->id;
            $this->company = $company->parent;
        }

        $this->category = Category::where('name', 'ACTUAL')->first();

        if (!$this->category) {
            session()->flash('failed', __("ACTUAL category is not found"));
            return redirect()->route("financials::days_sales_outstandings.index");
        }

        $this->flag = DebtType::where('name', 'ACCOUNT RECEIVABLES')->first();

        if (!$this->flag) {
            session()->flash('failed', __("ACCOUNT RECEIVABLES debt type is not found"));
            return redirect()->route("financials::days_sales_outstandings.index");
        }

        $this->partners = TradingPartner::where('status', 1)->orderBy('sorting')->get();

        // Assign default value
        $agings = AgingDso::where('status', 1)->orderBy('sorting')->get();
        foreach ($agings as $aging) {
            $this->datas[] = [
                'aging_id' => $aging->id ?? '',
                'aging_name' => $aging->name ?? '',
                'value' => 0,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'selectCompany' => 'required',
            'project' => 'required',
            'posting_date' => 'required',
            'customer' => 'required',
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.value' => __('Value'),
        ]);

        try {
            $reference = \Str::random(10);
            
            for ($i=0; $i < count($this->datas); $i++) {
                // remove point from value
                $valueMask = str_replace('.', '', $this->datas[$i]['value']);

                $data = new $this->model;
                // $data->company_id = $this->company->id ?? Auth::user()->company->first()->id;
                $data->company_id = $this->selectCompany;
                $data->project_id = Project::find($this->project)->id ?? null;
                $data->project_name = Project::find($this->project)->name ?? null;
                $data->category_id = $this->category->id;
                $data->category_name = $this->category->name;
                $data->debt_type_id = $this->flag->id;
                $data->debt_type_name = $this->flag->name;
                $data->posting_date = $this->posting_date;
                $data->trading_partner = $this->partners->where('id', $this->partner)->first()->name ?? null;
                $data->customer_id = Customer::find($this->customer)->id ?? null;
                $data->customer_name = Customer::find($this->customer)->name ?? null;
                $data->aging_dso_id = $this->datas[$i]['aging_id'] ?? null;
                $data->aging_dso_name = $this->datas[$i]['aging_name'] ?? null;
                $data->value = $valueMask ?? 0;
                $data->reference_id = $reference;
                $data->created_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Days Sales Outstanding Data has been added.');

            session()->flash('success', __("Success to create data!"));
            return redirect()->route("financials::days_sales_outstandings.index");
        } catch (\Exception $ex) {
            $message = __("Failed to create data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::days_sales_outstandings.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.days-sales-outstandings.form');
    }
}
