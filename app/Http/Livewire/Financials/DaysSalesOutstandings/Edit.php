<?php

namespace App\Http\Livewire\Financials\DaysSalesOutstandings;

use Auth;
use Livewire\Component;
use Illuminate\Support\Facades\Log;
use App\Models\FinancialDaysSalesOutstanding;

class Edit extends Component
{
    public $datas = [];

    public function mount($reference)
    {
        $datas = FinancialDaysSalesOutstanding::where('reference_id', $reference)->get();
        foreach ($datas as $data) {
            $this->datas[$data->id] = [
                'aging_id' => $data->aging_dso_id ?? '',
                'aging_name' => $data->aging_dso_name ?? '',
                'value' => $data->value,
            ];
        }
    }

    public function submit()
    {
        $this->validate([
            'datas.*.value' => 'required',
        ], [
            // Custom error message
        ], [
            'datas.*.value' => __('Value'),
        ]);

        try {
            foreach ($this->datas as $key => $dt) {
                // remove point from value
                $valueMask = str_replace('.', '', $dt['value']);

                $data = FinancialDaysSalesOutstanding::find($key);
                $data->value = $valueMask;
                $data->updated_by = Auth::id();
                $data->save();
            }

            activity()->performedOn($data)->log('Days Sales Outstanding Data has been updated.');

            session()->flash('success', __("Success to edit data!"));
            return redirect()->route("financials::days_sales_outstandings.index");
        } catch (\Exception $ex) {
            $message = __("Failed to update data!");

            Log::error($message, [
                'trace' => $ex->getTraceAsString()
            ]);

            session()->flash('failed', $message);
            return redirect()->route("financials::days_sales_outstandings.index");
        }
    }

    public function render()
    {
        return view('livewire.financials.days-sales-outstandings.edit');
    }
}
