<?php
// Header menu
return [
    'items' => [
        [],
        [
            'title' => 'Dashboard',
            'root' => true,
            'toggle' => 'click',
            'submenu' => [
                'type' => 'classic',
                'alignment' => 'left',
                'items' => [
                    [
                        'title' => 'Financial Dashboard',
                        'desc' => '',
                        'icon' => 'flaticon-coins',
                        'bullet' => 'dot',
                        'submenu' => [
                            [
                                'title' => 'Liquidity',
                                'page' => 'financials/liquidities',
                                'permission' => 'financial-liquidity-list'
                            ],
                            [
                                'title' => 'Investment',
                                'page' => 'financials/investments',
                                'permission' => 'financial-investment-list'
                            ],
                            [
                                'title' => 'Days Sales Outstanding',
                                'page' => 'financials/days_sales_outstandings',
                                'permission' => 'financial-dso-list'
                            ],
                            [
                                'title' => 'Term Loan',
                                'page' => 'financials/term_loans',
                                'permission' => 'financial-term-loan-list'
                            ],
                            [
                                'title' => 'Term Loan Allocation',
                                'page' => 'financials/term_loan_allocations',
                                'permission' => 'financial-term-loan-allocation-list'
                            ],
                            [
                                'title' => 'Reverse Term Loan Allocation',
                                'page' => 'financials/term_loan_allocations/reverse',
                                'permission' => 'financial-term-loan-allocation-create'
                            ],
                            [
                                'title' => 'Revolving Loan',
                                'page' => 'financials/revolving_loans',
                                'permission' => 'financial-revolving-loan-list'
                            ],
                            [
                                'title' => 'Revolving Drawdown',
                                'page' => 'financials/revolving_drawdowns',
                                'permission' => 'financial-revolving-drawdown-list'
                            ],
                            [
                                'title' => 'Revolving Allocation',
                                'page' => 'financials/revolving_allocations',
                                'permission' => 'financial-revolving-allocation-list'
                            ],
                            [
                                'title' => 'Reverse Revolving Drawdown',
                                'page' => 'financials/revolving_drawdowns/reverse',
                                'permission' => 'financial-revolving-drawdown-create'
                            ],
                            [
                                'title' => 'NCL',
                                'page' => 'financials/ncls',
                                'permission' => 'financial-ncl-list'
                            ],
                            [
                                'title' => 'NCL Drawdown',
                                'page' => 'financials/ncl_drawdowns',
                                'permission' => 'financial-ncl-drawdown-list'
                            ],
                            [
                                'title' => 'Acceptation',
                                'page' => 'financials/acceptations',
                                'permission' => 'financial-acceptation-list'
                            ],
                            [
                                'title' => 'Reverse NCL Drawdown',
                                'page' => 'financials/ncl_drawdowns/reverse',
                                'permission' => 'financial-ncl-drawdown-create'
                            ],
                            [
                                'title' => 'Cash Conversion Cycle',
                                'page' => 'financials/cash_conversion_cycles',
                                'permission' => 'financial-ccc-list'
                            ],
                            [
                                'title' => 'Project Profitability',
                                'page' => 'financials/project_profitabilities',
                                'permission' => 'financial-project-profitability-list'
                            ]
                        ]
                    ],
                    [
                        'title' => 'PEN Dashboard',
                        'desc' => '',
                        'icon' => 'flaticon-home',
                        'bullet' => 'dot',
                        'submenu' => [
                            [
                                'title' => 'Demand Material Construction',
                                'page' => 'pens/demand_material_constructions',
                                'permission' => 'pen-demand-material-construction-list'
                            ],
                            [
                                'title' => 'Penyediaan Hunian MBR',
                                'page' => 'pens/hunian_mbrs',
                                'permission' => 'pen-penyediaan-hunian-mbr-list'
                            ],
                            [
                                'title' => 'Pendapatan Usaha',
                                'page' => 'pens/company_revenues',
                                'permission' => 'pen-pendapatan-usaha-list'
                            ],
                            [
                                'title' => 'Penyerapan Tenaga Kerja',
                                'page' => 'pens/penyerapan_tenaga_kerjas',
                                'permission' => 'pen-penyerapan-tenaga-kerja-list'
                            ],
                            [
                                'title' => 'Perolehan Kontrak Jumlah Proyek',
                                'page' => 'pens/kontrak_jumlah_proyeks',
                                'permission' => 'pen-perolehan-kontrak-jumlah-proyek-list'
                            ],
                            [
                                'title' => 'Proyek PSN',
                                'page' => 'pens/psn_projects',
                                'permission' => 'pen-proyek-psn-list'
                            ],
                            [
                                'title' => 'Realisasi Belanja UMKM',
                                'page' => 'pens/realisasi_belanja_umkms',
                                'permission' => 'pen-realisasi-belanja-umkm-list'
                            ],
                            [
                                'title' => 'Target Realisasi Capex',
                                'page' => 'pens/target_realisasi_capexes',
                                'permission' => 'pen-target-realisasi-capex-list'
                            ],
                            [
                                'title' => 'Tingkat Komponen Dalam Negeri',
                                'page' => 'pens/komponen_dalam_negeris',
                                'permission' => 'pen-tingkat-komponen-dalam-negeri-list'
                            ]
                        ]

                    ]
                ]
            ]
        ],
    ]

];
