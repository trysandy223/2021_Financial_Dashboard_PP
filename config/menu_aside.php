<?php
// Aside menu
return [

    'items' => [
        // Dashboard
        [
            'title' => 'Dashboard',
            'root' => true,
            'icon' => 'fas fa-home', // or can be 'flaticon-home' or any flaticon-*
            'page' => 'pages/dashboard',
            'new-tab' => false,
        ],

        // Main Menu
        [
            'section' => 'Main Menu',
        ],
        // References
        [
            'title' => 'References',
            'icon' => 'fas fa-database',
            'bullet' => 'dot',
            'root' => true,
            'permission' => 'master-list',
            'submenu' => [
                [
                    'title' => 'Category',
                    'page' => 'pages/categories'
                ],
                [
                    'title' => 'Planning Group',
                    'page' => 'pages/planning_groups'
                ],
                [
                    'title' => 'Currency',
                    'page' => 'pages/currencies'
                ],
                [
                    'title' => 'Distribution Channel',
                    'page' => 'pages/distribution_channels'
                ],
                [
                    'title' => 'Tax Classification',
                    'page' => 'pages/tax_classifications'
                ],
                [
                    'title' => 'Reconsiliation Account',
                    'page' => 'pages/reconsiliation_accounts'
                ],
                [
                    'title' => 'Terms Of Payment',
                    'page' => 'pages/terms_of_payments'
                ],
                [
                    'title' => 'Bank',
                    'page' => 'pages/banks'
                ],
                [
                    'title' => 'House Bank',
                    'page' => 'pages/house_banks'
                ],
                [
                    'title' => 'Account Group',
                    'page' => 'pages/account_groups'
                ],
                [
                    'title' => 'Flag CF',
                    'page' => 'pages/flag_cfs'
                ],
                [
                    'title' => 'Flag CCC',
                    'page' => 'pages/flag_cccs'
                ],
                [
                    'title' => 'Flag Investment',
                    'page' => 'pages/flag_investments'
                ],
                [
                    'title' => 'Flag Profitability',
                    'page' => 'pages/flag_profitabilities'
                ],
                [
                    'title' => 'Material',
                    'page' => 'pages/materials'
                ],
                [
                    'title' => 'Business Segment',
                    'page' => 'pages/business_segments'
                ]
            ]
        ],
        // Master Data
        [
            'title' => 'Master Data',
            'icon' => 'fas fa-server',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Company',
                    'page' => 'pages/companies',
                    'permission' => 'company-list'
                ],
                [
                    'title' => 'Customer',
                    'page' => 'pages/customers',
                    'permission' => 'customer-list'
                ],
                [
                    'title' => 'Project',
                    'page' => 'pages/projects',
                    'permission' => 'project-list'
                ],
                [
                    'title' => 'Trading Partner',
                    'page' => 'pages/trading_partners',
                    'permission' => 'trading-partner-list'
                ],
            ]
        ],
        // Authentication
        [
            'title' => 'Authentication',
            'icon' => 'fas fa-key',
            'bullet' => 'dot',
            'root' => true,
            'permission' => 'master-list',
            'submenu' => [
                [
                    'title' => 'User',
                    'page' => 'pages/users'
                ],
                [
                    'title' => 'Role',
                    'page' => 'pages/roles'
                ],
                [
                    'title' => 'Permission',
                    'page' => 'pages/permissions'
                ],
                [
                    'title' => 'Log Activity',
                    'page' => 'pages/log_activities'
                ]
            ]
        ],
    ]
];
