<?php

return [
    'parent_company' => [1, 2], // Filter company for PP Konsolidasi/PP Induk
    'sub_parent_company' => [3, 4, 9, 12], // Filter company for Infra, Gedung, & EPC

    'trading_partner_type' => [
        'PP' => 'PP',
        'Non PP' => 'Non PP'
    ],

    'company_type' => [
        'Company' => 'Company',
        'Division' => 'Division'
    ],

    'financial_investment_status' => [
        'IN PROGRESS' => 'IN PROGRESS',
        'IN OPERATION' => 'IN OPERATION'
    ],

    'financial_term_loan_type' => [
        'MATURITY' => 'MATURITY',
        'BULANAN' => 'BULANAN'
    ],

    'financial_revolving_drawdown_transaction_type' => [
        'PENCAIRAN' => 'PENCAIRAN',
        'PEMBAYARAN' => 'PEMBAYARAN'
    ],

    'financial_ncl_drawdown_transaction_type' => [
        'PENCAIRAN' => 'PENCAIRAN',
        'PEMBAYARAN' => 'PEMBAYARAN'
    ]
];