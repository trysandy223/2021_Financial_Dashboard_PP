<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialTermLoanAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_term_loan_allocations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained();

            $table->foreignId('project_id')->constrained();
            $table->string('project_name');
            $table->foreignId('category_id')->constrained();
            $table->string('category_name');
            $table->foreignId('trading_partner_id')->nullable();
            $table->string('trading_partner_name')->nullable();
            $table->date('posting_date');
  
            $table->foreignId('financial_term_loan_id')->constrained();
            $table->string('financial_term_loan_name');
            $table->string('bank_name');
            $table->string('source_group_name');
            $table->bigInteger('total')->nullable();
            $table->string('debt_payment_name');
            $table->string('debt_type_name');

            $table->unsignedBigInteger('debt_allocation_group_id')->nullable();
            $table->string('debt_allocation_group_name')->nullable();
            $table->string('result_allocation_name')->nullable();
            $table->bigInteger('value')->nullable();

            $table->boolean('is_reverse')->default(0);
            $table->string('reference_id')->index();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_term_loan_allocations');
    }
}
