<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialAcceptationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_acceptations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained();

            $table->foreignId('ncl_drawdown_id')->constrained('financial_ncl_drawdowns');
            $table->string('ncl_drawdown_name');
            $table->foreignId('project_id')->constrained();
            $table->string('project_name');
            $table->foreignId('category_id')->constrained();
            $table->string('category_name');
            $table->date('posting_date');
            $table->date('maturity_date')->nullable();
            $table->unsignedBigInteger('trading_partner_id')->nullable();
            $table->string('trading_partner_name')->nullable();
            $table->foreignId('bank_id')->constrained();
            $table->string('bank_name');
            $table->foreignId('debt_type_id')->constrained();
            $table->string('debt_type_name');

            $table->unsignedBigInteger('debt_allocation_group_id')->nullable();
            $table->string('debt_allocation_group_name')->nullable();
            $table->string('result_allocation_name')->nullable();
            $table->bigInteger('value')->nullable();

            $table->string('reference_id')->index();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_acceptations');
    }
}
