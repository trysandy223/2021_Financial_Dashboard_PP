<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained();
            $table->unsignedBigInteger('unit_id')->nullable();
            $table->string('code');
            $table->string('name');
            $table->string('manager')->nullable();
            $table->date('start_date')->nullable();
            $table->date('finish_date')->nullable();
            $table->bigInteger('rab')->nullable();
            $table->bigInteger('rkn')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();

            $table->unsignedBigInteger('project_type_id')->nullable();
            $table->string('project_type_name')->nullable();

            $table->unsignedBigInteger('project_strategic_id')->nullable();
            $table->string('project_strategic_name')->nullable();

            $table->unsignedBigInteger('payment_type_id')->nullable();
            $table->string('payment_type_name')->nullable();

            $table->unsignedBigInteger('business_segment_id')->nullable();
            $table->string('business_segment_name')->nullable();

            $table->unsignedBigInteger('customer_id')->nullable();
            $table->string('customer_name')->nullable();

            $table->tinyInteger('is_api')->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
