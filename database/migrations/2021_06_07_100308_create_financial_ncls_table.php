<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialNclsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_ncls', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained();

            $table->foreignId('project_id')->constrained();
            $table->string('project_name');
            $table->foreignId('category_id')->constrained();
            $table->string('category_name');
            $table->foreignId('trading_partner_id')->nullable();
            $table->string('trading_partner_name')->nullable();
            $table->foreignId('bank_id')->constrained();
            $table->string('bank_name');
            $table->foreignId('source_group_id')->constrained();
            $table->string('source_group_name');
            $table->foreignId('debt_type_id')->constrained();
            $table->string('debt_type_name');
            $table->date('posting_date');

            $table->string('debt_name');
            $table->date('credit_loan_opening_date')->nullable();
            $table->date('maturity_date')->nullable();
            $table->double('interest_rate')->nullable();
            $table->bigInteger('value')->nullable();
            $table->double('debt_equity')->nullable();
            $table->double('debt_ebitda')->nullable();
            $table->double('iscr')->nullable();
            $table->unsignedBigInteger('debt_payment_id')->nullable();
            $table->string('debt_payment_name')->nullable();

            $table->string('reference_id')->index();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_ncls');
    }
}
