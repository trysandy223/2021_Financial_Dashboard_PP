<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialRevolvingAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_revolving_allocations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained();

            $table->foreignId('project_id')->constrained();
            $table->string('project_name');
            $table->foreignId('category_id')->constrained();
            $table->string('category_name');
            $table->foreignId('trading_partner_id')->nullable();
            $table->string('trading_partner_name')->nullable();
            $table->foreignId('revolving_drawdown_id')->constrained('financial_revolving_drawdowns');
            $table->string('revolving_drawdown_name');
            $table->date('posting_date');

            $table->unsignedBigInteger('debt_allocation_group_id')->nullable();
            $table->string('debt_allocation_group_name')->nullable();
            $table->string('result_allocation_name')->nullable();
            $table->bigInteger('value')->nullable();

            $table->boolean('is_reverse')->default(0);
            $table->string('reference_id')->index();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_revolving_allocations');
    }
}
