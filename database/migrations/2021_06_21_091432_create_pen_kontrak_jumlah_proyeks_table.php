<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenKontrakJumlahProyeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pen_kontrak_jumlah_proyeks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained();
            
            $table->date('date')->nullable();
            $table->integer('psn_project_total')->nullable();
            $table->bigInteger('psn_project_value')->nullable();
            $table->integer('non_psn_project_total')->nullable();
            $table->bigInteger('non_psn_project_value')->nullable();

            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pen_kontrak_jumlah_proyeks');
    }
}
