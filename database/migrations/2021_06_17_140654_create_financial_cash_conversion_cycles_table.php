<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialCashConversionCyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_cash_conversion_cycles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained();

            $table->foreignId('project_id')->constrained();
            $table->string('project_name');
            $table->date('posting_date');
            $table->string('trading_partner')->nullable();

            $table->foreignId('category_id')->constrained();
            $table->string('category_name');
            $table->foreignId('flag_ccc_id')->nullable();
            $table->string('flag_ccc_name')->nullable();
            $table->bigInteger('value');

            $table->string('reference_id')->index();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_cash_conversion_cycles');
    }
}
