<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCronJobLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cron_job_logs', function (Blueprint $table) {
            $table->dateTime('start_at')->nullable();
            $table->dateTime('finish_at')->nullable();
            $table->string('data_type', 255)->nullable();
            $table->json('data')->nullable();
            $table->string('description', 255)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('api_url', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cron_job_logs');
    }
}
