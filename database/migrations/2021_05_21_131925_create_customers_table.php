<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained();

            $table->string('title')->nullable();
            $table->string('code');
            $table->string('name');
            $table->string('search_term')->nullable();
            $table->text('street')->nullable();
            $table->string('district')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('country')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('extension_1')->nullable();
            $table->string('extension_2')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('npwp')->nullable();
            $table->bigInteger('credit_limit')->nullable();

            $table->unsignedBigInteger('account_group_id')->nullable();
            $table->string('account_group_name')->nullable();

            $table->unsignedBigInteger('reconsiliation_account_id')->nullable();
            $table->string('reconsiliation_account_name')->nullable();

            $table->unsignedBigInteger('planning_group_id')->nullable();
            $table->string('planning_group_name')->nullable();

            $table->unsignedBigInteger('terms_of_payment_id')->nullable();
            $table->string('terms_of_payment_name')->nullable();

            $table->unsignedBigInteger('house_bank_id')->nullable();
            $table->string('house_bank_name')->nullable();

            $table->unsignedBigInteger('currency_id')->nullable();
            $table->string('currency_name')->nullable();

            $table->unsignedBigInteger('tax_classification_id')->nullable();
            $table->string('tax_classification_name')->nullable();
            
            $table->tinyInteger('is_api')->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
