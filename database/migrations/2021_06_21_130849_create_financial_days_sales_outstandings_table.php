<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialDaysSalesOutstandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_days_sales_outstandings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained();

            $table->foreignId('project_id')->constrained();
            $table->string('project_name');
            $table->foreignId('category_id')->constrained();
            $table->string('category_name');
            $table->foreignId('debt_type_id')->constrained();
            $table->string('debt_type_name');
            $table->date('posting_date');
            $table->string('trading_partner')->nullable();

            $table->foreignId('customer_id')->constrained();
            $table->string('customer_name');
            $table->foreignId('aging_dso_id')->constrained();
            $table->string('aging_dso_name');
            $table->bigInteger('value')->nullable();

            $table->string('reference_id')->index();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_days_sales_outstandings');
    }
}
