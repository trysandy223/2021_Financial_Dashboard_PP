<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ModelHasRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {   
        \DB::table('model_has_roles')->insert(array (
            0 => 
            array (
                'model_id' => 1,
                'model_type' => 'App\\Models\\User',
                'role_id' => 1,
            ),
            1 => 
            array (
                'model_id' => 2,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            2 => 
            array (
                'model_id' => 3,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            3 => 
            array (
                'model_id' => 4,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            4 => 
            array (
                'model_id' => 5,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            5 => 
            array (
                'model_id' => 6,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            6 => 
            array (
                'model_id' => 7,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            7 => 
            array (
                'model_id' => 8,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            8 => 
            array (
                'model_id' => 9,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            9 => 
            array (
                'model_id' => 10,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            10 => 
            array (
                'model_id' => 11,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            11 => 
            array (
                'model_id' => 12,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            12 => 
            array (
                'model_id' => 13,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            13 => 
            array (
                'model_id' => 14,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            14 => 
            array (
                'model_id' => 15,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            15 => 
            array (
                'model_id' => 16,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            16 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            17 => 
            array (
                'model_id' => 18,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            18 => 
            array (
                'model_id' => 19,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            19 => 
            array (
                'model_id' => 20,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            20 => 
            array (
                'model_id' => 21,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            21 => 
            array (
                'model_id' => 22,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            22 => 
            array (
                'model_id' => 23,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            23 => 
            array (
                'model_id' => 24,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            24 => 
            array (
                'model_id' => 25,
                'model_type' => 'App\\Models\\User',
                'role_id' => 13,
            ),
            25 => 
            array (
                'model_id' => 26,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            26 => 
            array (
                'model_id' => 27,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            27 => 
            array (
                'model_id' => 28,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            28 => 
            array (
                'model_id' => 29,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            29 => 
            array (
                'model_id' => 30,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            30 => 
            array (
                'model_id' => 31,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            31 => 
            array (
                'model_id' => 32,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            32 => 
            array (
                'model_id' => 33,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            33 => 
            array (
                'model_id' => 34,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            34 => 
            array (
                'model_id' => 35,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            35 => 
            array (
                'model_id' => 36,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            36 => 
            array (
                'model_id' => 37,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            37 => 
            array (
                'model_id' => 38,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            38 => 
            array (
                'model_id' => 39,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            39 => 
            array (
                'model_id' => 40,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            40 => 
            array (
                'model_id' => 41,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            41 => 
            array (
                'model_id' => 42,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            42 => 
            array (
                'model_id' => 43,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            43 => 
            array (
                'model_id' => 44,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            44 => 
            array (
                'model_id' => 45,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            45 => 
            array (
                'model_id' => 46,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            46 => 
            array (
                'model_id' => 47,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            47 => 
            array (
                'model_id' => 48,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            48 => 
            array (
                'model_id' => 49,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            49 => 
            array (
                'model_id' => 50,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            50 => 
            array (
                'model_id' => 51,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            51 => 
            array (
                'model_id' => 52,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            52 => 
            array (
                'model_id' => 53,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            53 => 
            array (
                'model_id' => 54,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            54 => 
            array (
                'model_id' => 55,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            55 => 
            array (
                'model_id' => 56,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            56 => 
            array (
                'model_id' => 57,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            57 => 
            array (
                'model_id' => 58,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            58 => 
            array (
                'model_id' => 59,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            59 => 
            array (
                'model_id' => 60,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            60 => 
            array (
                'model_id' => 61,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            61 => 
            array (
                'model_id' => 62,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            62 => 
            array (
                'model_id' => 63,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            63 => 
            array (
                'model_id' => 64,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            64 => 
            array (
                'model_id' => 65,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            65 => 
            array (
                'model_id' => 66,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            66 => 
            array (
                'model_id' => 67,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            67 => 
            array (
                'model_id' => 68,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            68 => 
            array (
                'model_id' => 69,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            69 => 
            array (
                'model_id' => 70,
                'model_type' => 'App\\Models\\User',
                'role_id' => 6,
            ),
            70 => 
            array (
                'model_id' => 71,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            71 => 
            array (
                'model_id' => 72,
                'model_type' => 'App\\Models\\User',
                'role_id' => 7,
            ),
            72 => 
            array (
                'model_id' => 73,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            73 => 
            array (
                'model_id' => 74,
                'model_type' => 'App\\Models\\User',
                'role_id' => 8,
            ),
            74 => 
            array (
                'model_id' => 75,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            75 => 
            array (
                'model_id' => 76,
                'model_type' => 'App\\Models\\User',
                'role_id' => 5,
            ),
            76 => 
            array (
                'model_id' => 77,
                'model_type' => 'App\\Models\\User',
                'role_id' => 1,
            ),
            77 => 
            array (
                'model_id' => 78,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            78 => 
            array (
                'model_id' => 79,
                'model_type' => 'App\\Models\\User',
                'role_id' => 2,
            ),
            79 => 
            array (
                'model_id' => 80,
                'model_type' => 'App\\Models\\User',
                'role_id' => 1,
            ),
            80 => 
            array (
                'model_id' => 81,
                'model_type' => 'App\\Models\\User',
                'role_id' => 10,
            ),
            81 => 
            array (
                'model_id' => 82,
                'model_type' => 'App\\Models\\User',
                'role_id' => 10,
            ),
            82 => 
            array (
                'model_id' => 83,
                'model_type' => 'App\\Models\\User',
                'role_id' => 9,
            ),
            83 => 
            array (
                'model_id' => 84,
                'model_type' => 'App\\Models\\User',
                'role_id' => 9,
            ),
            84 => 
            array (
                'model_id' => 85,
                'model_type' => 'App\\Models\\User',
                'role_id' => 11,
            ),
            85 => 
            array (
                'model_id' => 86,
                'model_type' => 'App\\Models\\User',
                'role_id' => 11,
            ),
            86 => 
            array (
                'model_id' => 87,
                'model_type' => 'App\\Models\\User',
                'role_id' => 9,
            ),
            87 => 
            array (
                'model_id' => 88,
                'model_type' => 'App\\Models\\User',
                'role_id' => 9,
            ),
            88 => 
            array (
                'model_id' => 89,
                'model_type' => 'App\\Models\\User',
                'role_id' => 12,
            ),
            89 => 
            array (
                'model_id' => 90,
                'model_type' => 'App\\Models\\User',
                'role_id' => 12,
            ),
            90 => 
            array (
                'model_id' => 91,
                'model_type' => 'App\\Models\\User',
                'role_id' => 13,
            ),
            91 => 
            array (
                'model_id' => 92,
                'model_type' => 'App\\Models\\User',
                'role_id' => 14,
            ),
            92 => 
            array (
                'model_id' => 93,
                'model_type' => 'App\\Models\\User',
                'role_id' => 14,
            ),
        )); 
    }
}