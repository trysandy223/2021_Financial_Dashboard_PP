<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AgingDsosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aging_dsos = [
            [
                'name' => '0-30 Days',
                'sorting' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => '31-60 Days',
                'sorting' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => '61-90 Days',
                'sorting' => 3,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => '3-6 Months',
                'sorting' => 4,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => '6-12 Months',
                'sorting' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => '>1 Year',
                'sorting' => 6,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
        ];

        DB::table('aging_dsos')->insert($aging_dsos);
    }
}
