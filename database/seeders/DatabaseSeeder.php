<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CompaniesTableSeeder::class,
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            UserCompaniesTableSeeder::class,
            ModelHasRolesTableSeeder::class,
            CategoriesTableSeeder::class,
            BanksTableSeeder::class,
            PlanningGroupsTableSeeder::class,
            CurrenciesTableSeeder::class,
            DistributionChannelsTableSeeder::class,
            HouseBanksTableSeeder::class,
            TaxClassificationsTableSeeder::class,
            ReconsiliationAccountsTableSeeder::class,
            TermsOfPaymentTableSeeder::class,
            ProjectTypesTableSeeder::class,
            ProjectStrategicsTableSeeder::class,
            PaymentTypesTableSeeder::class,
            DataPeriodsTableSeeder::class,
            FlagCfsTableSeeder::class,
            FlagCccsTableSeeder::class,
            FlagInvestmentsTableSeeder::class,
            FlagProfitabilitiesTableSeeder::class,
            AccountGroupsTableSeeder::class,
            DebtTypesTableSeeder::class,
            DebtPaymentsTableSeeder::class,
            DebtAllocationGroupsTableSeeder::class,
            SourceGroupsTableSeeder::class,
            AgingDsosTableSeeder::class,
            MaterialsTableSeeder::class,
            PenProjectCategoriesTableSeeder::class,
            BusinessSegmentsTableSeeder::class,
            CustomersTableSeeder::class,
            ProjectsTableSeeder::class,
            TradingPartnersTableSeeder::class
        ]);
    }
}
