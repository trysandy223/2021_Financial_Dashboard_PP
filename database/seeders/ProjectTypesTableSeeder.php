<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProjectTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project_types = [
            [
                'name' => 'CONSTRUCTION',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'INVESTMENT',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]
        ];

        DB::table('project_types')->insert($project_types);
    }
}
