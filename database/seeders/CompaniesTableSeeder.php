<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert(array (
            0 => 
            array (
                'code' => 'E_PP',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 1,
                'is_project' => 1,
                'level' => '0',
                'name' => 'PP Konsolidasi',
                'parent_id' => NULL,
                'parent_level' => 0,
                'portion' => 50,
                'sorting' => 0,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            1 => 
            array (
                'code' => 'PP01',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 2,
                'is_project' => 1,
                'level' => '2',
                'name' => 'PP Induk',
                'parent_id' => 1,
                'parent_level' => 1,
                'portion' => 30,
                'sorting' => 2,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            2 => 
            array (
                'code' => 'PP011',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 3,
                'is_project' => 1,
                'level' => '2.1',
                'name' => 'Divisi Infra 1',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 1,
                'status' => 1,
                'type' => 'Division',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            3 => 
            array (
                'code' => 'PP012',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 4,
                'is_project' => 1,
                'level' => '2.2',
                'name' => 'Divisi Infra 2',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 2,
                'status' => 1,
                'type' => 'Division',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            4 => 
            array (
                'code' => 'PP013',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 9,
                'is_project' => 1,
                'level' => '2.4',
                'name' => 'Divisi Gedung',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 4,
                'status' => 1,
                'type' => 'Division',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            5 => 
            array (
                'code' => 'PP014',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 12,
                'is_project' => 1,
                'level' => '2.5',
                'name' => 'Divisi EPC',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 5,
                'status' => 1,
                'type' => 'Division',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            6 => 
            array (
                'code' => 'PP02',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 15,
                'is_project' => 1,
                'level' => '2.6',
                'name' => 'PP Presisi',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 6,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            7 => 
            array (
                'code' => 'PP03',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 18,
                'is_project' => 1,
                'level' => '2.7',
                'name' => 'PP Properti Tbk',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 7,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            8 => 
            array (
                'code' => 'PP04',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 21,
                'is_project' => 1,
                'level' => '2.8',
                'name' => 'PP Urban',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 8,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            9 => 
            array (
                'code' => 'PP05',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 24,
                'is_project' => 1,
                'level' => '2.9',
                'name' => 'PP Energi',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 9,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            10 => 
            array (
                'code' => 'PP06',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 27,
                'is_project' => 1,
                'level' => '2.10',
                'name' => 'PP Infrastruktur',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 10,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            11 => 
            array (
                'code' => 'PP07',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 30,
                'is_project' => 1,
                'level' => '2.11',
                'name' => 'PP Colomadu',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 11,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            12 => 
            array (
                'code' => 'PP08',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 33,
                'is_project' => 1,
                'level' => '2.12',
                'name' => 'PP Centurion',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 12,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            13 => 
            array (
                'code' => 'PP09',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 36,
                'is_project' => 1,
                'level' => '2.13',
                'name' => 'PP Semarang Demak',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 13,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            14 => 
            array (
                'code' => 'PP10',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 39,
                'is_project' => 1,
                'level' => '2.14',
                'name' => 'PP Celebes Railway Indonesia',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 14,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            15 => 
            array (
                'code' => 'PP11',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 42,
                'is_project' => 1,
                'level' => '2.15',
                'name' => 'PP Banjaratma',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 15,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            16 => 
            array (
                'code' => 'PP41',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 45,
                'is_project' => 1,
                'level' => '2.8.1',
                'name' => 'PT Griyaton Indonesia',
                'parent_id' => 21,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 1,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            17 => 
            array (
                'code' => 'PP31',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 46,
                'is_project' => 1,
                'level' => '2.7.1',
                'name' => 'PT Hasta Kreasi Mandiri',
                'parent_id' => 18,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 1,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            18 => 
            array (
                'code' => 'PP32',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 47,
                'is_project' => 1,
                'level' => '2.7.2',
                'name' => 'PT Gitanusa Sarana Niaga',
                'parent_id' => 18,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 2,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            19 => 
            array (
                'code' => 'PP33',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 48,
                'is_project' => 1,
                'level' => '2.7.3',
                'name' => 'PT Wisma Seratus Sejahtera',
                'parent_id' => 18,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 1,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            20 => 
            array (
                'code' => 'PP35',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 50,
                'is_project' => 1,
                'level' => '2.7.5',
                'name' => 'PT PPRO Sampurna Jaya',
                'parent_id' => 18,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 5,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            21 => 
            array (
                'code' => 'PP36',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 51,
                'is_project' => 1,
                'level' => '2.7.6',
                'name' => 'PT PPRO BIJB Aerocity Development',
                'parent_id' => 18,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 6,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            22 => 
            array (
                'code' => 'PP38',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 53,
                'is_project' => 1,
                'level' => '2.7.8',
                'name' => 'PT Limasland Realty Cilegon',
                'parent_id' => 18,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 8,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            23 => 
            array (
                'code' => 'PP39',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 54,
                'is_project' => 1,
                'level' => '2.7.9',
                'name' => 'PT Grahaprima Realtindo',
                'parent_id' => 18,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 9,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            24 => 
            array (
                'code' => 'PPF1',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 55,
                'is_project' => 1,
                'level' => '2.26',
                'name' => 'PT. JASAMARGA KUALANAMU TOL',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 25,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            25 => 
            array (
                'code' => 'PPF2',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 56,
                'is_project' => 1,
                'level' => '2.27',
                'name' => 'PT. CITRA WASPPHUTOWA',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 26,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            26 => 
            array (
                'code' => 'PPF3',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 57,
                'is_project' => 1,
                'level' => '2.28',
                'name' => 'PT. JASAMARGA BALIKPAPAN SAMARINDA',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 27,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            27 => 
            array (
                'code' => 'PPF4',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 58,
                'is_project' => 1,
                'level' => '2.29',
                'name' => 'PT. JASAMARGA PANDAAN MALANG',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 28,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            28 => 
            array (
                'code' => 'PPF5',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 59,
                'is_project' => 1,
                'level' => '2.30',
                'name' => 'PT. JASAMARGA MANADO BITUNG',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 29,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            29 => 
            array (
                'code' => 'PPF6',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 60,
                'is_project' => 1,
                'level' => '2.31',
                'name' => 'PT. WIKA SERANG PANIMBANG',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 30,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            30 => 
            array (
                'code' => 'PPF7',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 61,
                'is_project' => 1,
                'level' => '2.32',
                'name' => 'PT. JASAMARGA JOGJA BAWEN',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 31,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            31 => 
            array (
                'code' => 'PPF8',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 62,
                'is_project' => 1,
                'level' => '2.33',
                'name' => 'PT. PRIMA MULTI TERMINAL',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 32,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            32 => 
            array (
                'code' => 'PPF9',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 63,
                'is_project' => 1,
                'level' => '2.34',
                'name' => 'PT. SINERGI INVESTASI PROPERTI',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 33,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            33 => 
            array (
                'code' => 'PPF10',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 64,
                'is_project' => 1,
                'level' => '2.35',
                'name' => 'PT. INDONESIA FERRY PROPERTY',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 34,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            34 => 
            array (
                'code' => 'PPF11',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 65,
                'is_project' => 1,
                'level' => '2.36',
                'name' => 'PT. JASAMARGA RESTAREA BATANG',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 35,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            35 => 
            array (
                'code' => 'PPF12',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 66,
                'is_project' => 1,
                'level' => '2.37',
                'name' => 'PT. SOLO CITRA METRO PLASMA POWER',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 36,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            36 => 
            array (
                'code' => 'PP12',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 67,
                'is_project' => 1,
                'level' => '2.38',
                'name' => 'PT. KAWASAN INDUSTRI TERPADU BATANG',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 37,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            37 => 
            array (
                'code' => 'PP13',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 68,
                'is_project' => 1,
                'level' => '2.39',
                'name' => 'KSU DANAREKSA',
                'parent_id' => 2,
                'parent_level' => 2,
                'portion' => 0,
                'sorting' => 38,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            38 => 
            array (
                'code' => 'PP21',
                'created_at' => '2021-06-10 02:01:02',
                'created_by' => 1,
                'description' => NULL,
                'id' => 69,
                'is_project' => 1,
                'level' => '2.6.1',
                'name' => 'Lancar Jaya Mandiri Abadi',
                'parent_id' => 15,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 39,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-06-10 02:01:02',
                'updated_by' => NULL,
            ),
            39 => 
            array (
                'code' => 'PP34',
                'created_at' => '2021-07-27 13:44:53',
                'created_by' => 1,
                'description' => 'PP Properti Jababeka Residen',
                'id' => 70,
                'is_project' => 1,
                'level' => '2.7.4',
                'name' => 'PP Properti Jababeka Residen',
                'parent_id' => 18,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 4,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-07-27 13:44:53',
                'updated_by' => NULL,
            ),
            40 => 
            array (
                'code' => 'PP37',
                'created_at' => '2021-07-27 13:45:37',
                'created_by' => 1,
                'description' => 'PP Properti Suramadu',
                'id' => 71,
                'is_project' => 1,
                'level' => '2.7.7',
                'name' => 'PP Properti Suramadu',
                'parent_id' => 18,
                'parent_level' => 3,
                'portion' => 0,
                'sorting' => 7,
                'status' => 1,
                'type' => 'Company',
                'updated_at' => '2021-07-27 13:46:20',
                'updated_by' => 1,
            ),
        ));
    }
}
