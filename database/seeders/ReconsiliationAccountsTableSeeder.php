<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ReconsiliationAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reconsiliation_accounts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => '1110100000',
                'name' => 'Piutang Konstruksi',
                'sorting' => 1,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => '1110200000',
                'name' => 'Piutang Retensi',
                'sorting' => 2,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => '1139900000',
                'name' => 'Piutang Kepada Anak Perusahaan',
                'sorting' => 3,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => '2050100000',
                'name' => 'Penerimaan Uang Muka Pemilik Proyek',
                'sorting' => 4,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => '1150200000',
                'name' => 'Piutang Proyek Investment',
                'sorting' => 5,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => '2030100000',
                'name' => 'Utang Bank Jangka Pendek',
                'sorting' => 6,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            6 => 
            array (
                'id' => 7,
                'code' => '1190100000',
            'name' => 'Piutang (Laba) JO',
                'sorting' => 7,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            7 => 
            array (
                'id' => 8,
                'code' => '1139900001',
            'name' => 'Piutang Kepada Anak Perusahaan (DN)',
                'sorting' => 8,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
        ));
    }
}
