<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TaxClassificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tax_classifications = [
            [
                'code' => '0',
                'name' => 'Ekspor, ZEE, Berikat',
                'sorting' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'code' => '1',
                'name' => 'Swasta',
                'sorting' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'code' => '2',
                'name' => 'Bendaharawan',
                'sorting' => 3,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
        ];

        DB::table('tax_classifications')->insert($tax_classifications);
    }
}
