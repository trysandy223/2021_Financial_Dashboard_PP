<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DebtAllocationGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $debt_allocation_groups = [
            [
                'name' => 'CONSTRUCTION PROJECT',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'SHAREHOLDER LOANS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'SUBSIDIARY INVESTMENT',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'CORP. INVEST & OTHERS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'OTHER DEBT PAYMENT TERM',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'OTHER DEBT PAYMENT REVOLVING',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]
        ];

        DB::table('debt_allocation_groups')->insert($debt_allocation_groups);
    }
}
