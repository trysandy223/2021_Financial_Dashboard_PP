<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class MaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $materials = [
            [
                'name' => 'Semen',
                'satuan' => 'Ton',
                'sorting' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Beton Ready Mix',
                'satuan' => 'm3',
                'sorting' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Beton',
                'satuan' => 'm3',
                'sorting' => 3,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Besi',
                'satuan' => 'Kg',
                'sorting' => 4,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Aspal',
                'satuan' => 'Kg',
                'sorting' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]
        ];

        DB::table('materials')->insert($materials);
    }
}
