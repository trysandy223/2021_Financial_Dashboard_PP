<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $arrayOfActions = ['list', 'create', 'edit', 'delete'];
        $arrayOfPermissionNames = [
            'master',
            'company',
            'customer',
            'project',
            'trading-partner',
            'financial-liquidity',
            'financial-investment',
            'financial-term-loan',
            'financial-term-loan-allocation',
            'financial-revolving-loan',
            'financial-revolving-drawdown',
            'financial-revolving-allocation',
            'financial-ncl',
            'financial-ncl-drawdown',
            'financial-acceptation',
            'financial-ccc',
            'financial-project-profitability',
            'financial-dso',
            'pen-demand-material-construction',
            'pen-penyediaan-hunian-mbr',
            'pen-pendapatan-usaha',
            'pen-penyerapan-tenaga-kerja',
            'pen-perolehan-kontrak-jumlah-proyek',
            'pen-proyek-psn',
            'pen-realisasi-belanja-umkm',
            'pen-target-realisasi-capex',
            'pen-tingkat-komponen-dalam-negeri',
        ];

        foreach ($arrayOfPermissionNames as $permission) {
            foreach ($arrayOfActions as $action) {
                DB::table('permissions')->insert([
                    'name' => $permission . '-' . $action, 
                    'guard_name' => 'web',
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }
        }
    }
}
