<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DistributionChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $distribution_channels = [
            [
                'code' => '001',
                'name' => 'TEST 1',
                'sorting' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'code' => '002',
                'name' => 'TEST 2',
                'sorting' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'code' => '003',
                'name' => 'TEST 3',
                'sorting' => 3,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
        ];

        DB::table('distribution_channels')->insert($distribution_channels);
    }
}
