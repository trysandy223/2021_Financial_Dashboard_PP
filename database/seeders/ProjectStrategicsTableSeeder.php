<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProjectStrategicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project_strategics = [
            [
                'name' => 'PSN',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'NON PSN',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]
        ];

        DB::table('project_strategics')->insert($project_strategics);
    }
}
