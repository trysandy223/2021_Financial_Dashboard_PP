<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserCompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_companies')->insert(array (
            0 => 
            array (
                'company_id' => 1,
                'user_id' => 2,
            ),
            1 => 
            array (
                'company_id' => 2,
                'user_id' => 3,
            ),
            2 => 
            array (
                'company_id' => 2,
                'user_id' => 5,
            ),
            3 => 
            array (
                'company_id' => 2,
                'user_id' => 6,
            ),
            4 => 
            array (
                'company_id' => 2,
                'user_id' => 7,
            ),
            5 => 
            array (
                'company_id' => 2,
                'user_id' => 9,
            ),
            6 => 
            array (
                'company_id' => 15,
                'user_id' => 11,
            ),
            7 => 
            array (
                'company_id' => 18,
                'user_id' => 12,
            ),
            8 => 
            array (
                'company_id' => 21,
                'user_id' => 13,
            ),
            9 => 
            array (
                'company_id' => 24,
                'user_id' => 14,
            ),
            10 => 
            array (
                'company_id' => 27,
                'user_id' => 15,
            ),
            11 => 
            array (
                'company_id' => 30,
                'user_id' => 16,
            ),
            12 => 
            array (
                'company_id' => 33,
                'user_id' => 17,
            ),
            13 => 
            array (
                'company_id' => 36,
                'user_id' => 18,
            ),
            14 => 
            array (
                'company_id' => 42,
                'user_id' => 20,
            ),
            15 => 
            array (
                'company_id' => 1,
                'user_id' => 1,
            ),
            16 => 
            array (
                'company_id' => 39,
                'user_id' => 19,
            ),
            17 => 
            array (
                'company_id' => 2,
                'user_id' => 10,
            ),
            18 => 
            array (
                'company_id' => 2,
                'user_id' => 8,
            ),
            19 => 
            array (
                'company_id' => 2,
                'user_id' => 21,
            ),
            20 => 
            array (
                'company_id' => 2,
                'user_id' => 22,
            ),
            21 => 
            array (
                'company_id' => 2,
                'user_id' => 4,
            ),
            22 => 
            array (
                'company_id' => 1,
                'user_id' => 23,
            ),
            23 => 
            array (
                'company_id' => 1,
                'user_id' => 24,
            ),
            24 => 
            array (
                'company_id' => 1,
                'user_id' => 25,
            ),
            25 => 
            array (
                'company_id' => 1,
                'user_id' => 26,
            ),
            26 => 
            array (
                'company_id' => 1,
                'user_id' => 27,
            ),
            27 => 
            array (
                'company_id' => 18,
                'user_id' => 28,
            ),
            28 => 
            array (
                'company_id' => 18,
                'user_id' => 29,
            ),
            29 => 
            array (
                'company_id' => 15,
                'user_id' => 30,
            ),
            30 => 
            array (
                'company_id' => 15,
                'user_id' => 31,
            ),
            31 => 
            array (
                'company_id' => 15,
                'user_id' => 32,
            ),
            32 => 
            array (
                'company_id' => 15,
                'user_id' => 33,
            ),
            33 => 
            array (
                'company_id' => 15,
                'user_id' => 34,
            ),
            34 => 
            array (
                'company_id' => 15,
                'user_id' => 35,
            ),
            35 => 
            array (
                'company_id' => 15,
                'user_id' => 36,
            ),
            36 => 
            array (
                'company_id' => 15,
                'user_id' => 37,
            ),
            37 => 
            array (
                'company_id' => 21,
                'user_id' => 38,
            ),
            38 => 
            array (
                'company_id' => 21,
                'user_id' => 39,
            ),
            39 => 
            array (
                'company_id' => 21,
                'user_id' => 40,
            ),
            40 => 
            array (
                'company_id' => 21,
                'user_id' => 41,
            ),
            41 => 
            array (
                'company_id' => 21,
                'user_id' => 42,
            ),
            42 => 
            array (
                'company_id' => 21,
                'user_id' => 43,
            ),
            43 => 
            array (
                'company_id' => 21,
                'user_id' => 44,
            ),
            44 => 
            array (
                'company_id' => 21,
                'user_id' => 45,
            ),
            45 => 
            array (
                'company_id' => 24,
                'user_id' => 46,
            ),
            46 => 
            array (
                'company_id' => 24,
                'user_id' => 47,
            ),
            47 => 
            array (
                'company_id' => 24,
                'user_id' => 48,
            ),
            48 => 
            array (
                'company_id' => 24,
                'user_id' => 49,
            ),
            49 => 
            array (
                'company_id' => 24,
                'user_id' => 50,
            ),
            50 => 
            array (
                'company_id' => 24,
                'user_id' => 51,
            ),
            51 => 
            array (
                'company_id' => 24,
                'user_id' => 52,
            ),
            52 => 
            array (
                'company_id' => 24,
                'user_id' => 53,
            ),
            53 => 
            array (
                'company_id' => 27,
                'user_id' => 54,
            ),
            54 => 
            array (
                'company_id' => 27,
                'user_id' => 55,
            ),
            55 => 
            array (
                'company_id' => 30,
                'user_id' => 56,
            ),
            56 => 
            array (
                'company_id' => 30,
                'user_id' => 57,
            ),
            57 => 
            array (
                'company_id' => 30,
                'user_id' => 58,
            ),
            58 => 
            array (
                'company_id' => 30,
                'user_id' => 59,
            ),
            59 => 
            array (
                'company_id' => 30,
                'user_id' => 60,
            ),
            60 => 
            array (
                'company_id' => 30,
                'user_id' => 61,
            ),
            61 => 
            array (
                'company_id' => 30,
                'user_id' => 62,
            ),
            62 => 
            array (
                'company_id' => 30,
                'user_id' => 63,
            ),
            63 => 
            array (
                'company_id' => 33,
                'user_id' => 64,
            ),
            64 => 
            array (
                'company_id' => 33,
                'user_id' => 65,
            ),
            65 => 
            array (
                'company_id' => 33,
                'user_id' => 66,
            ),
            66 => 
            array (
                'company_id' => 33,
                'user_id' => 67,
            ),
            67 => 
            array (
                'company_id' => 33,
                'user_id' => 68,
            ),
            68 => 
            array (
                'company_id' => 36,
                'user_id' => 69,
            ),
            69 => 
            array (
                'company_id' => 36,
                'user_id' => 70,
            ),
            70 => 
            array (
                'company_id' => 36,
                'user_id' => 71,
            ),
            71 => 
            array (
                'company_id' => 36,
                'user_id' => 72,
            ),
            72 => 
            array (
                'company_id' => 36,
                'user_id' => 73,
            ),
            73 => 
            array (
                'company_id' => 36,
                'user_id' => 74,
            ),
            74 => 
            array (
                'company_id' => 36,
                'user_id' => 75,
            ),
            75 => 
            array (
                'company_id' => 36,
                'user_id' => 76,
            ),
            76 => 
            array (
                'company_id' => 1,
                'user_id' => 77,
            ),
            77 => 
            array (
                'company_id' => 1,
                'user_id' => 78,
            ),
            78 => 
            array (
                'company_id' => 1,
                'user_id' => 79,
            ),
            79 => 
            array (
                'company_id' => 1,
                'user_id' => 80,
            ),
            80 => 
            array (
                'company_id' => 1,
                'user_id' => 81,
            ),
            81 => 
            array (
                'company_id' => 1,
                'user_id' => 82,
            ),
            82 => 
            array (
                'company_id' => 1,
                'user_id' => 83,
            ),
            83 => 
            array (
                'company_id' => 1,
                'user_id' => 84,
            ),
            84 => 
            array (
                'company_id' => 1,
                'user_id' => 85,
            ),
            85 => 
            array (
                'company_id' => 1,
                'user_id' => 86,
            ),
            86 => 
            array (
                'company_id' => 1,
                'user_id' => 87,
            ),
            87 => 
            array (
                'company_id' => 1,
                'user_id' => 88,
            ),
            88 => 
            array (
                'company_id' => 1,
                'user_id' => 89,
            ),
            89 => 
            array (
                'company_id' => 1,
                'user_id' => 90,
            ),
            90 => 
            array (
                'company_id' => 1,
                'user_id' => 91,
            ),
            91 => 
            array (
                'company_id' => 1,
                'user_id' => 92,
            ),
            92 => 
            array (
                'company_id' => 1,
                'user_id' => 93,
            ),
        ));
    }
}
