<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AccountGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account_groups = [
            [
                'code' => 'ZC01',
                'name' => 'ACCOUNT GROUP 1',
                'sorting' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'code' => 'ZC02',
                'name' => 'ACCOUNT GROUP 2',
                'sorting' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'code' => 'ZC03',
                'name' => 'ACCOUNT GROUP 3',
                'sorting' => 3,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'code' => 'ZC04',
                'name' => 'ACCOUNT GROUP 4',
                'sorting' => 4,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'code' => 'ZC05',
                'name' => 'ACCOUNT GROUP 5',
                'sorting' => 5,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'code' => 'ZC06',
                'name' => 'ACCOUNT GROUP 6',
                'sorting' => 6,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
        ];

        DB::table('account_groups')->insert($account_groups);
    }
}
