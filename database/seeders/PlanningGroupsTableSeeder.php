<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PlanningGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('planning_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'C1',
                'name' => 'Pemerintah',
                'sorting' => 1,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'C2',
                'name' => 'BUMN/BUMD',
                'sorting' => 2,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'C3',
                'name' => 'Swasta Lokal',
                'sorting' => 3,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'C4',
                'name' => 'Asing',
                'sorting' => 4,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'C5',
                'name' => 'Affiliated',
                'sorting' => 5,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'C6',
                'name' => 'Joint Operation',
                'sorting' => 6,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }
}
