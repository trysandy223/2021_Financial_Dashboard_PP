<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TermsOfPaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terms_of_payments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'ZT01',
                'name' => 'within 7 days Due net',
                'sorting' => 1,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'ZT02',
                'name' => 'within 14 days Due net',
                'sorting' => 2,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'ZT03',
                'name' => 'within 21 days Due net',
                'sorting' => 3,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'ZT04',
                'name' => 'within 28 days Due net',
                'sorting' => 4,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'ZT05',
                'name' => 'within 30 days Due net',
                'sorting' => 5,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'ZT06',
                'name' => 'within 9 days Due net',
                'sorting' => 6,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'ZT07',
                'name' => 'within 16 days Due net',
                'sorting' => 7,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'ZT08',
                'name' => 'within 23 days Due net',
                'sorting' => 8,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'ZT09',
                'name' => 'within 32 days Due net',
                'sorting' => 9,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            9 => 
            array (
                'id' => 10,
                'code' => 'ZT16',
                'name' => 'within 40 days Due net',
                'sorting' => 10,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            10 => 
            array (
                'id' => 11,
                'code' => 'ZT17',
                'name' => 'within 45 days Due net',
                'sorting' => 11,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            11 => 
            array (
                'id' => 12,
                'code' => 'ZT18',
                'name' => 'within 56 days Due net',
                'sorting' => 12,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
        ));
    }
}
