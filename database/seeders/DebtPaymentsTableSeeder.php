<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DebtPaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $debt_payments = [
            [
                'name' => 'LATE PAYMENT',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'NOT LATE',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]
        ];

        DB::table('debt_payments')->insert($debt_payments);
    }
}
