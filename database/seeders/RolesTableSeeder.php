<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Superadmin',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Project Admin',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Open API',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Guest & Open API',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],

            // Financial Dashboard
            [
                'name' => 'Admin Fd',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Fd Fin',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Fd Inv',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Fd Ops',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],


            // PEN Dashboard
            [
                'name' => 'Admin PEN',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'PEN Material',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'PEN TK',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'PEN UMKM',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Fd PEN Inv',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'PEN TKD',
                'guard_name' => 'web',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]
        ];

        foreach ($roles as $role) {
            $data = Role::create($role);

            if ($role['name'] == 'Superadmin') {
                $data->givePermissionTo(Permission::all());
            }

            if ($role['name'] == 'Project Admin') {
                $data->givePermissionTo(Permission::whereNotIn('name', [
                    'master-list', 'master-create', 'master-edit', 'master-delete',
                    'company-list', 'company-create', 'company-edit', 'company-delete',
                    'customer-list', 'customer-create', 'customer-edit', 'customer-delete',
                    'project-list', 'project-create', 'project-edit', 'project-delete',
                    'trading-partner-list', 'trading-partner-create', 'trading-partner-edit', 'trading-partner-delete'
                ])->get());
            }

            // Assign permissions to Financial Dashboard roles
            if ($role['name'] == 'Admin Fd') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'financial-liquidity-list', 'financial-liquidity-create', 'financial-liquidity-edit', 'financial-liquidity-delete',
                    'financial-investment-list', 'financial-investment-create', 'financial-investment-edit', 'financial-investment-delete',
                    'financial-term-loan-list', 'financial-term-loan-create', 'financial-term-loan-edit', 'financial-term-loan-delete',
                    'financial-term-loan-allocation-list', 'financial-term-loan-allocation-create', 'financial-term-loan-allocation-edit', 'financial-term-loan-allocation-delete',
                    'financial-revolving-loan-list', 'financial-revolving-loan-create', 'financial-revolving-loan-edit', 'financial-revolving-loan-delete',
                    'financial-revolving-drawdown-list', 'financial-revolving-drawdown-create', 'financial-revolving-drawdown-edit', 'financial-revolving-drawdown-delete',
                    'financial-revolving-allocation-list', 'financial-revolving-allocation-create', 'financial-revolving-allocation-edit', 'financial-revolving-allocation-delete',
                    'financial-ncl-list', 'financial-ncl-create', 'financial-ncl-edit', 'financial-ncl-delete',
                    'financial-ncl-drawdown-list', 'financial-ncl-drawdown-create', 'financial-ncl-drawdown-edit', 'financial-ncl-drawdown-delete',
                    'financial-acceptation-list', 'financial-acceptation-create', 'financial-acceptation-edit', 'financial-acceptation-delete',
                    'financial-ccc-list', 'financial-ccc-create', 'financial-ccc-edit', 'financial-ccc-delete',
                    'financial-project-profitability-list', 'financial-project-profitability-create', 'financial-project-profitability-edit', 'financial-project-profitability-delete',
                    'financial-dso-list', 'financial-dso-create', 'financial-dso-edit', 'financial-dso-delete'
                ])->get());
            }

            if ($role['name'] == 'Fd Fin') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'financial-liquidity-list', 'financial-liquidity-create', 'financial-liquidity-edit', 'financial-liquidity-delete',
                    'financial-term-loan-list', 'financial-term-loan-create', 'financial-term-loan-edit', 'financial-term-loan-delete',
                    'financial-term-loan-allocation-list', 'financial-term-loan-allocation-create', 'financial-term-loan-allocation-edit', 'financial-term-loan-allocation-delete',
                    'financial-revolving-loan-list', 'financial-revolving-loan-create', 'financial-revolving-loan-edit', 'financial-revolving-loan-delete',
                    'financial-revolving-drawdown-list', 'financial-revolving-drawdown-create', 'financial-revolving-drawdown-edit', 'financial-revolving-drawdown-delete',
                    'financial-revolving-allocation-list', 'financial-revolving-allocation-create', 'financial-revolving-allocation-edit', 'financial-revolving-allocation-delete',
                    'financial-ccc-list', 'financial-ccc-create', 'financial-ccc-edit', 'financial-ccc-delete'
                ])->get());
            }

            if ($role['name'] == 'Fd Inv') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'financial-investment-list', 'financial-investment-create', 'financial-investment-edit', 'financial-investment-delete'
                ])->get());
            }

            if ($role['name'] == 'Fd Ops') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'financial-project-profitability-list', 'financial-project-profitability-create', 'financial-project-profitability-edit', 'financial-project-profitability-delete'
                ])->get());
            }


            // Assign permissions to PEN Dashboard roles
            if ($role['name'] == 'Admin PEN') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'pen-pendapatan-usaha-list', 'pen-pendapatan-usaha-create', 'pen-pendapatan-usaha-edit', 'pen-pendapatan-usaha-delete',
                    'pen-perolehan-kontrak-jumlah-proyek-list', 'pen-perolehan-kontrak-jumlah-proyek-create', 'pen-perolehan-kontrak-jumlah-proyek-edit', 'pen-perolehan-kontrak-jumlah-proyek-delete',
                    'pen-proyek-psn-list', 'pen-proyek-psn-create', 'pen-proyek-psn-edit', 'pen-proyek-psn-delete'
                ])->get());
            }

            if ($role['name'] == 'PEN Material') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'pen-demand-material-construction-list', 'pen-demand-material-construction-create', 'pen-demand-material-construction-edit', 'pen-demand-material-construction-delete'
                ])->get());
            }

            if ($role['name'] == 'PEN TK') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'pen-penyerapan-tenaga-kerja-list', 'pen-penyerapan-tenaga-kerja-create', 'pen-penyerapan-tenaga-kerja-edit', 'pen-penyerapan-tenaga-kerja-delete'
                ])->get());
            }

            if ($role['name'] == 'PEN UMKM') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'pen-realisasi-belanja-umkm-list', 'pen-realisasi-belanja-umkm-create', 'pen-realisasi-belanja-umkm-edit', 'pen-realisasi-belanja-umkm-delete'
                ])->get());
            }

            if ($role['name'] == 'Fd PEN Inv') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'financial-investment-list', 'financial-investment-create', 'financial-investment-edit', 'financial-investment-delete',
                    'pen-target-realisasi-capex-list', 'pen-target-realisasi-capex-create', 'pen-target-realisasi-capex-edit', 'pen-target-realisasi-capex-delete'
                ])->get());
            }

            if ($role['name'] == 'PEN TKD') {
                $data->givePermissionTo(Permission::whereIn('name', [
                    'pen-tingkat-komponen-dalam-negeri-list', 'pen-tingkat-komponen-dalam-negeri-create', 'pen-tingkat-komponen-dalam-negeri-edit', 'pen-tingkat-komponen-dalam-negeri-delete'
                ])->get());
            }
        }
    }
}
