<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class HouseBanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('house_banks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'BII01',
                'name' => 'BIIIDJKT0001',
                'sorting' => 1,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'BJB01',
                'name' => 'BJBIDJKT0001',
                'sorting' => 2,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'BKP01',
                'name' => 'BKPIDJKT0001',
                'sorting' => 3,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'BKP02',
                'name' => 'BKPIDSBY0001',
                'sorting' => 4,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'BNI01',
                'name' => 'BNIIDJKT0001',
                'sorting' => 5,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'BNI02',
                'name' => 'BNIIDSBY0001',
                'sorting' => 6,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'BOT01',
                'name' => 'BOTIDJKT0001',
                'sorting' => 7,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'BPD01',
                'name' => 'BPDIDBPN0001',
                'sorting' => 8,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'BRI01',
                'name' => 'BRIIDJKT0001',
                'sorting' => 9,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            9 => 
            array (
                'id' => 10,
                'code' => 'BRI02',
                'name' => 'BRIIDPLB0001',
                'sorting' => 10,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            10 => 
            array (
                'id' => 11,
                'code' => 'BRI03',
                'name' => 'BRIIDSBY0001',
                'sorting' => 11,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            11 => 
            array (
                'id' => 12,
                'code' => 'BRI04',
                'name' => 'BRIIDSBY0002',
                'sorting' => 12,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            12 => 
            array (
                'id' => 13,
                'code' => 'BRI05',
                'name' => 'BRIIDTKG0001',
                'sorting' => 13,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            13 => 
            array (
                'id' => 14,
                'code' => 'BSM01',
                'name' => 'BSMIDJKT0001',
                'sorting' => 14,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            14 => 
            array (
                'id' => 15,
                'code' => 'BTN01',
                'name' => 'BTNIDJKT0001',
                'sorting' => 15,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            15 => 
            array (
                'id' => 16,
                'code' => 'CIMB1',
                'name' => 'CIMBIDJKT0001',
                'sorting' => 16,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            16 => 
            array (
                'id' => 17,
                'code' => 'CITI1',
                'name' => 'CITIIDJKT0001',
                'sorting' => 17,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            17 => 
            array (
                'id' => 18,
                'code' => 'DBS01',
                'name' => 'DBSIDJKT0001',
                'sorting' => 18,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            18 => 
            array (
                'id' => 19,
                'code' => 'DKI01',
                'name' => 'DKIIDJKT0001',
                'sorting' => 19,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            19 => 
            array (
                'id' => 20,
                'code' => 'DNM01',
                'name' => 'DNMIDJKT0001',
                'sorting' => 20,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            20 => 
            array (
                'id' => 21,
                'code' => 'EXM01',
                'name' => 'EXIMIDJKT0001',
                'sorting' => 21,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            21 => 
            array (
                'id' => 22,
                'code' => 'HSBC1',
                'name' => 'HSBCIDJKT0001',
                'sorting' => 22,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            22 => 
            array (
                'id' => 23,
                'code' => 'JTM01',
                'name' => 'JTMIDSBY0001',
                'sorting' => 23,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            23 => 
            array (
                'id' => 24,
                'code' => 'MAYB1',
                'name' => 'MAYIDJKT0001',
                'sorting' => 24,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            24 => 
            array (
                'id' => 25,
                'code' => 'MDR01',
                'name' => 'MDRIDJKT0001',
                'sorting' => 25,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            25 => 
            array (
                'id' => 26,
                'code' => 'MDR02',
                'name' => 'MDRIDMDN0001',
                'sorting' => 26,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            26 => 
            array (
                'id' => 27,
                'code' => 'MDR03',
                'name' => 'MDRIDMKS0001',
                'sorting' => 27,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            27 => 
            array (
                'id' => 28,
                'code' => 'MDR04',
                'name' => 'MDRIDPDG0001',
                'sorting' => 28,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            28 => 
            array (
                'id' => 29,
                'code' => 'MDR05',
                'name' => 'MDRIDPKB0001',
                'sorting' => 29,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            29 => 
            array (
                'id' => 30,
                'code' => 'MDR06',
                'name' => 'MDRIDSBY0001',
                'sorting' => 30,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            30 => 
            array (
                'id' => 31,
                'code' => 'PMT01',
                'name' => 'PMTIDJKT0001',
                'sorting' => 31,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            31 => 
            array (
                'id' => 32,
                'code' => 'PRB01',
                'name' => 'PRBIDJKT0001',
                'sorting' => 32,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
            32 => 
            array (
                'id' => 33,
                'code' => 'UOB01',
                'name' => 'UOBIDJKT0001',
                'sorting' => 33,
                'status' => 1,
                'updated_by' => NULL,
                'created_at' => '2021-06-10 02:01:04',
                'updated_at' => '2021-06-10 02:01:04',
            ),
        ));
    }
}
