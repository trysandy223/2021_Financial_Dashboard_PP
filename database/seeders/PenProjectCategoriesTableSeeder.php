<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PenProjectCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pen_project_categories = [
            [
                'name' => '-',
                'sorting' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]
        ];

        DB::table('pen_project_categories')->insert($pen_project_categories);
    }
}
