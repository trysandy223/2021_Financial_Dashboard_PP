<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth Route
Auth::routes(['register' => false]);

// Main Route
Route::get('/', function () { return redirect('login'); });

// References, Master Data, and User Management Route Group
Route::group([
    'namespace'  => 'Pages',
    'prefix'     => 'pages',
    'as'         => 'pages::',
    'middleware' => 'auth'
    ], function () {

    // Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    // Users
    Route::group([
        'prefix'     => 'users',
        'as'         => 'users.',
        ], function () {
            Route::get('/', 'UserController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'UserController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'UserController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'UserController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'UserController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'UserController@update')->name('update')->middleware(['permission:master-edit']);
            Route::get('/edit-personal', 'UserController@editPersonal')->name('edit-personal');
            Route::patch('/edit-personal', 'UserController@updatePersonal')->name('update-personal');
    });

    // Roles
    Route::group([
        'prefix'     => 'roles',
        'as'         => 'roles.',
        ], function () {
            Route::get('/', 'RoleController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'RoleController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'RoleController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'RoleController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'RoleController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'RoleController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Permissions
    Route::group([
        'prefix'     => 'permissions',
        'as'         => 'permissions.',
        ], function () {
            Route::get('/', 'PermissionController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'PermissionController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'PermissionController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'PermissionController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'PermissionController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'PermissionController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Categories
    Route::group([
        'prefix'     => 'categories',
        'as'         => 'categories.',
        ], function () {
            Route::get('/', 'CategoryController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'CategoryController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'CategoryController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'CategoryController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'CategoryController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'CategoryController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Banks
    Route::group([
        'prefix'     => 'banks',
        'as'         => 'banks.',
        ], function () {
            Route::get('/', 'BankController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'BankController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'BankController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'BankController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'BankController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'BankController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Reconsiliation Account
    Route::group([
        'prefix'     => 'reconsiliation_accounts',
        'as'         => 'reconsiliation_accounts.',
        ], function () {
            Route::get('/', 'ReconsiliationAccountController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'ReconsiliationAccountController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'ReconsiliationAccountController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'ReconsiliationAccountController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'ReconsiliationAccountController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'ReconsiliationAccountController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Distribution Channel
    Route::group([
        'prefix'     => 'distribution_channels',
        'as'         => 'distribution_channels.',
        ], function () {
            Route::get('/', 'DistributionChannelController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'DistributionChannelController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'DistributionChannelController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'DistributionChannelController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'DistributionChannelController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'DistributionChannelController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Planning Group
    Route::group([
        'prefix'     => 'planning_groups',
        'as'         => 'planning_groups.',
        ], function () {
            Route::get('/', 'PlanningGroupController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'PlanningGroupController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'PlanningGroupController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'PlanningGroupController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'PlanningGroupController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'PlanningGroupController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // House Bank
    Route::group([
        'prefix'     => 'house_banks',
        'as'         => 'house_banks.',
        ], function () {
            Route::get('/', 'HouseBankController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'HouseBankController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'HouseBankController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'HouseBankController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'HouseBankController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'HouseBankController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Terms Of Payment
    Route::group([
        'prefix'     => 'terms_of_payments',
        'as'         => 'terms_of_payments.',
        ], function () {
            Route::get('/', 'TermsOfPaymentController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'TermsOfPaymentController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'TermsOfPaymentController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'TermsOfPaymentController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'TermsOfPaymentController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'TermsOfPaymentController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Currencies
    Route::group([
        'prefix'     => 'currencies',
        'as'         => 'currencies.',
        ], function () {
            Route::get('/', 'CurrencyController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'CurrencyController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'CurrencyController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'CurrencyController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'CurrencyController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'CurrencyController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Tax Classifications
    Route::group([
        'prefix'     => 'tax_classifications',
        'as'         => 'tax_classifications.',
        ], function () {
            Route::get('/', 'TaxClassificationController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'TaxClassificationController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'TaxClassificationController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'TaxClassificationController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'TaxClassificationController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'TaxClassificationController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Account Groups
    Route::group([
        'prefix'     => 'account_groups',
        'as'         => 'account_groups.',
        ], function () {
            Route::get('/', 'AccountGroupController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'AccountGroupController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'AccountGroupController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'AccountGroupController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'AccountGroupController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'AccountGroupController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Flag CF
    Route::group([
        'prefix'     => 'flag_cfs',
        'as'         => 'flag_cfs.',
        ], function () {
            Route::get('/', 'FlagCfController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'FlagCfController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'FlagCfController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'FlagCfController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'FlagCfController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'FlagCfController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Flag CCC
    Route::group([
        'prefix'     => 'flag_cccs',
        'as'         => 'flag_cccs.',
        ], function () {
            Route::get('/', 'FlagCccController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'FlagCccController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'FlagCccController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'FlagCccController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'FlagCccController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'FlagCccController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Flag Investments
    Route::group([
        'prefix'     => 'flag_investments',
        'as'         => 'flag_investments.',
        ], function () {
            Route::get('/', 'FlagInvestmentController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'FlagInvestmentController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'FlagInvestmentController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'FlagInvestmentController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'FlagInvestmentController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'FlagInvestmentController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Flag Profitability
    Route::group([
        'prefix'     => 'flag_profitabilities',
        'as'         => 'flag_profitabilities.',
        ], function () {
            Route::get('/', 'FlagProfitabilityController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'FlagProfitabilityController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'FlagProfitabilityController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'FlagProfitabilityController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'FlagProfitabilityController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'FlagProfitabilityController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Material
    Route::group([
        'prefix'     => 'materials',
        'as'         => 'materials.',
        ], function () {
            Route::get('/', 'MaterialController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'MaterialController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'MaterialController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'MaterialController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'MaterialController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'MaterialController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Business Segment
    Route::group([
        'prefix'     => 'business_segments',
        'as'         => 'business_segments.',
        ], function () {
            Route::get('/', 'BusinessSegmentController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/create', 'BusinessSegmentController@create')->name('create')->middleware(['permission:master-create']);
            Route::post('/create', 'BusinessSegmentController@store')->name('store')->middleware(['permission:master-create']);
            Route::delete('/{id}/delete', 'BusinessSegmentController@destroy')->name('destroy')->middleware(['permission:master-delete']);
            Route::get('/{id}/edit', 'BusinessSegmentController@edit')->name('edit')->middleware(['permission:master-edit']);
            Route::patch('/{id}/edit', 'BusinessSegmentController@update')->name('update')->middleware(['permission:master-edit']);
    });

    // Companies
    Route::group([
        'prefix'     => 'companies',
        'as'         => 'companies.',
        ], function () {
            Route::get('/', 'CompanyController@index')->name('index')->middleware(['permission:company-list']);
            Route::get('/create', 'CompanyController@create')->name('create')->middleware(['permission:company-create']);
            Route::post('/create', 'CompanyController@store')->name('store')->middleware(['permission:company-create']);
            Route::delete('/{id}/delete', 'CompanyController@destroy')->name('destroy')->middleware(['permission:company-delete']);
            Route::get('/{id}/edit', 'CompanyController@edit')->name('edit')->middleware(['permission:company-edit']);
            Route::patch('/{id}/edit', 'CompanyController@update')->name('update')->middleware(['permission:company-edit']);
            Route::post('/treeUnit', 'CompanyController@getTree')->name('getTree');
            Route::get('/{id}/detail', 'CompanyController@show')->name('show');
            Route::post('/company-selectbox', 'CompanyController@selectBox')->name('selectBox');
    });

    // Customers
    Route::group([
        'prefix'     => 'customers',
        'as'         => 'customers.',
        ], function () {
            Route::get('/', 'CustomerController@index')->name('index')->middleware(['permission:customer-list']);
            Route::get('/create', 'CustomerController@create')->name('create')->middleware(['permission:customer-create']);
            Route::post('/create', 'CustomerController@store')->name('store')->middleware(['permission:customer-create']);
            Route::delete('/{id}/delete', 'CustomerController@destroy')->name('destroy')->middleware(['permission:customer-delete']);
            Route::get('/{id}/edit', 'CustomerController@edit')->name('edit')->middleware(['permission:customer-edit']);
            Route::patch('/{id}/edit', 'CustomerController@update')->name('update')->middleware(['permission:customer-edit']);
            Route::get('/list', 'CustomerController@list')->name('list');
            Route::post('/selectbox', 'CustomerController@selectBox')->name('selectBox');
    });

    // Projects
    Route::group([
        'prefix'     => 'projects',
        'as'         => 'projects.',
        ], function () {
            Route::get('/', 'ProjectController@index')->name('index')->middleware(['permission:project-list']);
            Route::get('/create', 'ProjectController@create')->name('create')->middleware(['permission:project-create']);
            Route::post('/create', 'ProjectController@store')->name('store')->middleware(['permission:project-create']);
            Route::delete('/{id}/delete', 'ProjectController@destroy')->name('destroy')->middleware(['permission:project-delete']);
            Route::get('/{id}/edit', 'ProjectController@edit')->name('edit')->middleware(['permission:project-edit']);
            Route::patch('/{id}/edit', 'ProjectController@update')->name('update')->middleware(['permission:project-edit']);
            Route::post('/selectbox', 'ProjectController@selectBox')->name('selectBox');
    });

    // Trading Partners
    Route::group([
        'prefix'     => 'trading_partners',
        'as'         => 'trading_partners.',
        ], function () {
            Route::get('/', 'TradingPartnerController@index')->name('index')->middleware(['permission:trading-partner-list']);
            Route::get('/create', 'TradingPartnerController@create')->name('create')->middleware(['permission:trading-partner-create']);
            Route::post('/create', 'TradingPartnerController@store')->name('store')->middleware(['permission:trading-partner-create']);
            Route::delete('/{id}/delete', 'TradingPartnerController@destroy')->name('destroy')->middleware(['permission:trading-partner-delete']);
            Route::get('/{id}/edit', 'TradingPartnerController@edit')->name('edit')->middleware(['permission:trading-partner-edit']);
            Route::patch('/{id}/edit', 'TradingPartnerController@update')->name('update')->middleware(['permission:trading-partner-edit']);
            Route::post('/selectbox', 'TradingPartnerController@selectBox')->name('selectBox');
    });

    // Log Activities
    Route::group([
        'prefix'     => 'log_activities',
        'as'         => 'log_activities.',
        ], function ($router) {
            Route::get('/', 'LogActivityController@index')->name('index')->middleware(['permission:master-list']);
            Route::get('/detail/{id}', 'LogActivityController@show')->name('show')->middleware(['permission:master-list']);
            Route::get('/export', 'LogActivityController@export')->name('export');
    });
});

// Financial Data Route Group
Route::group([
    'namespace'  => 'Financials',
    'prefix'     => 'financials',
    'as'         => 'financials::',
    'middleware' => 'auth'
    ], function () {

    // Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    // Liquidity
    Route::group([
        'prefix'     => 'liquidities',
        'as'         => 'liquidities.',
        ], function () {
            Route::get('/', 'LiquidityController@index')->name('index')->middleware(['permission:financial-liquidity-list']);
            Route::get('/create', 'LiquidityController@create')->name('create')->middleware(['permission:financial-liquidity-create']);
            Route::get('/{id}/edit', 'LiquidityController@edit')->name('edit')->middleware(['permission:financial-liquidity-edit']);
            Route::delete('/{id}/delete', 'LiquidityController@destroy')->name('destroy')->middleware(['permission:financial-liquidity-delete']);
            Route::get('/export', 'LiquidityController@export')->name('export');
            Route::post('/import', 'LiquidityController@import')->name('import');
    });

    // Investment
    Route::group([
        'prefix'     => 'investments',
        'as'         => 'investments.',
        ], function () {
            Route::get('/', 'InvestmentController@index')->name('index')->middleware(['permission:financial-investment-list']);
            Route::get('/create', 'InvestmentController@create')->name('create')->middleware(['permission:financial-investment-create']);
            Route::get('/{id}/edit', 'InvestmentController@edit')->name('edit')->middleware(['permission:financial-investment-edit']);
            Route::delete('/{id}/delete', 'InvestmentController@destroy')->name('destroy')->middleware(['permission:financial-investment-delete']);
    });

    // Term Loan
    Route::group([
        'prefix'     => 'term_loans',
        'as'         => 'term_loans.',
        ], function () {
            Route::get('/', 'TermLoanController@index')->name('index')->middleware(['permission:financial-term-loan-list']);
            Route::get('/create', 'TermLoanController@create')->name('create')->middleware(['permission:financial-term-loan-create']);
            Route::get('/{id}/edit', 'TermLoanController@edit')->name('edit')->middleware(['permission:financial-term-loan-edit']);
            Route::delete('/{id}/delete', 'TermLoanController@destroy')->name('destroy')->middleware(['permission:financial-term-loan-delete']);
            Route::post('/selectbox', 'TermLoanController@selectBox')->name('selectBox');
    });

    // Term Loan Allocation
    Route::group([
        'prefix'     => 'term_loan_allocations',
        'as'         => 'term_loan_allocations.',
        ], function () {
            Route::get('/', 'TermLoanAllocationController@index')->name('index')->middleware(['permission:financial-term-loan-allocation-list']);
            Route::get('/create', 'TermLoanAllocationController@create')->name('create')->middleware(['permission:financial-term-loan-allocation-create']);
            Route::get('/reverse', 'TermLoanAllocationController@reverse')->name('reverse')->middleware(['permission:financial-term-loan-allocation-create']);
            Route::get('/{id}/edit', 'TermLoanAllocationController@edit')->name('edit')->middleware(['permission:financial-term-loan-allocation-edit']);
            Route::delete('/{id}/delete', 'TermLoanAllocationController@destroy')->name('destroy')->middleware(['permission:financial-term-loan-allocation-delete']);
            Route::get('/export', 'TermLoanAllocationController@export')->name('export');
            Route::post('/import', 'TermLoanAllocationController@import')->name('import');
    });

    // Revolving Loan
    Route::group([
        'prefix'     => 'revolving_loans',
        'as'         => 'revolving_loans.',
        ], function () {
            Route::get('/', 'RevolvingLoanController@index')->name('index')->middleware(['permission:financial-revolving-loan-list']);
            Route::get('/create', 'RevolvingLoanController@create')->name('create')->middleware(['permission:financial-revolving-loan-create']);
            Route::get('/{id}/edit', 'RevolvingLoanController@edit')->name('edit')->middleware(['permission:financial-revolving-loan-edit']);
            Route::delete('/{id}/delete', 'RevolvingLoanController@destroy')->name('destroy')->middleware(['permission:financial-revolving-loan-delete']);
            Route::post('/selectbox', 'RevolvingLoanController@selectBox')->name('selectBox');
    });

    // Revolving Drawdown
    Route::group([
        'prefix'     => 'revolving_drawdowns',
        'as'         => 'revolving_drawdowns.',
        ], function () {
            Route::get('/', 'RevolvingDrawdownController@index')->name('index')->middleware(['permission:financial-revolving-drawdown-list']);
            Route::get('/create', 'RevolvingDrawdownController@create')->name('create')->middleware(['permission:financial-revolving-drawdown-create']);
            Route::get('/reverse', 'RevolvingDrawdownController@reverse')->name('reverse')->middleware(['permission:financial-revolving-drawdown-create']);
            Route::get('/{id}/edit', 'RevolvingDrawdownController@edit')->name('edit')->middleware(['permission:financial-revolving-drawdown-edit']);
            Route::delete('/{id}/delete', 'RevolvingDrawdownController@destroy')->name('destroy')->middleware(['permission:financial-revolving-drawdown-delete']);
            Route::post('/selectbox', 'RevolvingDrawdownController@selectBox')->name('selectBox');
    });

    // Revolving Allocation
    Route::group([
        'prefix'     => 'revolving_allocations',
        'as'         => 'revolving_allocations.',
        ], function () {
            Route::get('/', 'RevolvingAllocationController@index')->name('index')->middleware(['permission:financial-revolving-allocation-list']);
            Route::get('/create', 'RevolvingAllocationController@create')->name('create')->middleware(['permission:financial-revolving-allocation-create']);
            Route::get('/{id}/edit', 'RevolvingAllocationController@edit')->name('edit')->middleware(['permission:financial-revolving-allocation-edit']);
            Route::delete('/{id}/delete', 'RevolvingAllocationController@destroy')->name('destroy')->middleware(['permission:financial-revolving-allocation-delete']);
            Route::get('/export', 'RevolvingAllocationController@export')->name('export');
            Route::post('/import', 'RevolvingAllocationController@import')->name('import');
    });

    // NCL
    Route::group([
        'prefix'     => 'ncls',
        'as'         => 'ncls.',
        ], function () {
            Route::get('/', 'NclController@index')->name('index')->middleware(['permission:financial-ncl-list']);
            Route::get('/create', 'NclController@create')->name('create')->middleware(['permission:financial-ncl-create']);
            Route::get('/{id}/edit', 'NclController@edit')->name('edit')->middleware(['permission:financial-ncl-edit']);
            Route::delete('/{id}/delete', 'NclController@destroy')->name('destroy')->middleware(['permission:financial-ncl-delete']);
            Route::post('/selectbox', 'NclController@selectBox')->name('selectBox');
    });

    // NCL Drawdown
    Route::group([
        'prefix'     => 'ncl_drawdowns',
        'as'         => 'ncl_drawdowns.',
        ], function () {
            Route::get('/', 'NclDrawdownController@index')->name('index')->middleware(['permission:financial-ncl-drawdown-list']);
            Route::get('/create', 'NclDrawdownController@create')->name('create')->middleware(['permission:financial-ncl-drawdown-create']);
            Route::get('/reverse', 'NclDrawdownController@reverse')->name('reverse')->middleware(['permission:financial-ncl-drawdown-create']);
            Route::get('/{id}/edit', 'NclDrawdownController@edit')->name('edit')->middleware(['permission:financial-ncl-drawdown-edit']);
            Route::delete('/{id}/delete', 'NclDrawdownController@destroy')->name('destroy')->middleware(['permission:financial-ncl-drawdown-delete']);
            Route::post('/selectbox', 'NclDrawdownController@selectBox')->name('selectBox');
    });

    // Acceptation
    Route::group([
        'prefix'     => 'acceptations',
        'as'         => 'acceptations.',
        ], function () {
            Route::get('/', 'AcceptationController@index')->name('index')->middleware(['permission:financial-acceptation-list']);
            Route::get('/create', 'AcceptationController@create')->name('create')->middleware(['permission:financial-acceptation-create']);
            Route::get('/{id}/edit', 'AcceptationController@edit')->name('edit')->middleware(['permission:financial-acceptation-edit']);
            Route::delete('/{id}/delete', 'AcceptationController@destroy')->name('destroy')->middleware(['permission:financial-acceptation-delete']);
            Route::get('/export', 'AcceptationController@export')->name('export');
            Route::post('/import', 'AcceptationController@import')->name('import');
    });

    // Cash Conversion Cycle
    Route::group([
        'prefix'     => 'cash_conversion_cycles',
        'as'         => 'cash_conversion_cycles.',
        ], function () {
            Route::get('/', 'CashConversionCycleController@index')->name('index')->middleware(['permission:financial-ccc-list']);
            Route::get('/create', 'CashConversionCycleController@create')->name('create')->middleware(['permission:financial-ccc-create']);
            Route::get('/{id}/edit', 'CashConversionCycleController@edit')->name('edit')->middleware(['permission:financial-ccc-edit']);
            Route::delete('/{id}/delete', 'CashConversionCycleController@destroy')->name('destroy')->middleware(['permission:financial-ccc-delete']);
    });

    // Project Profitability
    Route::group([
        'prefix'     => 'project_profitabilities',
        'as'         => 'project_profitabilities.',
        ], function () {
            Route::get('/', 'ProjectProfitabilityController@index')->name('index')->middleware(['permission:financial-project-profitability-list']);
            Route::get('/create', 'ProjectProfitabilityController@create')->name('create')->middleware(['permission:financial-project-profitability-create']);
            Route::get('/{id}/edit', 'ProjectProfitabilityController@edit')->name('edit')->middleware(['permission:financial-project-profitability-edit']);
            Route::delete('/{id}/delete', 'ProjectProfitabilityController@destroy')->name('destroy')->middleware(['permission:financial-project-profitability-delete']);
    });

    // Days Sales Outstanding
    Route::group([
        'prefix'     => 'days_sales_outstandings',
        'as'         => 'days_sales_outstandings.',
        ], function () {
            Route::get('/', 'DaysSalesOutstandingController@index')->name('index')->middleware(['permission:financial-dso-list']);
            Route::get('/create', 'DaysSalesOutstandingController@create')->name('create')->middleware(['permission:financial-dso-create']);
            Route::get('/{id}/edit', 'DaysSalesOutstandingController@edit')->name('edit')->middleware(['permission:financial-dso-edit']);
            Route::delete('/{id}/delete', 'DaysSalesOutstandingController@destroy')->name('destroy')->middleware(['permission:financial-dso-delete']);
    });
});

// PEN Data Route Group
Route::group([
    'namespace'  => 'Pens',
    'prefix'     => 'pens',
    'as'         => 'pens::',
    'middleware' => 'auth'
    ], function () {

    // Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    // Demand Material Construction
    Route::group([
        'prefix'     => 'demand_material_constructions',
        'as'         => 'demand_material_constructions.',
        ], function () {
            Route::get('/', 'DemandMaterialConstructionController@index')->name('index')->middleware(['permission:pen-demand-material-construction-list']);
            Route::get('/get-project', 'DemandMaterialConstructionController@getProject')->name('get-project');
            Route::get('/get-material', 'DemandMaterialConstructionController@getMaterial')->name('get-material');
            Route::get('/create', 'DemandMaterialConstructionController@create')->name('create')->middleware(['permission:pen-demand-material-construction-create']);
            Route::post('/create', 'DemandMaterialConstructionController@store')->name('store')->middleware(['permission:pen-demand-material-construction-create']);
            Route::delete('/{id}/delete', 'DemandMaterialConstructionController@destroy')->name('destroy')->middleware(['permission:pen-demand-material-construction-delete']);
            Route::get('/{id}/edit', 'DemandMaterialConstructionController@edit')->name('edit')->middleware(['permission:pen-demand-material-construction-edit']);
            Route::patch('/{id}/edit', 'DemandMaterialConstructionController@update')->name('update')->middleware(['permission:pen-demand-material-construction-edit']);
            Route::get('/export', 'DemandMaterialConstructionController@export')->name('export');
            Route::post('/import', 'DemandMaterialConstructionController@import')->name('import');
            Route::get('/download-template', 'DemandMaterialConstructionController@downloadTemplate')->name('download-template');
    });

    // Penyediaan Hunian MBR
    Route::group([
        'prefix'     => 'hunian_mbrs',
        'as'         => 'hunian_mbrs.',
        ], function () {
            Route::get('/', 'HunianMbrController@index')->name('index')->middleware(['permission:pen-penyediaan-hunian-mbr-list']);
            Route::get('/get-project', 'HunianMbrController@getProject')->name('get-project');
            Route::get('/create', 'HunianMbrController@create')->name('create')->middleware(['permission:pen-penyediaan-hunian-mbr-create']);
            Route::post('/create', 'HunianMbrController@store')->name('store')->middleware(['permission:pen-penyediaan-hunian-mbr-create']);
            Route::delete('/{id}/delete', 'HunianMbrController@destroy')->name('destroy')->middleware(['permission:pen-penyediaan-hunian-mbr-delete']);
            Route::get('/{id}/edit', 'HunianMbrController@edit')->name('edit')->middleware(['permission:pen-penyediaan-hunian-mbr-edit']);
            Route::patch('/{id}/edit', 'HunianMbrController@update')->name('update')->middleware(['permission:pen-penyediaan-hunian-mbr-edit']);
            Route::get('/export', 'HunianMbrController@export')->name('export');
            Route::post('/import', 'HunianMbrController@import')->name('import');
            Route::get('/download-template', 'HunianMbrController@downloadTemplate')->name('download-template');
    });

    // Company Revenue
    Route::group([
        'prefix'     => 'company_revenues',
        'as'         => 'company_revenues.',
        ], function () {
            Route::get('/', 'CompanyRevenueController@index')->name('index')->middleware(['permission:pen-pendapatan-usaha-list']);
            Route::get('/create', 'CompanyRevenueController@create')->name('create')->middleware(['permission:pen-pendapatan-usaha-create']);
            Route::post('/create', 'CompanyRevenueController@store')->name('store')->middleware(['permission:pen-pendapatan-usaha-create']);
            Route::delete('/{id}/delete', 'CompanyRevenueController@destroy')->name('destroy')->middleware(['permission:pen-pendapatan-usaha-delete']);
            Route::get('/{id}/edit', 'CompanyRevenueController@edit')->name('edit')->middleware(['permission:pen-pendapatan-usaha-edit']);
            Route::patch('/{id}/edit', 'CompanyRevenueController@update')->name('update')->middleware(['permission:pen-pendapatan-usaha-edit']);
            Route::get('/export', 'CompanyRevenueController@export')->name('export');
            Route::post('/import', 'CompanyRevenueController@import')->name('import');
            Route::get('/download-template', 'CompanyRevenueController@downloadTemplate')->name('download-template');
    });

    // Penyerapan Tenaga Kerja
    Route::group([
        'prefix'     => 'penyerapan_tenaga_kerjas',
        'as'         => 'penyerapan_tenaga_kerjas.',
        ], function () {
            Route::get('/', 'PenyerapanTenagaKerjaController@index')->name('index')->middleware(['permission:pen-penyerapan-tenaga-kerja-list']);
            Route::get('/get-project', 'PenyerapanTenagaKerjaController@getProject')->name('get-project');
            Route::get('/create', 'PenyerapanTenagaKerjaController@create')->name('create')->middleware(['permission:pen-penyerapan-tenaga-kerja-create']);
            Route::post('/create', 'PenyerapanTenagaKerjaController@store')->name('store')->middleware(['permission:pen-penyerapan-tenaga-kerja-create']);
            Route::delete('/{id}/delete', 'PenyerapanTenagaKerjaController@destroy')->name('destroy')->middleware(['permission:pen-penyerapan-tenaga-kerja-delete']);
            Route::get('/{id}/edit', 'PenyerapanTenagaKerjaController@edit')->name('edit')->middleware(['permission:pen-penyerapan-tenaga-kerja-edit']);
            Route::patch('/{id}/edit', 'PenyerapanTenagaKerjaController@update')->name('update')->middleware(['permission:pen-penyerapan-tenaga-kerja-edit']);
            Route::get('/export', 'PenyerapanTenagaKerjaController@export')->name('export');
            Route::post('/import', 'PenyerapanTenagaKerjaController@import')->name('import');
            Route::get('/download-template', 'PenyerapanTenagaKerjaController@downloadTemplate')->name('download-template');
    });

    // Kontrak Jumlah Proyek
    Route::group([
        'prefix'     => 'kontrak_jumlah_proyeks',
        'as'         => 'kontrak_jumlah_proyeks.',
        ], function () {
            Route::get('/', 'KontrakJumlahProyekController@index')->name('index')->middleware(['permission:pen-perolehan-kontrak-jumlah-proyek-list']);
            Route::get('/create', 'KontrakJumlahProyekController@create')->name('create')->middleware(['permission:pen-perolehan-kontrak-jumlah-proyek-create']);
            Route::post('/create', 'KontrakJumlahProyekController@store')->name('store')->middleware(['permission:pen-perolehan-kontrak-jumlah-proyek-create']);
            Route::delete('/{id}/delete', 'KontrakJumlahProyekController@destroy')->name('destroy')->middleware(['permission:pen-perolehan-kontrak-jumlah-proyek-delete']);
            Route::get('/{id}/edit', 'KontrakJumlahProyekController@edit')->name('edit')->middleware(['permission:pen-perolehan-kontrak-jumlah-proyek-edit']);
            Route::patch('/{id}/edit', 'KontrakJumlahProyekController@update')->name('update')->middleware(['permission:pen-perolehan-kontrak-jumlah-proyek-edit']);
            Route::get('/export', 'KontrakJumlahProyekController@export')->name('export');
            Route::post('/import', 'KontrakJumlahProyekController@import')->name('import');
            Route::get('/download-template', 'KontrakJumlahProyekController@downloadTemplate')->name('download-template');
    });

    // PSN Project
    Route::group([
        'prefix'     => 'psn_projects',
        'as'         => 'psn_projects.',
        ], function () {
            Route::get('/', 'PsnProjectController@index')->name('index')->middleware(['permission:pen-proyek-psn-list']);
            Route::get('/get-project', 'PsnProjectController@getProject')->name('get-project');
            Route::get('/create', 'PsnProjectController@create')->name('create')->middleware(['permission:pen-proyek-psn-create']);
            Route::post('/create', 'PsnProjectController@store')->name('store')->middleware(['permission:pen-proyek-psn-create']);
            Route::delete('/{id}/delete', 'PsnProjectController@destroy')->name('destroy')->middleware(['permission:pen-proyek-psn-delete']);
            Route::get('/{id}/edit', 'PsnProjectController@edit')->name('edit')->middleware(['permission:pen-proyek-psn-edit']);
            Route::patch('/{id}/edit', 'PsnProjectController@update')->name('update')->middleware(['permission:pen-proyek-psn-edit']);
            Route::get('/export', 'PsnProjectController@export')->name('export');
            Route::post('/import', 'PsnProjectController@import')->name('import');
            Route::get('/download-template', 'PsnProjectController@downloadTemplate')->name('download-template');
    });

    // Realisasi Belanja UMKM
    Route::group([
        'prefix'     => 'realisasi_belanja_umkms',
        'as'         => 'realisasi_belanja_umkms.',
        ], function () {
            Route::get('/', 'RealisasiBelanjaUmkmController@index')->name('index')->middleware(['permission:pen-realisasi-belanja-umkm-list']);
            Route::get('/create', 'RealisasiBelanjaUmkmController@create')->name('create')->middleware(['permission:pen-realisasi-belanja-umkm-create']);
            Route::post('/create', 'RealisasiBelanjaUmkmController@store')->name('store')->middleware(['permission:pen-realisasi-belanja-umkm-create']);
            Route::delete('/{id}/delete', 'RealisasiBelanjaUmkmController@destroy')->name('destroy')->middleware(['permission:pen-realisasi-belanja-umkm-delete']);
            Route::get('/{id}/edit', 'RealisasiBelanjaUmkmController@edit')->name('edit')->middleware(['permission:pen-realisasi-belanja-umkm-edit']);
            Route::patch('/{id}/edit', 'RealisasiBelanjaUmkmController@update')->name('update')->middleware(['permission:pen-realisasi-belanja-umkm-edit']);
            Route::get('/export', 'RealisasiBelanjaUmkmController@export')->name('export');
            Route::post('/import', 'RealisasiBelanjaUmkmController@import')->name('import');
            Route::get('/download-template', 'RealisasiBelanjaUmkmController@downloadTemplate')->name('download-template');
    });

    // Target Realisasi Capex
    Route::group([
        'prefix'     => 'target_realisasi_capexes',
        'as'         => 'target_realisasi_capexes.',
        ], function () {
            Route::get('/', 'TargetRealisasiCapexController@index')->name('index')->middleware(['permission:pen-target-realisasi-capex-list']);
            Route::get('/create', 'TargetRealisasiCapexController@create')->name('create')->middleware(['permission:pen-target-realisasi-capex-create']);
            Route::post('/create', 'TargetRealisasiCapexController@store')->name('store')->middleware(['permission:pen-target-realisasi-capex-create']);
            Route::delete('/{id}/delete', 'TargetRealisasiCapexController@destroy')->name('destroy')->middleware(['permission:pen-target-realisasi-capex-delete']);
            Route::get('/{id}/edit', 'TargetRealisasiCapexController@edit')->name('edit')->middleware(['permission:pen-target-realisasi-capex-edit']);
            Route::patch('/{id}/edit', 'TargetRealisasiCapexController@update')->name('update')->middleware(['permission:pen-target-realisasi-capex-edit']);
            Route::get('/export', 'TargetRealisasiCapexController@export')->name('export');
            Route::post('/import', 'TargetRealisasiCapexController@import')->name('import');
            Route::get('/download-template', 'TargetRealisasiCapexController@downloadTemplate')->name('download-template');
    });

    // Komponen Dalam Negeri
    Route::group([
        'prefix'     => 'komponen_dalam_negeris',
        'as'         => 'komponen_dalam_negeris.',
        ], function () {
            Route::get('/', 'KomponenDalamNegeriController@index')->name('index')->middleware(['permission:pen-tingkat-komponen-dalam-negeri-list']);
            Route::get('/get-project', 'KomponenDalamNegeriController@getProject')->name('get-project');
            Route::get('/create', 'KomponenDalamNegeriController@create')->name('create')->middleware(['permission:pen-tingkat-komponen-dalam-negeri-create']);
            Route::post('/create', 'KomponenDalamNegeriController@store')->name('store')->middleware(['permission:pen-tingkat-komponen-dalam-negeri-create']);
            Route::delete('/{id}/delete', 'KomponenDalamNegeriController@destroy')->name('destroy')->middleware(['permission:pen-tingkat-komponen-dalam-negeri-delete']);
            Route::get('/{id}/edit', 'KomponenDalamNegeriController@edit')->name('edit')->middleware(['permission:pen-tingkat-komponen-dalam-negeri-edit']);
            Route::patch('/{id}/edit', 'KomponenDalamNegeriController@update')->name('update')->middleware(['permission:pen-tingkat-komponen-dalam-negeri-edit']);
            Route::get('/export', 'KomponenDalamNegeriController@export')->name('export');
            Route::post('/import', 'KomponenDalamNegeriController@import')->name('import');
            Route::get('/download-template', 'KomponenDalamNegeriController@downloadTemplate')->name('download-template');
    });
});

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');
